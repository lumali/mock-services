﻿/*
 * GET /v2/workersInsurance/claims/{id}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to retrieve 
 */
exports.getV2WorkersinsuranceClaimdetails = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get locations 
    var responseObject = helpers.getClaimByClaimId_deprecated(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 404, ERRORS.Error404);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

