﻿/*
 * GET /v2/workersInsurance/costCentres
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * policyNumber(type: string) - query parameter - Detail to pull cost centre
 * location(type: string) - query parameter - Detail to pull cost centre
 */
exports.getV2WorkersinsuranceCostcentres = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validQueryParam(req, "policyNumber")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validQueryParam(req, "location")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get cost centres  
    var responseObject = helpers.getCostCentres(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.render("simpleLookup-response", responseObject);
};


/*
 * GET /v2/workersInsurance/locations
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * policyNumber(type: string) - query parameter - Detail to pull cost centre 
 */
exports.getV2WorkersinsuranceLocations = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validQueryParam(req, "policyNumber")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get locations 
    var responseObject = helpers.getLocations(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.render("simpleLookup-response", responseObject);
};


/*
 * GET /v2/workersInsurance/policyNumbers
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * date(type: string) - query parameter - 
 */
exports.getV2WorkersinsurancePolicynumbers = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validPolicyNumberRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get claim numbers 
    var responseObject = helpers.getPolicyNumbers(req);

    // if invalid response object  
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.render("policyNumber-response", responseObject);
};


/*
 * POST /v2/workersInsurance/claims
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.postV2WorkersinsuranceClaims = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validClaimRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // try add claim 
    var responseObject = helpers.addClaim(req);

    // if add is not allowed 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.render("success-response", responseObject);
};


/*
 * GET /v2/workersInsurance/claims/{id}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to retrieve 
 */
exports.getV2WorkersinsuranceClaimdetails = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get locations 
    var responseObject = helpers.getClaimByClaimId(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 404, ERRORS.Error404);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};


/*
 * PATCH /v2/workersInsurance/claims/{id}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to patch
 */
exports.patchV2WorkersinsuranceClaims = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validClaimRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.updateClaimById(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(204);

    // set response body and send
    res.json(responseObject);
};


/*
 * POST /v2/workersInsurance/claims/{id}/lodge
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to patch
 */
exports.postV2WorkersinsuranceClaimsLodge = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validClaimRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.lodgeClaimById(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);
    // set response body and send
    res.json(responseObject);
};


/*
 * POST /v2/workersInsurance/claims/{id}/attachmentLocations
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to add attachments to
 */
exports.postV2WorkersinsuranceClaimsAttachmentlocations = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    //if (!helpers.validTrackingIdHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validAttachmentRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.addAttachmentByClaimId(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.render("attachment-response", responseObject);
};


/*
 * POST /v2/workersInsurance/claims/{id}/enquiry
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to add attachments to
 */
exports.postV2WorkersinsuranceClaimsEnquiry = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validEnquiry(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.enquiryAgainstClaimByClaimId(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400b);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.json(responseObject);
};


/*
 * GET /v2/workersInsurance/claims/{id}/documents
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to add attachments to
 */
exports.getV2WorkersinsuranceClaimsDocuments = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.getClaimsDocuments(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 404, ERRORS.Error400b);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};


/*
 * POST /v2/workersInsurance/claims/{id}/documents
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to add attachments to
 */
exports.postV2WorkersinsuranceClaimsDocuments = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validClaimDocumentRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.postClaimsDocuments(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 404, ERRORS.Error400b);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};


/*
 * PATCH /v2/workersInsurance/claims/{id}/documents
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to patch
 */
exports.patchV2WorkersinsuranceClaimsDocuments = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validClaimDocumentRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.updateClaimDocumentsById(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.json(responseObject);
};


/*
 * POST /v2/workersInsurance/claims/checkDuplicate
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.postV2WorkersinsuranceClaimsCheckDuplicate = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validDuplicateClaimCheckRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get duplicate 
    var responseObject = helpers.getDuplicateClaim(req);

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};