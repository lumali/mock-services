﻿
/*
 * Authenticated Lodgement for 2.1b
 *
 * Authenticated Lodgement
 */

var constants = require("./utilities/constants.js");
var ERRORS = constants.errors();
var SUCCESS = constants.successResponses();
var TEMPSTORE = constants.tempStore();
var helpers = require("./utilities/helpers.js");
var v2 = require("./routes/v2.js");
var v1_9 = require("./routes/v1_9.js");

// create state.AddUpdateClaimRepo if it doesn't exist
state.AddUpdateClaimRepo = state.AddUpdateClaimRepo || [];

// create state.EnquiryAgainstClaimRepo if it doesn't exist
state.EnquiryAgainstClaimRepo = state.EnquiryAgainstClaimRepos || [];

// create state.AddUpdateClaimDocumentsRepo if it doesn't exist
state.AddUpdateClaimDocumentsRepo = state.AddUpdateClaimDocumentsRepo || [];


/* Route definition styles:
 *
 *	define(path, method, function)
 *	soap(path, soapAction, function)
 *
 */
Sandbox.define("/v2/portal/workersInsurance/me/costCentres", "GET", v2.getV2WorkersinsuranceCostcentres);
Sandbox.define("/v2/portal/workersInsurance/me/locations", "GET", v2.getV2WorkersinsuranceLocations);
Sandbox.define("/v2/portal/workersInsurance/me/policyNumbers", "GET", v2.getV2WorkersinsurancePolicynumbers);
Sandbox.define("/v2/portal/workersInsurance/me/claims", "POST", v2.postV2WorkersinsuranceClaims);
Sandbox.define("/v2/portal/workersInsurance/me/claims/{id}", "GET", v2.getV2WorkersinsuranceClaimdetails);
Sandbox.define("/v2/portal/workersInsurance/me/claims/{id}", "PATCH", v2.patchV2WorkersinsuranceClaims);
Sandbox.define("/v2/portal/workersInsurance/me/claims/{id}/lodge", "POST", v2.postV2WorkersinsuranceClaimsLodge);
Sandbox.define("/v2/portal/workersInsurance/me/claims/{id}/attachmentLocations", "POST", v2.postV2WorkersinsuranceClaimsAttachmentlocations);
Sandbox.define("/v2/portal/workersInsurance/me/claims/{id}/enquiry", "POST", v2.postV2WorkersinsuranceClaimsEnquiry);
Sandbox.define("/v2/portal/workersInsurance/me/claims/{id}/documents", "GET", v2.getV2WorkersinsuranceClaimsDocuments);
Sandbox.define("/v2/portal/workersInsurance/me/claims/{id}/documents", "POST", v2.postV2WorkersinsuranceClaimsDocuments);
Sandbox.define("/v2/portal/workersInsurance/me/claims/{id}/documents", "PATCH", v2.patchV2WorkersinsuranceClaimsDocuments);
Sandbox.define("/v2/portal/workersInsurance/me/claims/checkDuplicate", "POST", v2.postV2WorkersinsuranceClaimsCheckDuplicate);


Sandbox.define("/v1_9/portal/workersInsurance/me/claims/{id}", "GET", v1_9.getV2WorkersinsuranceClaimdetails);


/* Unit Testing Helper: 
 *  This is used only for removing test data from state.AddUpdateClaimRepo 
 *
 */
Sandbox.define('/v2/workersInsurance/claims/{id}', 'DELETE', function (req, res) {
    // Check the request, make sure it is a compatible type
    if (!req.is('application/json')) {
        return res.send(400, 'Invalid content type, expected application/json');
    }

    // check if the claimNo exists 
    var existingClaim = _.find(state.AddUpdateClaimRepo, function (o) {
        return _.toString(o.data.attributes.claimNo) === _.toString(req.params.id);
    });

    // remove the claim
    if (existingClaim) state.AddUpdateClaimRepo = _.reject(state.AddUpdateClaimRepo, function (o) {
        return _.toString(o.data.attributes.claimNo) === _.toString(req.params.id);
    });

    // Set the type of response, sets the content type.
    res.type('application/json');

    // Set the status code of the response.
    res.status(200);

    // Send the response body.
    res.json({
        "status": "ok"
    });
});
