﻿

/*
 * GET /v2/my/paymentDetails
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.getV2MyPaymentdetails = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 

    // get bank details  
    var responseObject = helpers.getPaymentDetails(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.render("policiesWithPaymentDetails-response", responseObject);
};

/*
 * PATCH /v2/my/paymentDetails
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.patchV2MyPaymentdetails = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validPaymentDetails(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get bank details  
    var responseObject = helpers.updatePaymentDetails(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/lookup/bankName
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * bsb(type: string) - query parameter - BSB corresponding to a bank name
 */
exports.getV2LookupBankname = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    //if (!helpers.validTokenHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validQueryParam(req, "bsb")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get bank name 
    var responseObject = helpers.getBankName(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 404, ERRORS.Error404);
    }

    // set content-type 
    res.type('text/plain');

    // set status code 
    res.status(200);

    // set response body and send
    res.render("bankName-response", responseObject);
};

/*
 * GET /v2/my/payment/wage
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 */
exports.getV2MyPaymentWage = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    // get payment wage  
    var responseObject = helpers.getPaymentWageOverview(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/my/payment/wage/history
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 */
exports.postV2MyPaymentWageHistory = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validPaymentWageHistory(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    // get payment wage history
    var responseObject = helpers.getPaymentWageHistory(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/my/payment/medical
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 */
exports.getV2MyPaymentMedical = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    // get payment wage  
    var responseObject = helpers.getPaymentMedicalOverview(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/my/payment/medical/history
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 */
exports.postV2MyPaymentMedicalHistory = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validPaymentMedicalHistory(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    // get payment wage history
    var responseObject = helpers.getPaymentMedicalHistory(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/my/paymentDetail/forReimbursement
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 */
exports.getV2MyPaymentDetailForReimbursement = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 

    // get bank details  
    var responseObject = helpers.getPaymentDetailForReimbursement(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/my/payments/medical/reibursement
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 */
exports.postV2MyPaymentsMedicalReimbursement = function (req, res) {
    // validate access
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate request type
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate headers
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate data
    if (!helpers.postPaymentsMedicalReimbursement) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    //get description
    var responseObject = helpers.postMyPaymentsMedicalReimbursement(req);

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.json(responseObject);

};

/*
 * GET /v2/my/payments/wage/reibursement
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 */
exports.postV2MyPaymentsWageReimbursement = function (req, res) {
    // validate access
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate request type
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate headers
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate data
    if (!helpers.validatePaymentsWageReimbursement) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }


    //get description
    var responseObject = helpers.postMyPaymentsWageReimbursement(req);

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.json(responseObject);
}