﻿
/*
 * Payment Details for 2.1b
 *
 * Payment Details for 2.1b
 */


var constants = require("./utilities/constants.js");
var ERRORS = constants.errors();
var SUCCESS = constants.successResponses();
var TEMPSTORE = constants.tempStore();
var helpers = require("./utilities/helpers.js");
var v2 = require("./routes/v2.js")

/* Route definition styles:
 *
 *	define(path, method, function)
 *	soap(path, soapAction, function)
 *
 */
Sandbox.define("/v2/my/paymentDetails", "GET", v2.getV2MyPaymentdetails);
Sandbox.define("/v2/my/paymentDetails", "PATCH", v2.patchV2MyPaymentdetails);
Sandbox.define("/v2/lookup/bankName", "GET", v2.getV2LookupBankname);
Sandbox.define("/v2/my/payments/wage", "GET", v2.getV2MyPaymentWage);
Sandbox.define("/v2/my/payments/wage/history", "POST", v2.postV2MyPaymentWageHistory);
Sandbox.define("/v2/my/payments/medical", "GET", v2.getV2MyPaymentMedical);
Sandbox.define("/v2/my/payments/medical/history", "POST", v2.postV2MyPaymentMedicalHistory);
Sandbox.define("/v2/my/paymentDetail/forReimbursement", "GET", v2.getV2MyPaymentDetailForReimbursement);
Sandbox.define("/v2/my/payments/medical/reimbursement", "POST", v2.postV2MyPaymentsMedicalReimbursement);
Sandbox.define('/v2/my/payments/wage/reimbursement', 'POST', v2.postV2MyPaymentsWageReimbursement);