﻿

/*
 * GET /v2/portal/workersInsurance/me/claims/{id}/payments/medical
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.getV2PortalWorkersinsuranceMeClaimsPaymentsMedical = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage  
    var responseObject = helpers.getPaymentMedicalOverview(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/portal/workersInsurance/me/claims/{id}/payments/medical/history
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.postV2PortalWorkersinsuranceMeClaimsPaymentsMedicalHistory = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validPaymentMedicalHistory(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage history
    var responseObject = helpers.getPaymentMedicalHistory(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/portal/workersInsurance/me/claims/{id}/payments/medical/{invoiceId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 * invoiceId(type: integer) - path parameter - ID of medical invoice
 */
exports.getV2PortalWorkersinsuranceMeClaims = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validParam(req, "invoiceId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage  
    var responseObject = helpers.getPaymentMedicalDetail(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/portal/workersInsurance/me/claims/{id}/payments/wage
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.getV2PortalWorkersinsuranceMeClaimsPaymentsWage = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage  
    var responseObject = helpers.getPaymentWageOverview(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/portal/workersInsurance/me/claims/{id}/payments/wage/history
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.postV2PortalWorkersinsuranceMeClaimsPaymentsWageHistory = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validPaymentWageHistory(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage history
    var responseObject = helpers.getPaymentWageHistory(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/portal/workersInsurance/me/claims/{id}/payments/wage/{invoiceId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 * invoiceId(type: integer) - path parameter - ID of wage detail invoice
 */
exports.getV2PortalWorkersinsuranceMeClaims2 = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validParam(req, "invoiceId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage  
    var responseObject = helpers.getPaymentWageDetail(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/portal/workersInsurance/me/claims/{id}/payments/medical/reimbursement
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.postV2PortalWorkersinsuranceMeClaimsPaymentsMedicalReimbursement = function (req, res) {
    // validate access
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate request type
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate headers
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate data
    if (!helpers.postPaymentsMedicalReimbursement) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    //get description
    var responseObject = helpers.postMyPaymentsMedicalReimbursement(req);

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/portal/workersInsurance/me/claims/{id}/payments/wage/reimbursement
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.postV2PortalWorkersinsuranceMeClaimsPaymentsWageReimbursement = function (req, res) {
    // validate access
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate request type
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate headers
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate data
    if (!helpers.validatePaymentsWageReimbursement) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }


    //get description
    var responseObject = helpers.postMyPaymentsWageReimbursement(req);

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/portal/workersInsurance/me/claims/{id}/weekly/averageEarnings
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.postV2PortalWorkersinsuranceMeClaimsWeeklyAverageearnings = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate body
    if (!helpers.validWeeklyAverageEarnings(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage  
    var responseObject = helpers.postWeeklyAverageEarnings(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/portal/workersInsurance/me/claims/{id}/weekly/averageEarnings
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.getV2PortalWorkersinsuranceMeClaimsWeeklyAverageearnings = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // getWeeklyAverageEarnings
    var responseObject = helpers.getWeeklyAverageEarnings(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};