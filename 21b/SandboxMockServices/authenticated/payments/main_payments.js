﻿
/*
 * Claim Payment Overview, history and reimbursement requests for 2.1b
 *
 * Claim Payments for 2.1b
 */

var constants = require("./utilities/constants.js");
var ERRORS = constants.errors();
var SUCCESS = constants.successResponses();
var TEMPSTORE = constants.tempStore();
var helpers = require("./utilities/helpers.js");
var v2 = require("./routes/v2.js")

/* Route definition styles:
 *
 *	define(path, method, function)
 *	soap(path, soapAction, function)
 *
 */
Sandbox.define("/v2/portal/workersInsurance/me/claims/{id}/payments/medical", "GET", v2.getV2PortalWorkersinsuranceMeClaimsPaymentsMedical);
Sandbox.define("/v2/portal/workersInsurance/me/claims/{id}/payments/medical/history", "POST", v2.postV2PortalWorkersinsuranceMeClaimsPaymentsMedicalHistory);
Sandbox.define("/v2/portal/workersInsurance/me/claims/{id}/payments/medical/{invoiceId}", "GET", v2.getV2PortalWorkersinsuranceMeClaims);
Sandbox.define("/v2/portal/workersInsurance/me/claims/{id}/payments/wage", "GET", v2.getV2PortalWorkersinsuranceMeClaimsPaymentsWage);
Sandbox.define("/v2/portal/workersInsurance/me/claims/{id}/payments/wage/history", "POST", v2.postV2PortalWorkersinsuranceMeClaimsPaymentsWageHistory);
Sandbox.define("/v2/portal/workersInsurance/me/claims/{id}/payments/wage/{invoiceId}", "GET", v2.getV2PortalWorkersinsuranceMeClaims2);
Sandbox.define("/v2/portal/workersInsurance/me/claims/{id}/payments/medical/reimbursement", "POST", v2.postV2PortalWorkersinsuranceMeClaimsPaymentsMedicalReimbursement);
Sandbox.define("/v2/portal/workersInsurance/me/claims/{id}/payments/wage/reimbursement", "POST", v2.postV2PortalWorkersinsuranceMeClaimsPaymentsWageReimbursement);
Sandbox.define("/v2/portal/workersInsurance/me/claims/{id}/weekly/averageEarnings", "POST", v2.postV2PortalWorkersinsuranceMeClaimsWeeklyAverageearnings);
Sandbox.define("/v2/portal/workersInsurance/me/claims/{id}/weekly/averageEarnings", "GET", v2.getV2PortalWorkersinsuranceMeClaimsWeeklyAverageearnings);