﻿/*
 * POST /v2/users/user
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.postV2UsersUser = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validAddUserRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // try add user 
    var responseObject = helpers.addUser(req);

    // if add is not allowed 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/users/user
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * identityId(type: string) - query parameter - id of the user (generated when the user was added)
 */
exports.getV2UsersUser = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validQueryParam(req, "identityId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get user   
    var responseObject = helpers.getUser(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * PATCH /v2/users/user/{identityId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * identityId(type: string) - path parameter -
 */
exports.patchV2UsersUser = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    // if (!helpers.validParam(req, "identityId", true)) {
    //     return helpers.returnError(res, 400, ERRORS.Error400b);
    // }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validUpdateUserRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.updateUser(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * DELETE /v2/users/user/{identityId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * identityId(type: string) - path parameter -
 */
exports.deleteV2UsersUser = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    // if (!helpers.validParam(req, "identityId", true)) {
    //     return helpers.returnError(res, 400, ERRORS.Error400b);
    // }

    // response object  
    var responseObject = helpers.deleteUser(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // Set the type of response, sets the content type.
    res.type('application/json');

    // Set the status code of the response.
    res.status(200);

    // Send the response body.
    res.json({
        "status": "ok"
    });
};

/*
 * GET /v2/team
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.postV2Team = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validGetTeamRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // try get team 
    var responseObject = helpers.getTeam(req);

    // if add is not allowed 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/my/details
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.getV2MyDetails = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 


    // get cost centres  
    var responseObject = helpers.getMyDetails(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 404, ERRORS.Error404);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.render("accountDetails-response", responseObject);
};

/*
 * PATCH /v2/my/details
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.patchV2MyDetails = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validAccountDetails(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.updateMyDetails(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.render("accountDetails-response", responseObject);
};

/*
 * GET /v2/users/user/register
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * activationToken (type: string) - query parameter - encrypted token that Mulesoft will verify which embeds the okta ID of the user looking to register
 */
exports.getV2UsersUserRegister = function (req, res) {
    // validate activation token 
    if (!helpers.validActivationTokenParam(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get user register
    var responseObject = helpers.getUserRegister(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/users/user/register
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 */
exports.postV2UsersUserRegister = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validUserRegister(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get user   
    var responseObject = helpers.postUserRegister(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};