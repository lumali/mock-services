﻿
/*
 * Personal Account Details, Team, User for 2.1b
 *
 * Personal Account Details, Team, User for 2.1b
 */


var constants = require("./utilities/constants.js");
var ERRORS = constants.errors();
var SUCCESS = constants.successResponses();
var TEMPSTORE = constants.tempStore();
var helpers = require("./utilities/helpers.js");
var v2 = require("./routes/v2.js");

// create state.users if it doesn't exist
//state.users = state.users || [];

// track date of first operation 
state.dateOfFirstOp = state.dateOfFirstOp || new Date();

// create editable MockPolicyUsers that refreshes every 24 hours 
state.MockPolicyUsersEditable = state.MockPolicyUsersEditable || TEMPSTORE.MockPolicyUsersEditable;     // add it if it doesn't exists yet 
if (helpers.hasOneDayElapsed(state.dateOfFirstOp)) {
    // reset to constant mock data 
    state.MockPolicyUsersEditable = TEMPSTORE.MockPolicyUsersEditable;

    // reset to current date 
    state.dateOfFirstOp = new Date();
}


/* Route definition styles:
 *
 *	define(path, method, function)
 *	soap(path, soapAction, function)
 *
 */
Sandbox.define("/v2/users/user", "POST", v2.postV2UsersUser);
Sandbox.define("/v2/users/user", "GET", v2.getV2UsersUser);
Sandbox.define("/v2/users/user/{identityId}", "PATCH", v2.patchV2UsersUser);
Sandbox.define("/v2/team", "POST", v2.postV2Team);
Sandbox.define("/v2/my/details", "GET", v2.getV2MyDetails);
Sandbox.define("/v2/my/details", "PATCH", v2.patchV2MyDetails);
Sandbox.define("/v2/users/user/{identityId}", "DELETE", v2.deleteV2UsersUser);
Sandbox.define("/v2/users/user/register", "GET", v2.getV2UsersUserRegister);
Sandbox.define("/v2/users/user/register", "POST", v2.postV2UsersUserRegister);