﻿
/*
 * Health and Recovery for 2.1b
 *
 * Health and Recovery for 2.1b
 */

var constants = require("./utilities/constants.js");
var ERRORS = constants.errors();
var SUCCESS = constants.successResponses();
var TEMPSTORE = constants.tempStore();
var helpers = require("./utilities/helpers.js");
var v2 = require("./routes/v2.js")

/* Route definition styles:
 *
 *	define(path, method, function)
 *	soap(path, soapAction, function)
 *
 */
Sandbox.define("/v2/portal/workersInsurance/me/claims/{id}/injuryManagement", "GET", v2.getV2PortalWorkersinsuranceMeClaimsInjurymanagement);
Sandbox.define("/v2/portal/workersInsurance/me/claims/{id}/returnToWork", "GET", v2.getV2PortalWorkersinsuranceMeClaimsReturntowork);
Sandbox.define("/v2/portal/workersInsurance/me/claims/{id}/returnToWork", "POST", v2.postV2PortalWorkersinsuranceMeClaimsReturntowork);