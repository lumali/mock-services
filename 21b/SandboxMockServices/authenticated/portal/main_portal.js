﻿
/*
 * Portal home modules, Claim List for 2.1b
 *
 * Claim List, Claim Filter, Hero Module for Portal Home for 2.1b
 */


var constants = require("./utilities/constants.js");
var ERRORS = constants.errors();
var SUCCESS = constants.successResponses();
var TEMPSTORE = constants.tempStore();
var helpers = require("./utilities/helpers.js");
var v2 = require("./routes/v2.js");

/* Route definition styles:
 *
 *	define(path, method, function)
 *	soap(path, soapAction, function)
 *
 */
Sandbox.define("/v2/portal/userSummary", "GET", v2.getV2PortalUsersummary);
Sandbox.define("/v2/portal/claims", "POST", v2.postV2PortalClaims);