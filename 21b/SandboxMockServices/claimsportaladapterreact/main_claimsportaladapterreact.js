﻿
/*
 * Claims Portal Adapter for 2.1b
 *
 * Claims Portal Adapter for 2.1b
 */


var constants = require("./utilities/constants.js");
var ERRORS = constants.errors();
var SUCCESS = constants.successResponses();
var TEMPSTORE = constants.tempStore();
var helpers = require("./utilities/helpers.js");
var v2 = require("./routes/v2.js");

// create state.users if it doesn't exist
//state.users = state.users || [];

// track date of first operation 
state.dateOfFirstOp = state.dateOfFirstOp || new Date();

// create editable MockPolicyUsers that refreshes every 24 hours 
state.MockPolicyUsersEditable = state.MockPolicyUsersEditable || TEMPSTORE.MockPolicyUsersEditable;     // add it if it doesn't exists yet 
if (helpers.hasOneDayElapsed(state.dateOfFirstOp)) {
    // reset to constant mock data 
    state.MockPolicyUsersEditable = TEMPSTORE.MockPolicyUsersEditable;

    // reset to current date 
    state.dateOfFirstOp = new Date();
}

// track date of first operation of MockClaimDetails579713
state.dateOfFirstOpMockClaimDetails579713 = state.dateOfFirstOpMockClaimDetails579713 || new Date();
// create editable MockClaimDetails579713 that refreshes every 24 hours 
state.MockClaimDetails579713 = state.MockClaimDetails579713 || TEMPSTORE.MockClaimDetails579713Editable;     // add it if it doesn't exists yet 
if (helpers.hasOneDayElapsed(state.dateOfFirstOpMockClaimDetails579713)) {
    // reset to constant mock data 
    state.MockClaimDetails579713 = TEMPSTORE.MockClaimDetails579713Editable;

    // reset to current date 
    state.dateOfFirstOpMockClaimDetails579713 = new Date();
}

// create state.AddUpdateClaimRepo if it doesn't exist
state.AddUpdateClaimRepo = state.AddUpdateClaimRepo || [];

// create state.EnquiryAgainstClaimRepo if it doesn't exist
state.EnquiryAgainstClaimRepo = state.EnquiryAgainstClaimRepos || [];

// create state.AddUpdateClaimDocumentsRepo if it doesn't exist
state.AddUpdateClaimDocumentsRepo = state.AddUpdateClaimDocumentsRepo || [];
// add the mockClaim579713 to state.AddUpdateClaimRepo = state.AddUpdateClaimRepo
state.AddUpdateClaimRepo.push(TEMPSTORE.MockClaimDetails579713);

/* Route definition styles:
 *
 *	define(path, method, function)
 *	soap(path, soapAction, function)
 *
 */
Sandbox.define("/v1/portal/workersInsurance/me/processes/registerEmployerUser/{processId}", "GET", v2.getV2UsersUserRegister);
Sandbox.define("/v1/portal/workersInsurance/me/processes/registerEmployerUser", "POST", v2.postV2UsersUserRegister);

Sandbox.define("/v1/portal/workersInsurance/me/policyAuthorisations", "POST", v2.postV2PolicyAuthorisation);
Sandbox.define("/v1/portal/workersInsurance/me/policyAuthorisations/batch", "POST", v2.postV2UsersUser);

Sandbox.define("/v1/portal/workersInsurance/me/users/{identityId}", "GET", v2.getV2UsersUser);
Sandbox.define("/v1/portal/workersInsurance/me/users/{identityId}", "PATCH", v2.patchV2UsersUser);
Sandbox.define("/v1/portal/workersInsurance/users/user/{identityId}", "DELETE", v2.deleteV2UsersUser);

Sandbox.define("/v1/portal/workersInsurance/menew/users", "POST", v2.postV2Team);
Sandbox.define("/v1/portal/workersInsurance/me/users", "POST", v2.postV2Team_old);
Sandbox.define("/v1/portal/workersInsurance/me/users/search", "POST", v2.postV2Team);

Sandbox.define("/v1/portal/workersInsurance/me", "GET", v2.getV2MyDetails);
//Sandbox.define("/v1/portal/workersInsurance/me", "GET", v2.getV2MyDetailsNew);

Sandbox.define("/v1/portal/workersInsurance/menew", "PATCH", v2.patchV2MyDetails);
Sandbox.define("/v1/portal/workersInsurance/me", "PATCH", v2.patchV2MyDetailsNew);

Sandbox.define("/v1/portal/workersInsurance/me/claims/{claimId}/injuryManagementPlan", "GET", v2.getV2PortalWorkersinsuranceMeClaimsInjurymanagement);
Sandbox.define("/v1/portal/workersInsurance/me/claims/{claimId}/treatments", "GET", v2.getV2PortalWorkersinsuranceMeClaimsInjurymanagement);
Sandbox.define("/v1/portal/workersInsurance/me/claims/{claimId}/returnToWorkPlan", "GET", v2.getV2PortalWorkersinsuranceMeClaimsReturntowork);
Sandbox.define("/v1/portal/workersInsurance/me/claims/{claimId}/returnToWorkPlan", "POST", v2.postV2PortalWorkersinsuranceMeClaimsReturntowork);

//Sandbox.define("/v1/portal/workersInsurance/me/costCentres", "GET", v2.getV2WorkersinsuranceCostcentres);
//Sandbox.define("/v1/portal/workersInsurance/me/locations", "GET", v2.getV2WorkersinsuranceLocations);
Sandbox.define("/v1/portal/workersInsurance/me/policyNumbers", "GET", v2.getV2WorkersinsurancePolicynumbers);

Sandbox.define("/v1/portal/workersInsurance/me/policies/search", "POST", v2.postV1WorkersinsurancePoliciesSearch);
Sandbox.define("/v1/portal/workersInsurance/me/policies/{policyId}/locations", "GET", v2.getV2WorkersinsuranceLocations);
Sandbox.define("/v1/portal/workersInsurance/me/policies/{policyId}/locations/{locationId}/costCenters", "GET", v2.getV2WorkersinsuranceCostcentres);

Sandbox.define("/v1/portal/workersInsurance/me/claims", "POST", v2.postV2WorkersinsuranceClaims);
Sandbox.define("/v1/portal/workersInsurance/me/claims/{claimId}", "GET", v2.getV2WorkersinsuranceClaimdetails);
Sandbox.define("/v1/portal/workersInsurance/me/claims/{claimId}", "PATCH", v2.patchV2WorkersinsuranceClaims);
Sandbox.define("/v1/portal/workersInsurance/me/claims/{claimId}/lodge", "POST", v2.postV2WorkersinsuranceClaimsLodge);
//Sandbox.define("/v1/portal/workersInsurance/me/claims/{claimId}/attachmentLocations", "POST", v2.postV2UnauthWorkersinsuranceClaimsAttachmentlocations);
//Sandbox.define("/v1/portal/workersInsurance/me/claims/{claimId}/attachments", "PATCH", v2.patchV2WorkersinsuranceClaimsAttachments); //operation is same as /claims/{claimId}/attachmentLocations

Sandbox.define("/v1/portal/workersInsurance/me/claims/{claimId}/attachmentLocations", "POST", v2.UnauthenticatedClaimReimbursementAttachmentLocations);
Sandbox.define('/v1/portal/workersInsurance/me/claims/{claimId}/attachments', 'POST', v2.UnauthenticatedClaimReimbursementAttachment); //operation is same as /claims/{claimId}/attachmentLocations
Sandbox.define('/v1/portal/workersInsurance/claims/{claimId}/attachments', 'POST', v2.UnauthenticatedClaimReimbursementAttachment); //operation is same as /claims/{claimId}/attachmentLocations


Sandbox.define("/v1/portal/workersInsurance/me/claims/{id}/enquiry", "POST", v2.postV2WorkersinsuranceClaimsEnquiry);

Sandbox.define("/v1/portal/workersInsurance/me/claims/{claimId}/documents", "POST", v2.postV2WorkersinsuranceClaimsDocuments);
Sandbox.define("/v1/portal/workersInsurance/me/claims/{claimId}/documents", "PATCH", v2.patchV2WorkersinsuranceClaimsDocuments);

Sandbox.define("/v1/portal/workersInsurance/me/claims/{id}/documents", "GET", v2.getV2WorkersinsuranceClaimsDocuments);
Sandbox.define("/v1/portal/workersInsurance/me/claims/checkDuplicate", "POST", v2.postV2WorkersinsuranceClaimsCheckDuplicate);
Sandbox.define("/v1/portal/workersInsurance/payments/financialInstitutions", "GET", v2.getV2PaymentsFinancialInstitutions);

Sandbox.define("/v1/portal/workersInsurance/me/paymentPreference", "GET", v2.getV2MyPaymentdetails);
Sandbox.define("/v1/portal/workersInsurance/me/paymentPreference", "PATCH", v2.patchV2MyPaymentdetails);

Sandbox.define("/v1/my/paymentDetail/forReimbursement", "GET", v2.getV2MyPaymentDetailForReimbursement);

Sandbox.define("/v1/portal/workersInsurance/me/claims/{id}/payments/medical", "GET", v2.getV2PortalWorkersinsuranceMeClaimsPaymentsMedical);
Sandbox.define("/v1/portal/workersInsurance/me/claims/{claimId}/payments/medical/history", "POST", v2.postV2PortalWorkersinsuranceMeClaimsPaymentsMedicalHistory);
Sandbox.define("/v1/portal/workersInsurance/me/claims/{id}/payments/medical/{invoiceId}", "GET", v2.getV2PortalWorkersinsuranceMeClaims);
Sandbox.define("/v1/portal/workersInsurance/me/claims/{id}/payments/wage", "GET", v2.getV2PortalWorkersinsuranceMeClaimsPaymentsWage);
Sandbox.define("/v1/portal/workersInsurance/me/claims/{id}/payments/wage/history", "POST", v2.postV2PortalWorkersinsuranceMeClaimsPaymentsWageHistory);
Sandbox.define("/v1/portal/workersInsurance/me/claims/{id}/payments/wage/{invoiceId}", "GET", v2.getV2PortalWorkersinsuranceMeClaims2);
Sandbox.define("/v1/portal/workersInsurance/me/claims/{id}/payments/medical/reimbursement", "POST", v2.postV2PortalWorkersinsuranceMeClaimsPaymentsMedicalReimbursement);
Sandbox.define("/v1/portal/workersInsurance/me/claims/{id}/payments/wage/reimbursement", "POST", v2.postV2PortalWorkersinsuranceMeClaimsPaymentsWageReimbursement);
Sandbox.define("/v1/portal/workersInsurance/me/claims/{claimId}/preInjuryAvgWeeklyEarnings", "POST", v2.postV2PortalWorkersinsuranceMeClaimsWeeklyAverageearnings);
Sandbox.define("/v1/portal/workersInsurance/me/claims/{claimId}/preInjuryAvgWeeklyEarnings", "GET", v2.getV2PortalWorkersinsuranceMeClaimsWeeklyAverageearnings);

Sandbox.define("/v1/portal/workersInsurance/me/claims/{claimId}/medicalPayments/search", "POST", v2.postV2PortalWorkersinsuranceMeClaimsPaymentsMedicalSearch); // medical payment history & details
Sandbox.define("/v1/portal/workersInsurance/me/claims/{claimId}/weeklyReimbursements/search", "POST", v2.postV2PortalWorkersinsuranceMeClaimsPaymentsWageSearch); //wage history and degtails
Sandbox.define("/v1/portal/workersInsurance/me/claims/{claimId}/activities", "POST", v2.postV2WorkersinsuranceClaimsActivities);

Sandbox.define("/v1/portal/userSummary", "GET", v2.getV2PortalUsersummary);

Sandbox.define("/api/v1/authn", "POST", v2.postOKTAGetUserInfo);


Sandbox.define("/v1/portal/workersInsurance/me/claims/search", "POST", v2.postV2PortalClaims);

Sandbox.define("/v1/portal/workersInsurance/me/claims/{claimId}/medicalPayments/{medicalPaymentId}", "GET", v2.postV2WorkerInsuranceMeClaimsMedicalInvoice);

Sandbox.define("/v1/portal/workersInsurance/me/claims/{claimId}/weeklyReimbursements/{invoiceNumber}", "GET", v2.getV2WorkerInsuranceMeClaimsWageInvoice);

Sandbox.define("/v1/portal/workersInsurance/claims", "POST", v2.postV2UnauthWorkersinsuranceClaims);
Sandbox.define("/v1/portal/workersInsurance/claims/{claimId}", "PATCH", v2.patchV2UnauthWorkersinsuranceClaims);
Sandbox.define("/v1/portal/workersInsurance/claims/{claimId}/lodge", "POST", v2.postV2UnauthWorkersinsuranceClaimsLodge);
//Sandbox.define("/v1/portal/workersInsurance/claims/{id}/attachmentLocations", "POST", v2.postV2UnauthWorkersinsuranceClaimsAttachmentlocations);

Sandbox.define("/v1/portal/workersInsurance/claims/{id}/attachmentLocations", "POST", v2.UnauthenticatedClaimReimbursementAttachmentLocations);
Sandbox.define('/v1/portal/workersInsurance/unauth/workersInsurance/claims/{id}/callbackRequest', 'POST', v2.postV2UnauthWorkersinsuranceClaimscallbackRequest);
Sandbox.define('/v1/portal/workersInsurance/claims/{id}/reimbursement', 'POST', v2.postV2UnauthWorkersinsuranceClaimsReimbursement);

Sandbox.define("/v1/identity/users/activateAccount", "POST", v2.postUsersActivateaccount);
Sandbox.define("/v1/identity/users/confirmAccountPostActivation", "POST", v2.postUsersConfirmaccountpostactivation);




Sandbox.define("/v1/portal/workersInsurance/claims/attachments", "POST", v2.UnauthenticatedClaimReimbursementAttachment);
Sandbox.define("/v1/portal/workersInsurance/claims/attachmentLocations", "POST", v2.UnauthenticatedClaimReimbursementAttachmentLocations);
Sandbox.define("/v1/portal/workersInsurance/claims/attachmentLocations", "PATCH", v2.UnauthenticatedClaimReimbursementAttachmentLocations);

Sandbox.define("/v1/identity/passwords/recovery", "POST", v2.PasswordRecovery);
Sandbox.define("/v1/identity/passwords/recovery/tokens/verify", "POST", v2.RecoveryTokenVerify);
Sandbox.define("/v1/identity/passwords/recovery/questions/verify", "POST", v2.RecoveryQuestionVerify);
Sandbox.define("/v1/identity/passwords/reset", "POST", v2.ResetPassword);




/* Unit Testing Helper: 
 *  This is used only for removing test data from state.AddUpdateClaimRepo 
 *
 */
Sandbox.define('/v1/portal/workersInsurance/portal/workersInsurance/me/claims/{id}', 'DELETE', function (req, res) {
    // Check the request, make sure it is a compatible type
    if (!req.is('application/json')) {
        return res.send(400, 'Invalid content type, expected application/json');
    }

    // check if the claimNo exists 
    var existingClaim = _.find(state.AddUpdateClaimRepo, function (o) {
        return _.toString(o.data.attributes.claimNo) === _.toString(req.params.id);
    });

    // remove the claim
    if (existingClaim) state.AddUpdateClaimRepo = _.reject(state.AddUpdateClaimRepo, function (o) {
        return _.toString(o.data.attributes.claimNo) === _.toString(req.params.id);
    });

    // Set the type of response, sets the content type.
    res.type('application/json');

    // Set the status code of the response.
    res.status(200);

    // Send the response body.
    res.json({
        "status": "ok"
    });
});

Sandbox.define('/v2/unauth/portal/workersInsurance/me/claims/{id}', 'DELETE', function (req, res) {
    // Check the request, make sure it is a compatible type
    if (!req.is('application/json')) {
        return res.send(400, 'Invalid content type, expected application/json');
    }

    // remove the claim
    var existingClaim = _.find(state.claims, function (o) { return o.data.attributes.claimNo === req.params.id; });
    if (existingClaim) state.claims = _.reject(state.claims, function (o) { return o.data.attributes.claimNo === req.params.id; });

    // Set the type of response, sets the content type.
    res.type('application/json');

    // Set the status code of the response.
    res.status(200);

    // Send the response body.
    res.json({
        "status": "ok"
    });
});
