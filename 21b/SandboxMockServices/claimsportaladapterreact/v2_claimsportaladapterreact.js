﻿/*
 * POST /v2/users/user
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.postV2UsersUser = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validAddUserRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // try add user 
    var responseObject = helpers.addUser(req);

    // if add is not allowed 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/users/user
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.postV2UsersUser_old = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validAddUserRequest_old(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // try add user 
    var responseObject = helpers.addUser_old(req);

    // if add is not allowed 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST- /workersInsurance/me/policyAuthorisations
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.postV2PolicyAuthorisation = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validPolicyAuthorisation(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // try add user 
    var responseObject = helpers.policyAuthorisation(req);

    // if add is not allowed 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/users/user
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * identityId(type: string) - query parameter - id of the user (generated when the user was added)
 */
exports.getV2UsersUser = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validParam(req, "identityId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get user   
    var responseObject = helpers.getUser(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * PATCH /v2/users/user/{identityId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * identityId(type: string) - path parameter -
 */
exports.patchV2UsersUser = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validParam(req, "identityId", false)) {
        return helpers.returnError(res, 400, ERRORS.Error400b);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validUpdateUserRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.updateUser(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.json(responseObject);
};

/*
 * DELETE /v2/users/user/{identityId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * identityId(type: string) - path parameter -
 */
exports.deleteV2UsersUser = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    // if (!helpers.validParam(req, "identityId", true)) {
    //     return helpers.returnError(res, 400, ERRORS.Error400b);
    // }

    // response object  
    var responseObject = helpers.deleteUser(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // Set the type of response, sets the content type.
    res.type('application/json');

    // Set the status code of the response.
    res.status(200);

    // Send the response body.
    res.json({
        "status": "ok"
    });
};

/*
 * GET /v2/team_old
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.postV2Team_old = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validGetTeamRequest_old(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // try get team 
    var responseObject = helpers.getTeam_old(req);

    // if add is not allowed 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/team
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.postV2Team = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validGetTeamRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // try get team 
    var responseObject = helpers.getTeam(req);

    // if add is not allowed 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};


/*
 * GET /v2/my/details
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.getV2MyDetails = function (req, res) {
    // validate access 
    /* if (!helpers.validAccessToken(req)) {
         return helpers.returnError(res, 401, ERRORS.Error401);
     }
 
     // validate request type 
     if (!req.is('json') && !req.is('application/vnd.api+json')) {
         return helpers.returnError(res, 400, ERRORS.Error400);
     }
 
     // validate headers 
     if (!helpers.validOktaTokenJsonHeader(req)) {
         return helpers.returnError(res, 400, ERRORS.Error400);
     }
     if (!helpers.validTrackingIdHeader(req)) {
         return helpers.returnError(res, 400, ERRORS.Error400);
     }
 
     // validate data 
     */

    // get cost centres  
    var responseObject = helpers.getMyDetails(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 404, ERRORS.Error404);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};





/*
 * GET /v2/my/details
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.getV2MyDetailsNew = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 


    // get cost centres  
    var responseObject = helpers.getMyDetailsNew(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 404, ERRORS.Error404);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};




exports.patchV2MyDetailsNew = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validAccountDetails(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.updateMyDetails(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};




/*
 * PATCH /v2/my/details
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.patchV2MyDetails = function (req, res) {
    // validate access 
    //if (!helpers.validAccessToken(req)) {
    //    return helpers.returnError(res, 401, ERRORS.Error401);
    //}

    // validate request type 
    //if (!req.is('json') && !req.is('application/vnd.api+json')) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // validate headers 
    //if (!helpers.validOktaTokenJsonHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}
    //if (!helpers.validTrackingIdHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // check if valid JSON body 
    //if (!req.body || req.body.isInvalid) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // validate data in body 
    //if (!helpers.validAccountDetails(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // response object  
    //var responseObject = helpers.updateMyDetails(req);

    // if invalid response object 
    //if (!responseObject) {
    //    return helpers.returnError(res, 401, ERRORS.Error401);
    //}

    return helpers.returnError(res, 500, ERRORS.Error401);
    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(504);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/users/user/register
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * activationToken (type: string) - query parameter - encrypted token that Mulesoft will verify which embeds the okta ID of the user looking to register
 */
exports.getV2UsersUserRegister = function (req, res) {
    // validate activation token 
    //if (!helpers.validActivationTokenParam(req, "activationToken")) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get user register
    var responseObject = helpers.getUserRegister(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/users/user/register
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 */
exports.postV2UsersUserRegister = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validUserRegister(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get user   
    var responseObject = helpers.postUserRegister(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /workersInsurance/me/policies/{policyId}/locations/{locationId}/costCenters
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * policyNumber(type: string) - query parameter - Detail to pull cost centre
 * location(type: string) - query parameter - Detail to pull cost centre
 */
exports.getV2WorkersinsuranceCostcentres = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validParam(req, "policyId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validParam(req, "locationId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get cost centres  
    var responseObject = helpers.getCostCentres(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};


/*
 * GET /workersInsurance/me/policies/{policyId}/locations
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * policyId(type: string) - path parameter
 */
exports.getV2WorkersinsuranceLocations = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validParam(req, "policyId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get locations 
    var responseObject = helpers.getLocations(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};


/*
 * GET /v2/workersInsurance/policyNumbers
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * date(type: string) - query parameter - 
 */
exports.getV2WorkersinsurancePolicynumbers = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validPolicyNumberRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get claim numbers 
    var responseObject = helpers.getPolicyNumbers(req);

    // if invalid response object  
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.render("policyNumber-response", responseObject);
};

/*
 * POST /v1/portal/workersInsurance/me/policies/search
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * date(type: string) - query parameter - 
 */
exports.postV1WorkersinsurancePoliciesSearch = function (req, res) {
    // validate access 


    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validPoliciesSearchRequest(req)) {
        return helpers.returnError(res, 404, ERRORS.Error404);
    }

    // get claim numbers 
    var responseObject = helpers.getPoliciesSearch(req);

    // if invalid response object  
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};


/*
 * POST /v2/workersInsurance/claims
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.postV2WorkersinsuranceClaims = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validClaimRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // try add claim 
    var responseObject = helpers.addClaim(req);

    // if add is not allowed 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};


/*
 * GET /v2/workersInsurance/claims/{id}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to retrieve 
 */
exports.getV2WorkersinsuranceClaimdetails = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validParam(req, "claimId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get locations 
    var responseObject = helpers.getClaimByClaimId(req);

    // if invalid response object 
    if (!responseObject) {
        req.params.claimId = "2578840";
        responseObject = helpers.getClaimByClaimId(req);

        //return helpers.returnError(res, 404, ERRORS.Error404);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};


/*
 * PATCH /v2/workersInsurance/claims/{id}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to patch
 */
exports.patchV2WorkersinsuranceClaims = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validClaimRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validParam(req, "claimId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.updateClaimById(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.json(responseObject);
};


/*
 * POST /v2/workersInsurance/claims/{id}/lodge
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to patch
 */
exports.postV2WorkersinsuranceClaimsLodge = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validClaimRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validParam(req, "claimId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.lodgeClaimById(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);
    // set response body and send
    res.json(responseObject);
};


/*
 * POST /v2/workersInsurance/claims/{id}/attachmentLocations
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to add attachments to
 */
exports.postV2WorkersinsuranceClaimsAttachmentlocations = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    //if (!helpers.validTrackingIdHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validAttachmentRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validParam(req, "claimId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.addAttachmentByClaimId(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.render("attachment-response", responseObject);
};

/*
 * PATCH /v1/portal/workersInsurance/claims/{claimId}/attachments
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to add attachments to
 */
exports.patchV2WorkersinsuranceClaimsAttachments = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    //if (!helpers.validTrackingIdHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validAttachmentRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validParam(req, "claimId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.addAttachmentByClaimId(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.render("attachment-response", responseObject);
};

/*
 * POST /v2/workersInsurance/claims/{id}/enquiry
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to add attachments to
 */
exports.postV2WorkersinsuranceClaimsEnquiry = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validEnquiry(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.enquiryAgainstClaimByClaimId(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400b);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/portal/workersInsurance/me/claims/{claimId}/activities
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to add attachments to
 */
exports.postV2WorkersinsuranceClaimsActivities = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validActivities(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validParam(req, "claimId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.activitiesByClaimId(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/workersInsurance/claims/{id}/documents
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to add attachments to
 */
exports.getV2WorkersinsuranceClaimsDocuments = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.getClaimsDocuments(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 404, ERRORS.Error400b);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};


/*
 * POST /v2/workersInsurance/claims/{id}/documents
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to add attachments to
 */
exports.postV2WorkersinsuranceClaimsDocuments = function (req, res) {
    // validate access 

    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validParam(req, "claimId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validClaimDocumentRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.postClaimsDocuments(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 404, ERRORS.Error400b);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};


/*
 * PATCH /v2/workersInsurance/claims/{id}/documents
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to patch
 */
exports.patchV2WorkersinsuranceClaimsDocuments = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validClaimDocumentRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validParam(req, "claimId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.updateClaimDocumentsById(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.json(responseObject);
};


/*
 * POST /v2/workersInsurance/claims/checkDuplicate
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.postV2WorkersinsuranceClaimsCheckDuplicate = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validDuplicateClaimCheckRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get duplicate 
    var responseObject = helpers.getDuplicateClaim(req);

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};



/*
 * GET /v2/portal/workersInsurance/me/claims/{claimId}/injuryManagement
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to retrieve
 */
exports.getV2PortalWorkersinsuranceMeClaimsInjurymanagement = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validParam(req, "claimId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get injury management 
    var responseObject = helpers.getInjuryManagement(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/portal/workersInsurance/me/claims/{id}/returnToWork
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to retrieve
 */
exports.getV2PortalWorkersinsuranceMeClaimsReturntowork = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validParam(req, "claimId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get return to work
    var responseObject = helpers.getReturnToWork(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/portal/workersInsurance/me/claims/{id}/returnToWork
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to retrieve
 */
exports.postV2PortalWorkersinsuranceMeClaimsReturntowork = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validParam(req, "claimId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get bank details  
    var responseObject = helpers.postReturnToWork(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};



/*
 * GET /v2/portal/workersInsurance/me/claims/{id}/payments/medical
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.getV2PortalWorkersinsuranceMeClaimsPaymentsMedical = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "claimId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    // validate param
    if (!helpers.validParam(req, "medicalPaymentId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage  
    var responseObject = helpers.getPaymentMedicalOverview(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/portal/workersInsurance/me/claims/{id}/payments/medical/history
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.postV2PortalWorkersinsuranceMeClaimsPaymentsMedicalHistory = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "claimId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validPaymentMedicalHistory(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage history
    var responseObject = helpers.getPaymentMedicalHistory(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /workersInsurance/me/claims/{claimId}/medicalPayments/search
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.postV2PortalWorkersinsuranceMeClaimsPaymentsMedicalSearch = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "claimId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validPaymentMedicalHistory(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage history
    var responseObject = helpers.getMedicalPaymentSearch(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/portal/workersInsurance/me/claims/{id}/payments/medical/{invoiceId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 * invoiceId(type: integer) - path parameter - ID of medical invoice
 */
exports.getV2PortalWorkersinsuranceMeClaims = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validParam(req, "invoiceId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage  
    var responseObject = helpers.getPaymentMedicalDetail(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/portal/workersInsurance/me/claims/{id}/payments/wage
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.getV2PortalWorkersinsuranceMeClaimsPaymentsWage = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage  
    var responseObject = helpers.getPaymentWageOverview(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/portal/workersInsurance/me/claims/{id}/payments/wage/history
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.postV2PortalWorkersinsuranceMeClaimsPaymentsWageHistory = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validPaymentWageHistory(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage history
    var responseObject = helpers.getPaymentWageHistory(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/portal/workersInsurance/me/claims/{claimId}/weeklyReimbursements/search
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.postV2PortalWorkersinsuranceMeClaimsPaymentsWageSearch = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "claimId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validPaymentWageSearch(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage history
    var responseObject = helpers.getPaymentWageSearch(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/portal/workersInsurance/me/claims/{id}/payments/wage/{invoiceId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 * invoiceId(type: integer) - path parameter - ID of wage detail invoice
 */
exports.getV2PortalWorkersinsuranceMeClaims2 = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validParam(req, "invoiceId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage  
    var responseObject = helpers.getPaymentWageDetail(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/portal/workersInsurance/me/claims/{id}/payments/medical/reimbursement
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.postV2PortalWorkersinsuranceMeClaimsPaymentsMedicalReimbursement = function (req, res) {
    // validate access
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate request type
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate headers
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate data
    if (!helpers.postPaymentsMedicalReimbursement) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    //get description
    var responseObject = helpers.postMyPaymentsMedicalReimbursement(req);

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/portal/workersInsurance/me/claims/{id}/payments/wage/reimbursement
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.postV2PortalWorkersinsuranceMeClaimsPaymentsWageReimbursement = function (req, res) {
    // validate access
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate request type
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate headers
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate data
    if (!helpers.validatePaymentsWageReimbursement) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }


    //get description
    var responseObject = helpers.postMyPaymentsWageReimbursement(req);

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/portal/workersInsurance/me/claims/{id}/weekly/averageEarnings
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.postV2PortalWorkersinsuranceMeClaimsWeeklyAverageearnings = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "claimId", true)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate body
    if (!helpers.validWeeklyAverageEarnings(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage  
    var responseObject = helpers.postWeeklyAverageEarnings(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/my/paymentDetails
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.getV2MyPaymentdetails = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 

    // get bank details  
    var responseObject = helpers.getPaymentDetails(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * PATCH /v2/my/paymentDetails
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.patchV2MyPaymentdetails = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validPaymentDetails(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get bank details  
    var responseObject = helpers.updatePaymentDetails(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET workersInsurance/payments/financialInstitutions
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * bsb(type: string) - query parameter - BSB corresponding to a bank name
 */
exports.getV2PaymentsFinancialInstitutions = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    //if (!helpers.validTokenHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validQueryParam(req, "BSB")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get FinancialInstitution
    var responseObject = helpers.getFinancialInstitutions(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 404, ERRORS.Error404);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/my/paymentDetail/forReimbursement
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 */
exports.getV2MyPaymentDetailForReimbursement = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 

    // get bank details  
    var responseObject = helpers.getPaymentDetailForReimbursement(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};



/*
 * GET /v2/portal/workersInsurance/me/claims/{id}/payments/medical
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.getV2PortalWorkersinsuranceMeClaimsPaymentsMedical = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage  
    var responseObject = helpers.getPaymentMedicalOverview(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};


/*
 * GET /v2/portal/workersInsurance/me/claims/{id}/payments/medical/{invoiceId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 * invoiceId(type: integer) - path parameter - ID of medical invoice
 */
exports.getV2PortalWorkersinsuranceMeClaims = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validParam(req, "invoiceId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage  
    var responseObject = helpers.getPaymentMedicalDetail(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/portal/workersInsurance/me/claims/{id}/payments/wage
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.getV2PortalWorkersinsuranceMeClaimsPaymentsWage = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage  
    var responseObject = helpers.getPaymentWageOverview(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/portal/workersInsurance/me/claims/{id}/payments/wage/history
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.postV2PortalWorkersinsuranceMeClaimsPaymentsWageHistory = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validPaymentWageHistory(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage history
    var responseObject = helpers.getPaymentWageHistory(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/portal/workersInsurance/me/claims/{id}/payments/wage/{invoiceId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-TrackingID(type: string) - header parameter -
 * X-OktaTokenJson(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 * invoiceId(type: integer) - path parameter - ID of wage detail invoice
 */
exports.getV2PortalWorkersinsuranceMeClaims2 = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validParam(req, "invoiceId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage  
    var responseObject = helpers.getPaymentWageDetail(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/portal/workersInsurance/me/claims/{id}/payments/medical/reimbursement
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.postV2PortalWorkersinsuranceMeClaimsPaymentsMedicalReimbursement = function (req, res) {
    // validate access
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate request type
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate headers
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate data
    if (!helpers.postPaymentsMedicalReimbursement) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    //get description
    var responseObject = helpers.postMyPaymentsMedicalReimbursement(req);

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/portal/workersInsurance/me/claims/{id}/payments/wage/reimbursement
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.postV2PortalWorkersinsuranceMeClaimsPaymentsWageReimbursement = function (req, res) {
    // validate access
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate request type
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate headers
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //validate data
    if (!helpers.validatePaymentsWageReimbursement) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }


    //get description
    var responseObject = helpers.postMyPaymentsWageReimbursement(req);

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/portal/workersInsurance/me/claims/{id}/weekly/averageEarnings
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.postV2PortalWorkersinsuranceMeClaimsWeeklyAverageearnings = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "claimId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate body
    if (!helpers.validWeeklyAverageEarnings(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage  
    var responseObject = helpers.postWeeklyAverageEarnings(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/portal/workersInsurance/me/claims/{id}/weekly/averageEarnings
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim for context
 */
exports.getV2PortalWorkersinsuranceMeClaimsWeeklyAverageearnings = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "claimId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // getWeeklyAverageEarnings
    var responseObject = helpers.getWeeklyAverageEarnings(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v2/portal/userSummary
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.getV2PortalUsersummary = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 

    // get User Summary
    var responseObject = helpers.getUserSummary(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 404, ERRORS.Error404);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};


/*
 * GET /v1/workersInsurance/me/claims/{claimId}/medicalpayments/medicalpaymentId(Medical invoice)
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.postV2WorkerInsuranceMeClaimsMedicalInvoice = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "claimId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get payment wage history
    var responseObject = helpers.getPaymentMedicalInvoice(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
}


/*
 * /workersInsurance/me/claims/{claimId}/weeklyReimbursements/{invoiceNumber}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.getV2WorkerInsuranceMeClaimsWageInvoice = function (req, res) {
    // validate access 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate param
    if (!helpers.validParam(req, "claimId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validParam(req, "invoiceNumber")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    // get payment wage history
    var responseObject = helpers.getPaymentWageInvoice(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
}

/*
 * POST /v2/portal/claims
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.postV2PortalClaims = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validGetPortalClaimsRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // try get claims  
    var responseObject = helpers.getPortalClaims(req);

    // if add is not allowed 
    if (!responseObject) {
        return helpers.returnError(res, 404, ERRORS.Error404);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};


exports.getV2PortalClaimdetails = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validQueryParam(req, "claimNumber")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get locations 
    var responseObject = helpers.getPortalClaimDetails(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 404, ERRORS.Error404);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};


exports.getV2PortalContactdetails = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data 
    if (!helpers.validQueryParam(req, "contactId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // get locations 
    var responseObject = helpers.getContactDetails(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 404, ERRORS.Error404);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/unauth/workersInsurance/claims
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.postV2UnauthWorkersinsuranceClaims = function (req, res) {
    //// validate access 
    //if (!helpers.validAccessToken(req)) {
    //    return helpers.returnError(res, 401, ERRORS.Error401);
    //}

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    //if (!helpers.validTokenHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validClaimRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // try add claim 
    var responseObject = helpers.addClaim(req);

    // if add is not allowed 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    //res.render("success-response", responseObject);
    res.json(responseObject);
};

/*
 * PATCH /v2/unauth/workersInsurance/claims/{id}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to patch
 */
exports.patchV2UnauthWorkersinsuranceClaims = function (req, res) {
    //// validate access 
    //if (!helpers.validAccessToken(req)) {
    //    return helpers.returnError(res, 401, ERRORS.Error401);
    //}

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    //if (!helpers.validTokenHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validClaimRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validParam(req, "claimId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.updateClaimById(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};


/*
 * POST /v2/unauth/workersInsurance/claims/{id}/lodge
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to patch
 */
exports.postV2UnauthWorkersinsuranceClaimsLodge = function (req, res) {
    //// validate access 
    //if (!helpers.validAccessToken(req)) {
    //    return helpers.returnError(res, 401, ERRORS.Error401);
    //}

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    //if (!helpers.validTokenHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validClaimRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validParam(req, "claimId")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.lodgeClaimById(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);
    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v2/unauth/workersInsurance/claims/attachmentLocations
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * id(type: integer) - path parameter - ID of claim to add attachments to
 */
exports.postV2UnauthWorkersinsuranceClaimsAttachmentlocations = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    //if (!helpers.validTokenHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}
    //if (!helpers.validTrackingIdHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validAttachmentRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate parameters 
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // response object  
    var responseObject = helpers.addAttachmentByClaimId(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.render("attachment-response", responseObject);
};

exports.postV2UnauthWorkersinsuranceClaimscallbackRequest = function (req, res) {
    //// validate access 
    //if (!helpers.validAccessToken(req)) {
    //    return helpers.returnError(res, 401, ERRORS.Error401);
    //}

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    //if (!helpers.validTrackingIdHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }


    // validate parameters 
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400b);
    }

    // response object  
    var responseObject = helpers.timeslot(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.json(responseObject);
};

exports.postV2UnauthWorkersinsuranceClaimsReimbursement = function (req, res) {
    //// validate access 
    //if (!helpers.validAccessToken(req)) {
    //    return helpers.returnError(res, 401, ERRORS.Error401);
    //}

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validTokenHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }


    // validate parameters 
    if (!helpers.validParam(req, "id")) {
        return helpers.returnError(res, 400, ERRORS.Error400b);
    }

    // response object  
    var responseObject = helpers.submitClaimReimbursement(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(202);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /api/v1/authn
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.postOKTAGetUserInfo = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validOKTAGetUserInfoRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // try add user 
    var responseObject = helpers.oktaGetUserInfo(req);

    // if add is not allowed 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v1/portal/workersInsurance/passwords/recovery
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.postUsersPasswordRecovery = function (req, res) {
    // validate access 
    if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    //if (!helpers.validTokenHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}
    //if (!helpers.validOktaTokenJsonHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validUsersActivateAccountRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // activate user account 
    var responseObject = helpers.usersActivateAccount(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};


/*
 * POST /v1/portal/workersInsurance/users/activateAccount
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.postUsersActivateaccount = function (req, res) {
    // validate access 
    //if (!helpers.validAccessToken(req)) {
    //    return helpers.returnError(res, 401, ERRORS.Error401);
    //}

    // validate request type 
    //if (!req.is('json') && !req.is('application/vnd.api+json')) {
    //   return helpers.returnError(res, 400, ERRORS.Error400);
    //}
    // validate headers 
    //if (!helpers.validTokenHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}
    //if (!helpers.validOktaTokenJsonHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}
    //if (!helpers.validTrackingIdHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate data in body 
    if (!helpers.validUsersActivateAccountRequest(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // activate user account 
    var responseObject = helpers.usersActivateAccount(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};








/*
 * POST /v1/portal/workersInsurance/users/confirmAccountPostActivation
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.UnauthenticatedClaimReimbursementAttachment = function (req, res) {
    // validate access 

    // validate request type 
    /*if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }*/

    // validate headers 
    //if (!helpers.validTokenHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}
    //if (!helpers.validOktaTokenJsonHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }


    // activate user account 
    var responseObject = ""

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};






/*
 * POST /v1/portal/workersInsurance/users/confirmAccountPostActivation
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.UnauthenticatedClaimReimbursementAttachmentLocations = function (req, res) {

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // activate user account 
    var responseObject = helpers.getattachmentLocations(req);

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};













/*
 * POST /v1/portal/workersInsurance/users/confirmAccountPostActivation
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.postUsersConfirmaccountpostactivation = function (req, res) {
    // validate access 
    /* if (!helpers.validAccessToken(req)) {
         return helpers.returnError(res, 401, ERRORS.Error401);
     }
 
     // validate request type 
     if (!req.is('json') && !req.is('application/vnd.api+json')) {
         return helpers.returnError(res, 400, ERRORS.Error400);
     }*/

    // validate headers 
    //if (!helpers.validTokenHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}
    //if (!helpers.validOktaTokenJsonHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}
    //if (!helpers.validTrackingIdHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // check if valid JSON body 
    //if (!req.body || req.body.isInvalid) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // validate data in body 
    //if (!helpers.validUsersConfirmAccountPostActivationRequest(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // activate user account 
    var responseObject = helpers.usersConfirmAccountPostActivation(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};


/*
 * POST /v1/portal/workersInsurance/users/activateAccount
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.PasswordRecovery = function (req, res) {
    // validate access 
    /*  if (!helpers.validAccessToken(req)) {
          return helpers.returnError(res, 401, ERRORS.Error401);
      }
  
      // validate request type 
      if (!req.is('json') && !req.is('application/vnd.api+json')) {
          return helpers.returnError(res, 400, ERRORS.Error400);
      }
  
      // validate headers 
      //if (!helpers.validTokenHeader(req)) {
      //    return helpers.returnError(res, 400, ERRORS.Error400);
      //}
      if (!helpers.validOktaTokenJsonHeader(req)) {
          return helpers.returnError(res, 400, ERRORS.Error400);
      }
      if (!helpers.validTrackingIdHeader(req)) {
          return helpers.returnError(res, 400, ERRORS.Error400);
      }
  
      // check if valid JSON body 
      if (!req.body || req.body.isInvalid) {
          return helpers.returnError(res, 400, ERRORS.Error400);
      }
  
      // validate data in body 
      if (!helpers.validUsersRecoveryTokenRequest(req)) {
          return helpers.returnError(res, 400, ERRORS.Error400);
      }*/

    // activate user account 
    var responseObject = helpers.usersForgetPassword(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};





exports.RecoveryTokenVerify = function (req, res) {
    // activate user account 
    var responseObject = helpers.usersForgetPasswordTokenVerify(req);
    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};



/*
 * POST /v1/portal/workersInsurance/users/activateAccount
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.RecoveryQuestionVerify = function (req, res) {
    // validate access 
    /* if (!helpers.validAccessToken(req)) {
         return helpers.returnError(res, 401, ERRORS.Error401);
     }
 
     // validate request type 
     if (!req.is('json') && !req.is('application/vnd.api+json')) {
         return helpers.returnError(res, 400, ERRORS.Error400);
     }
 
     // validate headers 
     //if (!helpers.validTokenHeader(req)) {
     //    return helpers.returnError(res, 400, ERRORS.Error400);
     //}
     if (!helpers.validOktaTokenJsonHeader(req)) {
         return helpers.returnError(res, 400, ERRORS.Error400);
     }
     if (!helpers.validTrackingIdHeader(req)) {
         return helpers.returnError(res, 400, ERRORS.Error400);
     }
 
     // check if valid JSON body 
     if (!req.body || req.body.isInvalid) {
         return helpers.returnError(res, 400, ERRORS.Error400);
     }*/

    // activate user account 
    var responseObject = helpers.usersForgetPasswordQuestionsVerify(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};


/*
 * POST /v1/portal/workersInsurance/users/activateAccount
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.ResetPassword = function (req, res) {
    // validate access 
    /* if (!helpers.validAccessToken(req)) {
         return helpers.returnError(res, 401, ERRORS.Error401);
     }
 
     // validate request type 
     if (!req.is('json') && !req.is('application/vnd.api+json')) {
         return helpers.returnError(res, 400, ERRORS.Error400);
     }
 
     // validate headers 
     //if (!helpers.validTokenHeader(req)) {
     //    return helpers.returnError(res, 400, ERRORS.Error400);
     //}
     if (!helpers.validOktaTokenJsonHeader(req)) {
         return helpers.returnError(res, 400, ERRORS.Error400);
     }
     if (!helpers.validTrackingIdHeader(req)) {
         return helpers.returnError(res, 400, ERRORS.Error400);
     }
 
     // check if valid JSON body 
     if (!req.body || req.body.isInvalid) {
         return helpers.returnError(res, 400, ERRORS.Error400);
     }*/

    // activate user account 
    var responseObject = helpers.usersResetPassword(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};






/*
 * POST /v1/portal/workersInsurance/users/activateAccount
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.RecoveryTokenVerify = function (req, res) {
    // validate access 
    /*if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    //if (!helpers.validTokenHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }*/

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // activate user account 
    var responseObject = helpers.usersForgetPasswordTokenVerify(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};



/*
 * POST /v1/portal/workersInsurance/users/activateAccount
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.RecoveryQuestionVerify = function (req, res) {
    // validate access 
    /*if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }*/

    // validate request type 
    /* if (!req.is('json') && !req.is('application/json')) {
         return helpers.returnError(res, 400, ERRORS.Error400);
     }
 
     // validate headers 
     //if (!helpers.validTokenHeader(req)) {
     //    return helpers.returnError(res, 400, ERRORS.Error400);
     //}
     if (!helpers.validOktaTokenJsonHeader(req)) {
         return helpers.returnError(res, 400, ERRORS.Error400);
     }
     if (!helpers.validTrackingIdHeader(req)) {
         return helpers.returnError(res, 400, ERRORS.Error400);
     }
 
     // check if valid JSON body 
     if (!req.body || req.body.isInvalid) {
         return helpers.returnError(res, 400, ERRORS.Error400);
     }*/

    // activate user account 
    var responseObject = helpers.usersForgetPasswordQuestionsVerify(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};


/*
 * POST /v1/portal/workersInsurance/users/activateAccount
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-Token(type: string) - header parameter -
 * X-TrackingID(type: string) - header parameter -
 */
exports.ResetPassword = function (req, res) {
    // validate access 
    /*if (!helpers.validAccessToken(req)) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    //if (!helpers.validTokenHeader(req)) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }*/

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // activate user account 
    var responseObject = helpers.usersResetPassword(req);

    // if invalid response object 
    if (!responseObject) {
        return helpers.returnError(res, 401, ERRORS.Error401);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};
