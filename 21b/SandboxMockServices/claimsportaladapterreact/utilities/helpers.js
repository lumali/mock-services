﻿/*
 * Common functions 
 *
 */

// returnError 
exports.returnError = function (res, code, message) {
    return res.json(code, { error: { 'message': message } });
};

// validateAccessToken 
exports.validAccessToken = function (req) {
    var accessToken = req.get("access_token");
    accessToken = accessToken ? accessToken : undefined;

    if (accessToken === undefined) return false;

    // Assume JWT 
    // header.payload.signature
    var jwtArray = accessToken.split('.');
    if (jwtArray.length < 3) return false;
    if (!jwtArray[0]) return false;
    if (!jwtArray[1]) return false;
    if (!jwtArray[2]) return false;

    // Other checking here 

    return true;
};

// validTokenHeader
exports.validTokenHeader = function (req) {
    var xToken = req.get("X-Token");
    xToken = xToken ? xToken : undefined;

    return (xToken !== undefined);
};

// validTrackingIdHeader
exports.validTrackingIdHeader = function (req) {
    var xTrackingId = req.get("X-TrackingID");
    xTrackingId = xTrackingId ? xTrackingId : undefined;

    return (xTrackingId !== undefined);
};

// validOktaTokenJsonHeader
exports.validOktaTokenJsonHeader = function (req) {
    var xOktaTokenJson = req.get("X-OktaTokenJson");
    xOktaTokenJson = xOktaTokenJson ? xOktaTokenJson : undefined;
    return (xOktaTokenJson !== undefined);
};

// validQueryParam
exports.validQueryParam = function (req, q) {
    var input = req.query[q] ? req.query[q] : undefined;
    if (input === undefined) return false;
    return true;
};

// validPolicyNumberRequest
exports.validPolicyNumberRequest = function (req) {
    var input = req.query.date ? req.query.date : undefined;
    if (input === undefined) return false;
    return true;
};

// validPolicyNumberRequest
exports.validPoliciesSearchRequest = function (req) {

    //var input = req.query.injuryDate ? req.query.injuryDate : undefined;
    var input = req.body.data.attributes.criteria.injuryDate ? req.body.data.attributes.criteria.injuryDate : undefined;
    if (input === undefined) return false;
    //else if (input != "1/02/2018" && input != "4/04/2017" && input != "1/05/2017") return false;
    return true;
};


// validClaimRequest 
exports.validClaimRequest = function (req) {
    var input = req.body.data ? req.body.data : undefined;

    if (input === undefined) return false;

    // required:
    //  - notifier.name
    //  - notifier.contact.phone.primary
    //  - injury.occurrence
    //  - employer
    if (!input.attributes.notifier.name.given || !input.attributes.notifier.name.family) return false;
    if (!input.attributes.notifier.contact.phone.primary) return false;
    if (!input.attributes.injury.occurrence.date || !input.attributes.injury.occurrence.time) return false;
    //if (!input.attributes.employer
    //    || !input.attributes.employer.name
    //    || !input.attributes.employer.abn
    //    || !input.attributes.employer.email) return false;

    return true;
};

// validParam 
exports.validParam = function (req, p, shouldTestForInt) {
    var params = req.params ? req.params : undefined;
    if (params === undefined) return false;

    var targetParam = params[p] ? params[p] : undefined;
    if (targetParam === undefined) return false;

    if (shouldTestForInt) {
        var intParam = parseInt(targetParam, 10);
        return _.isInteger(intParam);
    }

    return true;
};

// validAttachmentRequest
exports.validAttachmentRequest = function (req) {
    var input = req.body.meta ? req.body.meta : undefined;
    if (input === undefined) return false;
    if (!input.uploadedby) return false;
    if (!input.uploaderphoneno) return false;
    if (!input.uploaderemail) return false;
    if (!input.filename) return false;
    if (!input.documenttype) return false;
    if (!input.claimnumber) return false;
    if (!input.claimid) return false;

    return true;
};

// validAccountDetails
exports.validAccountDetails = function (req) {
    var input = req.body.data ? req.body.data : undefined;
    if (input === undefined) return false;

    if (!input.attributes.firstName) return false;
    if (!input.attributes.lastName) return false;
    if (!input.attributes.phone.contactNumber) return false;
    if (!input.attributes.phone.phoneType) return false;
    if (!input.attributes.contactPreference) return false;

    return true;
};

// validPaymentDetails
exports.validPaymentDetails = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.data.attributes) return false;
    if (!input.data.attributes.accountName) return false;
    if (!input.data.attributes.accountNumber) return false;
    if (!input.data.attributes.bsb) return false;

    return true;
};

// validAddUserRequest_old
exports.validAddUserRequest_old = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;

    if (!input.givenName) return false;
    if (!input.familyName) return false;
    if (!input.phone.contactNumber) return false;
    if (!input.phone.phoneType) return false;
    if (!input.emailAddress) return false;
    if (!input.policyRoles) return false;
    if (input.policyRoles.length < 1) return false;
    if (!input.policyRoles[0].policyNumber) return false;
    if (!input.policyRoles[0].policyName) return false;
    if (!input.policyRoles[0].role) return false;


    return true;
};

// validAddUserRequest
exports.validAddUserRequest = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;

    if (!input.data.type) return false;

    return true;
};

// validPolicyAuthorisation
exports.validPolicyAuthorisation = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;

    if (!input.data.type) return false;
    if (input.data.attributes.length < 1) return false;
    return true;
};

// validOKTAGetUserInfoRequest
exports.validOKTAGetUserInfoRequest = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;

    if (!input.token) return false;

    return true;
};

// validUpdateUserRequest
exports.validUpdateUserRequest = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;

    if (!input.data.id) return false;
    if (input.data.attributes.authorisations.length < 1) return false;

    return true;
}

// validGetTeamRequest
exports.validGetTeamRequest = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.data.attributes.params.page) return false;
    if (!input.data.attributes.params.pageSize) return false;

    return true;
}

exports.validGetPortalClaimsRequest = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.data) return false;
    if (!input.data.attributes) return false;
    if (!input.data.attributes.params) return false;
    if (!input.data.attributes.params.orderBy) return false;
    if (!input.data.attributes.params.page) return false;
    //if (!input.data.attributes.params.pageSize) return false;

    return true;
};

// validEnquiry
exports.validEnquiry = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.data) return false;
    if (!input.data.attributes) return false;
    if (!input.data.attributes.enquiry) return false;
    if (!input.data.attributes.enquiry.problemTitle) return false;
    if (!input.data.attributes.enquiry.problemDescription) return false;

    return true;
};

// validActivities
exports.validActivities = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.data) return false;
    if (!input.data.type) return false;
    if (!input.data.attributes) return false;

    return true;
};

// validClaimDocumentRequest 
exports.validClaimDocumentRequest = function (req) {
    var input = req.body.data ? req.body.data : undefined;

    if (input === undefined) return false;

    return true;
};

// validDuplicateClaimCheckRequest 
exports.validDuplicateClaimCheckRequest = function (req) {
    var input = req.body.data ? req.body.data : undefined;

    if (input === undefined) return false;

    if (!input.attributes
        || !input.attributes.worker
        || !input.attributes.worker.name
        || !input.attributes.worker.name.given
        || !input.attributes.worker.name.family
        || !input.attributes.injury
        || !input.attributes.injury.date) return false;

    return true;
};


// getDate
exports.getDate = function (dateString) {
    var date = dateString.split(/\D/);
    return new Date(date[2], date[1] - 1, date[0]);
}

// hasOneDayElapsed 
exports.hasOneDayElapsed = function (previousDate) {
    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    var currentDate = new Date();
    previousDate = new Date(previousDate);
    var diffDays = Math.round(Math.abs((currentDate.getTime() - previousDate.getTime()) / (oneDay)));

    return diffDays >= 1;
}


/*
 * Other functions 
 *
 */

// addClaim 
// object should have been validated prior calling this function 
exports.addClaim = function (req) {
    var input = req.body;

    var claimNo = _.floor(Math.random() * 9000000) + 1000000;
    input.data.attributes.claimNumber = claimNo;
    input.data.attributes.internalClaimId = "1-oPz6EZN9vrdxLXlq8E48YBVqW5AQwbDM";
    input.data.attributes.identityIndex = "874BC2B6884175B958283E5D157E2F";
    input.data.attributes.notifier.notifierId = "1-oPz6EZN9vrdxLXlq8E48YBVqW5AQwbDM";
    input.data.id = claimNo;

    var duplicate1 = {
        "claimNumber": "5511302",
        "lossDate": "03/08/2017",
        "injuryDescription": "Nunc purus. Phasellus in felis. Donec semper sapien a libero.",
        "incidentDescription": "Donec ut dolor.",
        "reporter": "Aura Delhay"
    };

    var duplicate2 = {
        "claimNumber": "1199016",
        "lossDate": "03/12/2017",
        "injuryDescription": "Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum.",
        "incidentDescription": "Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit.",
        "reporter": "Eddie Dwelly"
    };

    var workerGivenName = input.data.attributes.worker.name.given;
    var workerFamilyName = input.data.attributes.worker.name.family;
    var injuryDate = input.data.attributes.injury.occurrence.date;
    var inputDuplicates = input.data.attributes.duplicates ? input.data.attributes.duplicates : [];
    if (workerGivenName === "Hobey" && workerFamilyName === "Templman" && injuryDate === "2018-01-02") {
        if (_.size(inputDuplicates) === 0) {
            input.data.attributes.claimNumber = "";
            input.data.id = "";
            inputDuplicates.push(duplicate1);
            inputDuplicates.push(duplicate2);
            input.data.attributes.duplicates = inputDuplicates;
        } else if (_.size(inputDuplicates) !== 0) {
            var duplicates = inputDuplicates;
            _.forEach(duplicates, function (o) {
                o.claimNumber = claimNo;
            });
            input.data.attributes.duplicates = duplicates;
        }
    }

    // Add the claim 
    state.AddUpdateClaimRepo.push(input);

    //var getClaim = _.find(state.AddUpdateClaimRepo, function (o) {
    //    return _.toString(o.data.attributes.claimNumber) === _.toString(claimNo);
    //});

    var address = input.data.attributes.worker.address;
    _.forEach(address, function (o) {
        var addressId = _.floor(Math.random() * 9000000) + 1000000;
        if (o.type === 'residential') {
            o.addressId = _.toString(addressId);
        }

        if (o.type === 'postal') {
            o.addressId = _.toString(addressId);
        }
    });

    return input;
};

// called by updateClaimById and lodgeClaimById
updateOrLodgeExistingClaimById = function (req) {
    // check if the claimNo exists 
    var existingClaim = _.find(state.AddUpdateClaimRepo, function (o) {
        return _.toString(o.data.attributes.claimNumber) === _.toString(req.params.claimId);
    });

    if (!existingClaim) return false;

    // remove the claim
    state.AddUpdateClaimRepo = _.reject(state.AddUpdateClaimRepo, function (o) {
        return _.toString(o.data.attributes.claimNumber) === _.toString(req.params.claimId);
    });

    // modify input to ensure we retain the claimNo 
    var input = req.body;
    input.data.attributes.claimNumber = _.toString(req.params.claimId);
    input.data.attributes.internalClaimId = _.toString("1-oPz6EZN9vrdxLXlq8E48YBVqW5AQwbDM");
    input.data.attributes.identityIndex = _.toString("874BC2B6884175B958283E5D157E2F");
    input.data.attributes.notifier.notifierId = _.toString("1-oPz6EZN9vrdxLXlq8E48YBVqW5AQwbDM");
    // add the claim from input 
    state.AddUpdateClaimRepo.push(input);

    // look again for the updated claim
    var claimNo = input.data.attributes.claimNumber;
    var getClaim = _.find(state.AddUpdateClaimRepo, function (o) {
        return _.toString(o.data.attributes.claimNumber) === _.toString(claimNo);
    });

    return getClaim;

};

// updateClaimById 
exports.updateClaimById = function (req) {

    var claimNumber = req.params.claimId;

    // check if special claim
    if (claimNumber === "579713") {
        TEMPSTORE.MockClaimDetails579713 = req.body;
    }
    else {
        return updateOrLodgeExistingClaimById(req);
    }

    var responseObject = { description: SUCCESS.Success204 };
    return responseObject;
};

// lodgeClaimById 
exports.lodgeClaimById = function (req) {


    //var claimNumber = req.params.claimId;

    // check if special claim
    //if (claimNumber === "579713") {
    //    TEMPSTORE.MockClaimDetails579713 = req.body;
    //}
    //else {
    //    return updateOrLodgeExistingClaimById(req);
    //}

    var responseObject = {
        "data": {
            "type": "Claim",
            "id": "93oac6984fba1a00b916aa811310c721",
            "attributes": {
                "id": "93oac6984fba1a00b916aa811310c721",
                "claimNumber": "123456789",
                "status": "Open"
            }
        }
    };

    return responseObject;
};

// addAttachmentByClaimId 
exports.addAttachmentByClaimId = function (req) {
    var responseObject = {};
    responseObject.data = [];

    var item1 = {};
    item1.type = "type1";
    item1.id = "id1";
    item1.attributes = {};
    item1.attributes.method = "method1";
    item1.attributes.url = "url1";

    var item2 = {};
    item2.type = "type2";
    item2.id = "id2";
    item2.attributes = {};
    item2.attributes.method = "method2";
    item2.attributes.url = "url2";

    responseObject.data.push(item1);
    responseObject.data.push(item2);

    return responseObject;
};

// getPolicyNumbers
exports.getPolicyNumbers = function (req) {
    var dateQueryParam = req.query.date;

    var responseObject = {};
    responseObject.items = [];

    if (dateQueryParam !== "27/02/2018") {
        var item1 = {};
        item1.policyNumber = "111234";
        item1.businessName = "EFS Enterprise";

        var item2 = {};
        item2.policyNumber = "2020123";
        item2.businessName = "Grant, Trantow and Raynor";

        responseObject.items.push(item1);
        responseObject.items.push(item2);
    }

    return responseObject;
};

// getPoliciesSearch
exports.getPoliciesSearch = function (req) {
    var policySearchParam = req.body.data.attributes.params;

    var responseObject = {
        "data": []
    };

    var policy1 = {
        "type": "Policy",
        "id": "EwlDrvy50W7247dBDkn1OPoNGgKJQLmx",
        "attributes": {
            "policyNumber": "140000714",
            "employerName": "Guidewire",
            "effectiveDate": "2018-03-27T00:00:00.000Z",
            "expirationDate": "2019-03-27T00:00:00.000Z"
        }
    };

    var policy2 = {
        "type": "Policy",
        "id": "awlDrvy50W7247dBDkn1OPoNGgKJQLmxy",
        "attributes": {
            "policyNumber": "140000715",
            "employerName": "Guidewire",
            "effectiveDate": "2018-03-27T00:00:00.000Z",
            "expirationDate": "2019-03-27T00:00:00.000Z"
        }
    };

    var policy3 = {
        "type": "Policy",
        "id": "L1txtOBQg2pjReCXnXk4",
        "attributes": {
            "policyNumber": "140001801",
            "employerName": "Guidewire",
            "effectiveDate": "2018-03-27T00:00:00.000Z",
            "expirationDate": "2019-03-27T00:00:00.000Z"
        }
    };

    /*if (policySearchParam === undefined || policySearchParam === "undefined" || policySearchParam === null) {*/
    responseObject.data.push(policy1);
    responseObject.data.push(policy2);
    responseObject.data.push(policy3);
    /*}*/

    return responseObject;
};

containsObject = function (obj, list) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (list[i] === obj) {
            return true;
        }
    }

    return false;
}

// getCostCentres
exports.getCostCentres = function (req) {
    var policyIdParam = req.params.policyId;
    var policyIds = ["EwlDrvy50W7247dBDkn1OPoNGgKJQLmx", "awlDrvy50W7247dBDkn1OPoNGgKJQLmxy"]

    var locationIdParam = req.params.locationId;
    var locationIds = ["ZwlDrvy50W7247dBDkn1OPoNGgKJQLS", "ZwlDrvy50W7247dBDkn1OPoNGgKJQLmLLL"]

    var responseObject = {
        "data": {
            "type": "PolicyCostCenter",
            "attributes": {
                "costCenters": []
            }
        }
    };

    var costCenter1 = {
        "id": "owlDrvy50W7247dBDkn1OPoNGgKJQLmx",
        "name": "PolicyLocation : 1",
        "fullName": "PolicyLocation : 1"
    };

    var costCenter2 = {
        "id": "lDrvy50W7247dBDkn1OPoNGgKJQLmxyyy",
        "name": "PolicyLocation : 3",
        "fullName": "PolicyLocation : 3"
    };

    if (containsObject(policyIdParam, policyIds) && containsObject(locationIdParam, locationIds)) {
        responseObject.data.attributes.costCenters.push(costCenter1);
        responseObject.data.attributes.costCenters.push(costCenter2);
    }

    return responseObject;
};





// getattachmentLocations
exports.getattachmentLocations = function (req) {

    var responseObject = {
        "data": {
            "type": "StagingLocation",
            "id": "2100010-qzukf-sample.pdf",
            "attributes": {
                "scenario": "nisp21portal",
                "fileName": "sample.pdf",
                "mime": "application/pdf",
                "objectKey": "Test1234",
                "uploadUrl": "http://filestreamtesting.s3.amazonaws.com",
                "downloadUrl": " https://icare-portal-adapter-react.getsandbox.com/s3/upload",
                "deleteUrl": " https://icare-portal-adapter-react.getsandbox.com/s3/upload",
                "metadata": {
                    "claimNumber": "2100010"
                }
            }
        }
    };

    return responseObject;
};

// getLocations
exports.getLocations = function (req) {
    var policyIdParam = req.params.policyId;
    var policyIds = ["EwlDrvy50W7247dBDkn1OPoNGgKJQLmx", "awlDrvy50W7247dBDkn1OPoNGgKJQLmxy"]

    var responseObject = {
        "data": {
            "type": "PolicyLocation",
            "attributes": {
                "locations": []
            }
        }
    };

    var location1 = {
        "isRatedLocation": false,
        "id": "ZwlDrvy50W7247dBDkn1OPoNGgKJQLS",
        "locationRetired": false,
        "location": 1,
        "primaryLocation": false,
        "accountLocation": {
            "nonSpecific": false,
            "address": {
                "id": "pc:1902",
                "displayName": "1: 309 Kent Street, SYDNEY NSW",
                "isAddressVerified": true,
                "addressLine1": "309 Kent Street",
                "city": "SYDNEY",
                "state": "AU_NSW",
                "postalCode": "2000",
                "country": "AU",
                "addressType": "business"
            }
        }
    };

    var location2 = {
        "isRatedLocation": false,
        "id": "ZwlDrvy50W7247dBDkn1OPoNGgKJQLmLLL",
        "locationRetired": false,
        "location": 2,
        "primaryLocation": false,
        "accountLocation": {
            "nonSpecific": false,
            "address": {
                "publicID": "pc:1903",
                "displayName": "2: 310 Kent Street, SYDNEY NSW",
                "isAddressVerified": true,
                "addressLine1": "309 Kent Street",
                "city": "SYDNEY",
                "state": "AU_NSW",
                "postalCode": "2000",
                "country": "AU",
                "addressType": "business"
            }
        }
    };

    if (containsObject(policyIdParam, policyIds)) {
        responseObject.data.attributes.locations.push(location1);
        responseObject.data.attributes.locations.push(location2);
    }

    return responseObject;
};

// getMyDetails
exports.getMyDetails = function (req) {
    var policyUsers = state.MockPolicyUsersEditable;

    // Get user email address from the X-OktaTokenJson 
    var xOktaTokenJson = req.get("X-OktaTokenJson");
    xOktaTokenJson = xOktaTokenJson ? xOktaTokenJson : undefined;
    //if (xOktaTokenJson === undefined) return false;
    if (xOktaTokenJson === undefined) { xOktaTokenJson = "icareemployer3@gmail.com"; }

    // check if the user email address exists 
    var targetUser = _.filter(policyUsers, function (o) {
        return _.toLower(_.toString(o.Email)) === _.toLower(_.toString(xOktaTokenJson));
    });

    if (!targetUser || targetUser.length < 1) return false;

    var responseObject = {
        "data": {
            "type": "User",
            "id": (targetUser.length !== 0) ? targetUser[0].IdentityId : "",
            "attributes": {
                "identityId": (targetUser.length !== 0) ? targetUser[0].IdentityId : "",
                "contactInfo": {
                    "contactId": (targetUser.length !== 0) ? "003N000001BktKs" : "",
                    "firstName": (targetUser.length !== 0) ? targetUser[0].FirstName : "",
                    "lastName": (targetUser.length !== 0) ? targetUser[0].LastName : "",
                    "userType": (targetUser.length !== 0) ? targetUser[0].UserType : "",
                    "email": (targetUser.length !== 0) ? targetUser[0].Email : "",
                    "phone": {
                        "contactNumber": (targetUser.length !== 0) ? targetUser[0].PhoneNumber : "",
                        "phoneType": (targetUser.length !== 0) ? targetUser[0].PhoneType : ""
                    }
                },
                "authorisations": []
            }
        }
    };

    var authorisation = {
        "type": "PolicyAuthorisation",
        "assertions": []
    };

    var assertions = [];
    _.forEach(targetUser, function (o) {
        var assertion = {
            "policyNumber": o.policyNumber,
            "employerName": "Bunnings",
            "authorisationRowId": "124",
            "effectiveDate": "YYYY-MM-DDTHH:MM:SSZ",
            "expirationDate": "YYYY-MM-DDTHH:MM:SSZ",
            "role": _.toLower(o.DetailedAuthDbRole),
            "scope": [
                "ALL"
            ]
        };

        assertions.push(assertion);
    });

    authorisation.assertions = assertions;
    responseObject.data.attributes.authorisations.push(authorisation);

    return responseObject;
};

// updateMyDetails
exports.updateMyDetails = function (req) {
    var policyUsers = state.MockPolicyUsersEditable;

    // Get user email address from the X-OktaTokenJson 
    var xOktaTokenJson = req.get("X-OktaTokenJson");
    xOktaTokenJson = xOktaTokenJson ? xOktaTokenJson : undefined;
    if (xOktaTokenJson === undefined) return false;

    // check if the user email address exists 
    var targetUser = _.filter(policyUsers, function (o) {
        return _.toLower(_.toString(o.Email)) === _.toLower(_.toString(xOktaTokenJson));
    });

    if (!targetUser || targetUser.length < 1) return false;

    var responseObject = {
        "data": {
            "type": "Contact",
            "id": "C029341234",
            "attributes": {
                "version": 1,
                "sourceSystem": "GWCC"
            },
            "links": {
                "self": "https://st.api-nonprod.icare.nsw.gov.au/v1/customer/contacts/C0001"
            }
        }
    }

    return responseObject;
};

// getPaymentDetails
exports.getPaymentDetails = function (req) {
    var mockBsbBanks = TEMPSTORE.MockBsbBanks;
    var mockPaymentDetails = TEMPSTORE.MockPaymentDetails;

    var xToken = req.get("X-OktaTokenJson");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var targetPaymentDetails = _.findLast(mockPaymentDetails, function (o) {
        return _.toLower(_.toString(o.email)) === _.toLower(_.toString(xToken));
    });

    var bsbPayment = targetPaymentDetails !== undefined ? targetPaymentDetails.bsb : "";
    var accountNumPayment = targetPaymentDetails !== undefined ? targetPaymentDetails.accountNumber : "";

    var bsbBank = _.find(mockBsbBanks, function (v) {
        return (v.BSB === bsbPayment && v.AccountNumber === accountNumPayment);
    });

    var responseObject = {
        "data": {
            "type": "PaymentPreference",
            "attributes": {
                "accountName": "MBA Checking",
                "accountNumber": "12356789",
                "bsb": "111555",
                "bankName": "WBC",
                "bankBranchName": "Royal Exchange"
            }
        }
    };

    return responseObject;
};

// updatePaymentDetails
exports.updatePaymentDetails = function (req) {

    var responseObject = {
        "data": {
            "type": "PaymentPreference",
            "attributes": {
                "status": "completed"
            }
        }
    };

    return responseObject;

};

// getFinancialInstitutions 
exports.getFinancialInstitutions = function (req) {
    var input = _.toString(req.query.BSB);

    var responseObject = {
        "data": {
            "type": "FinancialInstitution",
            "id": "",
            "attributes": {
                "BSB": "",
                "bankCode": "",
                "name": "",
                "facility": "",
                "address": {}
            }
        }
    };

    responseObject.data.id = "062-666"
    responseObject.data.attributes.BSB = "062-666"
    responseObject.data.attributes.bankCode = "CBA";
    responseObject.data.attributes.name = "Commonwealth Bank of Australia";
    responseObject.data.attributes.facility = "Lake Haven";

    responseObject.data.attributes.address.addressLine1 = "Shop 72, Lake Haven Shopping Centre";
    responseObject.data.attributes.address.suburbOrPlaceOrLocality = "Lake Haven";
    responseObject.data.attributes.address.stateOrTerritoryCode = "NSW";
    responseObject.data.attributes.address.postCode = "2263";

    return responseObject;
};

// addUser_old
exports.addUser_old = function (req) {
    var input = req.body;
    var policyUsers = state.MockPolicyUsersEditable;

    // Get user email address from the X-OktaTokenJson 
    var xOktaTokenJson = req.get("X-OktaTokenJson");
    xOktaTokenJson = xOktaTokenJson ? xOktaTokenJson : undefined;
    if (xOktaTokenJson === undefined) return false;

    var userPoliciesArr = [];

    // Get the policies associated with the current user (MockPolicyUsers) 
    var userPoliciesFromMockPolicyUsers = _.filter(policyUsers, function (o) {
        var isValid = _.toLower(o.Email) === _.toLower(xOktaTokenJson);
        if (isValid) userPoliciesArr.push(o.policyNumber);

        return isValid;
    });

    //Get policy roles from body inputs
    var inputPoliciesArr = [];
    _.forEach(input.policyRoles, function (o) {
        inputPoliciesArr.push(o.policyNumber);
    });

    //Get data from state.MockPolicyUsers based on okta token email
    var userPolicy = _.find(policyUsers, function (o) {
        return _.toLower(o.Email) === _.toLower(xOktaTokenJson);
    });

    //Get data from state.MockPolicyUsers based on the body inputs
    var inputUserPolicy = _.find(policyUsers, function (o) {
        return _.toLower(o.Email) === _.toLower(input.emailAddress);
    });

    var emptyObject = [];
    if (inputUserPolicy !== undefined) {
        if (userPolicy.OktaRole !== inputUserPolicy.OktaRole) return emptyObject;
    }

    // Get all users from MockPolicyUsers that has access to the same policies 
    var fromMockPolicyUsers = _.filter(policyUsers, function (o) {
        return _.indexOf(userPoliciesArr, o.policyNumber) > -1;
    });

    var existPolicyUser = _.find(fromMockPolicyUsers, function (o) {
        return _.toLower(o.Email) === _.toLower(input.emailAddress) && _.indexOf(inputPoliciesArr, o.policyNumber) > -1;
    });

    if (existPolicyUser) return emptyObject;

    // Generate a 7-digit random identityId 
    var identityId = _.floor(Math.random() * 9000000) + 1000000;
    input.identityId = _.toString(identityId);

    // Add the user 
    var addedEntries = [];
    _.forEach(input.policyRoles, function (o) {
        var newUser = {
            "IsOktaUser": "",
            "IdentityId": input.identityId,
            "Email": input.emailAddress,
            "FirstName": input.givenName,
            "LastName": input.familyName,
            "OktaRole": "",
            "PhoneNumber": input.contactNumber,
            "DetailedAuthDbRole": o.role,
            "policyNumber": o.policyNumber,
            "associatedClaims": "",
            "numOpenNotificationsClaims": 0
        };

        state.MockPolicyUsersEditable.push(newUser);
        addedEntries.push(newUser);
    });

    var responseObject = addedEntries;

    return responseObject;
};

/**
   * Creates a string that can be used for dynamic id attributes
   * Example: "id-so7567s1pcpojemi"
   * @returns {string}
   */
createUniqueId = function () {
    return Math.random().toString(36).substr(2, 16);
};

// addUser
exports.addUser = function (req) {
    var input = req.body;
    var policyUsers = state.MockPolicyUsersEditable;

    // Get user email address from the X-OktaTokenJson 
    var xOktaTokenJson = req.get("X-OktaTokenJson");
    xOktaTokenJson = xOktaTokenJson ? xOktaTokenJson : undefined;
    if (xOktaTokenJson === undefined) return false;

    //Get data from state.MockPolicyUsers based on okta token email
    var userPolicy = _.find(policyUsers, function (o) {
        return _.toLower(o.Email) === _.toLower(xOktaTokenJson);
    });

    /*var authorisation = input.data.attributes[0];
    var policy1 = input.data.attributes[1];
    var policy2 = input.data.attributes[2];*/

    var responseObject = {
        "data": {
            "type": "PolicyAuthorisationBatch",
            "attributes": []
        }
    };

    var i = 0;
    _.forEach(input.data.attributes, function (o) {

        if (_.parseInt(o.authorisationRowId) > 0) {

        }
        else (o.policyNumber !== "")
        {
            var authID = _.toString(_.parseInt(input.data.attributes.length) + i + 1);
            o.authorisationRowId = authID;
        }
        i = i + 1;
        responseObject.data.attributes.push(o);

    });


    /*responseObject.data.attributes.push(authorisation);

    var policy1AuthRowId = _.toString(_.parseInt(authorisation.authorisationRowId) + 1);
    policy1.authorisationRowId = policy1AuthRowId;
    responseObject.data.attributes.push(policy1);

    var policy2AuthRowId = _.toString(_.parseInt(authorisation.authorisationRowId) + 2);
    policy2.authorisationRowId = policy2AuthRowId;
    responseObject.data.attributes.push(policy2);*/

    return responseObject;
};

// getUser 
exports.getUser = function (req) {
    var identityIdParam = req.params.identityId;
    // check if the identityId exists 
    var existingUser = _.filter(state.MockPolicyUsersEditable, function (o) {
        return _.toString(o.IdentityId) === _.toString(identityIdParam);
    });

    var responseObject = {
        "data": {
            "type": "User",
            "id": (existingUser.length !== 0) ? existingUser[0].IdentityId : "",
            "attributes": {
                "identityId": (existingUser.length !== 0) ? existingUser[0].IdentityId : "",
                "contactInfo": {
                    "contactId": (existingUser.length !== 0) ? "003N000001BktKs" : "",
                    "firstName": (existingUser.length !== 0) ? existingUser[0].FirstName : "",
                    "lastName": (existingUser.length !== 0) ? existingUser[0].LastName : "",
                    "email": (existingUser.length !== 0) ? existingUser[0].Email : "",
                    "phone": {
                        "contactNumber": (existingUser.length !== 0) ? existingUser[0].PhoneNumber : "",
                        "phoneType": (existingUser.length !== 0) ? existingUser[0].PhoneType : ""
                    }
                },
                "authorisations": []
            }
        }
    };

    var authorisation = {
        "type": "PolicyAuthorisation",
        "assertions": []
    };

    var assertions = [];
    _.forEach(existingUser, function (o) {
        var assertion = {
            "policyNumber": o.policyNumber,
            "employerName": "Bunnings",
            "authorisationRowId": "124",
            "effectiveDate": "YYYY-MM-DDTHH:MM:SSZ",
            "expirationDate": "YYYY-MM-DDTHH:MM:SSZ",
            "role": _.toLower(o.DetailedAuthDbRole),
            "scope": [
                "ALL"
            ]
        };

        assertions.push(assertion);
    });

    authorisation.assertions = assertions;
    responseObject.data.attributes.authorisations.push(authorisation);

    return responseObject;
};

// updateUser
exports.updateUser = function (req) {
    // check if the identityId exists 
    var existingUser = _.filter(state.MockPolicyUsersEditable, function (o) {
        return _.toString(o.IdentityId) === _.toString(req.params.identityId);
    });

    //if (!existingUser || existingUser.length == 0) return false;

    var responseObject = { description: SUCCESS.Success202c };
    return responseObject;
};

// deleteUser 
exports.deleteUser = function (req) {
    // do not delete if MockPolicyUsersEditable doesn't exist 
    if (!state.MockPolicyUsersEditable) return true;

    // check if the identityId exists 
    var existingUser = _.filter(state.MockPolicyUsersEditable, function (o) {
        return _.toString(o.IdentityId) === _.toString(req.params.identityId);
    });

    if (!existingUser || existingUser.length == 0) return false;

    // remove the existing user 
    state.MockPolicyUsersEditable = _.reject(state.MockPolicyUsersEditable, function (o) {
        return _.toString(o.IdentityId) === _.toString(req.params.identityId);
    });

    return true;
};


// getTeam 
exports.getTeam = function (req) {
    var stateUsers = state.users;                       // these are users added through the /users/user (Add a new user) endpoint 
    var policyUsers = state.MockPolicyUsersEditable;

    // Get user email address from the X-OktaTokenJson 
    var xOktaTokenJson = req.get("X-OktaTokenJson");
    xOktaTokenJson = xOktaTokenJson ? xOktaTokenJson : undefined;
    if (xOktaTokenJson === undefined) return false;

    var userPoliciesArr = [];

    // Get the policies associated with the current user (MockPolicyUsers) 
    var userPoliciesFromMockPolicyUsers = _.filter(policyUsers, function (o) {
        var isValid = _.toLower(o.Email) === _.toLower(xOktaTokenJson);
        if (isValid) userPoliciesArr.push(o.policyNumber);

        return isValid;
    });

    var userPolicy = _.find(policyUsers, function (o) {
        return _.toLower(o.Email) === _.toLower(xOktaTokenJson);
    });

    // Get all users from MockPolicyUsers that has access to the same policies 
    var fromMockPolicyUsers = _.filter(policyUsers, function (o) {
        return _.indexOf(userPoliciesArr, o.policyNumber) > -1;
    });


    var responseObject =
    {
        "meta": {
            "totalRecords": 0
        },
        "data": []
    };

    responseObject.meta.totalRecords = fromMockPolicyUsers.length;

    _.forEach(fromMockPolicyUsers, function (o) {
        var fromMockPolicyUser = _.find(policyUsers, function (p) {
            return _.toLower(p.Email) === _.toLower(o.Email);
        });

        if (userPolicy.OktaRole === fromMockPolicyUser.OktaRole) {
            var dataToPush =
            {
                "type": "User",
                "id": fromMockPolicyUser.IdentityId,
                "attributes": {
                    "identityId": fromMockPolicyUser.IdentityId,
                    "contactInfo": {
                        "contactId": "003N000001BktKs",
                        "firstName": fromMockPolicyUser.FirstName,
                        "lastName": fromMockPolicyUser.LastName,
                        "email": fromMockPolicyUser.Email,
                        "userType": fromMockPolicyUser.UserType,
                        "phone": {
                            "contactNumber": '' + fromMockPolicyUser.PhoneNumber,
                            "phoneType": fromMockPolicyUser.PhoneType
                        }
                    },
                    "authorisations": []
                }
            };

            var authorisation = {
                "type": "PolicyAuthorisation",
                "assertions": []
            };

            var assertions = [];
            _.forEach(fromMockPolicyUsers.slice(0, 5), function (o) {

                var assertion = {
                    "policyNumber": o.policyNumber,
                    "employerName": "Bunnings",
                    "effectiveDate": "YYYY-MM-DDTHH:MM:SSZ",
                    "expirationDate": "YYYY-MM-DDTHH:MM:SSZ",
                    "role": _.toLower(o.DetailedAuthDbRole),
                    "scope": ["ALL"]
                };

                var assertionSub = {
                    "policyNumber": o.policyNumber,
                    "role": _.toLower(o.DetailedAuthDbRole),
                    "scope": ["ALL"]
                };

                assertions.push(assertion);
                assertions.push(assertionSub);
            });
            authorisation.assertions = assertions;
            dataToPush.attributes.authorisations.push(authorisation);
            responseObject.data.push(dataToPush);
        }
    });

    return responseObject;
};

// getTeam_old
exports.getTeam_old = function (req) {
    var stateUsers = state.users;                       // these are users added through the /users/user (Add a new user) endpoint 
    var policyUsers = state.MockPolicyUsersEditable;

    // Get user email address from the X-OktaTokenJson 
    var xOktaTokenJson = req.get("X-OktaTokenJson");
    xOktaTokenJson = xOktaTokenJson ? xOktaTokenJson : undefined;
    if (xOktaTokenJson === undefined) return false;

    var userPoliciesArr = [];

    // Get the policies associated with the current user (MockPolicyUsers) 
    var userPoliciesFromMockPolicyUsers = _.filter(policyUsers, function (o) {
        var isValid = _.toLower(o.Email) === _.toLower(xOktaTokenJson);
        if (isValid) userPoliciesArr.push(o.policyNumber);

        return isValid;
    });

    var userPolicy = _.find(policyUsers, function (o) {
        return _.toLower(o.Email) === _.toLower(xOktaTokenJson);
    });

    // Get all users from MockPolicyUsers that has access to the same policies 
    var fromMockPolicyUsers = _.filter(policyUsers, function (o) {
        return _.indexOf(userPoliciesArr, o.policyNumber) > -1;
    });

    // All users 
    var allUsers = [];

    _.forEach(fromMockPolicyUsers, function (o) {
        var fromMockPolicyUser = _.find(policyUsers, function (p) {
            return _.toLower(p.Email) === _.toLower(o.Email);
        });

        if (userPolicy.OktaRole === fromMockPolicyUser.OktaRole) {
            var user = {
                "phone": {}
            };
            user.given = o.FirstName;
            user.family = o.LastName;
            user.phone.contactNumber = o.PhoneNumber;
            user.phone.phoneType = o.PhoneType;
            user.emailAddress = o.Email;
            user.role = o.DetailedAuthDbRole;
            user.identityId = o.IdentityId;
            allUsers.push(user);
        }
    });

    // Sorted 
    var sortFilter = req.body.sort.columnName;
    var sortDirectionFilter = req.body.sort.direction;
    var sortedUsers = _.sortBy(allUsers, [function (o) { return o[sortFilter]; }]);         // ascending 
    if (sortDirectionFilter !== "asc") sortedUsers = _.reverse(sortedUsers);                // descending 

    // Paginated 
    var paginatedUsers = _.chunk(sortedUsers, req.body.pageSize);
    var targetPage = paginatedUsers[req.body.page - 1];

    // Response object 
    var responseObject = {};
    responseObject.totalResults = sortedUsers.length;
    responseObject.totalPages = paginatedUsers.length;
    responseObject.currentPage = req.body.page;
    responseObject.items = targetPage;

    return responseObject;
};

// getUserSummary
exports.getUserSummary = function (req) {
    var policyUsers = TEMPSTORE.MockPolicyUsers;

    // Get target userId from the X-Token 
    var xToken = req.get("X-Token");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    // check if the userId exists 
    var targetUser = _.find(policyUsers, function (o) {
        return _.toLower(_.toString(o.Email)) === _.toLower(_.toString(xToken));
    });

    if (!targetUser) return false;

    // Create UserSummary response object 
    var userSummary = {};
    userSummary.givenName = targetUser.FirstName;
    userSummary.familyName = targetUser.LastName;
    userSummary.userId = targetUser.Email;
    userSummary.numOpenNotificationsClaims = targetUser.numOpenNotificationsClaims;


    return userSummary;
}

// getPortalClaims 
exports.getPortalClaims = function (req) {
    var mockClaimSummary = TEMPSTORE.MockClaimSummary;
    var mockClaimDetails = TEMPSTORE.MockClaimDetails;
    var mockPolicyUsers = TEMPSTORE.MockPolicyUsers;

    // Attach ClaimDetails to its corresponding ClaimSummary 
    var mockClaimSummaryAndDetails = [];
    _.forEach(mockClaimSummary, function (mcs, index) {
        var combined = mcs;
        combined.data = mockClaimDetails[index].data;
        mockClaimSummaryAndDetails.push(combined);
    });

    // Get target userId from the X-Token 
    var xToken = req.get("X-Token");
    xToken = xToken ? xToken : undefined;
    //if (xToken === undefined) return false;
    if (xToken === undefined) { xToken = "icareemployer3@gmail.com"; }

    var userPoliciesArr = [];
    var associatedClaimsPerPolicy = {};

    // Get the policies associated with the current user 
    var userPolicies = _.filter(mockPolicyUsers, function (o) {
        var isValid = _.toLower(o.Email) === _.toLower(xToken);
        if (isValid) {
            userPoliciesArr.push(o.policyNumber);
            associatedClaimsPerPolicy[o.policyNumber] = _.split(o.associatedClaims, ",");
            _.forEach(associatedClaimsPerPolicy[o.policyNumber], function (v, i) {
                associatedClaimsPerPolicy[o.policyNumber][i] = _.trim(v);
            });

        }
        return isValid;
    });

    // Claims list per user 
    var targetMockClaimSummaryAndDetails = [];
    _.forEach(userPoliciesArr, function (o, i) {
        var targetClaims = [];

        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] === "*") {
            // mockClaimSummary
            targetClaims = _.filter(mockClaimSummaryAndDetails, function (v) {
                return o === v.policyNumber;
            });
            targetMockClaimSummaryAndDetails = _.concat(targetMockClaimSummaryAndDetails, targetClaims);
        }

        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] !== "*" && associatedClaimsPerPolicy[o][0] !== "null") {
            // mockClaimSummary
            targetClaims = _.filter(mockClaimSummaryAndDetails, function (v) {
                return _.indexOf(associatedClaimsPerPolicy[o], v.claimNumber) > -1;
            });
            targetMockClaimSummaryAndDetails = _.concat(targetMockClaimSummaryAndDetails, targetClaims);
        }
    });

    var filteredClaims = targetMockClaimSummaryAndDetails;

    // Filter: Claim List
    if (req.body.data.attributes.criteria) {
        var policyNumberFilter = req.body.data.attributes.criteria.policyNumber ? req.body.data.attributes.criteria.policyNumber : undefined;
        var dateOfInjuryRangeDaysFilter = req.body.data.attributes.criteria.dateOfInjuryRangeDaysFilter ? req.body.data.attributes.criteria.dateOfInjuryRangeDaysFilter : undefined;
        var returnToWorkEstimation = req.body.data.attributes.criteria.returnToWorkEstimation ? req.body.data.attributes.criteria.returnToWorkEstimation : undefined;
        var claimStateFilter = req.body.data.attributes.criteria.claimState ? req.body.data.attributes.criteria.claimState : undefined;
        var claimStateFilterLowerCased = _.map(claimStateFilter, function (o) { return _.toLower(o); });
        var injuredFirstNameFilter = req.body.data.attributes.criteria.injuredFirstName ? req.body.data.attributes.criteria.injuredFirstName : undefined;
        var injuredLastNameFilter = req.body.data.attributes.criteria.injuredLastName ? req.body.data.attributes.criteria.injuredLastName : undefined;
        var claimNumberFilter = req.body.data.attributes.criteria.claimNumber ? req.body.data.attributes.criteria.claimNumber : undefined;

        filteredClaims = _.filter(filteredClaims, function (o) {
            var returnValue = true;
            // policy 
            if (policyNumberFilter !== undefined
                && policyNumberFilter !== "undefined"
                && o.policyNumber.indexOf(policyNumberFilter) === -1) returnValue = false;

            // claimStatus: ["open", "saved", "closed"] 
            if (claimStateFilter !== undefined
                && claimStateFilter !== "undefined"
                && claimStateFilter.length > 0
                && _.indexOf(claimStateFilterLowerCased, _.toLower(o.claimState)) === -1) returnValue = false;

            // injuredFirstNameFilter 
            if (injuredFirstNameFilter !== undefined
                && injuredFirstNameFilter !== "undefined"
                && _.toLower(o.injuredFirstName).indexOf(_.toLower(injuredFirstNameFilter)) === -1) returnValue = false;

            // injuredLastNameFilter 
            if (injuredLastNameFilter !== undefined
                && injuredLastNameFilter !== "undefined"
                && _.toLower(o.injuredLastName).indexOf(_.toLower(injuredLastNameFilter)) === -1) returnValue = false;

            // claimNumberFilter 
            if (claimNumberFilter !== undefined
                && claimNumberFilter !== "undefined"
                && o.claimNumber.indexOf(claimNumberFilter) === -1) returnValue = false;

            return returnValue;
        });
    }

    // Remove data after filtering as it will make the response object too big 
    var filteredClaimsWithoutDetails = [];
    _.forEach(filteredClaims, function (o) {
        var claim = o;
        claim.data = null;
        filteredClaimsWithoutDetails.push(claim);
    });
    filteredClaims = filteredClaimsWithoutDetails;

    if (filteredClaims.length === 0) {
        var responseObjectEmpty = {
            "meta": {
                "totalRecords": 0,
                "totalPages": 0,
                "currentPage": req.body.data.attributes.params.page
            },
            "data": []
        };

        return responseObjectEmpty;
    }

    // Sort 
    var sortFilter = req.body.data.attributes.params.orderBy[0].fieldName;
    var sortDirectionFilter = req.body.data.attributes.params.orderBy[0].direction;
    var sortedClaims = _.sortBy(filteredClaims, [function (o) {
        return helpers.getDate(o.dateOfInjury);
    }]);

    if (sortFilter !== "dateOfInjury") {
        sortedClaims = _.sortBy(filteredClaims, [function (o) { return o[sortFilter]; }]);
    }

    if (sortDirectionFilter !== "ascending") sortedClaims = _.reverse(sortedClaims); // descending 

    // Paginate 
    var pageSize = req.body.data.attributes.params.pageSize;
    var paginatedClaims = _.chunk(sortedClaims, 1); // pageSize === 0
    if (pageSize !== 0) {
        paginatedClaims = _.chunk(sortedClaims, pageSize);
    }
    var targetPage = paginatedClaims[req.body.data.attributes.params.page - 1];

    if (targetPage === undefined) return false;
    // Response object 
    var responseObject = {
        "meta": {
            "totalRecords": sortedClaims.length,
            "totalPages": paginatedClaims.length,
            "currentPage": req.body.data.attributes.params.page
        },
        "data": []
    };

    //id to match policy policy number
    if (pageSize !== 0) {
        _.forEach(targetPage, function (o) {
            var claim = {
                "type": req.body.data.type,
                "id": o.claimNumber,
                "attributes": {
                    "claimNumber": o.claimNumber,
                    "publicId": o.publicId,
                    "policyNumber": o.policyNumber,
                    "insuredContact": {
                        "displayName": o.insuredContact.displayName
                    },
                    "claimState": o.claimState,
                    "dateOfInjury": o.dateOfInjury,
                    "liabilityStatus": o.liabilityStatus,
                    "returnToWorkTimeframe": o.returnToWorkTimeframe,
                    "injuredFirstName": o.injuredFirstName,
                    "injuredLastName": o.injuredLastName
                }
            };

            responseObject.data.push(claim);
        });
    }

    return responseObject;
};


// getClaimByClaimId 
exports.getClaimByClaimId = function (req) {
    var claimNumber = req.params.claimId;
    var output = "-";

    if (claimNumber === "579713") {
        //Get claimDetail of "579713"
        output = TEMPSTORE.MockClaimDetails579713Editable;
    }
    else {
        // Get claimDetail from TEMPSTORE.MockClaimDetails given the claim number 
        var targetClaimDetails = _.find(TEMPSTORE.MockClaimDetails, function (o) {
            return o.data.attributes.claimNumber === claimNumber;
        });
        if (!targetClaimDetails) return false;

        /* Generate output JSON */
        output = targetClaimDetails;

        // Attach claimSummry data 
        var targetClaimSummary = _.find(TEMPSTORE.MockClaimSummary, function (o) {
            return o.claimNumber === claimNumber;
        });
        if (!targetClaimSummary) return false;
        output.data.attributes.claimStatus = targetClaimSummary.claimStatus;
        output.data.attributes.liabilityStatus = targetClaimSummary.liabilityStatus;
        output.data.attributes.certificateCapacityExpires = targetClaimSummary.certCapacityExpires;
        output.data.attributes.expectedTimeOffWork = targetClaimSummary.returnToWorkTimeframe;
        output.data.attributes.triageSegment = targetClaimSummary.triageSegment;
        output.data.attributes.wasIncident = targetClaimSummary.wasIncident;
        output.data.attributes.wasMedical = targetClaimSummary.wasMedical;
        output.data.attributes.wasWage = targetClaimSummary.wasWage;

        // Attach claimContacts data 
        var targetClaimContactIds = targetClaimSummary.relatedContacts;
        if (!targetClaimContactIds || targetClaimContactIds.split(",").length < 1) targetClaimDetails.data.attributes.claimContacts = null;
        targetClaimContactIds = targetClaimContactIds.split(",");
        var targetClaimContactArray = [];
        _.forEach(targetClaimContactIds, function (value) {
            var detail = _.find(TEMPSTORE.MockClaimContacts, function (o) {
                return o.contactId === value;
            });

            if (detail) targetClaimContactArray.push(detail);
        });
        output.data.attributes.claimContacts = targetClaimContactArray;
    }

    return output;
};

// getClaimByClaimNo
getClaimByClaimNo = function (claimNumber) {
    var output = "-";

    if (claimNumber === "579713") {
        //Get claimDetail of "579713"
        output = TEMPSTORE.MockClaimDetails579713Editable;
    }
    else {
        // Get claimDetail from TEMPSTORE.MockClaimDetails given the claim number 
        var targetClaimDetails = _.find(TEMPSTORE.MockClaimDetails, function (o) {
            return o.data.attributes.claimNumber === claimNumber;
        });
        if (!targetClaimDetails) return false;

        /* Generate output JSON */
        output = targetClaimDetails;
    }

    return output;
};

// enquiryAgainstClaimByClaimId 
exports.enquiryAgainstClaimByClaimId = function (req) {
    // check if the claimNo exists 
    var existingClaim = _.find(TEMPSTORE.AddUpdateClaimRepo, function (o) {
        return _.toString(o.data.attributes.claimNumber) === _.toString(req.params.id);
    });

    if (!existingClaim) {
        existingClaim = _.find(TEMPSTORE.MockClaimDetails, function (o) {
            return _.toString(o.data.attributes.claimNumber) === _.toString(req.params.id);
        });
    }

    if (!existingClaim) return false;

    // modify input to add an enquiry ID (Enquiry-{claimNo})
    var input = req.body;
    input.data.attributes.enquiry.problemCode = "Enquiry-" + existingClaim.data.attributes.claimNumber;

    // add enquiry to repo 
    state.EnquiryAgainstClaimRepo.push(input);

    var responseObject = { description: SUCCESS.Success202b };
    return responseObject;
};

// activitiesByClaimId 
exports.activitiesByClaimId = function (req) {
    // check if the claimNo exists 
    var input = req.body.data;
    var existingClaim = _.find(TEMPSTORE.AddUpdateClaimRepo, function (o) {
        return _.toString(o.data.attributes.claimNumber) === _.toString(req.params.claimId);
    });

    if (!existingClaim) {
        existingClaim = _.find(TEMPSTORE.MockClaimDetails, function (o) {
            return _.toString(o.data.attributes.claimNumber) === _.toString(req.params.claimId);
        });
    }

    if (!existingClaim) return false;

    var typeList = ["Portal- Enquiry & Update", "Portal- Weekly Earnings Update",
                    "Portal- Manage RTW Plan", "Portal- New Medical reimbursement",
                    "Portal- New Document", "Portal- Lodgement callback request"];
    var inputTypeValue = input.type;
    if (!containsObject(inputTypeValue, typeList)) {
        return false;
    }

    var responseObject = { description: SUCCESS.Success202c };
    return responseObject;
};

// getClaimsDocuments 
exports.getClaimsDocuments = function (req) {
    var claimDocuments = TEMPSTORE.MockDocuments;
    var policyUsers = TEMPSTORE.MockPolicyUsers;

    // Get target userId from the X-Token 
    var xToken = req.get("X-Token");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var userPoliciesArr = [];
    var associatedClaimsPerPolicy = {};

    // Get the policies associated with the current user 
    var userPolicies = _.filter(policyUsers, function (o) {
        var isValid = _.toLower(o.Email) === _.toLower(xToken);
        if (isValid) {
            userPoliciesArr.push(o.policyNumber);
            associatedClaimsPerPolicy[o.policyNumber] = _.split(o.associatedClaims, ",");
            _.forEach(associatedClaimsPerPolicy[o.policyNumber], function (v, i) {
                associatedClaimsPerPolicy[o.policyNumber][i] = _.trim(v);
            });

        }
        return isValid;
    });

    // Claims list per user 
    var claimDocumentsList = [];
    _.forEach(userPoliciesArr, function (o, i) {
        var targetDocuments = [];
        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] === "*") {
            targetDocuments = _.filter(claimDocuments, function (v) {
                return v.claimNumber === req.params.id;
            });

            claimDocumentsList = _.concat(claimDocumentsList, targetDocuments);
        }

        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] !== "*" && associatedClaimsPerPolicy[o][0] !== "null") {
            targetDocuments = _.filter(claimDocuments, function (v) {
                return _.indexOf(associatedClaimsPerPolicy[o], v.claimNumber) > -1;
            });

            claimDocumentsList = _.concat(claimDocumentsList, targetDocuments);
        }
    });

    if (_.size(claimDocumentsList) === 0) return false;

    var responseObject = {
        "data": {
            "type": "documents",
            "id": req.params.id,
            "attributes": {
                "occurrence": {
                    "certificateOfCapacity": [],
                    "medicalPayments": [],
                    "weeklyPayments": [],
                    "healthAndRecovery": [],
                    "other": []
                }
            }
        }
    };

    _.forEach(claimDocumentsList, function (document) {
        var docToPush = {
            "documentId": document.documentId,
            "url": document.url,
            "title": document.title,
            "fileSize": document.fileSize,
            "fileType": document.fileType,
            "uploadedDate": document.uploadedDate
        };

        switch (document.documentType) {
            case 'certCapacity':
                docToPush.certificateOfCapacityDate = document.certCapacityDate;
                responseObject.data.attributes.occurrence.certificateOfCapacity.push(docToPush);
                break;
            case 'medicalPayment':
                responseObject.data.attributes.occurrence.medicalPayments.push(docToPush);
                break;
            case 'weeklyPayment':
                responseObject.data.attributes.occurrence.weeklyPayments.push(docToPush);
                break;
            case 'healthRecovery':
                responseObject.data.attributes.occurrence.healthAndRecovery.push(docToPush);
                break;
            case 'other':
                responseObject.data.attributes.occurrence.other.push(docToPush);
                break;
        }
    });

    // Remove fields with empty values 
    if (_.size(responseObject.data.attributes.occurrence.certificateOfCapacity) === 0)
        delete responseObject.data.attributes.occurrence.certificateOfCapacity;
    if (_.size(responseObject.data.attributes.occurrence.medicalPayments) === 0)
        delete responseObject.data.attributes.occurrence.medicalPayments;
    if (_.size(responseObject.data.attributes.occurrence.weeklyPayments) === 0)
        delete responseObject.data.attributes.occurrence.weeklyPayments;
    if (_.size(responseObject.data.attributes.occurrence.healthAndRecovery) === 0)
        delete responseObject.data.attributes.occurrence.healthAndRecovery;
    if (_.size(responseObject.data.attributes.occurrence.other) === 0)
        delete responseObject.data.attributes.occurrence.other;

    return responseObject;
};

// postClaimsDocuments 
exports.postClaimsDocuments = function (req) {
    var claimDocuments = TEMPSTORE.MockDocuments;
    var policyUsers = TEMPSTORE.MockPolicyUsers;
    var input = req.body.data;
    var claimId = req.params.claimId;

    // Get target userId from the X-Token 
    var xToken = req.get("X-Token");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var userPoliciesArr = [];
    var associatedClaimsPerPolicy = {};

    // Get the policies associated with the current user 
    var userPolicies = _.filter(policyUsers, function (o) {
        var isValid = _.toLower(o.Email) === _.toLower(xToken);
        if (isValid) {
            userPoliciesArr.push(o.policyNumber);
            associatedClaimsPerPolicy[o.policyNumber] = _.split(o.associatedClaims, ",");
            _.forEach(associatedClaimsPerPolicy[o.policyNumber], function (v, i) {
                associatedClaimsPerPolicy[o.policyNumber][i] = _.trim(v);
            });

        }
        return isValid;
    });

    // Claims list per user 
    var claimDocumentsList = [];
    _.forEach(userPoliciesArr, function (o, i) {
        var targetDocuments = [];
        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] === "*") {
            targetDocuments = _.filter(claimDocuments, function (v) {
                return v.claimNumber === claimId;
            });

            if (_.size(targetDocuments) === 0) {
                targetDocuments = _.filter(claimDocuments, function (v) {
                    return v.claimNumber === "*";
                });
            }

            claimDocumentsList = _.concat(claimDocumentsList, targetDocuments);
        }

        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] !== "*" && associatedClaimsPerPolicy[o][0] !== "null") {
            targetDocuments = _.filter(claimDocuments, function (v) {
                return _.indexOf(associatedClaimsPerPolicy[o], v.claimNumber) > -1;
            });

            if (_.size(targetDocuments) === 0) {
                targetDocuments = _.filter(claimDocuments, function (v) {
                    return v.claimNumber === "*";
                });
            }

            claimDocumentsList = _.concat(claimDocumentsList, targetDocuments);
        }
    });

    //filter by Category?
    var categorySearch = undefined;
    if ((input.attributes.criteria !== undefined) && (input.attributes.criteria.document !== undefined) && (input.attributes.criteria.document.category !== undefined))
        categorySearch = input.attributes.criteria.document.category[0] ? input.attributes.criteria.document.category[0] : undefined;

    var targetDocuments = [];
    _.forEach(claimDocumentsList, function (document) {
        var docToPush = {
            "type": "ClaimDocument",
            "id": document.documentId,
            "attributes": {
                "claimNumber": claimId,
                "title": document.title,
                "fileType": document.fileType,
                "uploadedDate": document.uploadedDate,
                "category": document.category,
                "subCategory": document.subCategory,
                "viewURL": document.url
            }
        };

        if (categorySearch !== undefined) {
            if (document.category === categorySearch) {
                targetDocuments.push(docToPush);
            }
        }
        else {
            targetDocuments.push(docToPush);
        }


    });

    // Sorted
    targetDocuments = _.sortBy(targetDocuments, [function (o) { return new Date(o.uploadedDate); }]).reverse();




    // Paginate 
    var pSize = input.attributes.criteria.params.pageSize ? input.attributes.criteria.params.pageSize : input.attributes.pageSize ? input.attributes.pageSize : 10;
    var pCount = input.attributes.criteria.params.page ? input.attributes.criteria.params.page : input.attributes.page ? input.attributes.page : 1;

    var paginatedClaimDocumentsList = _.chunk(targetDocuments, pSize); // input.attributes.criteria.params.pageSize
    var targetPage = paginatedClaimDocumentsList[pCount - 1];

    var responseObject = {
        "data": (_.size(targetPage) === 0) ? [] : targetPage,
        "meta": {
            "totalRecords": targetDocuments.length,
            "totalPages": 2,
            "currentPage": 1
        }
    };

    return responseObject;
};

// updateClaimDocumentsById
exports.updateClaimDocumentsById = function (req) {
    var claimDocuments = TEMPSTORE.MockDocuments;
    var input = req.body;
    var claimNumber = _.toString(req.params.id);

    // check if the claimNo exists 
    var existingClaimDocuments = _.filter(claimDocuments, function (o) {
        return _.toString(o.claimNumber) === _.toString(req.params.id);
    });

    //if (_.size(existingClaimDocuments) === 0) return false;


    // Update the documents
    // TODO: Not needed now 

    var responseObject = { description: SUCCESS.Success204 };
    return responseObject;
};


// getDuplicateClaim 
exports.getDuplicateClaim = function (req) {
    var mockClaimSummary = TEMPSTORE.MockClaimSummary;
    var mockClaimDetails = TEMPSTORE.MockClaimDetails;
    var mockPolicyUsers = TEMPSTORE.MockPolicyUsers;

    // Get target userId from the X-Token 
    var xToken = req.get("X-Token");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var userPoliciesArr = [];
    var associatedClaimsPerPolicy = {};

    // Get the policies associated with the current user 
    var userPolicies = _.filter(mockPolicyUsers, function (o) {
        var isValid = _.toLower(o.Email) === _.toLower(xToken);
        if (isValid) {
            userPoliciesArr.push(o.policyNumber);
            associatedClaimsPerPolicy[o.policyNumber] = _.split(o.associatedClaims, ",");
            _.forEach(associatedClaimsPerPolicy[o.policyNumber], function (v, i) {
                associatedClaimsPerPolicy[o.policyNumber][i] = _.trim(v);
            });

        }
        return isValid;
    });

    // Claims list per user 
    var userClaimsList = [];
    _.forEach(userPoliciesArr, function (o, i) {
        var targetClaims = [];
        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] === "*") {
            targetClaims = _.filter(mockClaimSummary, function (v) {
                return o === v.policyNumber;
            });

            userClaimsList = _.concat(userClaimsList, targetClaims);
        }

        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] !== "*" && associatedClaimsPerPolicy[o][0] !== "null") {
            targetClaims = _.filter(mockClaimSummary, function (v) {
                return _.indexOf(associatedClaimsPerPolicy[o], v.claimNumber) > -1;
            });

            userClaimsList = _.concat(userClaimsList, targetClaims);
        }
    });

    // Filtered 
    var filteredClaims = userClaimsList;
    var workerGivenNameFilter = req.body.data.attributes.worker.name.given ? req.body.data.attributes.worker.name.given : undefined;
    var workerFamilyNameFilter = req.body.data.attributes.worker.name.family ? req.body.data.attributes.worker.name.family : undefined;
    var injuryDateFilter = req.body.data.attributes.injury.date ? req.body.data.attributes.injury.date : undefined;

    filteredClaims = _.filter(userClaimsList, function (o) {
        // Assume first that the item is to be included 
        var returnValue = true;

        // If any of the filters has value and the value is not contained in the item, then do not include that item 

        // family name 
        if (workerFamilyNameFilter !== undefined && workerFamilyNameFilter !== "undefined") {
            if (_.toLower(workerFamilyNameFilter) !== _.toLower(o.injuredLastName)) return false;
        }

        // given name 
        if (workerGivenNameFilter !== undefined && workerGivenNameFilter !== "undefined") {
            if (_.toLower(workerGivenNameFilter) !== _.toLower(o.injuredFirstName)) return false;
        }

        // injuryDate 
        if (injuryDateFilter !== undefined && injuryDateFilter !== "undefined") {
            if (_.toLower(injuryDateFilter) !== _.toLower(o.dateOfInjury)) return false;
        }

        return returnValue;
    });

    var duplicateClaims = _.map(filteredClaims, 'claimNumber');

    var responseObject = {
        "data": {
            "type": "string",
            "id": "string",
            "attributes": {
                "claims": duplicateClaims
            }
        }
    };

    return responseObject;
};

// getPaymentWageOverview 
exports.getPaymentWageOverview = function (req) {
    var mockWagePaymentOverview = TEMPSTORE.MockWagePaymentOverview;
    var claimNumber = req.params.id;

    // Get claimDetail from TEMPSTORE.MockClaimDetails given the claim number 
    var targetClaimDetails = _.find(TEMPSTORE.MockClaimDetails, function (o) {
        return o.data.attributes.claimNumber === claimNumber;
    });
    if (!targetClaimDetails) return false;

    var xToken = req.get("X-OktaTokenJson");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var latestWagePayment = _.find(mockWagePaymentOverview, function (o) {
        return _.toLower(_.toString(o.Email)) === _.toLower(_.toString(xToken));
    });

    var responseObject = {
        "data": {
            "type": "string",
            "id": "string",
            "attributes": {
                "hasAnyPaymentBeenMade": true,
                "overview": {
                    "lastPaymentMadeDate": (latestWagePayment !== undefined) ? latestWagePayment.Date : "",
                    "lastPaymentAmount": (latestWagePayment !== undefined) ? latestWagePayment.Amount : ""
                }
            }
        }
    };

    return responseObject;
};

// validPaymentWageHistory
exports.validPaymentWageHistory = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.data.id) return false;
    if (!input.data.attributes.page) return false;
    if (!input.data.attributes.pageSize) return false;

    return true;
};

// getPaymentWageHistory
exports.getPaymentWageHistory = function (req) {
    var mockWagePaymentHistory = TEMPSTORE.MockWagePaymentHistory;
    var claimNumber = req.params.id;

    // Get claimDetail from TEMPSTORE.MockClaimDetails given the claim number 
    var targetClaimDetails = _.find(TEMPSTORE.MockClaimDetails, function (o) {
        return o.data.attributes.claimNumber === claimNumber;
    });
    if (!targetClaimDetails) return false;

    var allowedUsers = [
        "icareemployer1@gmail.com",
        "icareemployer3@gmail.com",
        "icareiw1@gmail.com",
        "icareiw3@gmail.com",
    ];
    var input = req.body.data;

    var xToken = req.get("X-OktaTokenJson");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var targetUser = _.find(allowedUsers, function (o) {
        return _.toLower(_.toString(o)) === _.toLower(_.toString(xToken));
    });

    var wagePaymentHistoryList = [];
    if (targetUser !== null && targetUser !== undefined) {
        _.forEach(mockWagePaymentHistory, function (o, i) {
            var itemToPush = {
                "invoiceNumber": o.InvoiceNumber,
                "datePaid": o.DatePaid,
                "paymentMethodType": o.PaymentMethod,
                "amount": o.NetAmount,
                "payeeName": o.PayeeName,
                "payPeriod": {
                    "startDate": o.PayPeriodStart,
                    "endDate": o.PayPeriodEnd
                }
            };
            wagePaymentHistoryList.push(itemToPush);
        });
    }

    // Sorted
    wagePaymentHistoryList = _.sortBy(wagePaymentHistoryList, [function (o) { return new Date(o.datePaid); }]).reverse();

    // Paginate 
    var paginatedWagePaymentHistory = _.chunk(wagePaymentHistoryList, input.attributes.pageSize);
    var targetPage = paginatedWagePaymentHistory[input.attributes.page - 1];

    var responseObject = {
        "data": {
            "type": "string",
            "id": "string",
            "attributes": {
                "totalResults": wagePaymentHistoryList.length,
                "totalPages": paginatedWagePaymentHistory.length,
                "currentPage": input.attributes.page,
                "items": (_.size(targetPage) === 0) ? [] : targetPage
            }
        }
    };

    return responseObject;
};

// validPaymentWageHistory
exports.validPaymentWageSearch = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.data.attributes.criteria.type) return false;
    if (!input.data.attributes.params.page) return false;
    if (!input.data.attributes.params.pageSize) return false;

    return true;
};

// getPaymentWageSearch
exports.getPaymentWageSearch = function (req) {
    var mockWagePaymentHistory = TEMPSTORE.MockWagePaymentHistory;
    var mockWagePaymentOverview = TEMPSTORE.MockWagePaymentOverview;
    var claimNumber = req.params.claimId;

    // Get claimDetail from TEMPSTORE.MockClaimDetails given the claim number 
    var targetClaimDetails = _.find(TEMPSTORE.MockClaimDetails, function (o) {
        return o.data.attributes.claimNumber === claimNumber;
    });
    if (!targetClaimDetails) return false;

    var allowedUsers = [
        "icareemployer1@gmail.com",
        "icareemployer3@gmail.com",
        "icareiw1@gmail.com",
        "icareiw3@gmail.com",
    ];
    var input = req.body.data;

    var xToken = req.get("X-OktaTokenJson");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var targetUser = _.find(allowedUsers, function (o) {
        return _.toLower(_.toString(o)) === _.toLower(_.toString(xToken));
    });

    if (input.attributes.criteria.isLatest) {
        var latestWagePayment = _.find(mockWagePaymentOverview, function (o) {
            return _.toLower(_.toString(o.Email)) === _.toLower(_.toString(xToken));
        });

        var responseObjectDetails = {
            "meta": {
                "totalRecords": 1,
                "totalPages": 1,
                "currentPage": 1
            },
            "data": {
                "type": "PaymentWageDetails",
                "attributes": {
                    "overview": {
                        "lastPaymentMadeDate": latestWagePayment.Date,
                        "totalCostsPaid": {
                            "value": latestWagePayment.AmountValue,
                            "currencyType": latestWagePayment.AmountCurrencyType
                        }
                    }
                }
            }
        };

        return responseObjectDetails;
    }
    else {
        var wagePaymentHistoryList = [];
        if (targetUser !== null && targetUser !== undefined) {
            _.forEach(mockWagePaymentHistory, function (o, i) {
                var itemToPush = {
                    "id": o.Id,
                    "datePaid": o.DatePaid,
                    "paymentMethodType": o.PaymentMethod,
                    "amount": {
                        "value": o.AmountValue,
                        "currencyType": o.AmountCurrencyType
                    },
                    "payeeName": o.PayeeName,
                    "payPeriod": {
                        "startDate": o.PayPeriodStart,
                        "endDate": o.PayPeriodEnd
                    }
                };
                wagePaymentHistoryList.push(itemToPush);
            });
        }

        // Sorted
        wagePaymentHistoryList = _.sortBy(wagePaymentHistoryList, [function (o) { return new Date(o.datePaid); }]).reverse();

        // Paginate 
        var paginatedWagePaymentHistory = _.chunk(wagePaymentHistoryList, input.attributes.params.pageSize);
        var targetPage = paginatedWagePaymentHistory[input.attributes.params.page - 1];

        var responseObjectHistory = {
            "meta": {
                "totalRecords": wagePaymentHistoryList.length,
                "totalPages": paginatedWagePaymentHistory.length,
                "currentPage": input.attributes.params.page
            },
            "data": {
                "type": "PaymentWageHistory",
                "attributes": {
                    "items": (_.size(targetPage) === 0) ? [] : targetPage
                }
            }
        };

        return responseObjectHistory;
    }
};

// getPaymentMedical 
exports.getPaymentMedicalOverview = function (req) {
    var mockMedicalPaymentOverview = TEMPSTORE.MockMedicalPaymentOverview;
    var claimNumber = req.params.id;

    // Get claimDetail from TEMPSTORE.MockClaimDetails given the claim number 
    var targetClaimDetails = _.find(TEMPSTORE.MockClaimDetails, function (o) {
        return o.data.attributes.claimNumber === claimNumber;
    });
    if (!targetClaimDetails) return false;

    var xToken = req.get("X-OktaTokenJson");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var latestMedicalPayment = _.find(mockMedicalPaymentOverview, function (o) {
        return _.toLower(_.toString(o.Email)) === _.toLower(_.toString(xToken));
    });

    var responseObject = {
        "data": {
            "type": "string",
            "id": "string",
            "attributes": {
                "hasAnyPaymentBeenMade": true,
                "overview": {
                    "lastPaymentMadeDate": (latestMedicalPayment !== undefined) ? latestMedicalPayment.Date : "",
                    "totalCostsPaid": (latestMedicalPayment !== undefined) ? latestMedicalPayment.Amount : ""
                }
            }
        }
    };

    return responseObject;
};

//getPaymentMedicalInvoice
exports.sample = function (req) {
    var one = 1;
    var two = 2;
}

//getPaymentMedicalInvoice
exports.getPaymentMedicalInvoice = function (req) {

    var mockMedicalPaymentHistory = TEMPSTORE.MockMedicalPaymentHistory;
    var mockMedicalPaymentDetails = TEMPSTORE.MockMedicalPaymentDetails;
    var invoiceId = req.params.medicalPaymentId;
    if (invoiceId == 30) { invoiceId = "44444454"; }

    var targetMedicalHistory = _.find(mockMedicalPaymentHistory, function (o) {
        return o.invoiceNumber === invoiceId;
    });

    var targetMedicalDetails = _.filter(mockMedicalPaymentDetails, function (o) {
        return o.InvoiceNumber === invoiceId;
    });

    var allowedUsers = [
        "icareemployer1@gmail.com",
        "icareemployer2@gmail.com",
        "icareemployer3@gmail.com",
        "icareiw2@gmail.com",
        "icareiw3@gmail.com",
    ];

    var xToken = req.get("X-OktaTokenJson");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var targetUser = _.find(allowedUsers, function (o) {
        return _.toLower(_.toString(o)) === _.toLower(_.toString(xToken));
    });

    var responseObject = {
        "data": {
            "type": "PaymentMedicalInvoiceDetails",
            "id": "string",
            "attributes": {
                "invoiceNumber": "",
                "datePaid": "",
                "paymentMethodType": "",
                "netAmount": {},
                "items": []
            }
        }
    };

    if (targetUser !== undefined && targetMedicalHistory !== undefined) {
        responseObject.data.attributes.invoiceNumber = targetMedicalHistory.invoiceNumber;
        responseObject.data.attributes.datePaid = targetMedicalHistory.datePaid;
        responseObject.data.attributes.paymentMethodType = targetMedicalHistory.paymentType;
        responseObject.data.attributes.netAmount.value = targetMedicalHistory.amountValue;
        responseObject.data.attributes.netAmount.currencyType = targetMedicalHistory.amountCurrencyType;


        if (_.size(targetMedicalDetails) === 0) {
            var itemToPush = {
                "dateOfService": targetMedicalHistory.WhenToPay,
                "paycodeDescription": "Diagnostic Procedures",
                "amount": {
                    "value": Number(targetMedicalHistory.amountValue.replace(/[^0-9\.-]+/g, "")),
                    "currencyType": targetMedicalHistory.amountCurrencyType
                }
            };
            responseObject.data.attributes.lineItems.push(itemToPush);
        } else {
            _.forEach(targetMedicalDetails, function (o, i) {
                var itemToPush = {
                    "dateOfService": o.DateOfService,
                    "paycodeDescription": o.PaycodeDescription,
                    "amount": {
                        "value": Number(o.LineItemTotal.replace(/[^0-9\.-]+/g, "")),
                        "currencyType": "AUD"
                    }
                };
                responseObject.data.attributes.items.push(itemToPush);
            });
        }
    }

    return responseObject;
}


//getPaymentWageInvoice
exports.getPaymentWageInvoice = function (req) {

    var mockWagelPaymentHistory = TEMPSTORE.MockWagePaymentHistory;
    var mockWagePaymentDetails = TEMPSTORE.MockWagePaymentDetails;
    var invoiceId = req.params.invoiceNumber;
    if (invoiceId == 30) { invoiceId = "44444453"; }

    var targetWageHistory = _.find(mockWagelPaymentHistory, function (o) {
        return o.InvoiceNumber === invoiceId;
    });

    var targetWageDetails = _.filter(mockWagePaymentDetails, function (o) {
        return o.InvoiceNumber === invoiceId;
    });

    var allowedUsers = [
        "icareemployer1@gmail.com",
        "icareemployer2@gmail.com",
        "icareemployer3@gmail.com",
        "icareiw2@gmail.com",
        "icareiw3@gmail.com",
    ];

    var xToken = req.get("X-OktaTokenJson");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var targetUser = _.find(allowedUsers, function (o) {
        return _.toLower(_.toString(o)) === _.toLower(_.toString(xToken));
    });

    var responseObject = {
        "data": {
            "type": "PaymentWageInvoiceDetails",
            "id": "1",
            "attributes": {
                "datePaid": "",
                "paymentMethodType": "",
                "netAmount": {},
                "payeeName": "",
                "payPeriod": {},
                "payments": {
                    "items": [],
                    "totalAmount": {
                        "value": 2926.4,
                        "currencyType": "AUD"
                    }
                },
                "deductions": {
                    "items": [],
                    "totalAmount": {
                        "value": 170.66,
                        "currencyType": "AUD"
                    }
                }
            }
        }
    };

    if (targetUser !== undefined && targetWageHistory !== undefined) {
        responseObject.data.attributes.datePaid = targetWageHistory.DatePaid;
        responseObject.data.attributes.paymentMethodType = targetWageHistory.PaymentMethod;
        responseObject.data.attributes.netAmount.value = targetWageHistory.AmountValue;
        responseObject.data.attributes.netAmount.currencyType = targetWageHistory.AmountCurrencyType;
        responseObject.data.attributes.payeeName = targetWageHistory.PayeeName;
        responseObject.data.attributes.payPeriod.startDate = targetWageHistory.PayPeriodStart;
        responseObject.data.attributes.payPeriod.endDate = targetWageHistory.PayPeriodEnd;

        if (_.size(targetWageDetails) === 0) {
            var paymentItemToPush = {
                "payPeriod": {
                    "startDate": targetWageHistory.PayPeriodStart,
                    "endDate": targetWageHistory.PayPeriodEnd
                },
                "description": o.PaycodeDescriptions,
                "preInjuryAverageWeeklyEarnings": {
                    "value": 1650,
                    "currencyType": "AUD"
                },
                "weeklyBenefitRate": {
                    "value": 1650,
                    "currencyType": "AUD"
                },
                "earnings": {
                    "value": 200,
                    "currencyType": "AUD"
                },
                "hoursWorked": 20,
                "deductions": {
                    "value": 77.5,
                    "currencyType": "AUD"
                },
                "amount": {
                    "value": targetWageHistory.amountValue,
                    "currencyType": targetWageHistory.amountCurrencyType
                }
            };
            var deductionItemToPush = {
                "description": "PAYG Description",
                "value": o.Earnings,
                "currencyType": "AUD"
            };
            responseObject.data.attributes.payments.Items.push(paymentItemToPush);
            responseObject.data.attributes.deductions.items.push(deductionItemToPush);
        } else {
            _.forEach(targetWageDetails, function (o, i) {
                var paymentItemToPush = {
                    "payPeriod": {
                        "startDate": targetWageHistory.PayPeriodStart,
                        "endDate": targetWageHistory.PayPeriodEnd
                    },
                    "description": o.PaycodeDescriptions,
                    "preInjuryAverageWeeklyEarnings": {
                        "value": Number(o.PIAWE.replace(/[^0-9\.-]+/g, "")),
                        "currencyType": "AUD"
                    },
                    "weeklyBenefitRate": {
                        "value": Number(o.WeeklyBenefitRate.replace(/[^0-9\.-]+/g, "")),
                        "currencyType": "AUD"
                    },
                    "earnings": {
                        "value": o.Earnings,
                        "currencyType": "AUD"
                    },
                    "hoursWorked": o.HoursWorked,
                    "deductions": {
                        "value": Number(o.DeductionLineItemAmount.replace(/[^0-9\.-]+/g, "")),
                        "currencyType": "AUD"
                    },
                    "amount": {
                        "value": Number(o.Amount.replace(/[^0-9\.-]+/g, "")),
                        "currencyType": "AUD"
                    }

                };

                var deductionItemToPush = {
                    "description": o.PaycodeDescriptions,
                    "value": o.Earnings,
                    "currencyType": "AUD"
                };
                responseObject.data.attributes.payments.items.push(paymentItemToPush);
                responseObject.data.attributes.deductions.items.push(deductionItemToPush);
            });
        }
    }

    return responseObject;
}

// validPaymentMedicalHistory
exports.validPaymentMedicalHistory = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.data.attributes.criteria.type) return false;
    if (!input.data.attributes.params.page) return false;
    if (!input.data.attributes.params.pageSize) return false;

    return true;
};

// getPaymentWageHistory
exports.getPaymentMedicalHistory = function (req) {
    var mockMedicalPaymentHistory = TEMPSTORE.MockMedicalPaymentHistory;
    var claimNumber = req.params.claimId;

    // Get claimDetail from TEMPSTORE.MockClaimDetails given the claim number 
    var targetClaimDetails = _.find(TEMPSTORE.MockClaimDetails, function (o) {
        return o.data.attributes.claimNumber === claimNumber;
    });
    if (!targetClaimDetails) return false;

    var allowedUsers = [
        "icareemployer1@gmail.com",
        "icareemployer3@gmail.com",
        "icareiw1@gmail.com",
        "icareiw3@gmail.com",
    ];

    var input = req.body.data;

    var xToken = req.get("X-OktaTokenJson");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var targetUser = _.find(allowedUsers, function (o) {
        return _.toLower(_.toString(o)) === _.toLower(_.toString(xToken));
    });

    var medicalPaymentHistoryList = [];
    if (targetUser !== null && targetUser !== undefined) {
        _.forEach(mockMedicalPaymentHistory, function (o, i) {
            var itemToPush = {
                "id": o.id,
                "datePaid": o.datePaid,
                "invoiceNumber": o.invoiceNumber,
                "paymentType": o.paymentType,
                "amount": {
                    "value": o.amountValue,
                    "currencyType": o.amountCurrencyType
                }
            };
            medicalPaymentHistoryList.push(itemToPush);
        });
    }

    // Sorted
    medicalPaymentHistoryList = _.sortBy(medicalPaymentHistoryList, [function (o) { return new Date(o.datePaid); }]).reverse();

    // Paginate 
    var paginatedMedicalPaymentHistory = _.chunk(medicalPaymentHistoryList, input.attributes.params.pageSize);
    var targetPage = paginatedMedicalPaymentHistory[input.attributes.params.page - 1];

    var responseObject = {
        "meta": {
            "totalRecords": medicalPaymentHistoryList.length,
            "totalPages": paginatedMedicalPaymentHistory.length,
            "currentPage": input.attributes.params.page
        },
        "data": {
            "type": "PaymentMedicalHistory",
            "attributes": {
                "items": (_.size(targetPage) === 0) ? [] : targetPage
            }
        }
    };

    return responseObject;
};

// getMedicalPaymentSearch
exports.getMedicalPaymentSearch = function (req) {
    var mockMedicalPaymentHistory = TEMPSTORE.MockMedicalPaymentHistory;
    var mockMedicalPaymentOverview = TEMPSTORE.MockMedicalPaymentOverview;
    var claimNumber = req.params.claimId;

    // Get claimDetail from TEMPSTORE.MockClaimDetails given the claim number 
    var targetClaimDetails = _.find(TEMPSTORE.MockClaimDetails, function (o) {
        return o.data.attributes.claimNumber === claimNumber;
    });
    if (!targetClaimDetails) return false;

    var allowedUsers = [
        "icareemployer1@gmail.com",
        "icareemployer3@gmail.com",
        "icareiw1@gmail.com",
        "icareiw3@gmail.com",
    ];

    var input = req.body.data;

    var xToken = req.get("X-OktaTokenJson");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var targetUser = _.find(allowedUsers, function (o) {
        return _.toLower(_.toString(o)) === _.toLower(_.toString(xToken));
    });

    if (input.attributes.criteria.isLatest) {
        var latestMedicalPayment = _.find(mockMedicalPaymentOverview, function (o) {
            return _.toLower(_.toString(o.Email)) === _.toLower(_.toString(xToken));
        });

        var responseObjectDetails = {
            "meta": {
                "totalRecords": 1,
                "totalPages": 1,
                "currentPage": 1
            },
            "data": {
                "type": "PaymentMedicalDetails",
                "attributes": {
                    "overview": {
                        "lastPaymentMadeDate": latestMedicalPayment.Date,
                        "totalCostsPaid": {
                            "value": latestMedicalPayment.AmountValue,
                            "currencyType": latestMedicalPayment.AmountCurrencyType
                        }
                    }
                }
            }
        };

        return responseObjectDetails;
    }
    else {
        var medicalPaymentHistoryList = [];
        if (targetUser !== null && targetUser !== undefined) {
            _.forEach(mockMedicalPaymentHistory, function (o, i) {
                var itemToPush = {
                    "id": o.id,
                    "datePaid": o.datePaid,
                    "invoiceNumber": o.invoiceNumber,
                    "paymentType": o.paymentType,
                    "amount": {
                        "value": o.amountValue,
                        "currencyType": o.amountCurrencyType
                    }
                };
                medicalPaymentHistoryList.push(itemToPush);
            });
        }

        // Sorted
        medicalPaymentHistoryList = _.sortBy(medicalPaymentHistoryList, [function (o) { return new Date(o.datePaid); }]).reverse();

        // Paginate 
        var paginatedMedicalPaymentHistory = _.chunk(medicalPaymentHistoryList, input.attributes.params.pageSize);
        var targetPage = paginatedMedicalPaymentHistory[input.attributes.params.page - 1];

        var responseObjectHistory = {
            "meta": {
                "totalRecords": medicalPaymentHistoryList.length,
                "totalPages": paginatedMedicalPaymentHistory.length,
                "currentPage": input.attributes.params.page
            },
            "data": {
                "type": "PaymentMedicalHistory",
                "attributes": {
                    "items": (_.size(targetPage) === 0) ? [] : targetPage
                }
            }
        };

        return responseObjectHistory;
    }
};

// getPaymentMedicalDetails
exports.getPaymentMedicalDetail = function (req) {
    var mockMedicalPaymentHistory = TEMPSTORE.MockMedicalPaymentHistory;
    var mockMedicalPaymentDetails = TEMPSTORE.MockMedicalPaymentDetails;
    var invoiceId = req.params.invoiceId
    //
    var targetMedicalHistory = _.find(mockMedicalPaymentHistory, function (o) {
        return o.invoiceNumber === invoiceId;
    });

    var targetMedicalDetails = _.filter(mockMedicalPaymentDetails, function (o) {
        return o.InvoiceNumber === invoiceId;
    });

    var allowedUsers = [
        "icareemployer1@gmail.com",
        "icareemployer2@gmail.com",
        "icareiw2@gmail.com",
        "icareiw3@gmail.com",
    ];

    var xToken = req.get("X-OktaTokenJson");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var targetUser = _.find(allowedUsers, function (o) {
        return _.toLower(_.toString(o)) === _.toLower(_.toString(xToken));
    });

    var responseObject = {
        "data": {
            "type": "PaymentMedicalInvoiceDetails",
            "id": "string",
            "attributes": {
                "netAmount": {},
                "lineItems": []
            }
        }
    };

    if (targetUser !== undefined && targetMedicalHistory !== undefined) {
        responseObject.data.attributes.invoiceNumber = targetMedicalHistory.InvoiceNumber;
        responseObject.data.attributes.datePaid = targetMedicalHistory.WhenToPay;
        responseObject.data.attributes.paymentMethodType = targetMedicalHistory.PaymentMethod;
        responseObject.data.attributes.netAmount.value = targetMedicalHistory.amountValue;
        responseObject.data.attributes.netAmount.currencyType = targetMedicalHistory.amountCurrencyType;


        if (_.size(targetMedicalDetails) === 0) {
            var itemToPush = {
                "dateOfService": targetMedicalHistory.WhenToPay,
                "paycodeDescription": "Diagnostic Procedures",
                "amount": {
                    "value": targetMedicalHistory.amountValue,
                    "currencyType": targetMedicalHistory.amountCurrencyType
                }
            };
            responseObject.data.attributes.lineItems.push(itemToPush);
        } else {
            _.forEach(targetMedicalDetails, function (o, i) {
                var itemToPush = {
                    "dateOfService": o.DateOfService,
                    "paycodeDescription": o.PaycodeDescription,
                    "amount": {
                        "value": o.LineItemTotal,
                        "currencyType": "AUD"
                    }
                };
                responseObject.data.attributes.lineItems.push(itemToPush);
            });
        }
    } else {
        responseObject.data.attributes.datePaid = "";
        responseObject.data.attributes.invoiceNumber = invoiceId;
        responseObject.data.attributes.paymentMethodType = "";
        responseObject.data.attributes.amount = "";
    }

    return responseObject;
};

//getPaymentWageDetail
exports.getPaymentWageDetail = function (req) {
    var mockWagePaymentHistory = TEMPSTORE.MockWagePaymentHistory;
    var mockWagePaymentDetails = TEMPSTORE.MockWagePaymentDetails;
    var invoiceId = req.params.invoiceId
    //
    var targetWageHistory = _.find(mockWagePaymentHistory, function (o) {
        return o.InvoiceNumber === invoiceId;
    });

    var targetWageDetails = _.filter(mockWagePaymentDetails, function (o) {
        return o.InvoiceNumber === invoiceId;
    });

    var allowedUsers = [
        "icareemployer1@gmail.com",
        "icareemployer2@gmail.com",
        "icareiw2@gmail.com",
        "icareiw3@gmail.com",
    ];

    var xToken = req.get("X-OktaTokenJson");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var targetUser = _.find(allowedUsers, function (o) {
        return _.toLower(_.toString(o)) === _.toLower(_.toString(xToken));
    });

    var responseObject = {
        "data": {
            "type": "string",
            "id": "string",
            "attributes": {
                "invoiceNumber": "",
                "datePaid": "",
                "paymentMethodType": "",
                "grossAmount": "",
                "deductionsAmount": "",
                "netAmount": "",
                "payeeName": "",
                "payPeriod": {},
                "paymentLineItems": [],
                "deductionLineItems": []
            }
        }
    };

    if (targetUser !== undefined && targetWageHistory !== undefined) {
        responseObject.data.attributes.invoiceNumber = targetWageHistory.InvoiceNumber;
        responseObject.data.attributes.datePaid = targetWageHistory.DatePaid;
        responseObject.data.attributes.paymentMethodType = targetWageHistory.PaymentMethod;
        responseObject.data.attributes.grossAmount = targetWageHistory.GrossAmount;
        responseObject.data.attributes.deductionsAmount = targetWageHistory.DeductionsAmount;
        responseObject.data.attributes.netAmount = targetWageHistory.NetAmount;
        responseObject.data.attributes.payeeName = targetWageHistory.PayeeName;
        responseObject.data.attributes.payPeriod.startDate = targetWageHistory.PayPeriodStart;
        responseObject.data.attributes.payPeriod.endDate = targetWageHistory.PayPeriodEnd;

        if (_.size(targetWageDetails) === 0) {
            var itemToPush = {
                "payPeriod": {
                    "startDate": targetWageHistory.PayPeriodStart,
                    "endDate": targetWageHistory.PayPeriodEnd,
                },
                "description": "Weekly Payments",
                "preInjuryAverageWeeklyEarnings": "$1,650.00",
                "weeklyBenefitRate": "$1,567.50",
                "earnings": "$400.00",
                "hoursWorked": "30",
                "deductionAmount": "$50.00",
                "amount": "$1,217.50"
            };
            responseObject.data.attributes.paymentLineItems.push(itemToPush);

            var deductItemToPush = {
                "description": "PAYG",
                "amount": "$872.20"
            };
            responseObject.data.attributes.deductionLineItems.push(deductItemToPush);
        } else {
            _.forEach(targetWageDetails, function (o, i) {
                var itemToPush = {
                    "payPeriod": {
                        "startDate": o.DateFrom,
                        "endDate": o.DateTo,
                    },
                    "description": o.PaycodeDescriptions,
                    "preInjuryAverageWeeklyEarnings": o.PIAWE,
                    "weeklyBenefitRate": o.WeeklyBenefitRate,
                    "earnings": o.Earnings,
                    "hoursWorked": o.HoursWorked,
                    "deductionAmount": o.Deductions,
                    "amount": o.Amount
                };
                responseObject.data.attributes.paymentLineItems.push(itemToPush);

                var deductItemToPush = {
                    "description": o.DeductionLineItemDescription,
                    "amount": o.DeductionLineItemAmount
                };
                if (deductItemToPush.description !== undefined && deductItemToPush.description !== "") {
                    responseObject.data.attributes.deductionLineItems.push(deductItemToPush);
                }
            });
        }
    } else {
        responseObject.data.attributes.invoiceNumber = "";
        responseObject.data.attributes.datePaid = "";
        responseObject.data.attributes.paymentMethodType = "";
        responseObject.data.attributes.grossAmount = "";
        responseObject.data.attributes.deductionsAmount = "";
        responseObject.data.attributes.netAmount = "";
        responseObject.data.attributes.payeeName = "";
        responseObject.data.attributes.payPeriod.startDate = "";
        responseObject.data.attributes.payPeriod.endDate = "";
    }

    return responseObject;
};

// getPaymentDetailForReimbursement
exports.getPaymentDetailForReimbursement = function (req) {
    var mockBsbBanks = TEMPSTORE.MockBsbBanks;
    var mockPaymentDetails = TEMPSTORE.MockPaymentDetails;

    var xToken = req.get("X-OktaTokenJson");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var targetPaymentDetailForReimbursement = _.find(mockPaymentDetails, function (o) {
        return (_.toLower(_.toString(o.email)) === _.toLower(_.toString(xToken)) && o.preferredAccount === true);
    });

    if (targetPaymentDetailForReimbursement !== undefined) {
        var bsbBank = _.find(mockBsbBanks, function (v) {
            return (v.BSB === targetPaymentDetailForReimbursement.bsb && v.AccountNumber === targetPaymentDetailForReimbursement.accountNumber);
        });
    }

    var responseObject = {
        "data": {
            "type": "string",
            "id": "string",
            "attributes": {
                "accountName": (bsbBank !== undefined) ? bsbBank.AccountName : "",
                "accountNumber": (targetPaymentDetailForReimbursement !== undefined) ? targetPaymentDetailForReimbursement.accountNumber : "",
                "bsb": (targetPaymentDetailForReimbursement !== undefined) ? targetPaymentDetailForReimbursement.bsb : "",
                "bankName": (bsbBank !== undefined) ? bsbBank.BankName : ""
            }
        }
    };

    return responseObject;
};

// validClaimRequest 
exports.validWeeklyAverageEarnings = function (req) {
    var input = req.body.data ? req.body.data : undefined;
    if (input === undefined) return false;
    if (input.attributes === undefined) return false;

    return true;
};

// getPaymentMedicalDetail
exports.postWeeklyAverageEarnings = function (req) {
    var responseObject = { description: SUCCESS.Success202c };
    return responseObject;
};

// getInjuryManagement
exports.getInjuryManagement = function (req) {
    var mockImp = TEMPSTORE.MockImp;
    var input = _.toString(req.params.claimId);

    // check if the claimNo exists 
    var existingInsuranceManagementPlan = _.find(mockImp, function (o) {
        return _.toString(o.claimNumber) === input;
    });

    var defaultContactsFirst = {
        "contactId": "1",
        "name": {
            "given": "Tom",
            "family": "Josilevich"
        },
        "positionTitle": "Accounting",
        "phone": {
            "contactNumber": "0455511515",
            "phoneType": "work"
        },
        "email": "bdrysdell0@studiopress.com"
    };
    var defaultContactsSecond = {
        "contactId": "151",
        "name": {
            "given": "Wen",
            "family": "Aberdeen"
        },
        "positionTitle": "Training",
        "phone": {
            "contactNumber": "0410442115",
            "phoneType": "home"
        },
        "email": "bdrysdell0@studiopress.com"
    };

    var responseObject = {
        "data": {
            "type": "string",
            "id": input,
            "attributes": {
                "currentPlanDocument": {},
                "contacts": [],
                "goals": [],
                "actions": [],
                "treatments": [],
                "careServices": []
            }
        }
    };

    if (existingInsuranceManagementPlan === undefined) {
        responseObject.data.attributes.contacts.push(defaultContactsFirst);
        responseObject.data.attributes.contacts.push(defaultContactsSecond);
    } else {

        //CurrentPlanDocument
        responseObject.data.attributes.currentPlanDocument.documentId = existingInsuranceManagementPlan.currentPlanDocumentDocId;
        responseObject.data.attributes.currentPlanDocument.title = existingInsuranceManagementPlan.currentPlanDocumentName;
        responseObject.data.attributes.currentPlanDocument.fileType = existingInsuranceManagementPlan.currentPlanDocumentMimeType;
        responseObject.data.attributes.currentPlanDocument.url = existingInsuranceManagementPlan.currentPlanDocumenViewUrl;
        responseObject.data.attributes.currentPlanDocument.fileSize = existingInsuranceManagementPlan.currentPlanDocumentFileSize;
        responseObject.data.attributes.currentPlanDocument.uploadedDate = existingInsuranceManagementPlan.currentPlanDocumentUploadedDate;

        // contacts
        if (existingInsuranceManagementPlan.contactsFirstContactId !== "") {
            var contactsFirst = {
                "contactId": existingInsuranceManagementPlan.contactsFirstContactId,
                "name": {
                    "given": existingInsuranceManagementPlan.contactsFirstGivenName,
                    "family": existingInsuranceManagementPlan.contactsFirstFamilyName
                },
                "positionTitle": existingInsuranceManagementPlan.contactsFirstPositionTitle,
                "phone": {
                    "contactNumber": existingInsuranceManagementPlan.contactsFirstPhone,
                    "phoneType": existingInsuranceManagementPlan.contactsFirstPhoneType
                },
                "email": existingInsuranceManagementPlan.contactsFirstEmail
            };
            responseObject.data.attributes.contacts.push(contactsFirst);
        }

        if (existingInsuranceManagementPlan.contactsSecondContactId !== "") {
            var contactsSecond = {
                "contactId": existingInsuranceManagementPlan.contactsSecondContactId,
                "name": {
                    "given": existingInsuranceManagementPlan.contactsSecondGivenName,
                    "family": existingInsuranceManagementPlan.contactsSecondFamilyName
                },
                "positionTitle": existingInsuranceManagementPlan.contactsSecondPositionTitle,
                "phone": {
                    "contactNumber": existingInsuranceManagementPlan.contactsSecondPhone,
                    "phoneType": existingInsuranceManagementPlan.contactsSecondPhoneType
                },
                "email": existingInsuranceManagementPlan.contactsSecondEmail
            };
            responseObject.data.attributes.contacts.push(contactsSecond);
        }

        // goals
        if (existingInsuranceManagementPlan.goalsFirstGoalTitle !== "") {
            var goalsFirst = {
                "goalTitle": existingInsuranceManagementPlan.goalsFirstGoalTitle,
                "statusCode": existingInsuranceManagementPlan.goalsFirstStatusCode,
                "statusTitle": existingInsuranceManagementPlan.goalsFirstStatusTitle
            };
            responseObject.data.attributes.goals.push(goalsFirst);
        }

        if (existingInsuranceManagementPlan.goalsSecondGoalTitle !== "") {
            var goalsSecond = {
                "goalTitle": existingInsuranceManagementPlan.goalsSecondGoalTitle,
                "statusCode": existingInsuranceManagementPlan.goalsSecondStatusCode,
                "statusTitle": existingInsuranceManagementPlan.goalsSecondStatusTitle
            };
            responseObject.data.attributes.goals.push(goalsSecond);
        }

        // actions
        if (existingInsuranceManagementPlan.actionsFirstTitle !== "") {
            var actionFirst = {
                "date": existingInsuranceManagementPlan.actionsFirstDate,
                "title": existingInsuranceManagementPlan.actionsFirstTitle,
                "responsible": existingInsuranceManagementPlan.actionsFirstResponsible
            };
            responseObject.data.attributes.actions.push(actionFirst);
        }

        if (existingInsuranceManagementPlan.actionsSecondTitle !== "") {
            var actionSecond = {
                "date": existingInsuranceManagementPlan.actionsSecondDate,
                "title": existingInsuranceManagementPlan.actionsSecondTitle,
                "responsible": existingInsuranceManagementPlan.actionsSecondResponsible
            };
            responseObject.data.attributes.actions.push(actionSecond);
        }

        // treatments
        if (existingInsuranceManagementPlan.treatmentsFirstTreatment !== "") {
            var treatmentFirst = {
                "treatment": existingInsuranceManagementPlan.treatmentsFirstTreatment,
                "sessionsTaken": existingInsuranceManagementPlan.treatmentsFirstSessionsTaken,
                "sessionsTotal": existingInsuranceManagementPlan.treatmentsFirstSessionsTotal
            };
            responseObject.data.attributes.treatments.push(treatmentFirst);
        }

        if (existingInsuranceManagementPlan.treatmentsSecondTreatment !== "") {
            var treatmentSecond = {
                "treatment": existingInsuranceManagementPlan.treatmentsSecondTreatment,
                "sessionsTaken": existingInsuranceManagementPlan.treatmentsSecondSessionsTaken,
                "sessionsTotal": existingInsuranceManagementPlan.treatmentsSecondSessionsTotal
            };
            responseObject.data.attributes.treatments.push(treatmentSecond);
        }

        // careServices
        /*  if (existingInsuranceManagementPlan.careServicesFirstTitle !== "") {
            var careServiceFirst = {
                "title": existingInsuranceManagementPlan.careServicesFirstTitle,
                "task": existingInsuranceManagementPlan.careServicesFirstTask,
                "provider": existingInsuranceManagementPlan.careServicesFirstProvider,
                "approvalPeriod": {
                    "from": existingInsuranceManagementPlan.careServicesFirstApprovalPeriodFrom,
                    "to": existingInsuranceManagementPlan.careServicesFirstApprovalPeriodTo
                },
                "ratePerHour": {
                    "value": existingInsuranceManagementPlan.careServicesFirstRatePerHourValue,
                    "currencyType": existingInsuranceManagementPlan.careServicesFirstRatePerHourCurrency
                },
                "frequency": existingInsuranceManagementPlan.careServicesFirstFrequency,
                "totalHours": existingInsuranceManagementPlan.careServicesFirstTotalHours,
                "totalCost": {
                    "value": existingInsuranceManagementPlan.careServicesFirstTotalCostValue,
                    "currencyType": existingInsuranceManagementPlan.careServicesFirstTotalCostCurrency
                }
            };
            responseObject.data.attributes.careServices.push(careServiceFirst);
        }
        
      if (existingInsuranceManagementPlan.careServicesSecondTitle !== "") {
            var careServiceSecond = {
                "title": existingInsuranceManagementPlan.careServicesSecondTitle,
                "task": existingInsuranceManagementPlan.careServicesSecondTask,
                "provider": existingInsuranceManagementPlan.careServicesSecondProvider,
                "approvalPeriod": {
                    "from": existingInsuranceManagementPlan.careServicesSecondApprovalPeriodFrom,
                    "to": existingInsuranceManagementPlan.careServicesSecondApprovalPeriodTo
                },
                "ratePerHour": {
                    "value": existingInsuranceManagementPlan.careServicesSecondRatePerHourValue,
                    "currencyType": existingInsuranceManagementPlan.careServicesSecondRatePerHourCurrency
                },
                "frequency": existingInsuranceManagementPlan.careServicesSecondFrequency,
                "totalHours": existingInsuranceManagementPlan.careServicesSecondTotalHours,
                "totalCost": {
                    "value": existingInsuranceManagementPlan.careServicesSecondTotalCostValue,
                    "currencyType": existingInsuranceManagementPlan.careServicesSecondTotalCostCurrency
                }
            };
            responseObject.data.attributes.careServices.push(careServiceSecond);
        }
        
        if (existingInsuranceManagementPlan.careServicesThirdTitle !== "") {
            var careServiceThird = {
                "title": existingInsuranceManagementPlan.careServicesThirdTitle,
                "task": existingInsuranceManagementPlan.careServicesThirdTask,
                "provider": existingInsuranceManagementPlan.careServicesThirdProvider,
                "approvalPeriod": {
                    "from": existingInsuranceManagementPlan.careServicesThirdApprovalPeriodFrom,
                    "to": existingInsuranceManagementPlan.careServicesThirdApprovalPeriodTo
                },
                "ratePerHour": {
                    "value": existingInsuranceManagementPlan.careServicesThirdRatePerHourValue,
                    "currencyType": existingInsuranceManagementPlan.careServicesThirdRatePerHourCurrency
                },
                "ratePerHour": existingInsuranceManagementPlan.careServicesThirdRatePerHour,
                "frequency": existingInsuranceManagementPlan.careServicesThirdFrequency,
                "totalHours": existingInsuranceManagementPlan.careServicesThirdTotalHours,
                "totalCost": {
                    "value": existingInsuranceManagementPlan.careServicesThirdTotalCostValue,
                    "currencyType": existingInsuranceManagementPlan.careServicesThirdTotalCostCurrency
                }
            };
            responseObject.data.attributes.careServices.push(careServiceThird);
        }*/
    }

    return responseObject;
};

// getReturnToWork
exports.getReturnToWork = function (req) {
    var mockRtw = TEMPSTORE.MockRtw;
    var input = _.toString(req.params.claimId);

    // check if the claimNo exists 
    var existingReturnToWork = _.find(mockRtw, function (o) {
        return _.toString(o.claimNumber) === input;
    });

    var responseObject = {
        "data": {
            "type": "ReturnToWorkPlan",
            "id": input,
            "attributes": {
                "sessionId": "fed8b896-72a9-4615-8894-cc937ebaa615",
                "claimNumber": input,
                "internalClaimId": "1-nxEMJrLZlbYkB45gjpeRapNOq7K9jmWV",
                "currentRtwPlanDocument": {},
                "capacityForActivities": {},
                "rtwPlanTemplateDocument": {}
            }
        }
    };

    if (existingReturnToWork == undefined) {
        // rtwPlanTemplateDocument
        responseObject.data.attributes.rtwPlanTemplateDocument.documentId = "1000";
        responseObject.data.attributes.rtwPlanTemplateDocument.title = "BrandStorm HBC";
        responseObject.data.attributes.rtwPlanTemplateDocument.fileType = "application/pdf";
        responseObject.data.attributes.rtwPlanTemplateDocument.url = "http://capsicumgroup.com/wp-content/uploads/2013/03/Factsheet1.pdf";
        responseObject.data.attributes.rtwPlanTemplateDocument.fileSize = "103";
        responseObject.data.attributes.rtwPlanTemplateDocument.uploadedDate = "08/21/2017T03:01:52Z";
    } else {
        // currentRtwPlanDocument
        responseObject.data.attributes.currentRtwPlanDocument.documentId = existingReturnToWork.currentRtwPlanDocId;
        responseObject.data.attributes.currentRtwPlanDocument.title = existingReturnToWork.currentRtwPlanName;
        responseObject.data.attributes.currentRtwPlanDocument.fileType = existingReturnToWork.currentRtwPlanMimeType;
        responseObject.data.attributes.currentRtwPlanDocument.url = existingReturnToWork.currentRtwPlanUrl;
        responseObject.data.attributes.currentRtwPlanDocument.fileSize = existingReturnToWork.currentRtwPlanFileSize;
        responseObject.data.attributes.currentRtwPlanDocument.uploadedDate = existingReturnToWork.currentRtwPlanUploadedDate;

        // capacityForActivities
        responseObject.data.attributes.capacityForActivities.liftingCapacity = existingReturnToWork.liftingCapacity;
        responseObject.data.attributes.capacityForActivities.sittingTolerance = existingReturnToWork.sittingTolerance;
        responseObject.data.attributes.capacityForActivities.standingTolerance = existingReturnToWork.standingTolerance;
        responseObject.data.attributes.capacityForActivities.pushingAbility = existingReturnToWork.pushingAbility;
        responseObject.data.attributes.capacityForActivities.bendingAbility = existingReturnToWork.bendingAbility;
        responseObject.data.attributes.capacityForActivities.drivingAbility = existingReturnToWork.drivingAbility;
        responseObject.data.attributes.capacityForActivities.other = existingReturnToWork.other;

        // rtwPlanTemplateDocument
        responseObject.data.attributes.rtwPlanTemplateDocument.documentId = existingReturnToWork.rtwPlanTemplateDocumentDocId;
        responseObject.data.attributes.rtwPlanTemplateDocument.title = existingReturnToWork.rtwPlanTemplateDocumentName;
        responseObject.data.attributes.rtwPlanTemplateDocument.fileType = existingReturnToWork.rtwPlanTemplateDocumentMimeType;
        responseObject.data.attributes.rtwPlanTemplateDocument.url = existingReturnToWork.rtwPlanTemplateDocumentUrl;
        responseObject.data.attributes.rtwPlanTemplateDocument.fileSize = existingReturnToWork.rtwPlanTemplateDocumentFileSize;
        responseObject.data.attributes.rtwPlanTemplateDocument.uploadedDate = existingReturnToWork.rtwPlanTemplateDocumentUploadedDate;
    }

    return responseObject;
};

// postReturnToWork
exports.postReturnToWork = function (req) {
    var mockRtw = TEMPSTORE.MockRtw;
    var input = _.toString(req.params.claimId);

    // check if the claimNo exists 
    var existingReturnToWork = _.find(mockRtw, function (o) {
        return _.toString(o.claimNumber) === input;
    });

    var responseObject = {
        "data": {
            "type": "string",
            "id": input,
            "attributes": {
                "returnToWorkDocumentUrls": []
            }
        }
    };

    if (existingReturnToWork !== undefined) {
        responseObject.data.attributes.returnToWorkDocumentUrls.push(existingReturnToWork.currentRtwPlanUrl);
        responseObject.data.attributes.returnToWorkDocumentUrls.push(existingReturnToWork.rtwPlanTemplateDocumentUrl);
    }

    return responseObject;
};
// postPaymentsMedicalReimbursement
exports.postPaymentsMedicalReimbursement = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.data.id) return false;
    if (!input.data.attributes.documents) return false;

    return true;
};

//postMyPaymentsMedicalReimbursement Code Description
exports.postMyPaymentsMedicalReimbursement = function (req) {
    var responseObject = { description: SUCCESS.Success202c };
    return responseObject;
}

//postMyPaymentsWageReimbursement Code Description
exports.postMyPaymentsWageReimbursement = function (req) {
    var responseObject = { description: SUCCESS.Success202c };
    return responseObject;
}

//validatePaymentsWageReimbursement
exports.validatePaymentsWageReimbursement = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.data.attributes.dateClaimingFrom) return false;
    if (!input.data.attributes.didInjuredPersonWorkInPeriod) return false;
    if (!input.data.attributes.totalAmount) return false;
    if (!input.data.attributes.supportingDocumentUrls) return false;

    return true;
}

// validActivationToken
exports.validActivationToken = function (activationToken) {
    if (activationToken !== "a1b2c3d4") return false;
    return true;
}

// validActivationTokenParam
exports.validActivationTokenParam = function (req, q) {
    var input = req.query[q] ? req.query[q] : undefined;
    if (input === undefined) return false;
    if (!helpers.validActivationToken(input)) return false;
    return true;
};

// getUserRegister
exports.getUserRegister = function (req) {
    var responseObject = {
        "data": {
            "type": "EmployerRegistration",
            "id": "28002796-4227-475a-8c21-7ebf3269d72d",
            "attributes": {
                "status": "COMPLETED",
                "state": "ExecutionCompleted"
            }
        }
    };

    return responseObject;
};

// validUserRegister
exports.validUserRegister = function (req) {
    var input = req.body.data ? req.body.data : undefined;
    if (input === undefined) return false;

    if (!input.attributes.firstName) return false;
    if (!input.attributes.lastName) return false;
    if (!input.attributes.email) return false;
    if (!input.attributes.phone.contactNumber) return false;
    if (!input.attributes.phone.phoneType) return false;

    return true;
};

// postUserRegister
exports.postUserRegister = function (req) {
    var responseObject =
    {
        "data": {
            "type": "UserRegistration",
            "id": "1",
            "attributes": {
                "status": "inprogress"
            }
        }
    }
    return responseObject;
};
//getWeeklyAverageEarnings
exports.getWeeklyAverageEarnings = function (req) {
    var mockPaymentPiaweDetails = TEMPSTORE.MockPaymentPiaweDetails;
    var claimNumber = req.params.claimId;

    var targetPiaweDetails = _.find(mockPaymentPiaweDetails, function (o) {
        return o.claimNumber === claimNumber;
    });

    var responseObject = {
        "data": {
            "type": "string",
            "id": "string",
            "attributes": {
                "benefits": {
                    "motorVehicle": {},
                    "healthInsurance": {},
                    "accomodation": {},
                    "education": {},
                    "other": {}
                }
            }
        }
    };

    if (targetPiaweDetails !== undefined) {
        responseObject.data.attributes.ordinaryWeeklyWage = targetPiaweDetails.OrdinaryWeeklyWage;
        responseObject.data.attributes.ordinaryWeeklyHours = targetPiaweDetails.OrdinaryWeeklyHours;
        responseObject.data.attributes.anyShiftAllowancesOrOvertime = targetPiaweDetails.AnyShiftAllowancesOrOvertime;
        responseObject.data.attributes.averageShiftAllowance = targetPiaweDetails.AverageShiftAllowance;
        responseObject.data.attributes.averageOvertimeEarnings = targetPiaweDetails.AverageOvertimeEarnings;

        responseObject.data.attributes.benefits.motorVehicle.isActiveBenefit = targetPiaweDetails.benefitsMotorIsActive;
        responseObject.data.attributes.benefits.motorVehicle.averageWeeklyValue = targetPiaweDetails.benefitsMotorAvgWeekly;
        responseObject.data.attributes.benefits.healthInsurance.isActiveBenefit = targetPiaweDetails.benefitsHealthIsActive;
        responseObject.data.attributes.benefits.healthInsurance.averageWeeklyValue = targetPiaweDetails.benefitsHealthAvgWeekly;
        responseObject.data.attributes.benefits.accomodation.isActiveBenefit = targetPiaweDetails.benefitsAccomodationIsActive;
        responseObject.data.attributes.benefits.accomodation.averageWeeklyValue = targetPiaweDetails.benefitsAccomodationAvgWeekly;
        responseObject.data.attributes.benefits.education.isActiveBenefit = targetPiaweDetails.benefitsEducationIsActive;
        responseObject.data.attributes.benefits.education.averageWeeklyValue = targetPiaweDetails.benefitsEducationAvgWeekly;
        responseObject.data.attributes.benefits.other.isActiveBenefit = targetPiaweDetails.benefitsOtherIsActive;
        responseObject.data.attributes.benefits.other.averageWeeklyValue = targetPiaweDetails.benefitsOtherAvgWeekly;

        responseObject.data.attributes.hasMoreThanOneJob = targetPiaweDetails.HasMoreThanOneJob;
        responseObject.data.attributes.isApprentice = targetPiaweDetails.IsApprentice;
        responseObject.data.attributes.hasChangedEmploymentInLastYear = targetPiaweDetails.HasChangedEmploymentInLastYear;
        responseObject.data.attributes.employmentType = targetPiaweDetails.EmploymentType;
        responseObject.data.attributes.daysWorking = targetPiaweDetails.DaysWorking;
        responseObject.data.attributes.leaveTypesInLastYear = targetPiaweDetails.LeaveTypesInLastYear;

    } else {
        responseObject.data.attributes.ordinaryWeeklyWage = "";
        responseObject.data.attributes.ordinaryWeeklyHours = "";
        responseObject.data.attributes.anyShiftAllowancesOrOvertime = "";
        responseObject.data.attributes.averageShiftAllowance = "";
        responseObject.data.attributes.averageOvertimeEarnings = "";

        responseObject.data.attributes.benefits.motorVehicle.isActiveBenefit = "";
        responseObject.data.attributes.benefits.motorVehicle.averageWeeklyValue = "";
        responseObject.data.attributes.benefits.healthInsurance.isActiveBenefit = "";
        responseObject.data.attributes.benefits.healthInsurance.averageWeeklyValue = "";
        responseObject.data.attributes.benefits.accomodation.isActiveBenefit = "";
        responseObject.data.attributes.benefits.accomodation.averageWeeklyValue = "";
        responseObject.data.attributes.benefits.education.isActiveBenefit = "";
        responseObject.data.attributes.benefits.education.averageWeeklyValue = "";
        responseObject.data.attributes.benefits.other.isActiveBenefit = "";
        responseObject.data.attributes.benefits.other.averageWeeklyValue = "";

        responseObject.data.attributes.hasMoreThanOneJob = "";
        responseObject.data.attributes.isApprentice = "";
        responseObject.data.attributes.hasChangedEmploymentInLastYear = "";
        responseObject.data.attributes.employmentType = "";
        responseObject.data.attributes.daysWorking = "";
        responseObject.data.attributes.leaveTypesInLastYear = "";
    }

    return responseObject;
};

exports.timeslot = function (req) {

    var responseObject = { description: SUCCESS.Success202d };
    return responseObject;

};

exports.submitClaimReimbursement = function (req) {

    var responseObject = { description: SUCCESS.Success202c };
    return responseObject;

};

exports.oktaGetUserInfo = function (req) {
    var responseObject = {
        "stateToken": "00Gb7PT9xoZx4p7gPFf2wETj0QF3iDm6qNZ0e-tvTU",
        "expiresAt": "2018-07-02T08:58:04.000Z",
        "status": "PASSWORD_RESET",
        "recoveryType": "ACCOUNT_ACTIVATION",
        "_embedded": {
            "user": {
                "id": "00ufn33vrodqSD5l20h7",
                "profile": {
                    "login": "jipopaduco@topikt.com",
                    "firstName": "jipopaduco",
                    "lastName": "topkit",
                    "locale": "en",
                    "timeZone": "America/Los_Angeles"
                }
            },
            "policy": {
                "complexity": {
                    "minLength": 8,
                    "minLowerCase": 1,
                    "minUpperCase": 1,
                    "minNumber": 1,
                    "minSymbol": 0,
                    "excludeUsername": true
                },
                "age": {
                    "minAgeMinutes": 0,
                    "historyCount": 0
                }
            }
        },
        "_links": {
            "next": {
                "name": "resetPassword",
                "href": "https://icare-sandbox2.oktapreview.com/api/v1/authn/credentials/reset_password",
                "hints": {
                    "allow": [
                        "POST"
                    ]
                }
            },
            "cancel": {
                "href": "https://icare-sandbox2.oktapreview.com/api/v1/authn/cancel",
                "hints": {
                    "allow": [
                        "POST"
                    ]
                }
            }
        }
    };
    return responseObject;
};


exports.validUsersPasswordRecovery = function (req) {
    var input = req.body.data ? req.body.data : undefined;

    if (input === undefined) return false;
    if (!input.attributes.username) return false;

    return true;
};


// validUsersActivateAccountRequest
exports.validUsersActivateAccountRequest = function (req) {
    var input = req.body.data ? req.body.data : undefined;

    if (input === undefined) return false;
    if (!input.attributes.token) return false;

    return true;
};
// usersActivateAccount
exports.usersActivateAccount = function (req) {
    var input = req.body.data ? req.body.data : undefined;
    var responseObject =
    {
        "data": {
            "type": "ActivateUserResponse",
            "id": "00ub0oNGTSWTBKOLGLNR",
            "attributes": {
                "status": "PASSWORD_RESET",
                "expiresAt": "2013-06-24T16:39:18.000Z",
                "recoveryType": "ACCOUNT_ACTIVATION",
                "stateToken": "00ev999XWNZQE_Dv9h",
                "profile": {
                    "firstName": "Isaac",
                    "lastName": "Brock",
                    "login": "isaac.brock@example.com"
                }
            }
        }
    };

    if (input === undefined || !input.attributes.token || input.attributes.token === "expiredtoken1") {
        responseObject = {
            "errorCode": "E0000011",
            "errorSummary": "Invalid token provided",
            "errorLink": "E0000011",
            "errorId": "oaeJ8r7FHHkTG6B0iynM_IWNQ",
            "errorCauses": []
        };
    }

    return responseObject;
};

// validUsersConfirmAccountPostActivationRequest
exports.validUsersConfirmAccountPostActivationRequest = function (req) {
    var input = req.body.data ? req.body.data : undefined;

    if (input === undefined) return false;
    if (!input.attributes.stateToken) return false;
    if (!input.attributes.password) return false;
    if (!input.attributes.recoveryCredentials || input.attributes.recoveryCredentials.length === 1) return false;
    if (!input.attributes.recoveryCredentials[0].question) return false;
    if (!input.attributes.recoveryCredentials[0].answer) return false;

    return true;
};
// usersConfirmAccountPostActivation
exports.usersConfirmAccountPostActivation = function (req) {
    var input = req.body.data ? req.body.data : undefined;
    var responseObject =
    {
        "data": {
            "type": "ConfirmUserPostActivationResponse",
            "id": "00ub0oNGTSWTBKOLGLNR",
            "attributes": {
                "recoveryQuestion": {
                    "question": "What is my dog's name?"
                },
                "status": "ACTIVE",
                "created": "2013-06-24T16:39:18.000Z",
                "activated": "2013-06-24T16:39:19.000Z",
                "statusChanged": "2013-06-24T16:39:19.000Z",
                "lastLogin": "2013-06-24T17:39:19.000Z",
                "lastUpdated": "2013-07-02T21:36:25.344Z",
                "passwordChanged": "2013-07-02T21:36:25.344Z",
                "profile": {
                    "firstName": "Isaac",
                    "lastName": "Brock",
                    "email": "isaac.brock@example.com",
                    "login": "isaac.brock@example.com",
                    "mobilePhone": "555-415-1337"
                }
            }
        }
    };

    if (input === undefined || !input.attributes.stateToken || input.attributes.stateToken === "expiredtoken2") {
        responseObject = {
            "errorCode": "E0000011",
            "errorSummary": "Invalid token provided",
            "errorLink": "E0000011",
            "errorId": "oaeJ8r7FHHkTG6B0iynM_IWNQ",
            "errorCauses": []
        };
    }

    return responseObject;
};




// validUsersActivateAccountRequest
exports.validUsersRecoveryTokenRequest = function (req) {
    var input = req.body.data ? req.body.data : undefined;

    if (input === undefined) return false;
    if (!input.attributes.recoveryToken) return false;

    return true;
};


exports.usersForgetPassword = function (req) {
    var input = req.body.data ? req.body.data : undefined;
    var responseObject =
        {

            "data": {
                "type": "PasswordRecoveryResponse",
                "id": "C0312345667",
                "attributes": {
                    "expiresAt": "2015-11-03T10:15:57.000Z",
                    "status": "RECOVERY",
                    "recoveryToken": "VBQ0gwBp5LyJJFdbmWCM"
                }
            }

        };

    if (input === undefined || !input.attributes.username) {
        responseObject = {
            "errorCode": "E0000011",
            "errorSummary": "Invalid token provided",
            "errorLink": "E0000011",
            "errorId": "oaeJ8r7FHHkTG6B0iynM_IWNQ",
            "errorCauses": []
        };
    }

    return responseObject;
};



exports.usersForgetPasswordTokenVerify = function (req) {
    var input = req.body.data ? req.body.data : undefined;
    var responseObject =
        {


            "data": {
                "type": "RecoveryTokenVerificationResponse",
                "id": "C0312345667",
                "attributes": {
                    "stateToken": "00lMJySRYNz3u_rKQrsLvLrzxiARgivP8FB_1gpmVb",
                    "expiresAt": "2015-11-03T10:15:57.000Z",
                    "status": "RECOVERY"
                }
            },
            "included": [
                {
                    "type": "user",
                    "id": "00ub0oNGTSWTBKOLGLNR",
                    "attributes": {
                        "passwordChanged": "2015-09-08T20:14:45.000Z",
                        "profile": {
                            "login": "dade.murphy@example.com",
                            "firstName": "Dade",
                            "lastName": "Murphy",
                            "locale": "en_US",
                            "timeZone": "America/Los_Angeles"
                        },
                        "recoveryQuestion": {
                            "question": "Who's a major player in the cowboy scene?"
                        }
                    }
                }
            ]


        };

    if (input === undefined || !input.attributes.recoveryToken || input.attributes.recoveryToken === "expiredtoken1") {
        responseObject = {
            "errorCode": "E0000011",
            "errorSummary": "Invalid token provided",
            "errorLink": "E0000011",
            "errorId": "oaeJ8r7FHHkTG6B0iynM_IWNQ",
            "errorCauses": []
        };
    }

    return responseObject;
};



exports.usersForgetPasswordQuestionsVerify = function (req) {
    var input = req.body.data ? req.body.data : undefined;
    var responseObject =
        {

            "data": {
                "type": "RecoveryQuestionVerificationResponse",
                "id": "C0312345667",
                "attributes": {
                    "stateToken": "00lMJySRYNz3u_rKQrsLvLrzxiARgivP8FB_1gpmVb",
                    "expiresAt": "2015-11-03T10:15:57.000Z",
                    "status": "PASSWORD_RESET"
                }
            }


        };

    if (input === undefined || !input.attributes.stateToken || input.attributes.stateToken === "expiredtoken1" || !input.attributes.answer || input.attributes.answer === "12345") {
        responseObject = {
            "errorCode": "E0000011",
            "errorSummary": "Invalid answer provided",
            "errorLink": "E0000011",
            "errorId": "oaeJ8r7FHHkTG6B0iynM_IWNQ",
            "errorCauses": []
        };
    }

    return responseObject;
};



exports.usersResetPassword = function (req) {
    var input = req.body.data ? req.body.data : undefined;
    var responseObject =
        {
            "data": {
                "type": "PasswordResetResponse",
                "id": "C0312345667",
                "attributes": {
                    "expiresAt": "2015-11-03T10:15:57.000Z",
                    "status": "SUCCESS",
                    "sessionToken": "00t6IUQiVbWpMLgtmwSjMFzqykb5QcaBNtveiWlGeM"
                }
            }
        };

    if (input === undefined || !input.attributes.stateToken || input.attributes.stateToken === "expiredtoken1" || !input.attributes.newPassword) {
        responseObject = {
            "errorCode": "E0000011",
            "errorSummary": "Invalid answer provided",
            "errorLink": "E0000011",
            "errorId": "oaeJ8r7FHHkTG6B0iynM_IWNQ",
            "errorCauses": []
        };
    }

    return responseObject;
};

// policyAuthorisation
exports.policyAuthorisation = function (req) {
    var input = req.body;
    var policyUsers = state.MockPolicyUsersEditable;

    // Get user email address from the X-OktaTokenJson 
    var xOktaTokenJson = req.get("X-OktaTokenJson");
    xOktaTokenJson = xOktaTokenJson ? xOktaTokenJson : undefined;
    if (xOktaTokenJson === undefined) return false;

    //Get data from state.MockPolicyUsers based on okta token email
    var userPolicy = _.find(policyUsers, function (o) {
        return _.toLower(o.Email) === _.toLower(xOktaTokenJson);
    });



    var responseObject = {
        "data": {
            "type": "PolicyAuthorisation",
            "attributes": [
                {
                    "policyNumber": "1234567",
                    "identityId": "00ubzemkdtSSmQ9Po0h7",
                    "authorisationRowId": "122",
                    "assertions": [
                        {
                            "role": "claim-admin",
                            "scope": [
                                "ALL"
                            ]
                        }
                    ]
                },
                {
                    "policyNumber": "1234567",
                    "identityId": "00ubzemkdtSSmQ9Po0h7",
                    "authorisationRowId": "123",
                    "assertions": [
                        {
                            "role": "policy-admin",
                            "scope": [
                                "ALL"
                            ]
                        }
                    ]
                }
            ]
        }
    };

    return responseObject;
};


exports.injuryClassifications1 = function (req) {
    var injuryClassification1 = req.query.injuryClassification1;
    var injuryClassification2 = req.query.injuryClassification2;
    var injuryClassification3 = req.query.injuryClassification3;

    if (injuryClassification3) {
        var responseObjectIC4 = {
            "data": {
                "type": "IDC10Code",
                "id": "SO22",
                "attributes": {
                    "name": "SO22",
                    "odgrtwTimeframe": "13"
                }
            }
        };

        return responseObjectIC4;
    }

    if (injuryClassification2) {
        var responseObjectIC2 = {
            "data": [
                {
                    "type": "InjuryClassification3",
                    "id": "Fracture or Break",
                    "attributes": {
                        "name": "Fracture or Break"
                    }
                },
                {
                    "type": "InjuryClassification3",
                    "id": "Cut/Laceration",
                    "attributes": {
                        "name": "Cut/Laceration"
                    }
                }
            ]
        };

        return responseObjectIC2;
    }

    if (injuryClassification1) {
        var responseObjectIC1 = {
            "data": [
                {
                    "type": "InjuryClassification2",
                    "id": "Temple",
                    "attributes": {
                        "name": "Temple"
                    }
                },
                {
                    "type": "InjuryClassification2",
                    "id": "Nose",
                    "attributes": {
                        "name": "Nose"
                    }
                }
            ]
        };

        return responseObjectIC1;
    }

    var responseObject = {
        "data": [
            {
                "type": "InjuryClassification1",
                "id": "Head",
                "attributes": {
                    "name": "Head"
                }
            },
            {
                "type": "InjuryClassification1",
                "id": "Foot",
                "attributes": {
                    "name": "Foot"
                }
            }
        ]
    };

    return responseObject;
};

exports.injuryClassifications2 = function (req) {
    var responseObject = {
        "data": [
            {
                "type": "InjuryClassification2",
                "id": "Temple",
                "attributes": {
                    "name": "Temple"
                }
            },
            {
                "type": "InjuryClassification2",
                "id": "Nose",
                "attributes": {
                    "name": "Nose"
                }
            }
        ]
    };

    return responseObject;
};

exports.injuryClassifications3 = function (req) {
    var responseObject = {
        "data": [
            {
                "type": "InjuryClassification3",
                "id": "Fracture or Break",
                "attributes": {
                    "name": "Fracture or Break"
                }
            },
            {
                "type": "InjuryClassification3",
                "id": "Cut/Laceration",
                "attributes": {
                    "name": "Cut/Laceration"
                }
            }
        ]
    };

    return responseObject;
};

exports.injuryClassifications4 = function (req) {
    var responseObject = {
        "data": {
            "type": "IDC10Code",
            "id": "SO22",
            "attributes": {
                "name": "SO22",
                "odgrtwTimeframe": "13"
            }
        }
    };

    return responseObject;
};
