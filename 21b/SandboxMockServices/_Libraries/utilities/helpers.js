﻿/*
 * Common functions 
 *
 */

// returnError 
exports.returnError = function (res, code, message) {
    return res.json(code, { error: { 'message': message } });
};

// validateAccessToken 
exports.validAccessToken = function (req) {
    var accessToken = req.get("access_token");
    accessToken = accessToken ? accessToken : undefined;

    if (accessToken === undefined) return false;

    // Assume JWT 
    // header.payload.signature
    var jwtArray = accessToken.split('.');
    if (jwtArray.length < 3) return false;
    if (!jwtArray[0]) return false;
    if (!jwtArray[1]) return false;
    if (!jwtArray[2]) return false;

    // Other checking here 

    return true;
};

// validTokenHeader
exports.validTokenHeader = function (req) {
    var xToken = req.get("X-Token");
    xToken = xToken ? xToken : undefined;

    return (xToken !== undefined);
};

// validTrackingIdHeader
exports.validTrackingIdHeader = function (req) {
    var xTrackingId = req.get("X-TrackingID");
    xTrackingId = xTrackingId ? xTrackingId : undefined;

    return (xTrackingId !== undefined);
};

// validOktaTokenJsonHeader
exports.validOktaTokenJsonHeader = function (req) {
    var xOktaTokenJson = req.get("X-OktaTokenJson");
    xOktaTokenJson = xOktaTokenJson ? xOktaTokenJson : undefined;

    return (xOktaTokenJson !== undefined);
};

// validQueryParam
exports.validQueryParam = function (req, q) {
    var input = req.query[q] ? req.query[q] : undefined;
    if (input === undefined) return false;
    return true;
};

// validPolicyNumberRequest
exports.validPolicyNumberRequest = function (req) {
    var input = req.query.date ? req.query.date : undefined;
    if (input === undefined) return false;
    return true;
};


// validClaimRequest 
exports.validClaimRequest = function (req) {
    var input = req.body.data ? req.body.data : undefined;

    if (input === undefined) return false;

    // required:
    //  - notifier.name
    //  - notifier.contact.phone.primary
    //  - injury.occurrence
    //  - employer
    if (!input.attributes.notifier.name.given || !input.attributes.notifier.name.family) return false;
    if (!input.attributes.notifier.contact.phone.primary) return false;
    if (!input.attributes.injury.occurrence.date || !input.attributes.injury.occurrence.time) return false;
    //if (!input.attributes.employer
    //    || !input.attributes.employer.name
    //    || !input.attributes.employer.abn
    //    || !input.attributes.employer.email) return false;

    return true;
};

// validParam 
exports.validParam = function (req, p, shouldTestForInt) {
    var params = req.params ? req.params : undefined;
    if (params === undefined) return false;

    var targetParam = params[p] ? params[p] : undefined;
    if (targetParam === undefined) return false;

    if (shouldTestForInt) {
        var intParam = parseInt(targetParam, 10);
        return _.isInteger(intParam);
    }

    return true;
};

// validAttachmentRequest
exports.validAttachmentRequest = function (req) {
    var input = req.body.meta ? req.body.meta : undefined;
    if (input === undefined) return false;
    if (!input.uploadedby) return false;
    if (!input.uploaderphoneno) return false;
    if (!input.uploaderemail) return false;
    if (!input.filename) return false;
    if (!input.documenttype) return false;
    if (!input.claimnumber) return false;
    if (!input.claimid) return false;

    return true;
};

// validAccountDetails
exports.validAccountDetails = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;

    if (!input.contactNumber) return false;
    if (!input.emailAddress) return false;
    if (!input.contactPreference) return false;

    return true;
};

// validPaymentDetails
exports.validPaymentDetails = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.accountName) return false;
    if (!input.accountNumber) return false;
    if (!input.bsb) return false;
    if (!input.applicablePolicyNumber) return false;

    return true;
};

// validAddUserRequest
exports.validAddUserRequest = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;

    if (!input.givenName) return false;
    if (!input.familyName) return false;
    if (!input.contactNumber) return false;
    if (!input.emailAddress) return false;
    if (!input.policyRoles) return false;
    if (input.policyRoles.length < 1) return false;
    if (!input.policyRoles[0].policyNumber) return false;
    if (!input.policyRoles[0].policyName) return false;
    if (!input.policyRoles[0].role) return false;

    return true;
}

// validUpdateUserRequest
exports.validUpdateUserRequest = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;

    if (!input.policyRoles) return false;
    if (input.policyRoles.length < 1) return false;
    if (!input.policyRoles[0].policyNumber) return false;
    if (!input.policyRoles[0].role) return false;

    return true;
}

// validGetTeamRequest
exports.validGetTeamRequest = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.sort) return false;
    if (!input.sort.columnName) return false;
    if (!input.page) return false;
    if (!input.pageSize) return false;

    return true;
}

// validGetPortalClaimsRequest
exports.validGetPortalClaimsRequest = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.sort) return false;
    if (!input.sort.columnName) return false;
    if (!input.page) return false;
    if (!input.pageSize) return false;

    return true;
}

// validEnquiry
exports.validEnquiry = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.data) return false;
    if (!input.data.attributes) return false;
    if (!input.data.attributes.enquiry) return false;
    if (!input.data.attributes.enquiry.problemTitle) return false;
    if (!input.data.attributes.enquiry.problemDescription) return false;

    return true;
}

// validClaimDocumentRequest 
exports.validClaimDocumentRequest = function (req) {
    var input = req.body.data ? req.body.data : undefined;

    if (input === undefined) return false;

    return true;
};

// validDuplicateClaimCheckRequest 
exports.validDuplicateClaimCheckRequest = function (req) {
    var input = req.body.data ? req.body.data : undefined;

    if (input === undefined) return false;

    if (!input.attributes
        || !input.attributes.worker
        || !input.attributes.worker.name
        || !input.attributes.worker.name.given
        || !input.attributes.worker.name.family
        || !input.attributes.injury
        || !input.attributes.injury.date) return false;

    return true;
};


// getDate
exports.getDate = function (dateString) {
    var date = dateString.split(/\D/);
    return new Date(date[2], date[1] - 1, date[0]);
}

// hasOneDayElapsed 
exports.hasOneDayElapsed = function (previousDate) {
    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    var currentDate = new Date();
    previousDate = new Date(previousDate);
    var diffDays = Math.round(Math.abs((currentDate.getTime() - previousDate.getTime()) / (oneDay)));

    return diffDays >= 1;
}


/*
 * Other functions 
 *
 */

// addClaim 
// object should have been validated prior calling this function 
exports.addClaim = function (req) {
    var input = req.body;

    // Generate a 7-digit random claimNo 
    var claimNo = _.floor(Math.random() * 9000000) + 1000000;
    input.data.attributes.claimNo = claimNo;

    // Add the claim 
    state.AddUpdateClaimRepo.push(input);

    var responseObject = {};
    responseObject = {
        type: input.data.type,
        id: input.data.id,
        attributes: {
            claimNo: _.replace(claimNo, ".0", ""),
            id: input.data.attributes.id
        }
    };

    return responseObject;
};

// updateClaimById 
exports.updateClaimById = function (req) {
    // check if the claimNo exists 
    var existingClaim = _.find(state.AddUpdateClaimRepo, function (o) {
        return _.toString(o.data.attributes.claimNo) === _.toString(req.params.id);
    });

    if (!existingClaim) return false;

    // remove the claim
    state.AddUpdateClaimRepo = _.reject(state.AddUpdateClaimRepo, function (o) {
        return _.toString(o.data.attributes.claimNo) === _.toString(req.params.id);
    });

    // modify input to ensure we retain the claimNo 
    var input = req.body;
    input.data.attributes.claimNo = _.toString(req.params.id);

    // add the claim from input 
    state.AddUpdateClaimRepo.push(input);

    var responseObject = { description: SUCCESS.Success204 };
    return responseObject;
};

// lodgeClaimById 
exports.lodgeClaimById = function (req) {
    // check if the claimNo exists 
    var existingClaim = _.find(state.AddUpdateClaimRepo, function (o) {
        return _.toString(o.data.attributes.claimNo) === _.toString(req.params.id);
    });

    if (!existingClaim) return false;

    // remove the claim
    state.AddUpdateClaimRepo = _.reject(state.AddUpdateClaimRepo, function (o) {
        return _.toString(o.data.attributes.claimNo) === _.toString(req.params.id);
    });

    // modify input to ensure we retain the claimNo 
    var input = req.body;
    input.data.attributes.claimNo = _.toString(req.params.id);

    // add the claim from input 
    state.AddUpdateClaimRepo.push(input);

    var responseObject = { description: SUCCESS.Success202 };
    return responseObject;
};

// addAttachmentByClaimId 
exports.addAttachmentByClaimId = function (req) {
    var responseObject = {};
    responseObject.data = [];

    var item1 = {};
    item1.type = "type1";
    item1.id = "id1";
    item1.attributes = {};
    item1.attributes.method = "method1";
    item1.attributes.url = "url1";

    var item2 = {};
    item2.type = "type2";
    item2.id = "id2";
    item2.attributes = {};
    item2.attributes.method = "method2";
    item2.attributes.url = "url2";

    responseObject.data.push(item1);
    responseObject.data.push(item2);

    return responseObject;
};

// getPolicyNumbers
exports.getPolicyNumbers = function (req) {
    var dateQueryParam = req.query.date;

    var responseObject = {};
    responseObject.items = [];

    if (dateQueryParam !== "27/02/2018") {
        var item1 = {};
        item1.policyNumber = "111234";
        item1.businessName = "EFS Enterprise";

        var item2 = {};
        item2.policyNumber = "2020123";
        item2.businessName = "Grant, Trantow and Raynor";

        responseObject.items.push(item1);
        responseObject.items.push(item2);
    }

    return responseObject;
};

// getCostCentres
exports.getCostCentres = function (req) {
    var responseObject = {};
    responseObject.type = "type";
    responseObject.items = [];

    var item1 = {};
    item1.id = "cc1";
    item1.name = "Cost Centre 3001";
    item1.description = "Cost Centre 3001";

    var item2 = {};
    item2.id = "cc2";
    item2.name = "Cost Centre 3022";
    item2.description = "Cost Centre 3022";

    responseObject.items.push(item1);
    responseObject.items.push(item2);

    return responseObject;
};

// getLocations
exports.getLocations = function (req) {
    var responseObject = {};
    responseObject.type = "type";
    responseObject.items = [];

    var item1 = {};
    item1.id = "loc1";
    item1.name = "Sydney, NSW";
    item1.description = "Sydney, NSW";

    var item2 = {};
    item2.id = "loc2";
    item2.name = "Melbourne, VIC";
    item2.description = "Melbourne, VIC";

    responseObject.items.push(item1);
    responseObject.items.push(item2);

    return responseObject;
};

// getMyDetails
exports.getMyDetails = function (req) {
    var policyUsers = state.MockPolicyUsersEditable;

    // Get user email address from the X-OktaTokenJson 
    var xOktaTokenJson = req.get("X-OktaTokenJson");
    xOktaTokenJson = xOktaTokenJson ? xOktaTokenJson : undefined;
    if (xOktaTokenJson === undefined) return false;

    // check if the user email address exists 
    var targetUser = _.filter(policyUsers, function (o) {
        return _.toLower(_.toString(o.Email)) === _.toLower(_.toString(xOktaTokenJson));
    });

    if (!targetUser || targetUser.length < 1) return false;

    // Create response object 
    var responseObject = {};
    responseObject.contactNumber = targetUser[0].PhoneNumber;
    responseObject.emailAddress = targetUser[0].Email;
    responseObject.contactPreference = "phone";
    responseObject.givenName = targetUser[0].FirstName;
    responseObject.familyName = targetUser[0].LastName;
    responseObject.policyRoles = [];
    _.forEach(targetUser, function (o) {
        var newPolicy = {
            policyNumber: o.policyNumber,
            policyName: o.policyNumber,
            role: o.DetailedAuthDbRole
        };
        responseObject.policyRoles.push(newPolicy);
    });

    return responseObject;
};

// updateMyDetails
exports.updateMyDetails = function (req) {
    var policyUsers = state.MockPolicyUsersEditable;

    // Get user email address from the X-OktaTokenJson 
    var xOktaTokenJson = req.get("X-OktaTokenJson");
    xOktaTokenJson = xOktaTokenJson ? xOktaTokenJson : undefined;
    if (xOktaTokenJson === undefined) return false;

    // check if the user email address exists 
    var targetUser = _.filter(policyUsers, function (o) {
        return _.toLower(_.toString(o.Email)) === _.toLower(_.toString(xOktaTokenJson));
    });

    if (!targetUser || targetUser.length < 1) return false;

    // Create response object 
    var responseObject = {};
    responseObject.contactNumber = req.body.contactNumber;              // just echoing the input 
    responseObject.emailAddress = req.body.emailAddress;                // just echoing the input 
    responseObject.contactPreference = req.body.contactPreference;      // just echoing the input 
    responseObject.givenName = targetUser[0].FirstName;
    responseObject.familyName = targetUser[0].LastName;
    responseObject.policyRoles = [];
    _.forEach(targetUser, function (o) {
        var newPolicy = {
            policyNumber: o.policyNumber,
            policyName: o.policyNumber,
            role: o.DetailedAuthDbRole
        };
        responseObject.policyRoles.push(newPolicy);
    });

    return responseObject;
};

// getPaymentDetails
exports.getPaymentDetails = function (req) {
    var mockBsbBanks = TEMPSTORE.MockBsbBanks;
    var mockPaymentDetails = TEMPSTORE.MockPaymentDetails;

    var xToken = req.get("X-OktaTokenJson");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var targetPaymentDetails = _.filter(mockPaymentDetails, function (o) {
        return _.toLower(_.toString(o.email)) === _.toLower(_.toString(xToken));
    });

    var responseObject = {};
    responseObject.data = [];

    _.forEach(targetPaymentDetails, function (o) {
        var bsbBank = _.find(mockBsbBanks, function (v) {
            return (v.BSB === o.bsb && v.AccountNumber === o.accountNumber);
        });
        var userPaymentDetails = {
            "accountName": (bsbBank !== undefined) ? bsbBank.AccountName : "",
            "accountNumber": o.accountNumber,
            "bsb": o.bsb,
            "bankName": (bsbBank !== undefined) ? bsbBank.BankName : "",
            "policyName": o.policyName,
            "policyNumber": o.policyNumber,
        };
        responseObject.data.push(userPaymentDetails);
    });

    return responseObject;
};

// updatePaymentDetails
exports.updatePaymentDetails = function (req) {
    return req.body;
};

// getBankName 
exports.getBankName = function (req) {
    var bsbBanks = TEMPSTORE.MockBsbBanks;

    // check if the bank exists 
    var targetBank = _.find(bsbBanks, function (o) {
        return _.toString(o.BSB) === _.toString(req.query["bsb"]);
    });
    if (!targetBank) return {
        "BankName": null
    };

    return targetBank;
};

// addUser
exports.addUser = function (req) {
    var input = req.body;

    // Generate a 7-digit random identityId 
    var identityId = _.floor(Math.random() * 9000000) + 1000000;
    input.identityId = _.toString(identityId);

    // Add the user 
    var addedEntries = [];
    _.forEach(input.policyRoles, function (o) {
        var newUser = {
            "IsOktaUser": "",
            "IdentityId": input.identityId,
            "Email": input.emailAddress,
            "FirstName": input.givenName,
            "LastName": input.familyName,
            "OktaRole": "",
            "PhoneNumber": input.contactNumber,
            "DetailedAuthDbRole": o.role,
            "policyNumber": o.policyNumber,
            "associatedClaims": "",
            "numOpenNotificationsClaims": 0
        };

        state.MockPolicyUsersEditable.push(newUser);
        addedEntries.push(newUser);
    });

    var responseObject = addedEntries;

    return responseObject;
};

// getUser 
exports.getUser = function (req) {
    // check if the identityId exists 
    var existingUser = _.filter(state.MockPolicyUsersEditable, function (o) {
        return _.toString(o.IdentityId) === _.toString(req.query.identityId);
    });

    if (!existingUser || existingUser.length == 0) return false;

    var returnValue = {
        "givenName": existingUser[0].FirstName,
        "familyName": existingUser[0].LastName,
        "contactNumber": existingUser[0].PhoneNumber,
        "emailAddress": existingUser[0].Email,
        "identityId": existingUser[0].IdentityId,
        "policyRoles": []
    };

    _.forEach(existingUser, function (o) {
        var policyRole = {
            "policyNumber": o.policyNumber,
            "policyName": o.policyNumber,
            "role": o.DetailedAuthDbRole
        };

        returnValue.policyRoles.push(policyRole);
    });

    return returnValue;
};

// updateUser
exports.updateUser = function (req) {
    // check if the identityId exists 
    var existingUser = _.filter(state.MockPolicyUsersEditable, function (o) {
        return _.toString(o.IdentityId) === _.toString(req.params.identityId);
    });

    if (!existingUser || existingUser.length == 0) return false;

    // create users having the same count as the input policy roles 
    var newUsers = [];
    _.forEach(req.body.policyRoles, function (o) {
        var newUser = {
            "IsOktaUser": existingUser[0].IsOktaUser,
            "IdentityId": existingUser[0].IdentityId,
            "Email": existingUser[0].Email,
            "FirstName": existingUser[0].FirstName,
            "LastName": existingUser[0].LastName,
            "OktaRole": existingUser[0].OktaRole,
            "PhoneNumber": existingUser[0].PhoneNumber,
            "associatedClaims": existingUser[0].associatedClaims,
            "numOpenNotificationsClaims": existingUser[0].numOpenNotificationsClaims,

            // Use the input policy role 
            "policyNumber": o.policyNumber,
            "policyName": o.policyName,
            "DetailedAuthDbRole": o.role
        };

        newUsers.push(newUser);
    });

    // remove the existing user 
    state.MockPolicyUsersEditable = _.reject(state.MockPolicyUsersEditable, function (o) {
        return _.toString(o.IdentityId) === _.toString(req.params.identityId);
    });

    // add the updated user 
    _.forEach(newUsers, function (o) {
        state.MockPolicyUsersEditable.push(o);
    });

    var responseObject = newUsers;

    return responseObject;
};

// deleteUser 
exports.deleteUser = function (req) {
    // do not delete if MockPolicyUsersEditable doesn't exist 
    if (!state.MockPolicyUsersEditable) return true;

    // check if the identityId exists 
    var existingUser = _.filter(state.MockPolicyUsersEditable, function (o) {
        return _.toString(o.IdentityId) === _.toString(req.params.identityId);
    });

    if (!existingUser || existingUser.length == 0) return false;

    // remove the existing user 
    state.MockPolicyUsersEditable = _.reject(state.MockPolicyUsersEditable, function (o) {
        return _.toString(o.IdentityId) === _.toString(req.params.identityId);
    });

    return true;
};


// getTeam 
exports.getTeam = function (req) {
    var stateUsers = state.users;                       // these are users added through the /users/user (Add a new user) endpoint 
    var policyUsers = state.MockPolicyUsersEditable;

    // Get user email address from the X-OktaTokenJson 
    var xOktaTokenJson = req.get("X-OktaTokenJson");
    xOktaTokenJson = xOktaTokenJson ? xOktaTokenJson : undefined;
    if (xOktaTokenJson === undefined) return false;

    var userPoliciesArr = [];

    // Get the policies associated with the current user (MockPolicyUsers) 
    var userPoliciesFromMockPolicyUsers = _.filter(policyUsers, function (o) {
        var isValid = _.toLower(o.Email) === _.toLower(xOktaTokenJson);
        if (isValid) userPoliciesArr.push(o.policyNumber);

        return isValid;
    });

    // Get all users from MockPolicyUsers that has access to the same policies 
    var fromMockPolicyUsers = _.filter(policyUsers, function (o) {
        return _.indexOf(userPoliciesArr, o.policyNumber) > -1;
    });

    // All users 
    var allUsers = [];

    _.forEach(fromMockPolicyUsers, function (o) {
        var user = {};
        user.givenName = o.FirstName;
        user.familyName = o.LastName;
        user.contactNumber = o.PhoneNumber;
        user.emailAddress = o.Email;
        user.role = o.DetailedAuthDbRole;
        user.identityId = o.IdentityId;
        allUsers.push(user);
    });

    // Sorted 
    var sortFilter = req.body.sort.columnName;
    var sortDirectionFilter = req.body.sort.direction;
    var sortedUsers = _.sortBy(allUsers, [function (o) { return o[sortFilter]; }]);         // ascending 
    if (sortDirectionFilter !== "asc") sortedUsers = _.reverse(sortedUsers);                // descending 

    // Paginated 
    var paginatedUsers = _.chunk(sortedUsers, req.body.pageSize);
    var targetPage = paginatedUsers[req.body.page - 1];

    // Response object 
    var responseObject = {};
    responseObject.totalResults = sortedUsers.length;
    responseObject.totalPages = paginatedUsers.length;
    responseObject.currentPage = req.body.page;
    responseObject.items = targetPage;

    return responseObject;
};

// getUserSummary
exports.getUserSummary = function (req) {
    var policyUsers = TEMPSTORE.MockPolicyUsers;

    // Get target userId from the X-Token 
    var xToken = req.get("X-Token");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    // check if the userId exists 
    var targetUser = _.find(policyUsers, function (o) {
        return _.toLower(_.toString(o.Email)) === _.toLower(_.toString(xToken));
    });

    if (!targetUser) return false;

    // Create UserSummary response object 
    var userSummary = {};
    userSummary.givenName = targetUser.FirstName;
    userSummary.familyName = targetUser.LastName;
    userSummary.userId = targetUser.Email;
    userSummary.numOpenNotificationsClaims = targetUser.numOpenNotificationsClaims;


    return userSummary;
}

// getPortalClaims 
exports.getPortalClaims = function (req) {
    var mockClaimSummary = TEMPSTORE.MockClaimSummary;
    var mockClaimDetails = TEMPSTORE.MockClaimDetails;
    var mockPolicyUsers = TEMPSTORE.MockPolicyUsers;

    // Attach ClaimDetails to its corresponding ClaimSummary 
    var mockClaimSummaryAndDetails = [];
    _.forEach(mockClaimSummary, function (mcs, index) {
        var combined = mcs;
        combined.data = mockClaimDetails[index].data;
        mockClaimSummaryAndDetails.push(combined);
    });

    // Get target userId from the X-Token 
    var xToken = req.get("X-Token");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var userPoliciesArr = [];
    var associatedClaimsPerPolicy = {};

    // Get the policies associated with the current user 
    var userPolicies = _.filter(mockPolicyUsers, function (o) {
        var isValid = _.toLower(o.Email) === _.toLower(xToken);
        if (isValid) {
            userPoliciesArr.push(o.policyNumber);
            associatedClaimsPerPolicy[o.policyNumber] = _.split(o.associatedClaims, ",");
            _.forEach(associatedClaimsPerPolicy[o.policyNumber], function (v, i) {
                associatedClaimsPerPolicy[o.policyNumber][i] = _.trim(v);
            });

        }
        return isValid;
    });

    // Claims list per user 
    var targetMockClaimSummaryAndDetails = [];
    _.forEach(userPoliciesArr, function (o, i) {
        var targetClaims = [];

        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] === "*") {
            // mockClaimSummary
            targetClaims = _.filter(mockClaimSummaryAndDetails, function (v) {
                return o === v.policyNumber;
            });
            targetMockClaimSummaryAndDetails = _.concat(targetMockClaimSummaryAndDetails, targetClaims);
        }

        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] !== "*" && associatedClaimsPerPolicy[o][0] !== "null") {
            // mockClaimSummary
            targetClaims = _.filter(mockClaimSummaryAndDetails, function (v) {
                return _.indexOf(associatedClaimsPerPolicy[o], v.claimNumber) > -1;
            });
            targetMockClaimSummaryAndDetails = _.concat(targetMockClaimSummaryAndDetails, targetClaims);
        }
    });

    var filteredClaims = targetMockClaimSummaryAndDetails;

    // Filter: Claim List
    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    if (req.body.filter) {
        var policyNumberFilter = req.body.filter.policy ? req.body.filter.policy : undefined;
        var claimStatusFilterArr = req.body.filter.claimStatus ? req.body.filter.claimStatus : undefined;
        var claimStatusFilterArrLowerCased = _.map(claimStatusFilterArr, function (o) { return _.toLower(o); });
        var injuryDateFilter = req.body.filter.injuryDate ? req.body.filter.injuryDate : undefined;
        var expectedTimeOffWorkFilter = req.body.filter.expectedTimeOffWork ? req.body.filter.expectedTimeOffWork : undefined;
        var givenNameFilter = req.body.filter.name.givenName ? req.body.filter.name.givenName : undefined;
        var familyNameFilter = req.body.filter.name.familyName ? req.body.filter.name.familyName : undefined;
        var claimNumberFilter = req.body.filter.claimNumber ? req.body.filter.claimNumber : undefined;

        filteredClaims = _.filter(filteredClaims, function (o) {
            // Assume first that the item is to be included 
            var returnValue = true;

            // If any of the filters has value and the value is not contained in the item, then do not include that item 

            // policy 
            if (policyNumberFilter !== undefined
                && policyNumberFilter !== "undefined"
                && o.policyNumber.indexOf(policyNumberFilter) === -1) returnValue = false;

            // claimStatus: ["open", "saved", "closed"] 
            if (claimStatusFilterArr !== undefined
                && claimStatusFilterArr !== "undefined"
                && claimStatusFilterArr.length > 0
                && _.indexOf(claimStatusFilterArrLowerCased, _.toLower(o.claimStatus)) === -1) returnValue = false;

            // injuryDate: "last30days" or "last60days" or "last365days"
            if (injuryDateFilter !== undefined && injuryDateFilter !== "undefined") {
                //var injuryDateFilterNum = parseInt(injuryDateFilter.replace("last", "").replace("days", ""), 10);
                var currentDate = new Date();
                var injuryDate = helpers.getDate(o.dateOfInjury);
                var diffDays = Math.round(Math.abs((currentDate.getTime() - injuryDate.getTime()) / (oneDay)));

                switch (injuryDateFilter) {
                    case 'last30days':
                        if (diffDays > 30) returnValue = false;
                        break;

                    case 'last60days':
                        if (diffDays > 60) returnValue = false;
                        break;

                    case 'last365days':
                        if (diffDays > 365) returnValue = false;
                        break;
                }
            }

            // expectedTimeOffWork: ["0-2weeks", "2-4weeks", "4-6weeks", "6-8weeks", "8+weeks"] 
            if (expectedTimeOffWorkFilter !== undefined
                && expectedTimeOffWorkFilter !== "undefined"
                && expectedTimeOffWorkFilter.length > 0) {

                var isIncluded = [];
                _.forEach(expectedTimeOffWorkFilter, function (value, index) {

                    var returnToWorkTimeframeNum = parseInt(o.returnToWorkTimeframe, 10);
                    switch (value) {
                        case '0-2weeks':    // 0 - 14 days 
                            if (returnToWorkTimeframeNum <= 14) isIncluded.push(true);
                            break;

                        case '2-4weeks':    // 14 - 28 days 
                            if (returnToWorkTimeframeNum >= 14 && returnToWorkTimeframeNum <= 28) isIncluded.push(true);
                            break;

                        case '4-6weeks':    // 28 - 42 days 
                            if (returnToWorkTimeframeNum >= 28 && returnToWorkTimeframeNum <= 42) isIncluded.push(true);
                            break;

                        case '6-8weeks':    // 42 - 56 days 
                            if (returnToWorkTimeframeNum >= 42 && returnToWorkTimeframeNum <= 56) isIncluded.push(true);
                            break;

                        case '8+weeks':     // 56+ days 
                            if (returnToWorkTimeframeNum >= 56) isIncluded.push(true);
                            break;

                        default:
                            isIncluded.push(false);
                    }
                });

                if (_.findIndex(isIncluded, function (z) { return z; }) === -1) returnValue = false;
            }

            // givenNameFilter 
            if (givenNameFilter !== undefined
                && givenNameFilter !== "undefined"
                && _.toLower(o.workerGivenName).indexOf(_.toLower(givenNameFilter)) === -1) returnValue = false;

            // familyNameFilter 
            if (familyNameFilter !== undefined
                && familyNameFilter !== "undefined"
                && _.toLower(o.workerFamilyName).indexOf(_.toLower(familyNameFilter)) === -1) returnValue = false;

            // claimNumberFilter 
            if (claimNumberFilter !== undefined
                && claimNumberFilter !== "undefined"
                && o.claimNumber.indexOf(claimNumberFilter) === -1) returnValue = false;

            return returnValue;
        });
    }

    // Remove data after filtering as it will make the response object too big 
    var filteredClaimsWithoutDetails = [];
    _.forEach(filteredClaims, function (o) {
        var claim = o;
        claim.data = null;
        filteredClaimsWithoutDetails.push(claim);
    });
    filteredClaims = filteredClaimsWithoutDetails;


    // Sort 
    var sortFilter = req.body.sort.columnName;
    switch (sortFilter) {
        case 'recoveryTimeframe':
            sortFilter = "returnToWorkTimeframe";
            break;

        case 'lastName':
            sortFilter = "RowNumber";
            break;
    }
    var sortDirectionFilter = req.body.sort.direction;
    var sortedClaims = _.sortBy(filteredClaims, [function (o) {
        switch (sortFilter) {
            case 'dateOfInjury':
                return helpers.getDate(o[sortFilter]);
                break;
            default:
                return parseInt(o[sortFilter], 10);
                break;
        }
    }]);      // ascending 
    if (sortDirectionFilter !== "asc") sortedClaims = _.reverse(sortedClaims);                  // descending 

    // Paginate 
    var paginatedClaims = _.chunk(sortedClaims, req.body.pageSize);
    var targetPage = paginatedClaims[req.body.page - 1];

    // Response object 
    var responseObject = {};
    responseObject.totalResults = sortedClaims.length;
    responseObject.totalPages = paginatedClaims.length;
    responseObject.currentPage = req.body.page;
    responseObject.items = targetPage;

    return responseObject;
};

// getClaimByClaimId_deprecated 
exports.getClaimByClaimId_deprecated = function (req) {
    var claimNumber = req.params.id;

    // Get claimDetail from TEMPSTORE.MockClaimDetails given the claim number 
    var targetClaimDetails = _.find(TEMPSTORE.MockClaimDetails, function (o) {
        return o.data.attributes.claimNo === claimNumber;
    });
    if (!targetClaimDetails) return false;


    /* Generate output JSON in the schema as requested by FED */
    var output = { "claimOverviewSections": [] };
    var employerDetails = { "id": "employerDetails", "fields": [] };
    var injuredPersonDetails = { "id": "injuredPersonDetails", "fields": [] };
    var injuryDetails = { "id": "injuryDetails", "fields": [] };
    var injuredPersonWorkDetails = { "id": "injuredPersonWorkDetails", "fields": [] };
    var uploadSupportingDocuments = { "id": "uploadSupportingDocuments", "fields": [] };

    // Attach claimSummry data 
    var targetClaimSummary = _.find(TEMPSTORE.MockClaimSummary, function (o) {
        return o.claimNumber === claimNumber;
    });
    if (!targetClaimSummary) return false;
    output.claimNumber = targetClaimSummary.claimNumber;
    output.claimName = targetClaimSummary.claimName;
    output.policyNumber = targetClaimSummary.policyNumber;
    output.policyName = targetClaimSummary.policyName;
    output.claimStatus = targetClaimSummary.claimStatus;
    output.dateOfInjury = targetClaimSummary.dateOfInjury;
    output.expectedTimeOff = targetClaimSummary.returnToWorkTimeframe;
    output.liability = targetClaimSummary.liabilityStatus;
    output.certificateOfCapacityExpiry = targetClaimSummary.certCapacityExpires;
    output.wasIncident = targetClaimSummary.wasIncident;
    output.wasMedical = targetClaimSummary.wasMedical;
    output.wasWage = targetClaimSummary.wasWage;
    output.workStatusCode = targetClaimSummary.workStatusCode;
    output.workStatusStartDate = targetClaimSummary.workStatusStartDate;

    // Attach claimContacts data 
    var targetClaimContactIds = targetClaimSummary.relatedContacts;
    if (!targetClaimContactIds || targetClaimContactIds.split(",").length < 1) targetClaimDetails.data.attributes.claimContacts = null;
    targetClaimContactIds = targetClaimContactIds.split(",");
    var targetClaimContactArray = [];
    _.forEach(targetClaimContactIds, function (value) {
        var detail = _.find(TEMPSTORE.MockClaimContacts, function (o) {
            return o.contactId === value;
        });

        if (detail) targetClaimContactArray.push(detail);
    });
    output.claimContacts = targetClaimContactArray;

    // Employer Details 
    employerDetails.fields.push({
        "id": "employerRepresentativeFirstName",
        "value": targetClaimDetails.data.attributes.employer.representative.name.given
    });
    employerDetails.fields.push({
        "id": "employerRepresentativeLastName",
        "value": targetClaimDetails.data.attributes.employer.representative.name.family
    });
    employerDetails.fields.push({
        "id": "employerBestContactNumber",
        "value": targetClaimDetails.data.attributes.employer.representative.contact.phone.primary
    });
    employerDetails.fields.push({
        "id": "employerEmailAddress",
        "value": targetClaimDetails.data.attributes.employer.email
    });
    employerDetails.fields.push({
        "id": "injuryDate",
        "value": targetClaimDetails.data.attributes.injury.occurrence.date
    });
    employerDetails.fields.push({
        "id": "injuryTime",
        "value": targetClaimDetails.data.attributes.injury.occurrence.time
    });
    employerDetails.fields.push({
        "id": "associatedPolicy",
        "value": targetClaimDetails.data.attributes.employer.policyNo
    });
    employerDetails.fields.push({
        "id": "location",
        "value": targetClaimDetails.data.attributes.employer.suburb
    });
    employerDetails.fields.push({
        "id": "costCenter",
        "value": targetClaimDetails.data.attributes.employer.costCenter
    });
    employerDetails.fields.push({
        "id": "abn",
        "value": targetClaimDetails.data.attributes.employer.abn
    });
    employerDetails.fields.push({
        "id": "relationshipToInjuredPerson",
        "value": targetClaimDetails.data.attributes.employer.representative.contact.relationship.worker
    });
    employerDetails.fields.push({
        "id": "employerName",
        "value": targetClaimDetails.data.attributes.employer.name
    });
    employerDetails.fields.push({
        "id": "policyNo",
        "value": targetClaimDetails.data.attributes.employer.policyNo
    });
    employerDetails.fields.push({
        "id": "wicCode",
        "value": "WIC Code"
    });

    // Injured Person's Details 
    injuredPersonDetails.fields.push({
        "id": "workerFirstName",
        "value": targetClaimDetails.data.attributes.worker.name.given
    });
    injuredPersonDetails.fields.push({
        "id": "workerLastName",
        "value": targetClaimDetails.data.attributes.worker.name.family
    });
    injuredPersonDetails.fields.push({
        "id": "dateOfBirth",
        "value": targetClaimDetails.data.attributes.worker.dateOfBirth
    });
    injuredPersonDetails.fields.push({
        "id": "gender",
        "value": targetClaimDetails.data.attributes.worker.gender
    });
    injuredPersonDetails.fields.push({
        "id": "primaryPhone",
        "value": targetClaimDetails.data.attributes.worker.contact.phone.primary
    });
    injuredPersonDetails.fields.push({
        "id": "email",
        "value": targetClaimDetails.data.attributes.worker.contact.email
    });
    var homeAddress = targetClaimDetails.data.attributes.worker.address[0].addressLine1
        + " " + targetClaimDetails.data.attributes.worker.address[0].suburb
        + " " + targetClaimDetails.data.attributes.worker.address[0].city
        + " " + targetClaimDetails.data.attributes.worker.address[0].state
        + " " + targetClaimDetails.data.attributes.worker.address[0].postCode
        + " " + targetClaimDetails.data.attributes.worker.address[0].country;
    injuredPersonDetails.fields.push({
        "id": "homeAddress",
        "value": homeAddress
    });
    injuredPersonDetails.fields.push({
        "id": "sameAsResidential",
        "value": targetClaimDetails.data.attributes.worker.address[1].sameAsResidential
    });
    var postalAddress = targetClaimDetails.data.attributes.worker.address[1].addressLine1
        + " " + targetClaimDetails.data.attributes.worker.address[1].suburb
        + " " + targetClaimDetails.data.attributes.worker.address[1].city
        + " " + targetClaimDetails.data.attributes.worker.address[1].state
        + " " + targetClaimDetails.data.attributes.worker.address[1].postCode
        + " " + targetClaimDetails.data.attributes.worker.address[1].country;
    postalAddress = targetClaimDetails.data.attributes.worker.address[1].sameAsResidential ? homeAddress : postalAddress;
    injuredPersonDetails.fields.push({
        "id": "postalAddress",
        "value": postalAddress
    });
    injuredPersonDetails.fields.push({
        "id": "interpreterRequired",
        "value": targetClaimDetails.data.attributes.worker.interpreter.required
    });
    injuredPersonDetails.fields.push({
        "id": "languageForCommunication",
        "value": targetClaimDetails.data.attributes.worker.languageForCommunication
    });

    // Injury Details 
    injuryDetails.fields.push({
        "id": "injuryDate",
        "value": targetClaimDetails.data.attributes.injury.occurrence.date
    });
    injuryDetails.fields.push({
        "id": "injuryTime",
        "value": targetClaimDetails.data.attributes.injury.occurrence.time
    });
    injuryDetails.fields.push({
        "id": "multiple",
        "value": targetClaimDetails.data.attributes.injury.multiple
    });
    injuryDetails.fields.push({
        "id": "injuryWhatHow",
        "value": targetClaimDetails.data.attributes.injury.description
    });
    injuryDetails.fields.push({
        "id": "employerNotifiedOn",
        "value": targetClaimDetails.data.attributes.injury.employerNotifiedOn
    });
    injuryDetails.fields.push({
        "id": "medicalTreatment",
        "value": targetClaimDetails.data.attributes.injury.medicalTreatment.required
    });
    injuryDetails.fields.push({
        "id": "incidentOnly",
        "value": targetClaimSummary.wasIncident ? "Yes" : "No"
    });
    injuryDetails.fields.push({
        "id": "inTheCourseOfEmployment",
        "value": targetClaimDetails.data.attributes.injury.occurrence.inTheCourseOfEmployment
    });
    injuryDetails.fields.push({
        "id": "injuryDescription",
        "value": targetClaimDetails.data.attributes.injury.description
    });
    injuryDetails.fields.push({
        "id": "dutyStatusCode",
        "value": "Duty status code"
    });

    // Injured Person's Work Details 
    injuredPersonWorkDetails.fields.push({
        "id": "hasMultipleEmployments",
        "value": targetClaimDetails.data.attributes.worker.workDetail.hasMultipleEmployments
    });
    injuredPersonWorkDetails.fields.push({
        "id": "hadTimeOffWorkDueToInjury",
        "value": targetClaimDetails.data.attributes.worker.workDetail.hadTimeOffWorkDueToInjury
    });
    injuredPersonWorkDetails.fields.push({
        "id": "workCapacity",
        "value": targetClaimDetails.data.attributes.worker.workDetail.workCapacity
    });
    injuredPersonWorkDetails.fields.push({
        "id": "dateStoppedWork",
        "value": targetClaimDetails.data.attributes.worker.workDetail.dateStoppedWork
    });
    injuredPersonWorkDetails.fields.push({
        "id": "hasReturnedToWork",
        "value": targetClaimDetails.data.attributes.worker.workDetail.hasReturnedToWork
    });
    injuredPersonWorkDetails.fields.push({
        "id": "anticipatedLengthOffWork",
        "value": targetClaimDetails.data.attributes.worker.workDetail.anticipatedLengthOffWork
    });
    injuredPersonWorkDetails.fields.push({
        "id": "employmentStartDate",
        "value": targetClaimDetails.data.attributes.worker.workDetail.employmentStartDate
    });
    injuredPersonWorkDetails.fields.push({
        "id": "employmentType",
        "value": targetClaimDetails.data.attributes.worker.wageDetail.employmentType
    });
    injuredPersonWorkDetails.fields.push({
        "id": "daysWorking",
        "value": targetClaimDetails.data.attributes.worker.wageDetail.daysWorking
    });
    injuredPersonWorkDetails.fields.push({
        "id": "weeklyWage",
        "value": targetClaimDetails.data.attributes.worker.wageDetail.weeklyWage
    });
    injuredPersonWorkDetails.fields.push({
        "id": "weeklyHours",
        "value": targetClaimDetails.data.attributes.worker.wageDetail.weeklyHours
    });
    injuredPersonWorkDetails.fields.push({
        "id": "hasShiftAllowances",
        "value": targetClaimDetails.data.attributes.worker.wageDetail.hasShiftAllowances
    });
    injuredPersonWorkDetails.fields.push({
        "id": "weeklyShiftAllowance",
        "value": targetClaimDetails.data.attributes.worker.wageDetail.weeklyShiftAllowance
    });
    injuredPersonWorkDetails.fields.push({
        "id": "weeklyOvertimeEarnings",
        "value": targetClaimDetails.data.attributes.worker.wageDetail.weeklyOvertimeEarnings
    });
    injuredPersonWorkDetails.fields.push({
        "id": "leaveTakenInLastYear",
        "value": targetClaimDetails.data.attributes.worker.wageDetail.leaveTakenInLastYear
    });
    injuredPersonWorkDetails.fields.push({
        "id": "nonMonentaryAllowances",
        "value": targetClaimDetails.data.attributes.worker.wageDetail.nonMonentaryAllowances
    });
    injuredPersonWorkDetails.fields.push({
        "id": "occupation",
        "value": targetClaimDetails.data.attributes.worker.workDetail.occupation
    });

    // Upload Supporting Documents 
    uploadSupportingDocuments.fields.push({
        "id": "certificateOfCapacity",
        "value": targetClaimDetails.data.attributes.documents.occurrence.certificateOfCapacity
    });
    uploadSupportingDocuments.fields.push({
        "id": "medicalDetails",
        "value": targetClaimDetails.data.attributes.documents.occurrence.medicalDetails
    });
    uploadSupportingDocuments.fields.push({
        "id": "wageDetails",
        "value": targetClaimDetails.data.attributes.documents.occurrence.wageDetails
    });
    uploadSupportingDocuments.fields.push({
        "id": "other",
        "value": targetClaimDetails.data.attributes.documents.occurrence.other
    });

    // Remove fields with empty values 
    _.remove(employerDetails.fields, function (o) { typeof o.value === 'undefined' || o.value === null });
    _.remove(injuredPersonDetails.fields, function (o) { typeof o.value === 'undefined' || o.value === null });
    _.remove(injuryDetails.fields, function (o) { typeof o.value === 'undefined' || o.value === null });
    _.remove(injuredPersonWorkDetails.fields, function (o) { typeof o.value === 'undefined' || o.value === null });
    _.remove(uploadSupportingDocuments.fields, function (o) { typeof o.value === 'undefined' || o.value === null });

    // Attach the claimOverviewSections data 
    output.claimOverviewSections.push(employerDetails);
    output.claimOverviewSections.push(injuredPersonDetails);
    output.claimOverviewSections.push(injuryDetails);
    output.claimOverviewSections.push(injuredPersonWorkDetails);
    //output.claimOverviewSections.push(uploadSupportingDocuments);

    // Additional properties 
    output.injuryDescription = targetClaimDetails.data.attributes.injury.description;

    return output;
};

// getClaimByClaimId 
exports.getClaimByClaimId = function (req) {
    var claimNumber = req.params.id;

    // Get claimDetail from TEMPSTORE.MockClaimDetails given the claim number 
    var targetClaimDetails = _.find(TEMPSTORE.MockClaimDetails, function (o) {
        return o.data.attributes.claimNo === claimNumber;
    });
    if (!targetClaimDetails) return false;


    /* Generate output JSON */
    var output = targetClaimDetails;

    // Attach claimSummry data 
    var targetClaimSummary = _.find(TEMPSTORE.MockClaimSummary, function (o) {
        return o.claimNumber === claimNumber;
    });
    if (!targetClaimSummary) return false;
    output.data.attributes.claimStatus = targetClaimSummary.claimStatus;
    output.data.attributes.liabilityStatus = targetClaimSummary.liabilityStatus;
    output.data.attributes.certificateCapacityExpires = targetClaimSummary.certCapacityExpires;
    output.data.attributes.expectedTimeOffWork = targetClaimSummary.returnToWorkTimeframe;
    output.data.attributes.triageSegment = targetClaimSummary.triageSegment;
    output.data.attributes.wasIncident = targetClaimSummary.wasIncident;
    output.data.attributes.wasMedical = targetClaimSummary.wasMedical;
    output.data.attributes.wasWage = targetClaimSummary.wasWage;

    // Attach claimContacts data 
    var targetClaimContactIds = targetClaimSummary.relatedContacts;
    if (!targetClaimContactIds || targetClaimContactIds.split(",").length < 1) targetClaimDetails.data.attributes.claimContacts = null;
    targetClaimContactIds = targetClaimContactIds.split(",");
    var targetClaimContactArray = [];
    _.forEach(targetClaimContactIds, function (value) {
        var detail = _.find(TEMPSTORE.MockClaimContacts, function (o) {
            return o.contactId === value;
        });

        if (detail) targetClaimContactArray.push(detail);
    });
    output.data.attributes.claimContacts = targetClaimContactArray;

    return output;
};

// enquiryAgainstClaimByClaimId 
exports.enquiryAgainstClaimByClaimId = function (req) {
    // check if the claimNo exists 
    var existingClaim = _.find(state.AddUpdateClaimRepo, function (o) {
        return _.toString(o.data.attributes.claimNo) === _.toString(req.params.id);
    });

    if (!existingClaim) {
        existingClaim = _.find(TEMPSTORE.MockClaimDetails, function (o) {
            return _.toString(o.data.attributes.claimNo) === _.toString(req.params.id);
        });
    }

    if (!existingClaim) return false;

    // modify input to add an enquiry ID (Enquiry-{claimNo})
    var input = req.body;
    input.data.attributes.enquiry.problemCode = "Enquiry-" + existingClaim.data.attributes.claimNo;

    // add enquiry to repo 
    state.EnquiryAgainstClaimRepo.push(input);

    var responseObject = { description: SUCCESS.Success202b };
    return responseObject;
};

// getClaimsDocuments 
exports.getClaimsDocuments = function (req) {
    var claimDocuments = TEMPSTORE.MockDocuments;
    var policyUsers = TEMPSTORE.MockPolicyUsers;

    // Get target userId from the X-Token 
    var xToken = req.get("X-Token");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var userPoliciesArr = [];
    var associatedClaimsPerPolicy = {};

    // Get the policies associated with the current user 
    var userPolicies = _.filter(policyUsers, function (o) {
        var isValid = _.toLower(o.Email) === _.toLower(xToken);
        if (isValid) {
            userPoliciesArr.push(o.policyNumber);
            associatedClaimsPerPolicy[o.policyNumber] = _.split(o.associatedClaims, ",");
            _.forEach(associatedClaimsPerPolicy[o.policyNumber], function (v, i) {
                associatedClaimsPerPolicy[o.policyNumber][i] = _.trim(v);
            });

        }
        return isValid;
    });

    // Claims list per user 
    var claimDocumentsList = [];
    _.forEach(userPoliciesArr, function (o, i) {
        var targetDocuments = [];
        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] === "*") {
            targetDocuments = _.filter(claimDocuments, function (v) {
                return v.claimNumber === req.params.id;
            });

            claimDocumentsList = _.concat(claimDocumentsList, targetDocuments);
        }

        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] !== "*" && associatedClaimsPerPolicy[o][0] !== "null") {
            targetDocuments = _.filter(claimDocuments, function (v) {
                return _.indexOf(associatedClaimsPerPolicy[o], v.claimNumber) > -1;
            });

            claimDocumentsList = _.concat(claimDocumentsList, targetDocuments);
        }
    });

    if (_.size(claimDocumentsList) === 0) return false;

    var responseObject = {
        "data": {
            "type": "documents",
            "id": req.params.id,
            "attributes": {
                "occurrence": {
                    "certificateOfCapacity": [],
                    "medicalPayments": [],
                    "weeklyPayments": [],
                    "healthAndRecovery": [],
                    "other": []
                }
            }
        }
    };

    _.forEach(claimDocumentsList, function (document) {
        var docToPush = {
            "documentId": document.documentId,
            "url": document.url,
            "title": document.title,
            "fileSize": document.fileSize,
            "fileType": document.fileType,
            "uploadedDate": document.uploadedDate
        };

        switch (document.documentType) {
            case 'certCapacity':
                docToPush.certificateOfCapacityDate = document.certCapacityDate;
                responseObject.data.attributes.occurrence.certificateOfCapacity.push(docToPush);
                break;
            case 'medicalPayment':
                responseObject.data.attributes.occurrence.medicalPayments.push(docToPush);
                break;
            case 'weeklyPayment':
                responseObject.data.attributes.occurrence.weeklyPayments.push(docToPush);
                break;
            case 'healthRecovery':
                responseObject.data.attributes.occurrence.healthAndRecovery.push(docToPush);
                break;
            case 'other':
                responseObject.data.attributes.occurrence.other.push(docToPush);
                break;
        }
    });

    // Remove fields with empty values 
    if (_.size(responseObject.data.attributes.occurrence.certificateOfCapacity) === 0)
        delete responseObject.data.attributes.occurrence.certificateOfCapacity;
    if (_.size(responseObject.data.attributes.occurrence.medicalPayments) === 0)
        delete responseObject.data.attributes.occurrence.medicalPayments;
    if (_.size(responseObject.data.attributes.occurrence.weeklyPayments) === 0)
        delete responseObject.data.attributes.occurrence.weeklyPayments;
    if (_.size(responseObject.data.attributes.occurrence.healthAndRecovery) === 0)
        delete responseObject.data.attributes.occurrence.healthAndRecovery;
    if (_.size(responseObject.data.attributes.occurrence.other) === 0)
        delete responseObject.data.attributes.occurrence.other;

    return responseObject;
};

// postClaimsDocuments 
exports.postClaimsDocuments = function (req) {
    var claimDocuments = TEMPSTORE.MockDocuments;
    var policyUsers = TEMPSTORE.MockPolicyUsers;
    var input = req.body.data;

    // Get target userId from the X-Token 
    var xToken = req.get("X-Token");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var userPoliciesArr = [];
    var associatedClaimsPerPolicy = {};

    // Get the policies associated with the current user 
    var userPolicies = _.filter(policyUsers, function (o) {
        var isValid = _.toLower(o.Email) === _.toLower(xToken);
        if (isValid) {
            userPoliciesArr.push(o.policyNumber);
            associatedClaimsPerPolicy[o.policyNumber] = _.split(o.associatedClaims, ",");
            _.forEach(associatedClaimsPerPolicy[o.policyNumber], function (v, i) {
                associatedClaimsPerPolicy[o.policyNumber][i] = _.trim(v);
            });

        }
        return isValid;
    });

    // Claims list per user 
    var claimDocumentsList = [];
    _.forEach(userPoliciesArr, function (o, i) {
        var targetDocuments = [];
        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] === "*") {
            targetDocuments = _.filter(claimDocuments, function (v) {
                return v.claimNumber === req.params.id;
            });

            if (_.size(targetDocuments) === 0) {
                targetDocuments = _.filter(claimDocuments, function (v) {
                    return v.claimNumber === "*";
                });
            }

            claimDocumentsList = _.concat(claimDocumentsList, targetDocuments);
        }

        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] !== "*" && associatedClaimsPerPolicy[o][0] !== "null") {
            targetDocuments = _.filter(claimDocuments, function (v) {
                return _.indexOf(associatedClaimsPerPolicy[o], v.claimNumber) > -1;
            });

            if (_.size(targetDocuments) === 0) {
                targetDocuments = _.filter(claimDocuments, function (v) {
                    return v.claimNumber === "*";
                });
            }

            claimDocumentsList = _.concat(claimDocumentsList, targetDocuments);
        }
    });

    var targetDocuments = [];
    _.forEach(claimDocumentsList, function (document) {
        var docToPush = {
            "documentId": document.documentId,
            "url": document.url,
            "title": document.title,
            "fileSize": document.fileSize,
            "fileType": document.fileType,
            "uploadedDate": document.uploadedDate
        };
        targetDocuments.push(docToPush);
    });

    // Sorted
    targetDocuments = _.sortBy(targetDocuments, [function (o) { return new Date(o.uploadedDate); }]).reverse();

    // Paginate 
    var paginatedClaimDocumentsList = _.chunk(targetDocuments, input.attributes.pageSize);
    var targetPage = paginatedClaimDocumentsList[input.attributes.page - 1];

    var responseObject = {
        "data": {
            "type": "string",
            "id": req.params.id,
            "attributes": {
                "totalResults": targetDocuments.length,
                "totalPages": paginatedClaimDocumentsList.length,
                "currentPage": input.attributes.page,
                "items": (_.size(targetPage) === 0) ? [] : targetPage
            }
        }
    };

    return responseObject;
};

// updateClaimDocumentsById
exports.updateClaimDocumentsById = function (req) {
    var claimDocuments = TEMPSTORE.MockDocuments;
    var input = req.body;
    var claimNumber = _.toString(req.params.id);

    // check if the claimNo exists 
    var existingClaimDocuments = _.filter(claimDocuments, function (o) {
        return _.toString(o.claimNumber) === _.toString(req.params.id);
    });

    //if (_.size(existingClaimDocuments) === 0) return false;


    // Update the documents
    // TODO: Not needed now 

    var responseObject = { description: SUCCESS.Success204 };
    return responseObject;
};


// getDuplicateClaim 
exports.getDuplicateClaim = function (req) {
    var mockClaimSummary = TEMPSTORE.MockClaimSummary;
    var mockClaimDetails = TEMPSTORE.MockClaimDetails;
    var mockPolicyUsers = TEMPSTORE.MockPolicyUsers;

    // Get target userId from the X-Token 
    var xToken = req.get("X-Token");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var userPoliciesArr = [];
    var associatedClaimsPerPolicy = {};

    // Get the policies associated with the current user 
    var userPolicies = _.filter(mockPolicyUsers, function (o) {
        var isValid = _.toLower(o.Email) === _.toLower(xToken);
        if (isValid) {
            userPoliciesArr.push(o.policyNumber);
            associatedClaimsPerPolicy[o.policyNumber] = _.split(o.associatedClaims, ",");
            _.forEach(associatedClaimsPerPolicy[o.policyNumber], function (v, i) {
                associatedClaimsPerPolicy[o.policyNumber][i] = _.trim(v);
            });

        }
        return isValid;
    });

    // Claims list per user 
    var userClaimsList = [];
    _.forEach(userPoliciesArr, function (o, i) {
        var targetClaims = [];
        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] === "*") {
            targetClaims = _.filter(mockClaimSummary, function (v) {
                return o === v.policyNumber;
            });

            userClaimsList = _.concat(userClaimsList, targetClaims);
        }

        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] !== "*" && associatedClaimsPerPolicy[o][0] !== "null") {
            targetClaims = _.filter(mockClaimSummary, function (v) {
                return _.indexOf(associatedClaimsPerPolicy[o], v.claimNumber) > -1;
            });

            userClaimsList = _.concat(userClaimsList, targetClaims);
        }
    });

    // Filtered 
    var filteredClaims = userClaimsList;
    var workerGivenNameFilter = req.body.data.attributes.worker.name.given ? req.body.data.attributes.worker.name.given : undefined;
    var workerFamilyNameFilter = req.body.data.attributes.worker.name.family ? req.body.data.attributes.worker.name.family : undefined;
    var injuryDateFilter = req.body.data.attributes.injury.date ? req.body.data.attributes.injury.date : undefined;

    filteredClaims = _.filter(userClaimsList, function (o) {
        // Assume first that the item is to be included 
        var returnValue = true;

        // If any of the filters has value and the value is not contained in the item, then do not include that item 

        // family name 
        if (workerFamilyNameFilter !== undefined && workerFamilyNameFilter !== "undefined") {
            if (_.toLower(workerFamilyNameFilter) !== _.toLower(o.workerFamilyName)) return false;
        }

        // given name 
        if (workerGivenNameFilter !== undefined && workerGivenNameFilter !== "undefined") {
            if (_.toLower(workerGivenNameFilter) !== _.toLower(o.workerGivenName)) return false;
        }

        // injuryDate 
        if (injuryDateFilter !== undefined && injuryDateFilter !== "undefined") {
            if (_.toLower(injuryDateFilter) !== _.toLower(o.dateOfInjury)) return false;
        }

        return returnValue;
    });

    var duplicateClaims = _.map(filteredClaims, 'claimNumber');

    var responseObject = {
        "data": {
            "type": "string",
            "id": "string",
            "attributes": {
                "claims": duplicateClaims
            }
        }
    };

    return responseObject;
};

// getPaymentWageOverview 
exports.getPaymentWageOverview = function (req) {
    var mockWagePaymentOverview = TEMPSTORE.MockWagePaymentOverview;
    var claimNumber = req.params.id;

    // Get claimDetail from TEMPSTORE.MockClaimDetails given the claim number 
    var targetClaimDetails = _.find(TEMPSTORE.MockClaimDetails, function (o) {
        return o.data.attributes.claimNo === claimNumber;
    });
    if (!targetClaimDetails) return false;

    var xToken = req.get("X-OktaTokenJson");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var latestWagePayment = _.find(mockWagePaymentOverview, function (o) {
        return _.toLower(_.toString(o.Email)) === _.toLower(_.toString(xToken));
    });

    var responseObject = {
        "data": {
            "type": "string",
            "id": "string",
            "attributes": {
                "hasAnyPaymentBeenMade": true,
                "overview": {
                    "lastPaymentMadeDate": (latestWagePayment !== undefined) ? latestWagePayment.Date : "",
                    "lastPaymentAmount": (latestWagePayment !== undefined) ? latestWagePayment.Amount : ""
                }
            }
        }
    };

    return responseObject;
};

// validPaymentWageHistory
exports.validPaymentWageHistory = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.data.id) return false;
    if (!input.data.attributes.page) return false;
    if (!input.data.attributes.pageSize) return false;

    return true;
};

// getPaymentWageHistory
exports.getPaymentWageHistory = function (req) {
    var mockWagePaymentHistory = TEMPSTORE.MockWagePaymentHistory;
    var claimNumber = req.params.id;

    // Get claimDetail from TEMPSTORE.MockClaimDetails given the claim number 
    var targetClaimDetails = _.find(TEMPSTORE.MockClaimDetails, function (o) {
        return o.data.attributes.claimNo === claimNumber;
    });
    if (!targetClaimDetails) return false;

    var allowedUsers = [
        "icareemployer1@gmail.com",
        "icareemployer3@gmail.com",
        "icareiw1@gmail.com",
        "icareiw3@gmail.com",
    ];
    var input = req.body.data;

    var xToken = req.get("X-OktaTokenJson");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var targetUser = _.find(allowedUsers, function (o) {
        return _.toLower(_.toString(o)) === _.toLower(_.toString(xToken));
    });

    var wagePaymentHistoryList = [];
    if (targetUser !== null && targetUser !== undefined) {
        _.forEach(mockWagePaymentHistory, function (o, i) {
            var itemToPush = {
                "invoiceNumber": o.InvoiceNumber,
                "datePaid": o.DatePaid,
                "paymentMethodType": o.PaymentMethod,
                "amount": o.NetAmount,
                "payeeName": o.PayeeName,
                "payPeriod": {
                    "startDate": o.PayPeriodStart,
                    "endDate": o.PayPeriodEnd
                }
            };
            wagePaymentHistoryList.push(itemToPush);
        });
    }

    // Sorted
    wagePaymentHistoryList = _.sortBy(wagePaymentHistoryList, [function (o) { return helpers.getDate(o.datePaid); }]).reverse();

    // Paginate 
    var paginatedWagePaymentHistory = _.chunk(wagePaymentHistoryList, input.attributes.pageSize);
    var targetPage = paginatedWagePaymentHistory[input.attributes.page - 1];

    var responseObject = {
        "data": {
            "type": "string",
            "id": "string",
            "attributes": {
                "totalResults": wagePaymentHistoryList.length,
                "totalPages": paginatedWagePaymentHistory.length,
                "currentPage": input.attributes.page,
                "items": (_.size(targetPage) === 0) ? [] : targetPage
            }
        }
    };

    return responseObject;
};

// getPaymentMedical 
exports.getPaymentMedicalOverview = function (req) {
    var mockMedicalPaymentOverview = TEMPSTORE.MockMedicalPaymentOverview;
    var claimNumber = req.params.id;

    // Get claimDetail from TEMPSTORE.MockClaimDetails given the claim number 
    var targetClaimDetails = _.find(TEMPSTORE.MockClaimDetails, function (o) {
        return o.data.attributes.claimNo === claimNumber;
    });
    if (!targetClaimDetails) return false;

    var xToken = req.get("X-OktaTokenJson");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var latestMedicalPayment = _.find(mockMedicalPaymentOverview, function (o) {
        return _.toLower(_.toString(o.Email)) === _.toLower(_.toString(xToken));
    });

    var responseObject = {
        "data": {
            "type": "string",
            "id": "string",
            "attributes": {
                "hasAnyPaymentBeenMade": true,
                "overview": {
                    "lastPaymentMadeDate": (latestMedicalPayment !== undefined) ? latestMedicalPayment.Date : "",
                    "totalCostsPaid": (latestMedicalPayment !== undefined) ? latestMedicalPayment.Amount : ""
                }
            }
        }
    };

    return responseObject;
};

// validPaymentMedicalHistory
exports.validPaymentMedicalHistory = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.data.id) return false;
    if (!input.data.attributes.page) return false;
    if (!input.data.attributes.pageSize) return false;

    return true;
};

// getPaymentWageHistory
exports.getPaymentMedicalHistory = function (req) {
    var mockMedicalPaymentHistory = TEMPSTORE.MockMedicalPaymentHistory;
    var claimNumber = req.params.id;

    // Get claimDetail from TEMPSTORE.MockClaimDetails given the claim number 
    var targetClaimDetails = _.find(TEMPSTORE.MockClaimDetails, function (o) {
        return o.data.attributes.claimNo === claimNumber;
    });
    if (!targetClaimDetails) return false;

    var allowedUsers = [
        "icareemployer1@gmail.com",
        "icareemployer3@gmail.com",
        "icareiw1@gmail.com",
        "icareiw3@gmail.com",
    ];

    var input = req.body.data;

    var xToken = req.get("X-OktaTokenJson");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var targetUser = _.find(allowedUsers, function (o) {
        return _.toLower(_.toString(o)) === _.toLower(_.toString(xToken));
    });

    var medicalPaymentHistoryList = [];
    if (targetUser !== null && targetUser !== undefined) {
        _.forEach(mockMedicalPaymentHistory, function (o, i) {
            var itemToPush = {
                "datePaid": o.WhenToPay,
                "invoiceNumber": o.InvoiceNumber,
                "paymentType": o.PaymentMethod,
                "amount": o.InvoiceAmount
            };
            medicalPaymentHistoryList.push(itemToPush);
        });
    }

    // Sorted
    medicalPaymentHistoryList = _.sortBy(medicalPaymentHistoryList, [function (o) { return helpers.getDate(o.datePaid); }]).reverse();

    // Paginate 
    var paginatedMedicalPaymentHistory = _.chunk(medicalPaymentHistoryList, input.attributes.pageSize);
    var targetPage = paginatedMedicalPaymentHistory[input.attributes.page - 1];

    var responseObject = {
        "data": {
            "type": "string",
            "id": "string",
            "attributes": {
                "totalResults": medicalPaymentHistoryList.length,
                "totalPages": paginatedMedicalPaymentHistory.length,
                "currentPage": input.attributes.page,
                "items": (_.size(targetPage) === 0) ? [] : targetPage
            }
        }
    };

    return responseObject;
};

// getPaymentMedicalDetails
exports.getPaymentMedicalDetail = function (req) {
    var mockMedicalPaymentHistory = TEMPSTORE.MockMedicalPaymentHistory;
    var mockMedicalPaymentDetails = TEMPSTORE.MockMedicalPaymentDetails;
    var invoiceId = req.params.invoiceId
    //
    var targetMedicalHistory = _.find(mockMedicalPaymentHistory, function (o) {
        return o.InvoiceNumber === invoiceId;
    });

    var targetMedicalDetails = _.filter(mockMedicalPaymentDetails, function (o) {
        return o.InvoiceNumber === invoiceId;
    });

    var allowedUsers = [
        "icareemployer1@gmail.com",
        "icareemployer2@gmail.com",
        "icareiw2@gmail.com",
        "icareiw3@gmail.com",
    ];

    var xToken = req.get("X-OktaTokenJson");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var targetUser = _.find(allowedUsers, function (o) {
        return _.toLower(_.toString(o)) === _.toLower(_.toString(xToken));
    });

    var responseObject = {
        "data": {
            "type": "string",
            "id": "string",
            "attributes": {
                "lineItems": []
            }
        }
    };

    if (targetUser !== undefined && targetMedicalHistory !== undefined) {
        responseObject.data.attributes.datePaid = targetMedicalHistory.WhenToPay;
        responseObject.data.attributes.invoiceNumber = targetMedicalHistory.InvoiceNumber;
        responseObject.data.attributes.paymentMethodType = targetMedicalHistory.PaymentMethod;
        responseObject.data.attributes.amount = targetMedicalHistory.InvoiceAmount;

        if (_.size(targetMedicalDetails) === 0) {
            var itemToPush = {
                "dateOfService": targetMedicalHistory.WhenToPay,
                "paycodeDescription": "Diagnostic Procedures",
                "amount": targetMedicalHistory.InvoiceAmount
            };
            responseObject.data.attributes.lineItems.push(itemToPush);
        } else {
            // Sorted
            targetMedicalDetails = _.sortBy(targetMedicalDetails, [function (o) { return helpers.getDate(o.DateOfService); }]).reverse();
            _.forEach(targetMedicalDetails, function (o, i) {
                var itemToPush = {
                    "dateOfService": o.DateOfService,
                    "paycodeDescription": o.PaycodeDescription,
                    "amount": o.LineItemTotal
                };
                responseObject.data.attributes.lineItems.push(itemToPush);
            });
        }
    } else {
        responseObject.data.attributes.datePaid = "";
        responseObject.data.attributes.invoiceNumber = invoiceId;
        responseObject.data.attributes.paymentMethodType = "";
        responseObject.data.attributes.amount = "";
    }

    return responseObject;
};

//getPaymentWageDetail
exports.getPaymentWageDetail = function (req) {
    var mockWagePaymentHistory = TEMPSTORE.MockWagePaymentHistory;
    var mockWagePaymentDetails = TEMPSTORE.MockWagePaymentDetails;
    var invoiceId = req.params.invoiceId
    //
    var targetWageHistory = _.find(mockWagePaymentHistory, function (o) {
        return o.InvoiceNumber === invoiceId;
    });

    var targetWageDetails = _.filter(mockWagePaymentDetails, function (o) {
        return o.InvoiceNumber === invoiceId;
    });

    var allowedUsers = [
        "icareemployer1@gmail.com",
        "icareemployer2@gmail.com",
        "icareiw2@gmail.com",
        "icareiw3@gmail.com",
    ];

    var xToken = req.get("X-OktaTokenJson");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var targetUser = _.find(allowedUsers, function (o) {
        return _.toLower(_.toString(o)) === _.toLower(_.toString(xToken));
    });

    var responseObject = {
        "data": {
            "type": "string",
            "id": "string",
            "attributes": {
                "payPeriod": {},
                "lineItems": []
            }
        }
    };

    if (targetUser !== undefined && targetWageHistory !== undefined) {
        responseObject.data.attributes.invoiceNumber = targetWageHistory.InvoiceNumber;
        responseObject.data.attributes.datePaid = targetWageHistory.DatePaid;
        responseObject.data.attributes.paymentMethodType = targetWageHistory.PaymentMethod;
        responseObject.data.attributes.amount = targetWageHistory.NetAmount;
        responseObject.data.attributes.payeeName = targetWageHistory.PayeeName;
        responseObject.data.attributes.payPeriod.startDate = targetWageHistory.PayPeriodStart;
        responseObject.data.attributes.payPeriod.endDate = targetWageHistory.PayPeriodEnd;

        if (_.size(targetWageDetails) === 0) {
            var itemToPush = {
                "payPeriod": {
                    "startDate": targetWageHistory.DateFrom,
                    "endDate": targetWageHistory.DateTo,
                },
                "description": targetWageHistory.PaycodeDescriptions,
                "preInjuryAverageWeeklyEarnings": targetWageHistory.PIAWE,
                "weeklyBenefitRate": targetWageHistory.WeeklyBenefitRate,
                "earnings": targetWageHistory.Earnings,
                "hoursWorked": targetWageHistory.HoursWorked,
                "deductions": targetWageHistory.Deductions,
                "amount": targetWageHistory.Amount
            };
            responseObject.data.attributes.lineItems.push(itemToPush);
        } else {
            _.forEach(targetWageDetails, function (o, i) {
                var itemToPush = {
                    "payPeriod": {
                        "startDate": o.DateFrom,
                        "endDate": o.DateTo,
                    },
                    "description": o.PaycodeDescriptions,
                    "preInjuryAverageWeeklyEarnings": o.PIAWE,
                    "weeklyBenefitRate": o.WeeklyBenefitRate,
                    "earnings": o.Earnings,
                    "hoursWorked": o.HoursWorked,
                    "deductions": o.Deductions,
                    "amount": o.Amount
                };
                responseObject.data.attributes.lineItems.push(itemToPush);
            });
        }
    } else {
        responseObject.data.attributes.invoiceNumber = invoiceId;
        responseObject.data.attributes.datePaid = "";
        responseObject.data.attributes.paymentMethodType = "";
        responseObject.data.attributes.amount = "";
        responseObject.data.attributes.payeeName = "";
        responseObject.data.attributes.payPeriod.startDate = "";
        responseObject.data.attributes.payPeriod.endDate = "";
    }

    return responseObject;
};

// getPaymentDetailForReimbursement
exports.getPaymentDetailForReimbursement = function (req) {
    var mockBsbBanks = TEMPSTORE.MockBsbBanks;
    var mockPaymentDetails = TEMPSTORE.MockPaymentDetails;

    var xToken = req.get("X-OktaTokenJson");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var targetPaymentDetailForReimbursement = _.find(mockPaymentDetails, function (o) {
        return (_.toLower(_.toString(o.email)) === _.toLower(_.toString(xToken)) && o.preferredAccount === true);
    });

    if (targetPaymentDetailForReimbursement !== undefined) {
        var bsbBank = _.find(mockBsbBanks, function (v) {
            return (v.BSB === targetPaymentDetailForReimbursement.bsb && v.AccountNumber === targetPaymentDetailForReimbursement.accountNumber);
        });
    }

    var responseObject = {
        "data": {
            "type": "string",
            "id": "string",
            "attributes": {
                "accountName": (bsbBank !== undefined) ? bsbBank.AccountName : "",
                "accountNumber": (targetPaymentDetailForReimbursement !== undefined) ? targetPaymentDetailForReimbursement.accountNumber : "",
                "bsb": (targetPaymentDetailForReimbursement !== undefined) ? targetPaymentDetailForReimbursement.bsb : "",
                "bankName": (bsbBank !== undefined) ? bsbBank.BankName : ""
            }
        }
    };

    return responseObject;
};

// validClaimRequest 
exports.validWeeklyAverageEarnings = function (req) {
    var input = req.body.data ? req.body.data : undefined;
    if (input === undefined) return false;
    if (input.attributes === undefined) return false;

    return true;
};

// getPaymentMedicalDetail
exports.postWeeklyAverageEarnings = function (req) {
    var responseObject = { description: SUCCESS.Success202c };
    return responseObject;
};

// getInjuryManagement
exports.getInjuryManagement = function (req) {
    var mockImp = TEMPSTORE.MockImp;
    var input = _.toString(req.params.id);

    // check if the claimNo exists 
    var existingInsuranceManagementPlan = _.find(mockImp, function (o) {
        return _.toString(o.claimNumber) === input;
    });

    var defaultContactsFirst = {
        "contactId": "1",
        "name": {
            "given": "Tom",
            "family": "Josilevich"
        },
        "positionTitle": "Accounting",
        "phone": "0455-511-515",
        "email": "bdrysdell0@studiopress.com"
    };
    var defaultContactsSecond = {
        "contactId": "151",
        "name": {
            "given": "Wen",
            "family": "Aberdeen"
        },
        "positionTitle": "Training",
        "phone": "0410-442-115",
        "email": "bdrysdell0@studiopress.com"
    };

    var responseObject = {
        "data": {
            "type": "string",
            "id": input,
            "attributes": {
                "currentPlanDocument": {},
                "contacts": [],
                "goals": [],
                "actions": [],
                "treatments": [],
                "careServices": []
            }
        }
    };

    if (existingInsuranceManagementPlan === undefined) {
        responseObject.data.attributes.contacts.push(defaultContactsFirst);
        responseObject.data.attributes.contacts.push(defaultContactsSecond);
    } else {
        // CurrentPlanDocument
        responseObject.data.attributes.currentPlanDocument.documentId = existingInsuranceManagementPlan.currentPlanDocumentDocId;
        responseObject.data.attributes.currentPlanDocument.url = existingInsuranceManagementPlan.currentPlanDocumentUrl;
        responseObject.data.attributes.currentPlanDocument.title = existingInsuranceManagementPlan.currentPlanDocumentTitle;
        responseObject.data.attributes.currentPlanDocument.fileSize = existingInsuranceManagementPlan.currentPlanDocumentFileSize;
        responseObject.data.attributes.currentPlanDocument.fileType = existingInsuranceManagementPlan.currentPlanDocumentFileType;
        responseObject.data.attributes.currentPlanDocument.uploadedDate = existingInsuranceManagementPlan.currentPlanDocumentUploadedDate;

        // contacts
        if (existingInsuranceManagementPlan.contactsFirstContactId !== "" && existingInsuranceManagementPlan.contactsFirstContactId !== undefined) {
            var contactsFirst = {
                "contactId": existingInsuranceManagementPlan.contactsFirstContactId,
                "name": {
                    "given": existingInsuranceManagementPlan.contactsFirstGivenName,
                    "family": existingInsuranceManagementPlan.contactsFirstFamilyName
                },
                "positionTitle": existingInsuranceManagementPlan.contactsFirstPositionTitle,
                "phone": existingInsuranceManagementPlan.contactsFirstPhone,
                "email": existingInsuranceManagementPlan.contactsFirstEmail
            };
            responseObject.data.attributes.contacts.push(contactsFirst);
        }

        if (existingInsuranceManagementPlan.contactsSecondContactId !== "" && existingInsuranceManagementPlan.contactsSecondContactId !== undefined) {
            var contactsSecond = {
                "contactId": existingInsuranceManagementPlan.contactsSecondContactId,
                "name": {
                    "given": existingInsuranceManagementPlan.contactsSecondGivenName,
                    "family": existingInsuranceManagementPlan.contactsSecondFamilyName
                },
                "positionTitle": existingInsuranceManagementPlan.contactsSecondPositionTitle,
                "phone": existingInsuranceManagementPlan.contactsSecondPhone,
                "email": existingInsuranceManagementPlan.contactsSecondEmail
            };
            responseObject.data.attributes.contacts.push(contactsSecond);
        }

        // goals
        if (existingInsuranceManagementPlan.goalsFirstGoalTitle !== "" && existingInsuranceManagementPlan.goalsFirstGoalTitle !== undefined) {
            var goalsFirst = {
                "goalTitle": existingInsuranceManagementPlan.goalsFirstGoalTitle,
                "statusCode": existingInsuranceManagementPlan.goalsFirstStatusCode,
                "statusTitle": existingInsuranceManagementPlan.goalsFirstStatusTitle
            };
            responseObject.data.attributes.goals.push(goalsFirst);
        }

        if (existingInsuranceManagementPlan.goalsSecondGoalTitle !== "" && existingInsuranceManagementPlan.goalsSecondGoalTitle !== undefined) {
            var goalsSecond = {
                "goalTitle": existingInsuranceManagementPlan.goalsSecondGoalTitle,
                "statusCode": existingInsuranceManagementPlan.goalsSecondStatusCode,
                "statusTitle": existingInsuranceManagementPlan.goalsSecondStatusTitle
            };
            responseObject.data.attributes.goals.push(goalsSecond);
        }

        // actions
        if (existingInsuranceManagementPlan.actionsFirstDate !== "" && existingInsuranceManagementPlan.actionsFirstDate !== undefined) {
            var actionFirst = {
                "date": existingInsuranceManagementPlan.actionsFirstDate,
                "title": existingInsuranceManagementPlan.actionsFirstTitle,
                "responsible": existingInsuranceManagementPlan.actionsFirstResponsible
            };
            responseObject.data.attributes.actions.push(actionFirst);
        }

        if (existingInsuranceManagementPlan.actionsSecondDate !== "" && existingInsuranceManagementPlan.actionsSecondDate !== undefined) {
            var actionSecond = {
                "date": existingInsuranceManagementPlan.actionsSecondDate,
                "title": existingInsuranceManagementPlan.actionsSecondTitle,
                "responsible": existingInsuranceManagementPlan.actionsSecondResponsible
            };
            responseObject.data.attributes.actions.push(actionSecond);
        }

        // treatments
        if (existingInsuranceManagementPlan.treatmentsFirstTreatment !== "" && existingInsuranceManagementPlan.treatmentsFirstTreatment !== undefined) {
            var treatmentFirst = {
                "treatment": existingInsuranceManagementPlan.treatmentsFirstTreatment,
                "sessionsTaken": existingInsuranceManagementPlan.treatmentsFirstSessionsTaken,
                "sessionsTotal": existingInsuranceManagementPlan.treatmentsFirstSessionsTotal
            };
            responseObject.data.attributes.treatments.push(treatmentFirst);
        }

        if (existingInsuranceManagementPlan.treatmentsSecondTreatment !== "" && existingInsuranceManagementPlan.treatmentsSecondTreatment !== undefined) {
            var treatmentSecond = {
                "treatment": existingInsuranceManagementPlan.treatmentsSecondTreatment,
                "sessionsTaken": existingInsuranceManagementPlan.treatmentsSecondSessionsTaken,
                "sessionsTotal": existingInsuranceManagementPlan.treatmentsSecondSessionsTotal
            };
            responseObject.data.attributes.treatments.push(treatmentSecond);
        }

        // careServices
        if (existingInsuranceManagementPlan.careServicesFirstTitle !== "" && existingInsuranceManagementPlan.careServicesFirstTitle !== undefined) {
            var careServiceFirst = {
                "title": existingInsuranceManagementPlan.careServicesFirstTitle,
                "task": existingInsuranceManagementPlan.careServicesFirstTask,
                "provider": existingInsuranceManagementPlan.careServicesFirstProvider,
                "approvalPeriod": {
                    "from": existingInsuranceManagementPlan.careServicesFirstApprovalPeriodFrom,
                    "to": existingInsuranceManagementPlan.careServicesFirstApprovalPeriodTo
                },
                "ratePerHour": existingInsuranceManagementPlan.careServicesFirstRatePerHour,
                "frequency": existingInsuranceManagementPlan.careServicesFirstFrequency,
                "totalHours": existingInsuranceManagementPlan.careServicesFirstTotalHours,
                "totalCost": existingInsuranceManagementPlan.careServicesFirstTotalCost
            };
            responseObject.data.attributes.careServices.push(careServiceFirst);
        }

        if (existingInsuranceManagementPlan.careServicesSecondTitle !== "" && existingInsuranceManagementPlan.careServicesSecondTitle !== undefined) {
            var careServiceSecond = {
                "title": existingInsuranceManagementPlan.careServicesSecondTitle,
                "task": existingInsuranceManagementPlan.careServicesSecondTask,
                "provider": existingInsuranceManagementPlan.careServicesSecondProvider,
                "approvalPeriod": {
                    "from": existingInsuranceManagementPlan.careServicesSecondApprovalPeriodFrom,
                    "to": existingInsuranceManagementPlan.careServicesSecondApprovalPeriodTo
                },
                "ratePerHour": existingInsuranceManagementPlan.careServicesSecondRatePerHour,
                "frequency": existingInsuranceManagementPlan.careServicesSecondFrequency,
                "totalHours": existingInsuranceManagementPlan.careServicesSecondTotalHours,
                "totalCost": existingInsuranceManagementPlan.careServicesSecondTotalCost
            };
            responseObject.data.attributes.careServices.push(careServiceSecond);
        }

        if (existingInsuranceManagementPlan.careServicesThirdTitle !== "" && existingInsuranceManagementPlan.careServicesThirdTitle !== undefined) {
            var careServiceThird = {
                "title": existingInsuranceManagementPlan.careServicesThirdTitle,
                "task": existingInsuranceManagementPlan.careServicesThirdTask,
                "provider": existingInsuranceManagementPlan.careServicesThirdProvider,
                "approvalPeriod": {
                    "from": existingInsuranceManagementPlan.careServicesThirdApprovalPeriodFrom,
                    "to": existingInsuranceManagementPlan.careServicesThirdApprovalPeriodTo
                },
                "ratePerHour": existingInsuranceManagementPlan.careServicesThirdRatePerHour,
                "frequency": existingInsuranceManagementPlan.careServicesThirdFrequency,
                "totalHours": existingInsuranceManagementPlan.careServicesThirdTotalHours,
                "totalCost": existingInsuranceManagementPlan.careServicesThirdTotalCost
            };
            responseObject.data.attributes.careServices.push(careServiceThird);
        }

    }

    return responseObject;
};

// getReturnToWork
exports.getReturnToWork = function (req) {
    var mockRtw = TEMPSTORE.MockRtw;
    var input = _.toString(req.params.id);

    // check if the claimNo exists 
    var existingReturnToWork = _.find(mockRtw, function (o) {
        return _.toString(o.claimNumber) === input;
    });

    var responseObject = {
        "data": {
            "type": "string",
            "id": input,
            "attributes": {
                "currentRtwPlanDocument": {},
                "capacityForActivities": {},
                "rtwPlanTemplateDocument": {}
            }
        }
    };

    if (existingReturnToWork == undefined) {
        // rtwPlanTemplateDocument
        responseObject.data.attributes.rtwPlanTemplateDocument.documentId = "1000";
        responseObject.data.attributes.rtwPlanTemplateDocument.url = "http://capsicumgroup.com/wp-content/uploads/2013/03/Factsheet1.pdf";
        responseObject.data.attributes.rtwPlanTemplateDocument.title = "BrandStorm HBC";
        responseObject.data.attributes.rtwPlanTemplateDocument.fileSize = "103";
        responseObject.data.attributes.rtwPlanTemplateDocument.fileType = "pdf";
        responseObject.data.attributes.rtwPlanTemplateDocument.uploadedDate = "08/21/2017";
    } else {
        // currentRtwPlanDocument
        responseObject.data.attributes.currentRtwPlanDocument.documentId = existingReturnToWork.currentRtwPlanDocId;
        responseObject.data.attributes.currentRtwPlanDocument.url = existingReturnToWork.currentRtwPlanUrl;
        responseObject.data.attributes.currentRtwPlanDocument.title = existingReturnToWork.currentRtwPlanTitle;
        responseObject.data.attributes.currentRtwPlanDocument.fileSize = existingReturnToWork.currentRtwPlanFileSize;
        responseObject.data.attributes.currentRtwPlanDocument.fileType = existingReturnToWork.currentRtwPlanFileType;
        responseObject.data.attributes.currentRtwPlanDocument.uploadedDate = existingReturnToWork.currentRtwPlanUploadedDate;

        // capacityForActivities
        responseObject.data.attributes.capacityForActivities.liftingCapacity = existingReturnToWork.liftingCapacity;
        responseObject.data.attributes.capacityForActivities.sittingTolerance = existingReturnToWork.sittingTolerance;
        responseObject.data.attributes.capacityForActivities.standingTolerance = existingReturnToWork.standingTolerance;
        responseObject.data.attributes.capacityForActivities.pushingAbility = existingReturnToWork.pushingAbility;
        responseObject.data.attributes.capacityForActivities.bendingAbility = existingReturnToWork.bendingAbility;
        responseObject.data.attributes.capacityForActivities.drivingAbility = existingReturnToWork.drivingAbility;
        responseObject.data.attributes.capacityForActivities.other = existingReturnToWork.other;

        // rtwPlanTemplateDocument
        responseObject.data.attributes.rtwPlanTemplateDocument.documentId = existingReturnToWork.rtwPlanTemplateDocumentDocId;
        responseObject.data.attributes.rtwPlanTemplateDocument.url = existingReturnToWork.rtwPlanTemplateDocumentUrl;
        responseObject.data.attributes.rtwPlanTemplateDocument.title = existingReturnToWork.rtwPlanTemplateDocumentTitle;
        responseObject.data.attributes.rtwPlanTemplateDocument.fileSize = existingReturnToWork.rtwPlanTemplateDocumentFileSize;
        responseObject.data.attributes.rtwPlanTemplateDocument.fileType = existingReturnToWork.rtwPlanTemplateDocumentFileType;
        responseObject.data.attributes.rtwPlanTemplateDocument.uploadedDate = existingReturnToWork.rtwPlanTemplateDocumentUploadedDate;
    }

    return responseObject;
};

// postReturnToWork
exports.postReturnToWork = function (req) {
    var mockRtw = TEMPSTORE.MockRtw;
    var input = _.toString(req.params.id);

    // check if the claimNo exists 
    var existingReturnToWork = _.find(mockRtw, function (o) {
        return _.toString(o.claimNumber) === input;
    });

    var responseObject = {
        "data": {
            "type": "string",
            "id": input,
            "attributes": {
                "returnToWorkDocumentUrls": []
            }
        }
    };

    if (existingReturnToWork !== undefined) {
        responseObject.data.attributes.returnToWorkDocumentUrls.push(existingReturnToWork.currentRtwPlanUrl);
        responseObject.data.attributes.returnToWorkDocumentUrls.push(existingReturnToWork.rtwPlanTemplateDocumentUrl);
    }

    return responseObject;
};
// postPaymentsMedicalReimbursement
exports.postPaymentsMedicalReimbursement = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.data.id) return false;
    if (!input.data.attributes.documents) return false;

    return true;
};

//postMyPaymentsMedicalReimbursement Code Description
exports.postMyPaymentsMedicalReimbursement = function (req) {
    var responseObject = { description: SUCCESS.Success202c };
    return responseObject;
}

//postMyPaymentsWageReimbursement Code Description
exports.postMyPaymentsWageReimbursement = function (req) {
    var responseObject = { description: SUCCESS.Success202c };
    return responseObject;
}

//validatePaymentsWageReimbursement
exports.validatePaymentsWageReimbursement = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.data.attributes.dateClaimingFrom) return false;
    if (!input.data.attributes.didInjuredPersonWorkInPeriod) return false;
    if (!input.data.attributes.totalAmount) return false;
    if (!input.data.attributes.supportingDocumentUrls) return false;

    return true;
}

// validActivationToken
exports.validActivationToken = function (activationToken) {
    if (activationToken !== "a1b2c3d4") return false;
    return true;
}

// validActivationTokenParam
exports.validActivationTokenParam = function (req, q) {
    var input = req.query[q] ? req.query[q] : undefined;
    if (input === undefined) return false;
    if (!validActivationToken(input)) return false;
    return true;
};

// getUserRegister
exports.getUserRegister = function (req) {
    var activationToken = req.query["activationToken"];

    var responseObject = {
        "data": {
            "type": "string",
            "id": "string",
            "attributes": {
                "givenName": "J.K.",
                "familyName": "Rowling",
                "contactNumber": "0244445555",
                "emailAddress": "icareemployer1@gmail.com",
                "identityId": "ES3918DS"
            }
        }
    };

    return responseObject;
};

// validUserRegister
exports.validUserRegister = function (req) {
    var input = req.body.data ? req.body.data : undefined;
    if (input === undefined) return false;

    if (!validActivationToken(input.attributes.activationToken)) return false;
    if (!input.attributes.password) return false;
    if (!input.attributes.securityQuestion) return false;
    if (!input.attributes.securityAnswer) return false;

    return true;
};

// postUserRegister
exports.postUserRegister = function (req) {
    var responseObject = { description: SUCCESS.Success201 };
    return responseObject;
};

//getWeeklyAverageEarnings
exports.getWeeklyAverageEarnings = function (req) {
    var mockPaymentPiaweDetails = TEMPSTORE.MockPaymentPiaweDetails;
    var claimNumber = req.params.id;

    var targetPiaweDetails = _.find(mockPaymentPiaweDetails, function (o) {
        return o.claimNumber === claimNumber;
    });

    var responseObject = {
        "data": {
            "type": "string",
            "id": "string",
            "attributes": {
                "benefits": {
                    "motorVehicle": {},
                    "healthInsurance": {},
                    "accomodation": {},
                    "education": {},
                    "other": {}
                }
            }
        }
    };

    if (targetPiaweDetails !== undefined) {
        responseObject.data.attributes.ordinaryWeeklyWage = targetPiaweDetails.OrdinaryWeeklyWage;
        responseObject.data.attributes.ordinaryWeeklyHours = targetPiaweDetails.OrdinaryWeeklyHours;
        responseObject.data.attributes.anyShiftAllowancesOrOvertime = targetPiaweDetails.AnyShiftAllowancesOrOvertime;
        responseObject.data.attributes.averageShiftAllowance = targetPiaweDetails.AverageShiftAllowance;
        responseObject.data.attributes.averageOvertimeEarnings = targetPiaweDetails.AverageOvertimeEarnings;

        responseObject.data.attributes.benefits.motorVehicle.isActiveBenefit = targetPiaweDetails.benefitsMotorIsActive;
        responseObject.data.attributes.benefits.motorVehicle.averageWeeklyValue = targetPiaweDetails.benefitsMotorAvgWeekly;
        responseObject.data.attributes.benefits.healthInsurance.isActiveBenefit = targetPiaweDetails.benefitsHealthIsActive;
        responseObject.data.attributes.benefits.healthInsurance.averageWeeklyValue = targetPiaweDetails.benefitsHealthAvgWeekly;
        responseObject.data.attributes.benefits.accomodation.isActiveBenefit = targetPiaweDetails.benefitsAccomodationIsActive;
        responseObject.data.attributes.benefits.accomodation.averageWeeklyValue = targetPiaweDetails.benefitsAccomodationAvgWeekly;
        responseObject.data.attributes.benefits.education.isActiveBenefit = targetPiaweDetails.benefitsEducationIsActive;
        responseObject.data.attributes.benefits.education.averageWeeklyValue = targetPiaweDetails.benefitsEducationAvgWeekly;
        responseObject.data.attributes.benefits.other.isActiveBenefit = targetPiaweDetails.benefitsOtherIsActive;
        responseObject.data.attributes.benefits.other.averageWeeklyValue = targetPiaweDetails.benefitsOtherAvgWeekly;

        responseObject.data.attributes.hasMoreThanOneJob = targetPiaweDetails.HasMoreThanOneJob;
        responseObject.data.attributes.isApprentice = targetPiaweDetails.IsApprentice;
        responseObject.data.attributes.hasChangedEmploymentInLastYear = targetPiaweDetails.HasChangedEmploymentInLastYear;
        responseObject.data.attributes.employmentType = targetPiaweDetails.EmploymentType;
        responseObject.data.attributes.daysWorking = targetPiaweDetails.DaysWorking;
        responseObject.data.attributes.leaveTypesInLastYear = targetPiaweDetails.LeaveTypesInLastYear;

    } else {
        responseObject.data.attributes.ordinaryWeeklyWage = "";
        responseObject.data.attributes.ordinaryWeeklyHours = "";
        responseObject.data.attributes.anyShiftAllowancesOrOvertime = "";
        responseObject.data.attributes.averageShiftAllowance = "";
        responseObject.data.attributes.averageOvertimeEarnings = "";

        responseObject.data.attributes.benefits.motorVehicle.isActiveBenefit = "";
        responseObject.data.attributes.benefits.motorVehicle.averageWeeklyValue = "";
        responseObject.data.attributes.benefits.healthInsurance.isActiveBenefit = "";
        responseObject.data.attributes.benefits.healthInsurance.averageWeeklyValue = "";
        responseObject.data.attributes.benefits.accomodation.isActiveBenefit = "";
        responseObject.data.attributes.benefits.accomodation.averageWeeklyValue = "";
        responseObject.data.attributes.benefits.education.isActiveBenefit = "";
        responseObject.data.attributes.benefits.education.averageWeeklyValue = "";
        responseObject.data.attributes.benefits.other.isActiveBenefit = "";
        responseObject.data.attributes.benefits.other.averageWeeklyValue = "";

        responseObject.data.attributes.hasMoreThanOneJob = "";
        responseObject.data.attributes.isApprentice = "";
        responseObject.data.attributes.hasChangedEmploymentInLastYear = "";
        responseObject.data.attributes.employmentType = "";
        responseObject.data.attributes.daysWorking = "";
        responseObject.data.attributes.leaveTypesInLastYear = "";
    }

    return responseObject;
};

exports.timeslot = function (req) {

    var responseObject = { description: SUCCESS.Success202d };
    return responseObject;

}