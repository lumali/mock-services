﻿
/*
 * Unauthenticated Lodgement for 2.1b
 *
 * Unauthenticated Lodgement
 */

var constants = require("./utilities/constants.js");
var ERRORS = constants.errors();
var SUCCESS = constants.successResponses();
var helpers = require("./utilities/helpers.js");
var v2 = require("./routes/v2.js");

// create state.AddUpdateClaimRepo if it doesn't exist
state.AddUpdateClaimRepo = state.AddUpdateClaimRepo || [];


/* Route definition styles:
 *
 *	define(path, method, function)
 *	soap(path, soapAction, function)
 *
 */
Sandbox.define("/v2/workersInsurance/me/claims", "POST", v2.postV2WorkersinsuranceClaims);
Sandbox.define("/v2/workersInsurance/me/claims/{id}", "PATCH", v2.patchV2WorkersinsuranceClaims);
Sandbox.define("/v2/workersInsurance/me/claims/{id}/lodge", "POST", v2.postV2WorkersinsuranceClaimsLodge);
Sandbox.define("/v2/workersInsurance/me/claims/{id}/attachmentLocations", "POST", v2.postV2WorkersinsuranceClaimsAttachmentlocations);


/* Unit Testing Helper: 
 *  This is used only for removing test data from state.claims 
 *
 */
Sandbox.define('/v2/workersInsurance/me/claims/{id}', 'DELETE', function (req, res) {
    // Check the request, make sure it is a compatible type
    if (!req.is('application/json')) {
        return res.send(400, 'Invalid content type, expected application/json');
    }

    // remove the claim
    var existingClaim = _.find(state.claims, function (o) { return o.data.attributes.claimNo === req.params.id; });
    if (existingClaim) state.claims = _.reject(state.claims, function (o) { return o.data.attributes.claimNo === req.params.id; });

    // Set the type of response, sets the content type.
    res.type('application/json');

    // Set the status code of the response.
    res.status(200);

    // Send the response body.
    res.json({
        "status": "ok"
    });
});
