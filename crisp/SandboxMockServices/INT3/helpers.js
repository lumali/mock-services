/** Common Methods **/

/* 
    validateTokenHeaders
    Authorization
*/
exports.returnAuthorizationHeader = function (req) {

    // validate request headers 
    var authHeader = validators.validAuthorizationHeader(req);
    if (!authHeader) {
        var errorsArray = [];
        errorsArray.push(ERRORS.E401);
        return errorsArray;
    }

    return authHeader;
}

/* 
    validateTokenHeaders
    X-Token-ID1
*/
exports.returnXTokenID1Header = function (req) {

    // validate request headers 
    var authHeader = validators.validXTokenID1Header(req);
    if (!authHeader) {
        var errorsArray = [];
        errorsArray.push(ERRORS.E401);
        return errorsArray;
    }

    //decode authHeader
    return helpers.decode(authHeader);
}

/* 
    validateTokenHeaders
    X-Token-ID2
*/
exports.returnXTokenID2Header = function (req) {

    // validate request headers 
    var authHeader = validators.validXTokenID2Header(req);
    if (!authHeader) {
        var errorsArray = [];
        errorsArray.push(ERRORS.E401);
        return errorsArray;
    }

    //decode authHeader
    return helpers.decode(authHeader);
}

/* returnError */
exports.returnErrorResponse = function (res, errorsArray) {
    if (errorsArray.length) {
        return res.json(errorsArray[0].status, { "errors": errorsArray });
    }

    // default error
    return res.json(500, {
        "errors": [ERRORS.E500]
    });
};

/* getDate */
exports.getDate = function (dateString) {
    var date = dateString.split(/\D/);
    return new Date(date[2], date[1] - 1, date[0]);
}

/* hasOneDayElapsed */
exports.hasOneDayElapsed = function (previousDate) {
    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    var currentDate = new Date();
    previousDate = new Date(previousDate);
    var diffDays = Math.round(Math.abs((currentDate.getTime() - previousDate.getTime()) / (oneDay)));

    return diffDays >= 1;
}

/* genericRequestValidation */
exports.genericRequestValidation = function (req) {
    var errorsArray = [];

    // validate request type 
    if (!validators.validRequestMediaTypeHeader(req)) {
        errorsArray.push(ERRORS.E415);
    }

    //// validate request headers 
    //if (!validators.validAuthorizationHeader(req)) {
    //    errorsArray.push(ERRORS.E401);
    //}

    // validate request body 
    //if (!req.body || req.body.isInvalid) {
    //    errorsArray.push(ERRORS.E400);
    //}

    return errorsArray;
}

/* policySearchRequestValidation */
exports.policySearchRequestValidation = function (req) {
    var errorsArray = [];

    if (!validators.validPolicySearchRequest(req)) {
        errorsArray.push(ERRORS.E500);
    }

    return errorsArray;
}

/* policySearchRequestValidation */
exports.patchUserDetailsRequestValidation = function (req) {
    var errorsArray = [];

    if (!validators.validPatchUserDetailsRequest(req)) {
        errorsArray.push(ERRORS.E500);
    }

    return errorsArray;
}

/* emailQueryParamValidation */
exports.emailQueryParamValidation = function (req) {
    var errorsArray = [];

    if (!validators.validQueryParam(req, "email")) {
        errorsArray.push(ERRORS.E403);
    }

    return errorsArray;
}

/* createUniqueId: Creates a string that can be used for dynamic id attributes */
createUniqueId = function () {
    return Math.random().toString(36).substr(2, 16);
};

/* decode */
exports.decode = function (input) {
    _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var output = "";
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;
    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
    while (i < input.length) {
        enc1 = _keyStr.indexOf(input.charAt(i++));
        enc2 = _keyStr.indexOf(input.charAt(i++));
        enc3 = _keyStr.indexOf(input.charAt(i++));
        enc4 = _keyStr.indexOf(input.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        output = output + String.fromCharCode(chr1);

        if (enc3 != 64) {
            output = output + String.fromCharCode(chr2);
        }

        if (enc4 != 64) {
            output = output + String.fromCharCode(chr3);
        }
    }

    output = helpers._utf8_decode(output);
    return output;
}

/* _utf8_decode */
exports._utf8_decode = function (e) {
    var t = "";
    var n = 0;
    var r = 0;
    var c1 = 0;
    var c2 = 0;
    while (n < e.length) {
        r = e.charCodeAt(n);
        if (r < 128) {
            t += String.fromCharCode(r);
            n++;
        } else if (r > 191 && r < 224) {
            c2 = e.charCodeAt(n + 1);
            t += String.fromCharCode((r & 31) << 6 | c2 & 63);
            n += 2;
        } else {
            c2 = e.charCodeAt(n + 1);
            c3 = e.charCodeAt(n + 2);
            t += String.fromCharCode((r & 15) << 12 | (c2 & 63) <<
                6 | c3 & 63);
            n += 3;
        }
    }
    return t;
}



/** Specific Methods **/

/* sampleSuccessResponse */
exports.sampleSuccessResponse = function (req) {
    return {
        "data": {
            "type": "Quote",
            "id": "100000000003538",
            "attributes": {
                "accountID": "1400778",
                "periodStartDate": "2018-09-11T14:01:00Z",
                "periodEndDate": "2019-09-11T14:01:00Z",
                "estimatedPremium": {
                    "amount": 3007,
                    "amountCurrency": "aud"
                },
                "costCenters": [
                  {
                      "directWages": [
                        {
                            "pac": 0,
                            "grossValue": 100000,
                            "numberOfUnits": 0,
                            "totalWages": 100000,
                            "numberOfEmployees": 7,
                            "wagesAmount": 1000,
                            "businessDescription": "Business description",
                            "directWageID": {
                                "pacCode": "011100001",
                                "pacDescription": "Bulb propagating - outdoors",
                                "wicRate": 4.432,
                                "ddlContribution": 0.028,
                                "code": "011100",
                                "wicCode": "0",
                                "perCapitaFlag": false,
                                "wicDescription": "Plant Nurseries"
                            },
                            "numberOfApprentices": 2,
                            "apprenticesWages": 20000,
                            "wic": 0
                        }
                      ]
                  }
                ],
                "paymentPlans": [
                  {
                      "name": "icare NI Payment Plan_Yearly",
                      "downPayment": {
                          "amount": 3007,
                          "amountCurrency": "aud"
                      },
                      "total": {
                          "amount": 3007,
                          "amountCurrency": "aud"
                      },
                      "installment": {
                          "amount": 0,
                          "amountCurrency": "aud"
                      },
                      "billingId": "icare_bcpp:1"
                  },
                  {
                      "name": "icare NI Payment Plan_Quarterly",
                      "downPayment": {
                          "amount": 751.75,
                          "amountCurrency": "aud"
                      },
                      "total": {
                          "amount": 3007,
                          "amountCurrency": "aud"
                      },
                      "installment": {
                          "amount": 751.75,
                          "amountCurrency": "aud"
                      },
                      "billingId": "icare_bcpp:3"
                  }
                ],
                "lossHistoryEntry": [
                  {
                      "claimYear3": 100,
                      "claimYears2": 100,
                      "claimYears1": 100,
                      "basicTariffPremiumYears3": 100,
                      "basicTariffPremiumYears2": 100,
                      "basicTariffPremiumYears1": 100,
                      "costCenter": "abc",
                      "directWageId": {
                          "wicRate": 4.432,
                          "wicDescription": null,
                          "wicCode": "011100 - Plant Nurseries",
                          "perCapitaFlag": false,
                          "pacDescription": "Bulb propagating - outdoors",
                          "pacCode": "011100001",
                          "ddlContribution": 0.028,
                          "code": "011100"
                      },
                      "location": "Sydney",
                      "wages1": null,
                      "wages2": null,
                      "wages3": null
                  }
                ],
                "commencementDate": "2019-09-11T14:01:00Z"
            }
        }
    };
}


/* getReferenceData */
exports.getReferenceData = function (req) {
    var responseObject = {
        "data": {
            "type": "PolicyPortalReferenceData",
            "id": "PolicyPortalReferenceData",
            "attributes": {
                "icareTermTypeRationalised": [
                    {
                        "name": "ShortTerm_DR",
                        "value": "ShortTerm_DR"
                    }
                ],
                "businessType": [
                  {
                      "name": "Individuals/Sole Trader",
                      "value": "individualsoletrader",
                      "classification": "default"
                  },
                  {
                      "name": "Partnership",
                      "value": "partnership",
                      "classification": "default"
                  },
                  {
                      "name": "Trust",
                      "value": "trustestate",
                      "classification": "trust"
                  },
                  {
                      "name": "Company",
                      "value": "company",
                      "classification": "company"
                  },
                  {
                      "name": "Strata Plan",
                      "value": "strataplan",
                      "classification": "default"
                  },
                  {
                      "name": "Superannuation Fund",
                      "value": "superannuationfund",
                      "classification": "default"
                  },
                  {
                      "name": "Government",
                      "value": "government",
                      "classification": "default"
                  },
                  {
                      "name": "Overseas Registered Company",
                      "value": "overseasregcompany",
                      "classification": "company"
                  },
                  {
                      "name": "Joint Venture",
                      "value": "jointventure",
                      "classification": "default"
                  },
                  {
                      "name": "Registered Charities",
                      "value": "registeredcharities",
                      "classification": "default"
                  },
                  {
                      "name": "Church Group",
                      "value": "churchgroups",
                      "classification": "default"
                  },
                  {
                      "name": "Sporting Organisation",
                      "value": "sportingorganisation",
                      "classification": "default"
                  }
                ],
                "trusteeType": [
                    {
                        "name": "Partnership",
                        "value": "partnership",
                        "classification": "default"
                    },
                    {
                        "name": "Company",
                        "value": "proprietorylimited",
                        "classification": "company"
                    },
                    {
                        "name": "Sole proprietorship",
                        "value": "solepartnership",
                        "classification": "default"
                    }
                ],
                "labourHire": [
                    {
                        "name": "Labour hire firm",
                        "value": "1"
                    },
                    {
                        "name": "Not a labour hire firm",
                        "value": "2"
                    },
                    {
                        "name": "Group training apprenticeship scheme",
                        "value": "3"
                    }
                ],
                "contractorWorkType": [
                  "Labour only",
                  "Labour & tools",
                  "Labour & plant",
                  "Labour, plant & material"
                ],
                "cancelReason": [
                  {
                      "name": "Ceased to trade",
                      "value": "ceasedtotrade_icare"
                  },
                  {
                      "name": "Business sold",
                      "value": "businesssold_icare"
                  },
                  {
                      "name": "Ceased to employ",
                      "value": "ceasedtoemploy_icare"
                  },
                  {
                      "name": "Exempt employer",
                      "value": "exemptemployer_icare"
                  },
                  {
                      "name": "Other",
                      "value": "other_icare"
                  }
                ],
                "securityQuestion": [
                  {
                      "name": "What is the name of your first stuffed animal",
                      "value": "What is the name of your first stuffed animal"
                  },
                  {
                      "name": "What was the first computer game you played",
                      "value": "What was the first computer game you played"
                  },
                  {
                      "name": "What is your favorite movie quote",
                      "value": "What is your favorite movie quote"
                  },
                  {
                      "name": "What was your dream job as a child",
                      "value": "What was your dream job as a child"
                  },
                  {
                      "name": "Where did you meet your spouse/significant other",
                      "value": "Where did you meet your spouse/significant other"
                  },
                  {
                      "name": "Where did you go for your favorite vacation",
                      "value": "Where did you go for your favorite vacation"
                  },
                  {
                      "name": "Who is your favorite book/movie character",
                      "value": "Who is your favorite book/movie character"
                  },
                  {
                      "name": "Who is your favorite sports player",
                      "value": "Who is your favorite sports player"
                  }
                ]
            }
        }
    };

    return responseObject;
};

/* wicSearch */
exports.wicSearch = function (req) {
    var mockDataWICs = state.MockDataWICs;
    var mockDataPACS = state.MockDataPACS;

    var filteredItems = _.map(mockDataWICs, function (o) {
        return o;
    });

    // Filter 
    var searchTextFilter = req.query["searchText"];
    var pageFilter = parseInt(req.query["page"], 10);
    var pageSizeFilter = parseInt(req.query["pageSize"], 10);

    if (searchTextFilter) {

        /* Find WICS */

        // If search text is number search the wic codes
        if (_.isInteger(parseInt(searchTextFilter, 10))) {
            filteredItems = _.filter(filteredItems, function (o) {
                // searchTextFilter (partial match)
                return ((searchTextFilter !== undefined && searchTextFilter !== "undefined") &&
                        (_.toLower(_.toString(o.id)).indexOf(_.toLower(_.toString(searchTextFilter))) > -1)
                       );
            });
        } else {
            // Else, search the wic description
            filteredItems = _.filter(filteredItems, function (o) {
                // searchTextFilter (partial match)
                return ((searchTextFilter !== undefined && searchTextFilter !== "undefined") &&
                        (_.toLower(_.toString(o.description)).indexOf(_.toLower(_.toString(searchTextFilter))) > -1)
                       );
            });
        }
    }

    /* Find PACS per WIC */

    // Create new object containing the wic and all the pacs for that wic 
    var pacsPerWic = [];
    _.forEach(filteredItems, function (o) {
        pacsPerWic.push(
            {
                wic: o,
                pacs: _.filter(mockDataPACS, function (p) {
                    return (_.toLower(_.toString(o.id)) === _.toLower(_.toString(p.wic)));
                })
            }
        );
    });

    // Transform into a flat array 
    var pacsPerWicFlattened = [];
    _.forEach(pacsPerWic, function (o) {
        _.forEach(o.pacs, function (p) {
            pacsPerWicFlattened.push({
                "industry": o.wic.description,
                "wicClassification": o.wic.id + " - " + o.wic.description,
                "wicDetailedDescription": o.wic.detailedDescription,
                "primaryActivityDescription": p.description,
                "primaryActivityCode": parseInt(p.id, 10),
                "code": o.wic.id,
                "wicRate": o.wic.wicRate,
                "ddlContribution": o.wic.ddlContribution
            });
        });
    });

    // Paginate 
    var paginatedItems = _.chunk(pacsPerWicFlattened, pageSizeFilter);
    var targetPage = paginatedItems[pageFilter - 1];


    // Response object 
    var responseObject = {
        "meta": {
            "totalRecords": pacsPerWicFlattened.length,
            "totalPages": paginatedItems.length,
            "currentPage": pageFilter
        },
        "data": [
          {
              "type": "WIC",
              "attributes": pacsPerWicFlattened
          }
        ]
    };

    return responseObject;
}

/* businessNames */
exports.businessNames = function (req) {
    var responseObject = {
        "data": {
            "type": "Business",
            "id": "29080662568",
            "attributes": {
                "entityName": "Equfax Pty Limited",
                "abnStatus": "Active from 24 March 2000",
                "entityTypeInd": "PRV",
                "entityTypeText": "Australia Public Company",
                "gstStatus": "ACT",
                "getStatusFromDate": "",
                "addressState": "VIC",
                "postCode": "",
                "orgABN": "29080662568",
                "businesses": [
                        {
                            "name": "BAYCORP ADVANTAGE LIMITED-1",
                            "aBNStatusFromDate": "24 Apr 2001",
                            "ASICRegistration": {
                                "acn": "080662568",
                                "abn": "29080662568",
                                "registrationDate": "20-01-1998",
                                "status": "Registered",
                                "type": "Australia Property Company ...",
                                "localityOfRegisteredOffice": "",
                                "regulator": "Australian Securities & Investments Commission"
                            }
                        },
                        {
                            "name": "BAYCORP ADVANTAGE LIMITED-1",
                            "from": "24 Apr 2001",
                            "ASICRegistration": {
                                "acn": "080662568",
                                "abn": "29080662568",
                                "registrationDate": "20-01-1998",
                                "status": "Registered",
                                "type": "Australia Property Company ...",
                                "localityOfRegisteredOffice": "",
                                "regulator": "Australian Securities & Investments Commission"
                            }
                        }
                ]
            }
        }
    };

    return responseObject;
}


/* quickQuote */
exports.quickQuote = function (req) {
    // Add to state
    state.MockDataQuickQuotesEditable.push(req.body);

    // Generate a 16-digit quote number 
    var quoteId = _.floor(Math.random() * 9000) + 100000000000000;
    quoteId = _.replace(quoteId, ".0", "");

    // TODO: Transform to correct structure and return as the response object

    // TODO: For now return a static response
    var responseObject = {
        "data": {
            "type": "Quote",
            "id": quoteId,
            "attributes": {
                "accountID": "1400778",
                "periodStartDate": "2018-09-11T14:01:00Z",
                "periodEndDate": "2019-09-11T14:01:00Z",
                "estimatedPremium": {
                    "amount": 3007,
                    "amountCurrency": "[aud]"
                },
                "costCenters": [
                  {
                      "directWages": [
                        {
                            "pac": 0,
                            "grossValue": 100000,
                            "numberOfUnits": 0,
                            "totalWages": 100000,
                            "numberOfEmployees": 7,
                            "wagesAmount": 1000,
                            "businessDescription": "Business description",
                            "directWageID": {
                                "pacCode": "011100001",
                                "pacDescription": "Bulb propagating - outdoors",
                                "wicRate": 4.432,
                                "ddlContribution": 0.028,
                                "code": "011100",
                                "wicCode": "0",
                                "perCapitaFlag": false,
                                "wicDescription": "Plant Nurseries"
                            },
                            "numberOfApprentices": 2,
                            "apprenticesWages": 20000,
                            "wic": 0
                        }
                      ],
                      "centerNumber": "Center1",
                      "name": "Abc"
                  }
                ],
                "paymentPlans": [
                  {
                      "name": "icare NI Payment Plan_Yearly",
                      "downPayment": {
                          "amount": 3007,
                          "amountCurrency": "[aud]"
                      },
                      "total": {
                          "amount": 3007,
                          "amountCurrency": "[aud]"
                      },
                      "installment": {
                          "amount": 0,
                          "amountCurrency": "[aud]"
                      },
                      "billingId": "icare_bcpp:1"
                  },
                  {
                      "name": "icare NI Payment Plan_Quarterly",
                      "downPayment": {
                          "amount": 751.75,
                          "amountCurrency": "[aud]"
                      },
                      "total": {
                          "amount": 3007,
                          "amountCurrency": "[aud]"
                      },
                      "installment": {
                          "amount": 751.75,
                          "amountCurrency": "[aud]"
                      },
                      "billingId": "icare_bcpp:3"
                  }
                ],
                "lossHistoryEntry": [
                  {
                      "claimYear3": 100,
                      "claimYears2": 100,
                      "claimYears1": 100,
                      "basicTariffPremiumYears3": 100,
                      "basicTariffPremiumYears2": 100,
                      "basicTariffPremiumYears1": 100,
                      "costCenter": "abc",
                      "directWageId": {
                          "wicRate": 4.432,
                          "wicDescription": null,
                          "wicCode": "011100 - Plant Nurseries",
                          "perCapitaFlag": false,
                          "pacDescription": "Bulb propagating - outdoors",
                          "pacCode": "011100001",
                          "ddlContribution": 0.028,
                          "code": "011100"
                      },
                      "location": "Sydney",
                      "wages1": null,
                      "wages2": null,
                      "wages3": null
                  }
                ],
                "commencementDate": "2019-09-11T14:01:00Z"
            }
        }
    };

    return responseObject;
}

/* fullQuote */
exports.fullQuote = function (req) {
    //var responseObject = req.body;
    var quoteId = req.params.quoteId;

    //// Get the quote given the quote number
    //var targetQuote = _.find(state.MockDataFullQuotesEditable, function (o) {
    //    return o.data.id === quoteId;
    //});

    //// Do not proceed when there is an existing quote id
    //if (targetQuote) return ERRORS.E400;

    //// add to state 
    //state.MockDataFullQuotesEditable.push(responseObject);

    // TODO: Transform to correct structure and return as the response object

    // TODO: For now return a static response
    var annualAmount = 3659.79; // Confirmed
    var mobileNumber = req.body.data.attributes.draftData.accountContact[0].personContact.mobileNumber;
    if (mobileNumber === "987654321") {
        annualAmount = 36590.79; // Indicative
    }

    var firstName = req.body.data.attributes.draftData.accountContact[0].personContact.firstName;
    var lastName = req.body.data.attributes.draftData.accountContact[0].personContact.lastName;
    var email = req.body.data.attributes.draftData.accountContact[0].personContact.emailAddress1;
    var displayName = req.body.data.attributes.draftData.accountContact[0].personContact.displayName;

    var responseObject = {
        "data": {
            "type": "Quote",
            "id": quoteId,
            "attributes": {
                "draftData": {
                    "account": {
                        "displayName": "Testing",
                        "companyName": "Testing",
                        "accountHolder": false,
                        "primaryPhoneType": "mobile",
                        "comunicatnPrfrnce": "email",
                        "mobileNumber": "0426353063",
                        "emailAddress1": "xyz@test.com",
                        "accountOrgType": "partnership",
                        "isVedaValidated": false,
                        "primaryLanguage": "en_US",
                        "gstRegistration": false,
                        "itcEntitlement": "0",
                        "commencementDate": "2018-12-06T14:00:00Z",
                        "abn": "29080662568",
                        "publicId": "pc:4464"
                    },
                    "policyAddress": {
                        "displayName": "41 Navy Rd, LLANDILO NSW 2747",
                        "isAddressVerified": true,
                        "addressLine1": "41 Navy Rd",
                        "city": "LLANDILO",
                        "state": "AU_NSW",
                        "postalCode": "2747",
                        "country": "AU",
                        "addressType": "business",
                        "publicId": "pc:12018"
                    },
                    "productCode": "WC_ICARE",
                    "offering": "WCNI_icare",
                    "accountContact": [
                      {
                          "contactRole": [
                            "LocationContact_icare"
                          ],
                          "contactUpdateFlag": false,
                          "contactRetired": false,
                          "personContact": {
                              "displayName": displayName,
                              "firstName": firstName,
                              "lastName": lastName,
                              "primaryPhoneType": "mobile",
                              "mobileNumber": mobileNumber,
                              "communicationPreference": "email",
                              "emailAddress1": email,
                              "smsNotifications": true,
                              "publicId": "pc:4889"
                          },
                          "contactAddress": [
                            {
                                "displayName": "41 Navy Rd, LLANDILO NSW 2747",
                                "isAddressVerified": false,
                                "addressLine1": "41 Navy Rd",
                                "city": "LLANDILO",
                                "state": "AU_NSW",
                                "postalCode": "2747",
                                "country": "AU",
                                "addressType": "postal",
                                "publicId": "pc:14426"
                            }
                          ],
                          "publicId": "pc:3380"
                      }
                    ],
                    "productName": "Workers' Insurance",
                    "previousEndDate": "2019-09-30T14:01:00Z",
                    "periodStartDate": "2018-12-06T14:01:00Z",
                    "periodEndDate": "2019-11-30T14:01:00Z",
                    "preQualQuestionSets": [
                      {
                          "code": "WCNIPreQual_icare",
                          "answers": {
                              "annualGrossWages": false,
                              "employeeApprenticeTrainee": true,
                              "employeeTaxiJockeyBoxerWrestler": false,
                              "employeeNSW": true,
                              "groupOfCompanies": false
                          }
                      },
                      {
                          "code": "WCSPORTINGPreQual_icare",
                          "answers": {
                              "orgAffilOutsideNSW": null,
                              "orgDetails": null,
                              "orgRegOutsideNSW": null,
                              "orgBasedNSW": null,
                              "orgNSW": null,
                              "perPartOutNSW": null,
                              "affiDetails": null
                          }
                      }
                    ],
                    "lobs": {
                        "wcLine": {
                            "policyPeriod": {
                                "policyType": "Standard",
                                "termNumber": 0,
                                "brokerOrganisation": "Icare",
                                "termType": "Other",
                                "writtenDate": "2018-12-06T13:00:00Z",
                                "rateDate": "2018-12-06T06:54:24Z",
                                "gstRegistration": false,
                                "itcEntitlement": "0",
                                "schemeAgent": "EmployersMutual-016",
                                "reasonLowWages": "none",
                                "policy": {
                                    "primaryLanguage": "en_US",
                                    "priorPolicy": [

                                    ],
                                    "lossHistoryEntry": [

                                    ]
                                },
                                "policyTerm": {

                                },
                                "jobRoleAssignment": [
                                  {
                                      "assignedUser": "integportal user",
                                      "assignedGroup": "Icare Grp",
                                      "jobUserRoleAssignment": "100000000003955"
                                  },
                                  {
                                      "assignedUser": "Super User",
                                      "assignedGroup": "Icare Grp",
                                      "jobUserRoleAssignment": "100000000003955"
                                  }
                                ],
                                "policyLocation": [
                                  {
                                      "isRatedLocation": true,
                                      "locationRetired": false,
                                      "primaryLocation": true,
                                      "accountLocation": {
                                          "nonSpecific": true,
                                          "address": {
                                              "isAddressVerified": true,
                                              "addressLine1": "41 Navy Rd",
                                              "city": "LLANDILO",
                                              "state": "AU_NSW",
                                              "postalCode": "2747",
                                              "country": "AU",
                                              "addressType": "business",
                                              "publicId": "pc:14427"
                                          }
                                      },
                                      "costCenters": [
                                        {
                                            "fullName": "PolicyLocation : 1",
                                            "directWages": [
                                              {
                                                  "wic": 0,
                                                  "pac": 0,
                                                  "grossValue": 125000,
                                                  "numberOfApprentice": 0,
                                                  "numberOfUnits": 0,
                                                  "appWages": 0,
                                                  "totalWages": 125000,
                                                  "numnberOfEmployees": 10,
                                                  "wagesAmount": 0,
                                                  "businessDescription": "test",
                                                  "asbestoses": [

                                                  ],
                                                  "taxiPlates": [

                                                  ],
                                                  "contactorWages": [

                                                  ],
                                                  "directWageId": {
                                                      "pacCode": "011300001",
                                                      "pacDescription": "Bulb propagating - outdoors",
                                                      "wicRate": 4.432,
                                                      "ddlContribution": 0.028,
                                                      "code": "011300",
                                                      "wicCode": "0",
                                                      "perCapitaFlag": false,
                                                      "wicDescription": "Plant Nurseries"
                                                  }
                                              }
                                            ],
                                            "publicId": "pc:8993"
                                        }
                                      ],
                                      "publicId": "pc:5690"
                                  }
                                ],
                                "brokerName": "icarewc",
                                "wcLabourHireCode": "2",
                                "brokerId": "icarewc",
                                "publicId": "pc:4890"
                            }
                        }
                    },
                    "termType": "Other",
                    "icareTermType": "ShortTerm_DR"
                },
                "quoteData": {
                    "offeredQuotes": [
                      {
                          "branchCode": "WCNI_icare",
                          "isCustom": true,
                          "annualPremium": {
                              "amount": 3852.41,
                              "amountCurrency": "aud"
                          },
                          "termMonths": 11,
                          "taxes": {
                              "amount": 350.22,
                              "amountCurrency": "aud"
                          },
                          "lobs": {
                              "wcLine": {
                                  "wageDetails": {
                                      "wagePremium": [
                                        {
                                            "wic": "011300",
                                            "rate": 3.22,
                                            "totalAnnualWages": {
                                                "amount": 125000,
                                                "amountCurrency": "aud"
                                            },
                                            "averagePerformancePremium": {
                                                "amount": 4025.77,
                                                "amountCurrency": "aud"
                                            }
                                        }
                                      ],
                                      "totalAveragePerformancePremium": {
                                          "amount": 4025.77,
                                          "amountCurrency": "aud"
                                      }
                                  },
                                  "esiPremium": {
                                      "esiRate": 10,
                                      "averagePerformancePremium": {
                                          "amount": 4025.77,
                                          "amountCurrency": "aud"
                                      },
                                      "esi": {
                                          "amount": -402.58,
                                          "amountCurrency": "aud"
                                      }
                                  },
                                  "claimPerformance": {
                                      "claimPerformanceMeasure": 0,
                                      "schemePerformanceMeasure": 0,
                                      "claimPerformanceRate": 0,
                                      "claimPerformanceAdjustment": 0
                                  },
                                  "averagePerformancePremium": {
                                      "amount": 4025.77,
                                      "amountCurrency": "aud"
                                  },
                                  "annualisedAPP": {
                                      "amount": 4280,
                                      "amountCurrency": "aud"
                                  },
                                  "premium": {
                                      "amount": 3852.41,
                                      "amountCurrency": "aud"
                                  },
                                  "dustDiseaseLavy": {
                                      "amount": 10.97,
                                      "amountCurrency": "aud"
                                  },
                                  "asbestosCost": {
                                      "amount": 0,
                                      "amountCurrency": "aud"
                                  },
                                  "totalDDL": {
                                      "amount": 10.97,
                                      "amountCurrency": "aud"
                                  },
                                  "apprenticeDiscount": {
                                      "amount": 0,
                                      "amountCurrency": "aud"
                                  },
                                  "performanceDiscount": {
                                      "amount": 0,
                                      "amountCurrency": "aud"
                                  },
                                  "mineSafetyCost": {
                                      "amount": 0,
                                      "amountCurrency": "aud"
                                  }
                              }
                          },
                          "publicId": "pc:4890"
                      }
                    ]
                },
                "bindData": {
                    "paymentReference": "258251",
                    "fullPayDisPercentage": "5.00",
                    "paymentPlans": [
                      {
                          "name": "icare NI Payment Plan_Yearly",
                          "downPayment": {
                              "amount": annualAmount,
                              "amountCurrency": "aud"
                          },
                          "total": {
                              "amount": annualAmount,
                              "amountCurrency": "aud"
                          },
                          "installment": {
                              "amount": 0,
                              "amountCurrency": "aud"
                          },
                          "billingId": "icare_bcpp:1"
                      },
                      {
                          "name": "icare NI Payment Plan_Quarterly",
                          "downPayment": {
                              "amount": 963.11,
                              "amountCurrency": "aud"
                          },
                          "total": {
                              "amount": 3852.41,
                              "amountCurrency": "aud"
                          },
                          "installment": {
                              "amount": 963.1,
                              "amountCurrency": "aud"
                          },
                          "billingId": "icare_bcpp:3"
                      }
                    ],
                    "isUWIssueAvailable": false
                },
                "status": "Quoted",
                "accountId": "1400932",
                "sendDocument": null,
                "employerCategory": "Employer",
                "policyChangeReason": "PremiumChange"
            }
        }
    };

    return responseObject;
}

/* retrieveQuote */
exports.retrieveQuote = function (req) {
    var quoteId = req.params.quoteId;

    //// Get the quote given the quote number
    //var targetQuote = _.find(state.MockDataFullQuotesEditable, function (o) {
    //    return o.data.id === quoteId;
    //});
    //if (!targetQuote) return ERRORS.E404;

    ///* Generate output JSON */
    //var responseObject = targetQuote;

    if (quoteId === "100000000003411") {
        return ERRORS.E404;
    }

    return responseObject = {
        "data": {
            "type": "Quote",
            "id": quoteId,
            "attributes": {
                "accountId": "1400620",
                "status": "Bound",
                "draftData": {
                    "account": {
                        "publicId": "pc:4305",
                        "accountHolder": false,
                        "displayName": "Equfax Pty Limited",  // Entity name
                        "companyName": "Equfax Pty Limited",  // Entity name
                        "primaryPhoneType": "mobile",
                        "mobileNumber": "+61492403000",
                        "communicationPreference": "email",
                        "accountOrgType": "individualsoletrader",   // Is your business a
                        "isVedaValidated": false,
                        "gstRegistration": true,
                        "itcEntitlement": "100",    // ITC entitlement in %
                        "commencementDate": "2016-01-08T13:00:00Z", // What date did the business start trading in NSW? If the business has been purchased, please provide the date the original owner started trading
                        "emailAddress1": "amvohra@deloitte.com.au",
                        "primaryLanguage": "en_US",
                        "abn": "29080662568",
                        "tradingBusinessName": "BAYCORP ADVANTAGE LIMITED-1",
                    },
                    "policyAddress": {
                        "displayName": "IVY, LEVEL 2",
                        "isAddressVerified": true,
                        "addressLine1": "320-330 GEORGE ST",    // Enter your primary business premise
                        "city": "Sydney",
                        "state": "AU_NSW",
                        "postalCode": "2000",
                        "country": "AU",
                        "addressType": "business",
                        "publicId": "pc:12018"
                    },
                    "accountContact": [
                      {
                          "contactRole": [
                            "LocationContact_icare"
                          ],
                          "publicId": "pc:2787",
                          "contactUpdateFlag": false,
                          "contactRetired": false,
                          "personContact": {
                              "displayName": "James Vuong",
                              "firstName": "James",
                              "lastName": "Vuong",
                              "primaryPhoneType": "mobile",
                              "mobileNumber": "0444333222",
                              "communicationPreference": "email",
                              "emailAddress1": "jvuong@deloitte.com.au",
                              "smsNotifications": false,
                              "publicId": "pc:4306"
                          },
                          "contactAddress": [
                            {
                                "displayName": "Merival, Melbourne HQ",
                                "isAddressVerified": false,
                                "addressLine1": "899-890 Chad Street",
                                "city": "Melbourne",
                                "state": "AU_QLD",
                                "postalCode": "4487",
                                "country": "AU",
                                "publicId": "pc:6047",
                                "addressType": "postal"
                            }
                          ]
                      }
                    ],
                    "productCode": "WC_ICARE",
                    "productName": "Workers' Insurance",
                    "offering": "WCNI_icare",
                    "termType": "Annual",
                    "periodStartDate": "2018-11-14T13:01:00Z",  // Policy start date
                    "periodEndDate": "2019-11-14T13:01:00Z",    // Policy end date
                    "preQualQuestionSets": [
                      {
                          "code": "WCNIPreQual_icare",
                          "answers": {
                              "annualGrossWages": "",
                              "employeeApprenticeTrainee": "",
                              "employeePerCapita": "",
                              "employeeNSW": "true",
                              "groupOfCompanies": "true"
                          }
                      },
                      {
                          "code": "WCSPORTINGPreQual_icare",
                          "answers": {
                              "annualGrossWages": "",
                              "employeeApprenticeTrainee": "",
                              "employeePerCapita": "",
                              "employeeNSW": "",
                              "groupOfCompanies": ""
                          }
                      }
                    ],
                    "lobs": {
                        "wcLine": {
                            "policyPeriod": {
                                "policyType": "Standard",
                                "brokerId": "Branch1",	// Branch
                                "brokerOrganisation": "BrokerOrganisation1",	// Enter Broker Organization
                                "termType": "Annual",
                                "termNumber": 1,
                                "writtenDate": "2018-10-25T13:00:00Z",
                                "rateAsOfDate": "2018-10-26T08:40:33Z",
                                "gstRegistration": true,
                                "itcEntitlement": "100",
                                "wcLabourHireCode": "2",	// Labour hire
                                "schemeAgent": "EmployersMutual-016",
                                "policy": {
                                    "primaryLanguage": "en_US",
                                    "priorPolicy": [

                                    ],
                                    "lossHistoryEntry": [

                                    ]
                                },
                                "claim": [

                                ],
                                "policyTerm": {

                                },
                                "jobRoleAssignment": [
                                  {
                                      "assignedUser": "Super User",
                                      "assignedGroup": "Icare Grp",
                                      "jobUserRoleAssignment": "100000000003404"
                                  },
                                  {
                                      "assignedUser": "integportal user",
                                      "assignedGroup": "Icare Grp",
                                      "jobUserRoleAssignment": "100000000003404"
                                  }
                                ],
                                "policyLocation": [
                                  {
                                      "isRatedLocation": true,
                                      "publicId": "pc:5010",
                                      "locationRetired": false,
                                      "primaryLocation": true,
                                      "accountLocation": {
                                          "nonSpecific": true,
                                          "address": {
                                              "publicId": "pc:6048",
                                              "isAddressVerified": true,
                                              "postalCode": "2000",
                                              "country": "AU",
                                              "addressType": "business",
                                              "addressLine1": "1 Leeroy St",  // Enter your primary business premise
                                              "city": "Sydney",
                                              "state": "AU_NSW"
                                          }
                                      },
                                      "costCenters": [
                                        {
                                            "fullName": "PolicyLocation : 1",
                                            "directWages": [
                                              {
                                                  "wic": 0,
                                                  "pac": 0,
                                                  "grossValue": 300000,
                                                  "numberOfApprentices": 0,
                                                  "numberOfUnits": 0,
                                                  "apprenticesWages": 0,
                                                  "totalWages": 300000,
                                                  "numberOfEmployees": 15,
                                                  "wagesAmount": 300000,
                                                  "businessDescription": "Restaurant operation",
                                                  "directWageId": {
                                                      "pacCode": "573000007",
                                                      "pacDescription": "Restaurant operation",
                                                      "wicRate": 4.432,
                                                      "ddlContribution": 0.028,
                                                      "code": "573000",
                                                      "wicCode": "73000 - Cafes and Restaurants",
                                                      "perCapitaFlag": "N",
                                                      "wicDescription": "Cafes and Restaurants"
                                                  },
                                                  "asbestoses": [

                                                  ],
                                                  "taxiPlates": [

                                                  ],
                                                  "contactorWages": [

                                                  ]
                                              }
                                            ]
                                        }
                                      ]
                                  }
                                ],
                                "brokerName": "Branch1", // Branch
                                "publicId": "pc:4853"
                            }
                        }
                    }
                },
                "quoteData": {
                    "offeredQuotes": [
                      {
                          "publicId": "pc:4853",
                          "branchCode": "WCNI_icare",
                          "isCustom": true,
                          "annualPremium": {
                              "amount": 38630,
                              "amountCurrency": "aud"
                          },
                          "previousAnnualPremium": {

                          },
                          "transactionCost": {

                          },
                          "termMonths": 12,
                          "taxes": {
                              "claimPerformance": 3511.82,
                              "amountCurrency": "aud"
                          },
                          "lobs": {
                              "wcLine": {
                                  "wageDetails": {
                                      "totalAveragePerformancePremium": {
                                          "amount": 42800,
                                          "amountcurrency": "aud"
                                      },
                                      "wagePremium": [
                                        {
                                            "rate": 4.28,
                                            "totalAnnualWages": {
                                                "amount": 1000000,
                                                "amountcurrency": "aud"
                                            },
                                            "averagePerformancePremium": {
                                                "amount": 42800,
                                                "amountcurrency": "aud"
                                            }
                                        }
                                      ]
                                  },
                                  "esiPremium": {
                                      "esiRate": 10,
                                      "averagePerformancePremium": {
                                          "amount": 42800,
                                          "amountcurrency": "aud"
                                      },
                                      "esi": {
                                          "amount": -4280,
                                          "amountcurrency": "aud"
                                      }
                                  },
                                  "claimPerformance": {
                                      "totalPriorApp": {

                                      },
                                      "totalPriorLoss": {

                                      },
                                      "schemePerformanceMeasure": 0,
                                      "claimPerformanceAdjustment": 1,
                                      "claimPerformanceRate": 0,
                                      "claimPerformanceMeasure": 0,
                                      "premiumRiskAssessment": {

                                      },
                                      "cprAdjustment": {

                                      }
                                  },
                                  "averagePerformancePremium": {
                                      "amount": 42800,
                                      "amountcurrency": "aud"
                                  },
                                  "annualisedApp": {
                                      "amount": 42800,
                                      "amountcurrency": "aud"
                                  },
                                  "premium": {
                                      "amount": 38630,
                                      "amountcurrency": "aud"
                                  },
                                  "dustDiseaseLavy": {
                                      "amount": 110,
                                      "amountcurrency": "aud"
                                  },
                                  "asbestosCost": {
                                      "amount": 0,
                                      "amountcurrency": "aud"
                                  },
                                  "totalDdl": {
                                      "amount": 110,
                                      "amountcurrency": "aud"
                                  },
                                  "apprenticeDiscount": {
                                      "amount": 0,
                                      "amountcurrency": "aud"
                                  },
                                  "performanceDiscount": {
                                      "amount": 0,
                                      "amountcurrency": "aud"
                                  },
                                  "itcAmount": {

                                  },
                                  "mineSafetyCost": {
                                      "amount": 0,
                                      "amountcurrency": "aud"
                                  }
                              }
                          }
                      }
                    ]
                },
                "bindData": {
                    "paymentReference": "258251",
                    "bPayReference": "140062001320",
                    "chosenQuote": "pc:4853",
                    "registrationCode": "CAY5PFHQ",
                    "policyNumber": "140062001",
                    "paymentPlans": [
                      {
                          "name": "icare NI Payment Plan_Monthly",
                          "downPayment": {
                              "amount": 3219.13,
                              "amountcurrency": "aud"
                          },
                          "total": {
                              "amount": 38630,
                              "amountcurrency": "aud"
                          },
                          "installment": {
                              "amount": 3219.17,
                              "amountcurrency": "aud"
                          },
                          "billingId": "icare_bcpp:1"
                      }
                    ],
                    "isUwIssueAvailable": true,
                    "billingAddress": {

                    },
                    "paymentDetails": {
                        "bankAccountData": {

                        },
                        "creditCardData": {

                        }
                    },
                    "selectedPaymentPlan": "icare_bcpp:1"
                }
            }
        }
    };
}

/* saveQuote */
exports.saveQuote = function (req) {
    // var responseObject = req.body;
    var quoteId = req.params.quoteId;

    //// Get the quote given the quote number
    //var targetQuote = _.find(state.MockDataFullQuotesEditable, function (o) {
    //    return o.data.id === quoteId;
    //});
    //if (!targetQuote) return ERRORS.E404;

    //// remove from state 
    //state.MockDataFullQuotesEditable = _.reject(state.MockDataFullQuotesEditable, function (o) {
    //    return _.toString(o.data.id) === _.toString(quoteId);
    //});

    //// add back to state 
    //state.MockDataFullQuotesEditable.push(responseObject);

    // TODO: Return a static response for now
    var firstName = req.body.data.attributes.draftData.accountContact[0].personContact.firstName;
    var lastName = req.body.data.attributes.draftData.accountContact[0].personContact.lastName;
    var email = req.body.data.attributes.draftData.accountContact[0].personContact.emailAddress1;
    var displayName = req.body.data.attributes.draftData.accountContact[0].personContact.displayName;

    //var responseObject = {
    //    "data": {
    //        "type": "Quote",
    //        "id": quoteId,
    //        "attributes": {
    //            "accountId": "1401056",
    //            "status": "Draft",
    //            "draftData": {
    //                "account": {
    //                    "publicId": "pc:4943",
    //                    "accountHolder": false,
    //                    "displayName": "F & Co Services",
    //                    "companyName": "F & Co Services",
    //                    "primaryPhoneType": "mobile",
    //                    "mobileNumber": "0444444444",
    //                    "communicationPreference": "post",
    //                    "accountOrgType": "individualsoletrader",
    //                    "isVedaValidated": false,
    //                    "gstRegistration": false,   // Are you registered for GST
    //                    "itcEntitlement": "100",
    //                    "commencementDate": "2018-11-06T13:00:00Z",
    //                    "emailAddress1": email,
    //                    "isAnonymousAccount": false,
    //                    "primaryLanguage": "en_US"
    //                },
    //                "policyAddress": {
    //                    "displayName": "Lumley House, 321 Kent St, SYDNEY NSW 2000",
    //                    "isAddressVerified": true,
    //                    "addressLine1": "Lumley House, 321 Kent St",
    //                    "city": "SYDNEY",
    //                    "state": "AU_NSW",
    //                    "postalCode": "2000",
    //                    "country": "AU",
    //                    "addressType": "business",
    //                    "publicId": "pc:7638"
    //                },
    //                "accountContact": [
    //                  {
    //                      "contactRole": [
    //                        "LocationContact_icare"
    //                      ],
    //                      "publicId": "pc:3338",
    //                      "contactUpdateFlag": false,
    //                      "contactRetired": false,
    //                      "personContact": {
    //                          "displayName": displayName,
    //                          "firstName": firstName,
    //                          "lastName": lastName,
    //                          "primaryPhoneType": "mobile",
    //                          "mobileNumber": "0444444444",
    //                          "communicationPreference": "post",
    //                          "emailAddress1": email,
    //                          "smsNotifications": false,
    //                          "publicId": "pc:4944"
    //                      },
    //                      "contactAddress": [
    //                        {
    //                            "displayName": "Lumley House, 321 Kent St, SYDNEY NSW 2000",
    //                            "isAddressVerified": true,
    //                            "addressLine1": "Lumley House, 321 Kent St",
    //                            "city": "SYDNEY",
    //                            "state": "AU_NSW",
    //                            "postalCode": "2000",
    //                            "country": "AU",
    //                            "publicId": "pc:7640",
    //                            "addressType": "postal"
    //                        }
    //                      ]
    //                  }
    //                ],
    //                "productCode": "WC_ICARE",
    //                "productName": "Workers' Insurance",
    //                "offering": "WCNI_icare",
    //                "termType": "Annual",
    //                "periodStartDate": "2018-11-07T13:01:00Z",
    //                "periodEndDate": "2019-11-07T13:01:00Z",
    //                "preQualQuestionSets": [
    //                  {
    //                      "code": "WCNIPreQual_icare",
    //                      "answers": {
    //                          "annualGrossWages": "true",
    //                          "employeeApprenticeTrainee": "false",
    //                          "employeePerCapita": "",
    //                          "employeeNSW": "true",
    //                          "groupOfCompanies": "false"
    //                      }
    //                  },
    //                  {
    //                      "code": "WCSPORTINGPreQual_icare",
    //                      "answers": {
    //                          "annualGrossWages": "",
    //                          "employeeApprenticeTrainee": "",
    //                          "employeePerCapita": "",
    //                          "employeeNSW": "",
    //                          "groupOfCompanies": ""
    //                      }
    //                  }
    //                ],
    //                "lobs": {
    //                    "wcLine": {
    //                        "policyPeriod": {
    //                            "policyType": "Standard",
    //                            "brokerId": "icarewc",
    //                            "brokerOrganisation": "Icare",
    //                            "termType": "Annual",
    //                            "termNumber": 0,
    //                            "writtenDate": "2018-11-09T13:00:00Z",
    //                            "rateAsOfDate": "2018-11-11T14:38:24Z",
    //                            "gstRegistration": false,   // Are you registered for GST
    //                            "itcEntitlement": "100",
    //                            "isLegacy": false,
    //                            "wcLabourHireCode": "2",
    //                            "schemeAgent": "EmployersMutual-016",
    //                            "policy": {
    //                                "primaryLanguage": "en_US",
    //                                "priorPolicy": [

    //                                ],
    //                                "lossHistoryEntry": [

    //                                ]
    //                            },
    //                            "claim": [

    //                            ],
    //                            "policyTerm": {

    //                            },
    //                            "jobRoleAssignment": [
    //                              {
    //                                  "assignedUser": "integportal user",
    //                                  "assignedGroup": "Icare Grp",
    //                                  "jobUserRoleAssignment": "100000000003865"
    //                              },
    //                              {
    //                                  "assignedUser": "Super User",
    //                                  "assignedGroup": "Icare Grp",
    //                                  "jobUserRoleAssignment": "100000000003865"
    //                              }
    //                            ],
    //                            "policyLocation": [
    //                              {
    //                                  "isRatedLocation": true,
    //                                  "publicId": "pc:5684",
    //                                  "locationRetired": false,
    //                                  "primaryLocation": true,
    //                                  "accountLocation": {
    //                                      "nonSpecific": true,
    //                                      "address": {
    //                                          "publicId": "pc:7660",
    //                                          "isAddressVerified": true,
    //                                          "postalCode": "2000",
    //                                          "country": "AU",
    //                                          "addressType": "postal",
    //                                          "addressLine1": "Lumley House, 321 Kent St",
    //                                          "city": "SYDNEY",
    //                                          "state": "AU_NSW"
    //                                      }
    //                                  },
    //                                  "costCenters": [
    //                                    {
    //                                        "fullName": "PolicyLocation : 1",
    //                                        "directWages": [
    //                                          {
    //                                              "wic": 0,
    //                                              "pac": 0,
    //                                              "grossValue": 349399993,
    //                                              "numberOfApprentices": 0,
    //                                              "numberOfUnits": 0,
    //                                              "apprenticesWages": 0,
    //                                              "totalWages": 349399993,
    //                                              "numberOfEmployees": 2930,
    //                                              "wagesAmount": 0,
    //                                              "businessDescription": "Financial Services",
    //                                              "directWageId": {
    //                                                  "pacCode": "751900006",
    //                                                  "pacDescription": "Financial asset investment consultant service",
    //                                                  "wicRate": 0.215,
    //                                                  "ddlContribution": 0.028,
    //                                                  "code": "751900",
    //                                                  "wicCode": "0",
    //                                                  "perCapitaFlag": "N",
    //                                                  "wicDescription": "Services to Finance and Investment nec"
    //                                              },
    //                                              "asbestoses": [

    //                                              ],
    //                                              "taxiPlates": [

    //                                              ],
    //                                              "contactorWages": [

    //                                              ]
    //                                          }
    //                                        ]
    //                                    }
    //                                  ]
    //                              }
    //                            ],
    //                            "brokerName": "icarewc",
    //                            "publicId": "pc:5320"
    //                        }
    //                    }
    //                }
    //            },
    //            "quoteData": {
    //                "offeredQuotes": [

    //                ]
    //            },
    //            "bindData": {
    //                "paymentPlans": [

    //                ],
    //                "isUwIssueAvailable": false,
    //                "billingAddress": {

    //                },
    //                "paymentDetails": {
    //                    "bankAccountData": {

    //                    },
    //                    "creditCardData": {

    //                    }
    //                }
    //            }
    //        }
    //    }
    //};

    //return responseObject;

    var responseObject = req.body;
    responseObject.data.attributes.bindData = {
        "billingAddress": {

        },
        "isUwIssueAvailable": false,
        "paymentDetails": {
            "bankAccountData": {

            },
            "creditCardData": {

            }
        },
        "paymentPlans": []
    };
    responseObject.data.attributes.quoteData = {
        "offeredQuotes": []
    };
    responseObject.data.attributes.status = "Draft";

    return responseObject;
}

/* saveAuthQuote */
exports.saveAuthQuote = function (req) {
    // var responseObject = req.body;
    var quoteId = req.params.quoteId;

    //// Get the quote given the quote number
    //var targetQuote = _.find(state.MockDataFullQuotesEditable, function (o) {
    //    return o.data.id === quoteId;
    //});
    //if (!targetQuote) return ERRORS.E404;

    //// remove from state 
    //state.MockDataFullQuotesEditable = _.reject(state.MockDataFullQuotesEditable, function (o) {
    //    return _.toString(o.data.id) === _.toString(quoteId);
    //});

    //// add back to state 
    //state.MockDataFullQuotesEditable.push(responseObject);

    // TODO: Return a static response for now
    var firstName = req.body.data.attributes.draftData.accountContact[0].personContact.firstName;
    var lastName = req.body.data.attributes.draftData.accountContact[0].personContact.lastName;
    var email = req.body.data.attributes.draftData.accountContact[0].personContact.emailAddress1;
    var displayName = req.body.data.attributes.draftData.accountContact[0].personContact.displayName;

    //var responseObject = {
    //    "data": {
    //        "type": "Quote",
    //        "id": quoteId,
    //        "attributes": {
    //            "accountId": "1401056",
    //            "status": "Draft",
    //            "draftData": {
    //                "account": {
    //                    "publicId": "pc:4943",
    //                    "accountHolder": false,
    //                    "displayName": "F & Co Services",
    //                    "companyName": "F & Co Services",
    //                    "primaryPhoneType": "mobile",
    //                    "mobileNumber": "0444444444",
    //                    "communicationPreference": "post",
    //                    "accountOrgType": "individualsoletrader",
    //                    "isVedaValidated": false,
    //                    "gstRegistration": false,   // Are you registered for GST
    //                    "itcEntitlement": "100",
    //                    "commencementDate": "2018-11-06T13:00:00Z",
    //                    "emailAddress1": email,
    //                    "isAnonymousAccount": false,
    //                    "primaryLanguage": "en_US"
    //                },
    //                "policyAddress": {
    //                    "displayName": "Lumley House, 321 Kent St, SYDNEY NSW 2000",
    //                    "isAddressVerified": true,
    //                    "addressLine1": "Lumley House, 321 Kent St",
    //                    "city": "SYDNEY",
    //                    "state": "AU_NSW",
    //                    "postalCode": "2000",
    //                    "country": "AU",
    //                    "addressType": "business",
    //                    "publicId": "pc:7638"
    //                },
    //                "accountContact": [
    //                  {
    //                      "contactRole": [
    //                        "LocationContact_icare"
    //                      ],
    //                      "publicId": "pc:3338",
    //                      "contactUpdateFlag": false,
    //                      "contactRetired": false,
    //                      "personContact": {
    //                          "displayName": displayName,
    //                          "firstName": firstName,
    //                          "lastName": lastName,
    //                          "primaryPhoneType": "mobile",
    //                          "mobileNumber": "0444444444",
    //                          "communicationPreference": "post",
    //                          "emailAddress1": email,
    //                          "smsNotifications": false,
    //                          "publicId": "pc:4944"
    //                      },
    //                      "contactAddress": [
    //                        {
    //                            "displayName": "Lumley House, 321 Kent St, SYDNEY NSW 2000",
    //                            "isAddressVerified": true,
    //                            "addressLine1": "Lumley House, 321 Kent St",
    //                            "city": "SYDNEY",
    //                            "state": "AU_NSW",
    //                            "postalCode": "2000",
    //                            "country": "AU",
    //                            "publicId": "pc:7640",
    //                            "addressType": "postal"
    //                        }
    //                      ]
    //                  }
    //                ],
    //                "productCode": "WC_ICARE",
    //                "productName": "Workers' Insurance",
    //                "offering": "WCNI_icare",
    //                "termType": "Annual",
    //                "periodStartDate": "2018-11-07T13:01:00Z",
    //                "periodEndDate": "2019-11-07T13:01:00Z",
    //                "preQualQuestionSets": [
    //                  {
    //                      "code": "WCNIPreQual_icare",
    //                      "answers": {
    //                          "annualGrossWages": "true",
    //                          "employeeApprenticeTrainee": "false",
    //                          "employeePerCapita": "",
    //                          "employeeNSW": "true",
    //                          "groupOfCompanies": "false"
    //                      }
    //                  },
    //                  {
    //                      "code": "WCSPORTINGPreQual_icare",
    //                      "answers": {
    //                          "annualGrossWages": "",
    //                          "employeeApprenticeTrainee": "",
    //                          "employeePerCapita": "",
    //                          "employeeNSW": "",
    //                          "groupOfCompanies": ""
    //                      }
    //                  }
    //                ],
    //                "lobs": {
    //                    "wcLine": {
    //                        "policyPeriod": {
    //                            "policyType": "Standard",
    //                            "brokerId": "icarewc",
    //                            "brokerOrganisation": "Icare",
    //                            "termType": "Annual",
    //                            "termNumber": 0,
    //                            "writtenDate": "2018-11-09T13:00:00Z",
    //                            "rateAsOfDate": "2018-11-11T14:38:24Z",
    //                            "gstRegistration": false,   // Are you registered for GST
    //                            "itcEntitlement": "100",
    //                            "isLegacy": false,
    //                            "wcLabourHireCode": "2",
    //                            "schemeAgent": "EmployersMutual-016",
    //                            "policy": {
    //                                "primaryLanguage": "en_US",
    //                                "priorPolicy": [

    //                                ],
    //                                "lossHistoryEntry": [

    //                                ]
    //                            },
    //                            "claim": [

    //                            ],
    //                            "policyTerm": {

    //                            },
    //                            "jobRoleAssignment": [
    //                              {
    //                                  "assignedUser": "integportal user",
    //                                  "assignedGroup": "Icare Grp",
    //                                  "jobUserRoleAssignment": "100000000003865"
    //                              },
    //                              {
    //                                  "assignedUser": "Super User",
    //                                  "assignedGroup": "Icare Grp",
    //                                  "jobUserRoleAssignment": "100000000003865"
    //                              }
    //                            ],
    //                            "policyLocation": [
    //                              {
    //                                  "isRatedLocation": true,
    //                                  "publicId": "pc:5684",
    //                                  "locationRetired": false,
    //                                  "primaryLocation": true,
    //                                  "accountLocation": {
    //                                      "nonSpecific": true,
    //                                      "address": {
    //                                          "publicId": "pc:7660",
    //                                          "isAddressVerified": true,
    //                                          "postalCode": "2000",
    //                                          "country": "AU",
    //                                          "addressType": "postal",
    //                                          "addressLine1": "Lumley House, 321 Kent St",
    //                                          "city": "SYDNEY",
    //                                          "state": "AU_NSW"
    //                                      }
    //                                  },
    //                                  "costCenters": [
    //                                    {
    //                                        "fullName": "PolicyLocation : 1",
    //                                        "directWages": [
    //                                          {
    //                                              "wic": 0,
    //                                              "pac": 0,
    //                                              "grossValue": 349399993,
    //                                              "numberOfApprentices": 0,
    //                                              "numberOfUnits": 0,
    //                                              "apprenticesWages": 0,
    //                                              "totalWages": 349399993,
    //                                              "numberOfEmployees": 2930,
    //                                              "wagesAmount": 0,
    //                                              "businessDescription": "Financial Services",
    //                                              "directWageId": {
    //                                                  "pacCode": "751900006",
    //                                                  "pacDescription": "Financial asset investment consultant service",
    //                                                  "wicRate": 0.215,
    //                                                  "ddlContribution": 0.028,
    //                                                  "code": "751900",
    //                                                  "wicCode": "0",
    //                                                  "perCapitaFlag": "N",
    //                                                  "wicDescription": "Services to Finance and Investment nec"
    //                                              },
    //                                              "asbestoses": [

    //                                              ],
    //                                              "taxiPlates": [

    //                                              ],
    //                                              "contactorWages": [

    //                                              ]
    //                                          }
    //                                        ]
    //                                    }
    //                                  ]
    //                              }
    //                            ],
    //                            "brokerName": "icarewc",
    //                            "publicId": "pc:5320"
    //                        }
    //                    }
    //                }
    //            },
    //            "quoteData": {
    //                "offeredQuotes": [

    //                ]
    //            },
    //            "bindData": {
    //                "paymentPlans": [

    //                ],
    //                "isUwIssueAvailable": false,
    //                "billingAddress": {

    //                },
    //                "paymentDetails": {
    //                    "bankAccountData": {

    //                    },
    //                    "creditCardData": {

    //                    }
    //                }
    //            }
    //        }
    //    }
    //};

    //return responseObject;

    var responseObject = req.body;
    responseObject.data.attributes.bindData = {
        "billingAddress": {

        },
        "isUwIssueAvailable": false,
        "paymentDetails": {
            "bankAccountData": {

            },
            "creditCardData": {

            }
        },
        "paymentPlans": []
    };
    responseObject.data.attributes.quoteData = {
        "offeredQuotes": []
    };
    responseObject.data.attributes.status = "Draft";

    return responseObject;
}

/* deleteQuote */
exports.deleteQuote = function (req) {
    // check if the quoteId exists 
    var existigQuote = _.find(state.MockDataQuoteSearchEditable, function (o) {
        return _.toString(o.quoteNumber) === _.toString(req.params.quoteId);
    });

    // remove the quote
    if (existigQuote) {
        state.MockDataQuoteSearchEditable = _.reject(state.MockDataQuoteSearchEditable, function (o) {
            return _.toString(o.quoteNumber) === _.toString(req.params.quoteId);
        });

        return true;
    }

    // quote doesn't exists
    return false;
}

/* bindPolicy */
exports.bindPolicy = function (req) {
    // Add to state
    state.MockDataPoliciesEditable.push(req.body);

    // TODO: Transform to correct structure and return as the response object

    // TODO: Remove this.
    var policyStatus = "Bound";
    var selectedPaymentPlan = "icare_bcpp:2"; // monthly
    var annualAmount = 3659.79; // Confirmed
    var mobileNumber = req.body.data.attributes.draftData.accountContact[0].personContact.mobileNumber;
    if (mobileNumber === "987654321") {
        annualAmount = 36590.79; // Indicative
        var selectedPaymentPlan = "icare_bcpp:1"; // monthly
    }

    var firstName = req.body.data.attributes.draftData.accountContact[0].personContact.firstName;
    var lastName = req.body.data.attributes.draftData.accountContact[0].personContact.lastName;
    var email = req.body.data.attributes.draftData.accountContact[0].personContact.emailAddress1;
    var displayName = req.body.data.attributes.draftData.accountContact[0].personContact.displayName;

    var responseObject = {
        "data": {
            "type": "Policy",
            "id": "140079401",
            "attributes": {
                "accountId": "1400794",
                "quoteId": "100000000003686",
                "status": "Bound",
                "draftData": {
                    "account": {
                        "publicId": "pc:4618",
                        "accountHolder": false,
                        "displayName": "James",
                        "companyName": "James",
                        "primaryPhoneType": "mobile",
                        "mobileNumber": "0444444444",
                        "communicationPreference": "email",
                        "accountOrgType": "individualsoletrader",
                        "abn": "11223491505",
                        "acn": "34252",
                        "isVedaValidated": false,
                        "gstRegistration": true,
                        "itcEntitlement": "100",
                        "commencementDate": "2018-12-06T13:00:00Z",
                        "emailAddress1": "register.newuser@yopmail.com",
                        "primaryLanguage": "en_US"
                    },
                    "policyAddress": {
                        "displayName": "Kents, 187 Kent Rd, MULLION CREEK NSW 2800",
                        "isAddressVerified": true,
                        "addressLine1": "Kents, 187 Kent Rd",
                        "city": "MULLION CREEK",
                        "state": "AU_NSW",
                        "postalCode": "2800",
                        "country": "AU",
                        "addressType": "business",
                        "publicId": "pc:8531"
                    },
                    "accountContact": [
                      {
                          "contactRole": [
                            "LocationContact_icare"
                          ],
                          "publicId": "pc:2921",
                          "contactUpdateFlag": false,
                          "contactRetired": false,
                          "personContact": {
                              "displayName": "James Vuong",
                              "firstName": "James",
                              "lastName": "Vuong",
                              "primaryPhoneType": "mobile",
                              "mobileNumber": "0409778009",
                              "communicationPreference": "email",
                              "emailAddress1": "jvuong@deloitte.com.au",
                              "smsNotifications": false,
                              "publicId": "pc:4525"
                          },
                          "contactAddress": [
                            {
                                "displayName": "Kents, 187 Kent Rd, MULLION CREEK NSW 2800",
                                "isAddressVerified": false,
                                "addressLine1": "Kents, 187 Kent Rd",
                                "city": "MULLION CREEK",
                                "state": "AU_NSW",
                                "postalCode": "2800",
                                "country": "AU",
                                "publicId": "pc:8498",
                                "addressType": "postal"
                            }
                          ]
                      }
                    ],
                    "productCode": "WC_ICARE",
                    "productName": "Workers' Insurance",
                    "offering": "WCNI_icare",
                    "termType": "Other",
                    "periodStartDate": "2018-12-06T13:01:00Z",
                    "periodEndDate": "2019-11-30T13:01:00Z",
                    "preQualQuestionSets": [
                      {
                          "code": "WCNIPreQual_icare",
                          "answers": {
                              "annualGrossWages": "",
                              "employeeApprenticeTrainee": "true",
                              "employeePerCapita": "",
                              "employeeNSW": "true",
                              "groupOfCompanies": "false"
                          }
                      },
                      {
                          "code": "WCSPORTINGPreQual_icare",
                          "answers": {
                              "annualGrossWages": "",
                              "employeeApprenticeTrainee": "",
                              "employeePerCapita": "",
                              "employeeNSW": "",
                              "groupOfCompanies": ""
                          }
                      }
                    ],
                    "lobs": {
                        "wcLine": {
                            "policyPeriod": {
                                "policyType": "Standard",
                                "brokerId": "icarewc",
                                "brokerOrganisation": "Icare",
                                "termType": "Other",
                                "termNumber": 1,
                                "writtenDate": "2018-12-06T13:00:00Z",
                                "rateAsOfDate": "2018-12-06T23:39:27Z",
                                "gstRegistration": true,
                                "itcEntitlement": "100",
                                "wcLabourHireCode": "1",
                                "schemeAgent": "EmployersMutual-016",
                                "policy": {
                                    "primaryLanguage": "en_US",
                                    "priorPolicy": [

                                    ],
                                    "lossHistoryEntry": [

                                    ]
                                },
                                "claim": [

                                ],
                                "policyTerm": {

                                },
                                "jobRoleAssignment": [
                                  {
                                      "assignedUser": "integportal user",
                                      "assignedGroup": "Icare Grp",
                                      "jobUserRoleAssignment": "100000000003686"
                                  },
                                  {
                                      "assignedUser": "Super User",
                                      "assignedGroup": "Icare Grp",
                                      "jobUserRoleAssignment": "100000000003686"
                                  }
                                ],
                                "policyLocation": [
                                  {
                                      "isRatedLocation": true,
                                      "publicId": "pc:5320",
                                      "locationRetired": false,
                                      "primaryLocation": true,
                                      "accountLocation": {
                                          "nonSpecific": true,
                                          "address": {
                                              "publicId": "pc:8499",
                                              "isAddressVerified": true,
                                              "postalCode": "2800",
                                              "country": "AU",
                                              "addressType": "business",
                                              "addressLine1": "Kents, 187 Kent Rd",
                                              "city": "MULLION CREEK",
                                              "state": "AU_NSW"
                                          }
                                      },
                                      "costCenters": [
                                        {
                                            "fullName": "PolicyLocation : 1",
                                            "directWages": [
                                              {
                                                  "wic": 0,
                                                  "pac": 0,
                                                  "grossValue": 589685,
                                                  "numberOfApprentices": 0,
                                                  "numberOfUnits": 0,
                                                  "apprenticesWages": 0,
                                                  "totalWages": 589685,
                                                  "numberOfEmployees": 10,
                                                  "wagesAmount": 0,
                                                  "businessDescription": "Business",
                                                  "directWageId": {
                                                      "pacCode": "012400001",
                                                      "pacDescription": "Prime lamb raising",
                                                      "wicRate": 7.653,
                                                      "ddlContribution": 0.083,
                                                      "code": "012400",
                                                      "wicCode": "0",
                                                      "perCapitaFlag": "N",
                                                      "wicDescription": "Sheep Farming"
                                                  },
                                                  "asbestoses": [

                                                  ],
                                                  "taxiPlates": [

                                                  ],
                                                  "contactorWages": [

                                                  ]
                                              }
                                            ]
                                        }
                                      ]
                                  }
                                ],
                                "brokerName": "icarewc",
                                "publicId": "pc:5408"
                            }
                        }
                    }
                },
                "quoteData": {
                    "offeredQuotes": [
                      {
                          "publicId": "pc:5408",
                          "branchCode": "WCNI_icare",
                          "isCustom": true,
                          "annualPremium": {
                              "amount": 3659.79,
                              "amountCurrency": "aud"
                          },
                          "previousAnnualPremium": {

                          },
                          "transactionCost": {

                          },
                          "termMonths": 12,
                          "taxes": {
                              "claimPerformance": 3859.22,
                              "amountCurrency": "aud"
                          },
                          "lobs": {
                              "wcLine": {
                                  "wageDetails": {
                                      "totalAveragePerformancePremium": {
                                          "amount": 46879.96,
                                          "amountcurrency": "aud"
                                      },
                                      "wagePremium": [
                                        {
                                            "rate": 7.95,
                                            "totalAnnualWages": {
                                                "amount": 589685,
                                                "amountcurrency": "aud"
                                            },
                                            "averagePerformancePremium": {
                                                "amount": 46879.96,
                                                "amountcurrency": "aud"
                                            }
                                        }
                                      ]
                                  },
                                  "esiPremium": {
                                      "esiRate": 10,
                                      "averagePerformancePremium": {
                                          "amount": 46879.96,
                                          "amountcurrency": "aud"
                                      },
                                      "esi": {
                                          "amount": -4688,
                                          "amountcurrency": "aud"
                                      }
                                  },
                                  "claimPerformance": {
                                      "totalPriorApp": {

                                      },
                                      "totalPriorLoss": {

                                      },
                                      "schemePerformanceMeasure": 0,
                                      "claimPerformanceAdjustment": 1,
                                      "claimPerformanceRate": 0,
                                      "claimPerformanceMeasure": 0,
                                      "premiumRiskAssessment": {

                                      },
                                      "cprAdjustment": {

                                      }
                                  },
                                  "averagePerformancePremium": {
                                      "amount": 46879.96,
                                      "amountcurrency": "aud"
                                  },
                                  "annualisedApp": {
                                      "amount": 47008.75,
                                      "amountcurrency": "aud"
                                  },
                                  "premium": {
                                      "amount": 3659.79,
                                      "amountcurrency": "aud"
                                  },
                                  "dustDiseaseLavy": {
                                      "amount": 259.46,
                                      "amountcurrency": "aud"
                                  },
                                  "asbestosCost": {
                                      "amount": 0,
                                      "amountcurrency": "aud"
                                  },
                                  "totalDdl": {
                                      "amount": 259.46,
                                      "amountcurrency": "aud"
                                  },
                                  "apprenticeDiscount": {
                                      "amount": 0,
                                      "amountcurrency": "aud"
                                  },
                                  "performanceDiscount": {
                                      "amount": 0,
                                      "amountcurrency": "aud"
                                  },
                                  "itcAmount": {

                                  },
                                  "mineSafetyCost": {
                                      "amount": 0,
                                      "amountcurrency": "aud"
                                  }
                              }
                          }
                      }
                    ]
                },
                "bindData": {
                    "paymentReference": "258251",
                    "bPayReference": "140079401174",
                    "chosenQuote": "pc:5408",
                    "registrationCode": "6SPI8F3R",
                    "policyNumber": "140079401",
                    "paymentPlans": [
                      {
                          "name": "icare NI Payment Plan_Yearly",
                          "downPayment": {
                              "amount": 3659.79,
                              "amountcurrency": "aud"
                          },
                          "total": {
                              "amount": 3659.79,
                              "amountcurrency": "aud"
                          },
                          "installment": {
                              "amount": 3659.79,
                              "amountcurrency": "aud"
                          },
                          "billingId": "icare_bcpp:1"
                      }
                    ],
                    "isUwIssueAvailable": true,
                    "billingAddress": {

                    },
                    "paymentDetails": {
                        "bankAccountData": {

                        },
                        "creditCardData": {

                        }
                    },
                    "selectedPaymentPlan": selectedPaymentPlan
                }
            }
        }
    };

    return responseObject;
}

/* policyDetails */
exports.policyDetails = function (req) {
    var responseObject = {
        "data": {
            "type": "Policy",
            "id": req.params.policyId,
            "attributes": {
                "sendDocument": "true",
                "accountId": "1400932",
                "quoteId": "100000000003659",
                "policyPending": false,
                "updateTime": "2018-09-11T14:00:00Z",
                "employerCategory": "Employer",
                "status": "Quoted", //"[Draft,Quoted,Bound,Declined,NotTaken,Closed,Quoting,Binding,Renewing,NonRenewing,NotTaking,Canceling,Rescinding,Rescinded,Reinstating,AuditComplete,Waived,InForce_icare]",
                "policyChangeReason": "cancellationadjsutment", //"[cancellationadjsutment,Changetoestimatewages,Changetohindsight,ChangetoWIC,Expirydatechange,groupcascade,Hindsight,hindsight3yr,hindsight5yr,LPRfirstadjustment,LPRfourthadjustment,LPRsecondadjustment,LPRthirdadjustment,niladjustbatch,Nonpremiumchange,PremiumChange]",
                "draftData": {
                    "productName": "Workers' Insurance",
                    "productCode": "WC_ICARE",
                    "previousEndDate": "2019-09-11T14:01:00.000Z",
                    "periodStartDate": "2018-12-06T14:01:00.000Z",
                    "periodEndDate": "2019-12-06T14:01:00.000Z",
                    "offering": "WCNI_icare",
                    "accountNumber": "1233",
                    "termType": "Annual",
                    "account": {
                        "tempId": 1,
                        "publicId": "pc:4605",
                        "displayName": "Plumbing Corp. PTY LTD.",
                        "companyName": "Plumbing Corp. PTY LTD.",
                        "accountHolder": true,
                        "particle": "asd",
                        "primaryPhoneType": "home",
                        "communicationPreference": "email",
                        "workNumber": 12345566,
                        "mobileNumber": "0411444555",
                        "homeNumber": 12345566,
                        "website": "abc.com",
                        "emailAddress1": "xyz@test.com",
                        "accountOrgType": "partnership",
                        "abn": "88000014675",
                        "acn": "123453",
                        "isVedaValidated": false,
                        "acnStatus": "dfs",
                        "abnStatus": "sdf",
                        "trusteeType": "partnership",
                        "trustABNStatus": "ok",
                        "trustABN": "ABN123",
                        "trustACN": "1221",
                        "trustName": "abc",
                        "trusteeName": "wewq",
                        "tradingBusinessName": "Jim's Plumbing",
                        "primaryLanguage": "en_US",
                        "gstRegistration": true,
                        "itcEntitlement": "100",
                        "commencementDate": "2017-08-29T14:01:00.000Z",
                        "cluster": "eweq",
                        "newAccountReason": "brnfrmsplit",
                        "industryCode": "abc",
                        "descriptionOfBusiness": "weqw",
                        "serviceTier": "silver",
                        "yearBusinessStarted": "2010",
                        "isAnonymousAccount": false,
                        "accountId": "1234"
                    },
                    "policyAddress": {
                        "publicId": "123",
                        "displayName": "ade",
                        "isAddressVerified": false,
                        "addressLine1": "41 Navy Rd",
                        "addressLine2": "41 Navy Rd",
                        "addressLine3": "41 Navy Rd",
                        "city": "LLANDILO",
                        "state": "AU_NSW",
                        "postalCode": "2747",
                        "country": "AU",
                        "addressType": "postal",
                        "isAutofilled": true
                    },
                    "accountContact": [
                       {
                           "contactRole": [
                              "LocationContact_icare"
                           ],
                           "publicId": "pc:3104",
                           "contactUpdateFlag": true,
                           "contactRetired": false,
                           "personContact": {
                               "publicId": "pc:4706",
                               "lastName": "LastAuto",
                               "firstName": "FirstAuto",
                               "displayName": "FirstAuto LastAuto",
                               "tempId": 123,
                               "particle": "abc",
                               "primaryPhoneType": "work",
                               "workNumber": "0211000",
                               "mobileNumber": "0411444555",
                               "homeNumber": "0211000",
                               "faxPhone": "0211000",
                               "website": "abc.com",
                               "communicationPreference": "email",
                               "emailAddress1": "NISPUATAuto@gmail.com",
                               "smsNotifications": false
                           },
                           "contactAddress": [
                              {
                                  "publicId": "pc:8608",
                                  "displayName": "name",
                                  "isAddressVerified": false,
                                  "addressLine1": "32 Harris Street",
                                  "addressLine2": "32 Harris Street",
                                  "addressLine3": "32 Harris Street",
                                  "city": "BALMAIN",
                                  "state": "AU_NSW",
                                  "postalCode": "2041",
                                  "country": "[AU]",
                                  "addressType": "postal",
                                  "isAutofilled": true
                              }
                           ]
                       }
                    ],
                    "preQualQuestionSets": [
                       {
                           "code": "WCNIPreQual_icare",
                           "answers": {
                               "annualGrossWages": false,
                               "employeeApprenticeTrainee": true,
                               "employeePerCapita": false,
                               "employeeNSW": true,
                               "groupOfCompanies": false
                           }
                       }
                    ],
                    "lobs": {
                        "wcLine": {
                            "policyPeriod": {
                                "offerings": "offs",
                                "termNumber": 0,
                                "siFund": "[sifund]",
                                "brokerId": "icarewc",
                                "brokerOrganisation": "AON",
                                "termType": "[Annual,HalfYear,Other]",
                                "brokerName": "AON Parramatta",
                                "legacyPolicyNumber": "123456",
                                "schemeAgentContact": "cont",
                                "publicId": "pc:4890",
                                "wcLabourHireCode": "1", //"[1,2,3]",
                                "isLegacy": false,
                                "policyType": "Standard",
                                "writtenDate": "2018-09-19T14:01:00.000Z",
                                "rateAsOfDate": "2018-09-12T03:36:46Z",
                                "producerCodeOrg": "abc",
                                "gstRegistration": false,
                                "itcEntitlement": "100",
                                "schemeAgent": "Allianz-033",
                                "reasonLowWages": "none",
                                "groupNumber": "2318569",
                                "groupName": "Jim's Co.",
                                "policy": {
                                    "primaryLanguage": "en_US",
                                    "priorPolicy": [
                                       {
                                           "carrier": "car",
                                           "expirationDate": "2018-09-19T14:01:00.000Z",
                                           "effectiveDate": "2018-09-19T14:01:00.000Z",
                                           "annualPremiumAmount": 123442,
                                           "numLosses": 2,
                                           "totalLossesAmount": 450,
                                           "wcCarrier": "allianz",
                                           "policy": "pol",
                                           "comments": "adsf",
                                           "wagesAmount": 234324,
                                           "numberOfWICs": 4
                                       }
                                    ],
                                    "lossHistoryEntry": [
                                       {
                                           "locationName": "adsa",
                                           "costCenterNo": "123",
                                           "directWageId": {
                                               "wicRate": 4.432,
                                               "wicDescription": "Banks",
                                               "wicCode": "732100 - Banks",
                                               "perCapitaFlag": false,
                                               "pacDescription": "Development bank operation",
                                               "pacCode": "732100001",
                                               "ddlContribution": 0.028,
                                               "code": "732100"
                                           },
                                           "claimAmountYear3": null,
                                           "claimAmountYear2": 333344,
                                           "claimAmountYear1": 333344,
                                           "basicTariffPremiumAmountYear3": 333344,
                                           "basicTariffPremiumAmountYear2": 333344,
                                           "basicTariffPremiumAmountYear1": 333344,
                                           "wages1": 3452,
                                           "wages2": 3452,
                                           "wages3": 3452,
                                           "occurrenceDate": "2018-09-19T14:01:00.000Z",
                                           "description": "sfd",
                                           "totalIncurredAmount": 123333333,
                                           "lossStatus": "[Open,Closed]",
                                           "amountReserved": 12332432,
                                           "amountPaid": 324345
                                       }
                                    ]
                                },
                                "claim": [
                                   {
                                       "totalIncurred": 0,
                                       "status": "abc",
                                       "policyInForce": false,
                                       "lossDate": "2018-09-19T14:01:00.000Z",
                                       "claimNumber": "123"
                                   }
                                ],
                                "policyTerm": {
                                    "startDate": "2018-09-11T14:00:00Z",
                                    "organization": "ade",
                                    "endDate": "2018-09-11T14:00:00Z",
                                    "affinitygroupType": "[Closed,Open]",
                                    "affinityGroupName": "avb"
                                },
                                "jobRoleAssignment": [
                                   {
                                       "assignedUser": "integportal user",
                                       "assignedGroup": "Icare Grp",
                                       "jobUserRoleAssignment": "100000000003538"
                                   }
                                ],
                                "policyLocation": [
                                   {
                                       "publicId": "pc:5504",
                                       "primaryLocation": true,
                                       "locationRetired": false,
                                       "isRatedLocation": true,
                                       "accountLocation": {
                                           "nonSpecific": true,
                                           "locationName": "sds",
                                           "locationCode": "fsd",
                                           "address": {
                                               "publicId": "pc:8609",
                                               "displayName": "dsds",
                                               "isAddressVerified": false,
                                               "addressLine1": "481-499 Pittwater Road",
                                               "addressLine2": "",
                                               "addressLine3": "",
                                               "city": "BROOKVALE",
                                               "state": "NSW",
                                               "postalCode": "2100",
                                               "country": "AU",
                                               "addressType": "business",
                                               "isAutofilled": true
                                           }
                                       },
                                       "costCenters": [
                                          {
                                              "name": "wewe",
                                              "fullName": "PolicyLocation : 1",
                                              "centerNumber": "123",
                                              "publicId": "pc:7614",
                                              "directWages": [
                                                 {
                                                     "wic": 0,
                                                     "pac": 0,
                                                     "grossValue": 600000,
                                                     "numberOfUnits": 3444,
                                                     "numberOfApprentices": 0,
                                                     "numberOfEmployees": 12,
                                                     "apprenticesWages": 0,
                                                     "totalWages": 600000,
                                                     "wagesAmount": 0,
                                                     "claimAmountYear3": 1000.99,
                                                     "claimAmountYear2": 1000.99,
                                                     "claimAmountYear1": 1000.99,
                                                     "basicTariffPremiumAmountYear3": 1000.99,
                                                     "basicTariffPremiumAmountYear2": 1000.99,
                                                     "basicTariffPremiumAmountYear1": 1000.99,
                                                     "businessDescription": "General Commerce",
                                                     "directWageId": {
                                                         "wicRate": 4.432,
                                                         "wicDescription": "Plumbing Services",
                                                         "wicCode": "423100 - Plumbing Services",
                                                         "perCapitaFlag": false,
                                                         "pacDescription": "Development bank operation",
                                                         "pacCode": "423100007",
                                                         "ddlContribution": 0.028,
                                                         "code": "423100"
                                                     },
                                                     "asbestoses": [],
                                                     "taxiPlates": [],
                                                     "contactorWages": []
                                                 }
                                              ]
                                          }
                                       ]
                                   }
                                ]
                            }
                        }
                    }
                },
                "quoteData": {
                    "offeredQuotes": [
                       {
                           "publicId": "pc:5403",
                           "branchCode": "WCNI_icare",
                           "branchName": "branch123",
                           "isCustom": true,
                           "annualPremium": {
                               "amount": 18746,
                               "amountCurrency": "aud"
                           },
                           "previousAnnualPremium": {
                               "amount": 18374,
                               "amountCurrency": "aud"
                           },
                           "transactionCost": {
                               "amount": 18746,
                               "amountCurrency": "aud"
                           },
                           "termMonths": 12,
                           "taxes": {
                               "amountCurrency": "aud",
                               "amount": 3509.1
                           },
                           "lobs": {
                               "wcLine": {
                                   "wageDetails": {
                                       "wagePremium": {
                                           "wic": "ds",
                                           "rate": 21,
                                           "totalAnnualWages": {
                                               "amount": 18746,
                                               "amountCurrency": "aud"
                                           },
                                           "averagePerformancePremium": {
                                               "amount": 18746,
                                               "amountCurrency": "aud"
                                           }
                                       },
                                       "totalAveragePerformancePremium": {
                                           "amount": 18746,
                                           "amountCurrency": "aud"
                                       }
                                   },
                                   "esiPremium": {
                                       "esiRate": 1.2,
                                       "esi": {
                                           "amountCurrency": "aud",
                                           "amount": -4160
                                       },
                                       "averagePerformancePremium": {
                                           "amount": 157.1,
                                           "amountCurrency": "aud"
                                       }
                                   },
                                   "claimPerformance": {
                                       "totalPriorApp": {
                                           "amount": 157.1,
                                           "amountCurrency": "aud"
                                       },
                                       "totalPriorLoss": {
                                           "amount": 4268.27,
                                           "amountCurrency": "aud"
                                       },
                                       "schemePerformanceMeasure": 3,
                                       "claimPerformanceAdjustment": 2,
                                       "claimPerformanceRate": 2.1,
                                       "claimPerformanceMeasure": 5,
                                       "premiumRiskAssessment": {
                                           "amount": 4268.27,
                                           "amountCurrency": "aud"
                                       },
                                       "cprAdjustment": {
                                           "amount": 4268.27,
                                           "amountCurrency": "aud"
                                       }
                                   },
                                   "averagePerformancePremium": {
                                       "amount": 4268.27,
                                       "amountCurrency": "aud"
                                   },
                                   "annualisedApp": {
                                       "amount": 4280,
                                       "amountCurrency": "aud"
                                   },
                                   "premium": {
                                       "amount": 3852.41,
                                       "amountCurrency": "aud"
                                   },
                                   "dustDiseaseLavy": {
                                       "amount": 10.97,
                                       "amountCurrency": "aud"
                                   },
                                   "asbestosCost": {
                                       "amount": 0,
                                       "amountCurrency": "aud"
                                   },
                                   "totalDdl": {
                                       "amount": 157.1,
                                       "amountCurrency": "aud"
                                   },
                                   "apprenticeDiscount": {
                                       "amount": 0,
                                       "amountCurrency": "aud"
                                   },
                                   "performanceDiscount": {
                                       "amount": 0,
                                       "amountCurrency": "aud"
                                   },
                                   "mineSafetyCost": {
                                       "amount": 0,
                                       "amountCurrency": "aud"
                                   },
                                   "itcAmount": {
                                       "amount": 0,
                                       "amountCurrency": "aud"
                                   }
                               }
                           }
                       }
                    ]
                },
                "bindData": {
                    "bPayReference": "5123",
                    "paymentReference": "258251",
                    "accountNumber": "12324",
                    "fullPayDiscountPercentage": "20",
                    "chosenQuote": "pc:5403",
                    "paymentPlans": [
                       {
                           "name": "icare NI Payment Plan_Quarterly",
                           "downPayment": {
                               "amountCurrency": "aud",
                               "amount": 9650
                           },
                           "total": {
                               "amountCurrency": "aud",
                               "amount": 38600
                           },
                           "installment": {
                               "amountCurrency": "aud",
                               "amount": 9650
                           },
                           "billingId": "icare_bcpp:3"
                       }
                    ],
                    "selectedPaymentPlan": "icare_bcpp:1",
                    "isUwIssueAvailable": false,
                    "billingAddress": {
                        "publicId": "123",
                        "displayName": "ade",
                        "isAddressVerified": false,
                        "addressLine1": "41 Navy Rd",
                        "addressLine2": "41 Navy Rd",
                        "addressLine3": "41 Navy Rd",
                        "city": "LLANDILO",
                        "state": "AU_NSW",
                        "postalCode": "2747",
                        "country": "AU",
                        "addressType": "business",
                        "isAutofilled": true
                    },
                    "paymentDetails": {
                        "paymentMethod": "card",
                        "bankAccountData": {
                            "bankAbaNumber": "12323434",
                            "bankAccountNumber": "124345435",
                            "bankAccountType": "dss",
                            "bankName": "asdad"
                        },
                        "creditCardData": {
                            "creditCardExpDate": "2018-09-11T14:00:00Z",
                            "creditCardIssuer": "dsa",
                            "creditCardNumber": "12323434"
                        }
                    },
                    "registrationCode": "123234"
                }
            }
        }
    };

    return responseObject;
}

/* brokersPolicyList */
exports.brokersPolicyList = function (req) {
    var responseObject = {
        "data": {
            "type": "BrokersList",
            "attributes": [
              {
                  "description": "Steadfast Group",
                  "producerCode": "SG"
              },
              {
                  "description": "ABICO INSURANCE BROKERS",
                  "producerCode": "SG000021"
              },
              {
                  "description": "ABICO INSURANCE BROKERS (Branch)",
                  "producerCode": "SG000021.01GB"
              },
              {
                  "description": "ABICO QUINTON PTY LTD",
                  "producerCode": "SG000022"
              },
              {
                  "description": "ABICO QUINTON PTY LTD (Branch)",
                  "producerCode": "SG000022.01GB"
              },
              {
                  "description": "ACTION INSURANCE BROKERS",
                  "producerCode": "SG000023"
              },
              {
                  "description": "ACTION INSURANCE BROKERS (Branch)",
                  "producerCode": "SG000023.01WC"
              },
              {
                  "description": "ACTION INSURANCE BROKERS - WINDSOR (Branch)",
                  "producerCode": "SG000023.02WC"
              },
              {
                  "description": "ACTION INSURANCE BROKERS - PENRITH (Branch)",
                  "producerCode": "SG000023.04WC"
              },
              {
                  "description": "AGRIRISK SERVICES PTY LTD",
                  "producerCode": "SG000024"
              },
              {
                  "description": "AGRIRISK SERVICES PTY LTD (Branch)",
                  "producerCode": "SG000024.01GB"
              },
              {
                  "description": "ALFA INSURANCE BROKERS PTY LTD",
                  "producerCode": "SG000025"
              },
              {
                  "description": "ALFA INSURANCE BROKERS PTY LTD (Branch)",
                  "producerCode": "SG000025.01GB"
              },
              {
                  "description": "ANNIS GROUP PTY LTD",
                  "producerCode": "SG000026"
              },
              {
                  "description": "ANNIS GROUP PTY LTD (Branch)",
                  "producerCode": "SG000026.01GB"
              },
              {
                  "description": "ASSET INSURANCE BROKERS",
                  "producerCode": "SG000027"
              },
              {
                  "description": "ASSET INSURANCE BROKERS (Branch)",
                  "producerCode": "SG000027.01GB"
              },
              {
                  "description": "AUSTCOVER INSURANCE BROKERS",
                  "producerCode": "SG000028"
              },
              {
                  "description": "AUSTCOVER INSURANCE BROKERS (Branch)",
                  "producerCode": "SG000028.01GB"
              },
              {
                  "description": "AUSTCOVER - BRISBANE (Branch)",
                  "producerCode": "SG000028.02GB"
              },
            ]
        }
    };

    return responseObject;
}

/* brokersPolicyList */
exports.brokersPolicyListSingle = function (req) {
    var responseObject = {
        "data": {
            "type": "BrokersList",
            "attributes": [
              {
                  "description": "AUSTCOVER - BRISBANE (Branch)",
                  "producerCode": "SG000028.02GB"
              },
            ]
        }
    };

    return responseObject;
}


/* cancelPolicy */
exports.cancelPolicy = function (req) {
    var responseObject = {
        "data": {
            "type": "Policy",
            "id": "PC123456",
            "attributes": {
                "jobNumber": "123",
                "createTime": "2019-06-29T13:00:00.000Z",
                "closeDate": "2019-06-29T13:00:00.000Z",
                "status": "Closed",
                "latestPeriod": {
                    "effectiveDate": "2019-06-29T13:00:00.000Z",
                    "status": "Closed",
                    "displayStatus": "Closed",
                    "expirationDate": "2019-06-29T13:00:00.000Z",
                    "primaryInsuredName": "Monthly Billing",
                    "canceled": false,
                    "policyLines": [
                      {
                          "lineOfBusinessCode": "WCLine_icare",
                          "lineOfBusinessName": "ICARE Workers' Comp Line"
                      }
                    ],
                    "totalPremium": {
                        "currency": "[aud]",
                        "amount": 35274.2
                    },
                    "totalCost": {
                        "currency": "[aud]",
                        "amount": 35274.2
                    },
                    "taxesAndFees": {
                        "currency": "[aud]",
                        "amount": 0
                    },
                    "producerCodeRecord": "icarewc",
                    "producerCodeService": "icarewc",
                    "createdByMe": false,
                    "producerCodeRecordOrg": "Icare",
                    "producerCodeServiceOrg": "Icare",
                    "canRenew": false,
                    "canCancel": false,
                    "canChange": false,
                    "publicId": "p123"
                },
                "description": "description",
                "type": "[Cancellation]",
                "displayType": "type",
                "isUWIssueAvailable": false,
                "publicId": "p123"
            }
        }
    };

    return responseObject;
}

/* policyHistory */
/* exports.policyHistory = function (req) {
    var responseObject = {
        "data": {
            "type": "PolicyHistory",
            "id": "12345",
            "attributes": [
              {
                  "termNumber": 2,
                  "periodStartDate": "2017-08-29T14:01:00.000Z",
                  "periodEndDate": "2017-08-29T14:01:00.000Z",
                  "totalUnits": 0,
                  "totalEmployees": 9,
                  "transactionType": "Renewal",
                  "totalWages": 675000,
                  "totalPremium": {
                      "amount": 24873.75,
                      "amountCurrency": "aud"
                  }
              },
              {
                  "termNumber": 1,
                  "periodStartDate": "2017-08-29T14:01:00.000Z",
                  "periodEndDate": "2017-08-29T14:01:00.000Z",
                  "totalUnits": 0,
                  "totalEmployees": 8,
                  "transactionType": "Submission",
                  "totalWages": 580000,
                  "totalPremium": {
                      "amount": 20198.76,
                      "amountCurrency": "aud"
                  },
                  "docId": "3667020"
              }
            ]
        }
    };

    return responseObject;
} */
exports.policyHistory = function (req) {
    var policyId = req.params.policyId;

    var responseObject = {
        "data": {
            "type": "PolicyHistory",
            "id": "123463",
            "attributes": [
              {
                  "termNumber": 2,
                  "periodStartDate": "2018-06-29T14:00:00Z",
                  "periodEndDate": "2019-06-29T14:00:00Z",
                  "totalUnits": 0,
                  "totalEmployees": 9,
                  "transactionType": "Renewal",
                  "totalWages": 675000,
                  "totalPremium": {
                      "amount": 24873.75,
                      "amountCurrency": "aud"
                  },
                  "docId": "3667020",
                  "documentUrl": "https://drive.google.com/file/d/1wYhY9Rsd56Zoyw1gRQkms79_OebQHdxY/view?id=123463"
              }
            ]
        }
    };

    /* For Testing Document Listing page */
    if (policyId === '140155201') {
        responseObject = {
            "data": {
                "type": "PolicyHistory",
                "id": "140155201",
                "attributes": []
            }
        };
    }
    if (policyId === '456789') {
        responseObject = {
            "data": {
                "type": "PolicyHistory",
                "id": "456789",
                "attributes": [
                  {
                      "termNumber": 2,
                      "periodStartDate": "2018-06-29T14:00:00Z",
                      "periodEndDate": "2019-06-29T14:00:00Z",
                      "totalUnits": 0,
                      "totalEmployees": 9,
                      "transactionType": "Renewal",
                      "totalWages": 675000,
                      "totalPremium": {
                          "amount": 24873.75,
                          "amountCurrency": "aud"
                      },
                      "docId": "3667020"
                  },
                  {
                      "termNumber": 2,
                      "periodStartDate": "2017-01-01T14:00:00Z",
                      "periodEndDate": "2018-01-01T14:00:00Z",
                      "totalUnits": 0,
                      "totalEmployees": 9,
                      "transactionType": "Renewal",
                      "totalWages": 675000,
                      "totalPremium": {
                          "amount": 24873.75,
                          "amountCurrency": "aud"
                      },
                      "docId": "3667020",
                      "documentUrl": "https://drive.google.com/file/d/1wYhY9Rsd56Zoyw1gRQkms79_OebQHdxY/view?id=456789"
                  }
                ]
            }
        };
    }

    return responseObject;
}

/* quoteSearch */
exports.quoteSearch = function (req) {
    var userEmail = req.get("X-Mock-Email") || '';
    if (userEmail === '1xtestemployer2@gmail.com') {
        var responseEmptyObject = {
            "meta": {
                "totalRecords": 0,
                "totalPages": 0,
                "currentPage": req.body.data.attributes.params.page
            },
            "data": {
                "type": "Quote",
                "attributes": {
                    "entries": []
                }
            }
        };

        return responseEmptyObject;
    }
    var mockDataQuoteSearch = state.MockDataQuoteSearchEditable;
    var filteredItems = _.map(mockDataQuoteSearch, function (o) {
        return o;
    });

    // Filter 
    if (req.body.data.attributes.criteria) {
        var quoteNumberFilter = req.body.data.attributes.criteria.quoteNumber ? req.body.data.attributes.criteria.quoteNumber : undefined;
        var entityNameFilter = req.body.data.attributes.criteria.entityName ? req.body.data.attributes.criteria.entityName : undefined;
        var abnFilter = req.body.data.attributes.criteria.abn ? req.body.data.attributes.criteria.abn : undefined;
        var acnFilter = req.body.data.attributes.criteria.acn ? req.body.data.attributes.criteria.acn : undefined;

        filteredItems = _.filter(filteredItems, function (o) {
            // Assume first that the item is to be included 
            var returnValue = true;

            // Add logic below for the scenarios that will exclude the item 

            // quoteNumber (partial match)
            if (quoteNumberFilter !== undefined && quoteNumberFilter !== "undefined"
                && (_.toLower(_.toString(o.quoteNumber)).indexOf(_.toLower(_.toString(quoteNumberFilter))) === -1)
               ) returnValue = false;

            // entityName (partial match)
            if (entityNameFilter !== undefined && entityNameFilter !== "undefined"
                && (_.toLower(_.toString(o.entityName)).indexOf(_.toLower(_.toString(entityNameFilter))) === -1)
               ) returnValue = false;

            // abn (partial match)
            if (abnFilter !== undefined && abnFilter !== "undefined"
                && (_.toLower(_.toString(o.abn)).indexOf(_.toLower(_.toString(abnFilter))) === -1)
               ) returnValue = false;

            // acn (partial match)
            if (acnFilter !== undefined && acnFilter !== "undefined"
                && (_.toLower(_.toString(o.acn)).indexOf(_.toLower(_.toString(acnFilter))) === -1)
               ) returnValue = false;

            return returnValue;
        });
    }

    // Sort 
    var sortFilter = req.body.data.attributes.params.orderBy[0].fieldName;
    var sortDirectionFilter = sortFilter.indexOf("Desc") > -1 ? "descending" : "ascending";
    sortFilter = sortFilter.replace("Desc", "").replace("Asc", "");
    var sortedItems = filteredItems;
    sortedItems = _.sortBy(filteredItems, [function (o) { return o[sortFilter]; }]);
    if (sortFilter === "totalPremium") {
        sortedItems = _.sortBy(filteredItems, [function (o) { return o[sortFilter].amount; }]);
    }
    if (sortFilter === "quoteExpiryDate") {
        sortedItems = _.sortBy(filteredItems, [function (o) { return (o.quoteExpiryDate && helpers.getDate(o.quoteExpiryDate)); }]);
    }
    if (sortDirectionFilter !== "ascending") sortedItems = _.reverse(sortedItems); // descending 


    // Paginate 
    var pageSize = req.body.data.attributes.params.pageSize;
    var paginatedItems = _.chunk(sortedItems, 1); // pageSize === 0
    if (pageSize !== 0) {
        paginatedItems = _.chunk(sortedItems, pageSize);
    }
    var targetPage = paginatedItems[req.body.data.attributes.params.page - 1];

    if (targetPage === undefined) {
        targetPage = [];
    }

    // Response object 
    var responseObject = {
        "meta": {
            "totalRecords": sortedItems.length,
            "totalPages": paginatedItems.length,
            "currentPage": req.body.data.attributes.params.page
        },
        "data": {
            "type": "Quote",
            "attributes": {
                "entries": targetPage
            }
        }
    };

    return responseObject;
}

/* policySearch */
exports.policySearch = function (req) {
    var userRole = req.get("X-Mock-Role") || '';
    var mockDataPolicySearch = [];
    if (userRole === 'JWTR-Policy_Portal-Employer-Dev') {
        mockDataPolicySearch = state.MockDataPolicySearchEmployer;
    } else {
        mockDataPolicySearch = state.MockDataPolicySearchBroker;
    }

    var filteredItems = _.map(mockDataPolicySearch, function (o) {
        return o;
    });

    // Filter 
    if (req.body.data.attributes.criteria) {
        var policyNumberFilter = req.body.data.attributes.criteria.policyNumber ? req.body.data.attributes.criteria.policyNumber : undefined;
        var entityNameFilter = req.body.data.attributes.criteria.entityName ? req.body.data.attributes.criteria.entityName : undefined;
        var abnFilter = req.body.data.attributes.criteria.abn ? req.body.data.attributes.criteria.abn : undefined;
        var acnFilter = req.body.data.attributes.criteria.acn ? req.body.data.attributes.criteria.acn : undefined;
        var renewalDueFilter = req.body.data.attributes.criteria.renewalDue ? req.body.data.attributes.criteria.renewalDue : undefined;
        var paymentDueFilter = req.body.data.attributes.criteria.paymentDue ? req.body.data.attributes.criteria.paymentDue : undefined;
        var cardExpiryFilter = req.body.data.attributes.criteria.cardExpiry ? req.body.data.attributes.criteria.cardExpiry : undefined;

        filteredItems = _.filter(filteredItems, function (o) {
            // Assume first that the item is to be included 
            var returnValue = true;

            // Add logic below for the scenarios that will exclude the item 

            // policyNumber (partial match)
            if (policyNumberFilter !== undefined && policyNumberFilter !== "undefined"
                && (_.toLower(_.toString(o.policyNumber)).indexOf(_.toLower(_.toString(policyNumberFilter))) === -1)
               ) returnValue = false;

            // entityName (partial match)
            if (entityNameFilter !== undefined && entityNameFilter !== "undefined"
                && (_.toLower(_.toString(o.entityName)).indexOf(_.toLower(_.toString(entityNameFilter))) === -1)
               ) returnValue = false;

            // abn (partial match)
            if (abnFilter !== undefined && abnFilter !== "undefined"
                && (_.toLower(_.toString(o.abn)).indexOf(_.toLower(_.toString(abnFilter))) === -1)
               ) returnValue = false;

            // acn (partial match)
            if (acnFilter !== undefined && acnFilter !== "undefined"
                && (_.toLower(_.toString(o.acn)).indexOf(_.toLower(_.toString(acnFilter))) === -1)
               ) returnValue = false;

            // renewalDue (partial match)
            if (renewalDueFilter !== undefined && renewalDueFilter !== "undefined" && renewalDueFilter === "true") {

                var renewalDueItems = _.filter(o.actions, function (o) {
                    return (o.actionType === "renewalDue");
                });

                if (!renewalDueItems || renewalDueItems.length === 0) {
                    returnValue = false;
                }
            }

            // paymentDue (partial match)
            if (paymentDueFilter !== undefined && paymentDueFilter !== "undefined" && paymentDueFilter === "true") {

                var paymentDueItems = _.filter(o.actions, function (o) {
                    return (o.actionType === "paymentDue");
                });

                if (!paymentDueItems || paymentDueItems.length === 0) {
                    returnValue = false;
                }
            }

            // cardExpiry (partial match)
            if (cardExpiryFilter !== undefined && cardExpiryFilter !== "undefined" && cardExpiryFilter === "true") {

                var cardExpiryItems = _.filter(o.actions, function (o) {
                    return (o.actionType === "creditCardExpiry");
                });

                if (!cardExpiryItems || cardExpiryItems.length === 0) {
                    returnValue = false;
                }
            }

            return returnValue;
        });
    }

    // Sort 
    var sortFilter = req.body.data.attributes.params.orderBy[0].fieldName;
    var sortDirectionFilter = sortFilter.indexOf("Desc") > -1 ? "descending" : "ascending";
    sortFilter = sortFilter.replace("Desc", "").replace("Asc", "");
    var sortedItems = filteredItems;
    sortedItems = _.sortBy(filteredItems, [function (o) { return o[sortFilter]; }]);
    if (sortFilter === "totalPremium") {
        sortedItems = _.sortBy(filteredItems, [function (o) { return o[sortFilter].amount; }]);
    }
    if (sortFilter === "policyStartDate") {
        sortedItems = _.sortBy(filteredItems, [function (o) { return (o.policyStartDate && helpers.getDate(o.policyStartDate)); }]);
    }
    if (sortDirectionFilter !== "ascending") sortedItems = _.reverse(sortedItems); // descending 


    // Paginate 
    var pageSize = req.body.data.attributes.params.pageSize;
    var paginatedItems = _.chunk(sortedItems, 1); // pageSize === 0
    if (pageSize !== 0) {
        paginatedItems = _.chunk(sortedItems, pageSize);
    }
    var targetPage = paginatedItems[req.body.data.attributes.params.page - 1];

    if (targetPage === undefined) {
        targetPage = [];
    }

    // Response object 
    var responseObject = {
        "meta": {
            "totalRecords": sortedItems.length,
            "totalPages": paginatedItems.length,
            "currentPage": req.body.data.attributes.params.page
        },
        "data": {
            "type": "Policy",
            "attributes": {
                "entries": targetPage
            }
        }
    };

    return responseObject;
}

/* registerUser */
exports.registerUser = function (req) {
    var firstName = req.body.data.attributes.firstName;
    var lastName = req.body.data.attributes.lastName;
    var email = req.body.data.attributes.email;
    var policyNumber = req.body.data.attributes.policyNumber;

    var responseObject = {
        "data": {
            "type": "type",
            "attributes": {
                "accountId": "accountId_" + email,
                "accountName": firstName + " " + lastName,
                "contactId": "",
                "publicId": "publicId",
                "policyNumber": policyNumber,
                "email": email,
                "firstName": firstName,
                "lastName": lastName,
                "authorities": [
                  {
                      "access": {
                          "accessType": "",
                          "policyNumber": "",
                          "status": ""
                      }
                  }
                ]
            }
        }
    };

    return responseObject;
}

/* getDocuments */
exports.getDocuments = function (req) {
    var mockDataDocuments = state.MockDataDocuments;
    var filteredItems = _.map(mockDataDocuments, function (o) {
        return o;
    });

    // Filter
    var policyId = req.params.policyId;
    var periodStartDate = req.query["periodStartDate"];
    var periodEndDate = req.query["periodEndDate"];
    var documentTypeId = req.query["documentTypeId"];
    var pageSize = parseInt(req.query["pageSize"], 10);
    var page = parseInt(req.query["page"], 10);

    if (periodStartDate && periodEndDate) {
        filteredItems = _.filter(filteredItems, function (o) {
            return (
                    (periodStartDate !== undefined && periodStartDate !== "undefined") &&
                    (periodEndDate !== undefined && periodEndDate !== "undefined") &&
                    (new Date(o.metadata.policyPeriodStart).toDateString() === helpers.getDate(periodStartDate).toDateString()) &&
                    (new Date(o.metadata.policyPeriodEnd).toDateString() === helpers.getDate(periodEndDate).toDateString())
                   );
        });
    }

    // Paginate 
    var paginatedItems = _.chunk(filteredItems, pageSize);
    var targetPage = paginatedItems[page - 1];

    var responseObject = {
        "meta": {
            "totalRecords": filteredItems.length,
            "totalPages": paginatedItems.length,
            "currentPage": page
        },
        "data": {
            "type": "DocumentSearch",
            "id": "93oac6984fba1a00b916aa811310c721",
            "attributes": {
                "documents": (!targetPage || !targetPage.length) ? [] : targetPage
            }
        }
    };

    if (policyId === '140155201') {
        responseObject = {
            "meta": {
                "totalRecords": 0,
                "totalPages": 0,
                "currentPage": 0
            },
            "data": {
                "type": "DocumentSearch",
                "id": "93oac6984fba1a00b916aa811310c721",
                "attributes": {
                    "documents": []
                }
            }
        };
    }

    return responseObject;
}

/* stagingLocations */
exports.stagingLocations = function (req) {
    var policyId = req.params.policyId;
    var fileName = req.body.data.attributes.fileName;
    var documentType = req.body.data.attributes.documentType;

    var responseObject = {
        "data": {
            "type": "DocumentLocation",
            "id": "id-" + fileName,
            "attributes": {
                "objectKey": "ppm/" + fileName,
                "uploadUrl": "https://icare-policy-portal-adapter-react-dev.getsandbox.com/v2/portal/workersInsurance/policies/me/" + policyId + "/upload",
                "downloadUrl": "https://icare-policy-portal-adapter-react-dev.getsandbox.com/v2/portal/workersInsurance/policies/me/" + policyId + "/download",
                "deleteUrl": "https://icare-policy-portal-adapter-react-dev.getsandbox.com/v2/portal/workersInsurance/policies/me/" + policyId + "/delete"
            }
        }
    };

    return responseObject;
}

/* archive */
exports.archive = function (req) {
    var responseObject = "";
    return responseObject;
}

/* upload */
exports.upload = function (req) {
    var responseObject = "";
    return responseObject;
}

/* delete */
exports.delete = function (req) {
    var responseObject = "";
    return responseObject;
}

/* download */
exports.download = function (req) {
    var responseObject = "";
    return responseObject;
}

/* usersReponse*/
exports.usersGetDetails = function (req, userEmail) {
    var responseObject = {
        "data": {
            "type": "UserProfile",
            "id": "00ubzemkdtSSmQ9Po0h7",
            "attributes": {
                "identityId": "00ubzemkdtSSmQ9Po0h7",
                "contactInfo": {
                    "contactId": "1234",
                    "firstName": "James",
                    "lastName": "Vuong",
                    "email": userEmail,
                    "phone": {
                        "contactNumber": "0409778009",
                        "phoneType": "mobile"
                    },
                    "contactPreference": "email",
                    "agreeToSmsNotification": true
                },
                "authorisations": [
                    {
                        "type": "PolicyAuthorisation",
                        "assertions": [
                              {
                                  "groupName": "Motor Trader Association",
                                  "brokerCode": "11765",
                                  "role": "Administrator"
                              },
                              {
                                  "groupName": "67890 General Group Name",
                                  "brokerCode": "67890",
                                  "role": "General"
                              },
                              {
                                  "groupName": "XXXXX Trader Association",
                                  "brokerCode": "12225",
                                  "role": "Administrator"
                              },
                              {
                                  "groupName": "12345 Group Name",
                                  "brokerCode": "12345",
                                  "role": "General"
                              }
                        ]
                    }
                ]
            }
        }
    };
    return responseObject;
}

exports.patchUserDetails = function (req) {
    var responseObject = {
        "data": {
            "attributes": {
                "sourceSystem": "portal-adapter-wi",
                "version": 9
            },
            "id": "0032O000002hKG8QAM",
            "links": {
                "self": "https://sit2.partner-nonprod.icare.nsw.gov.au/v1/customer/contacts/0032O000002hKG8QAM"
            },
            "type": "Contact"
        }
    };

    return responseObject;
}

/* usersReponse*/
exports.usersGetDetailsEmployer = function (req, userEmail) {
    var responseObject = {
        "data": {
            "type": "UserProfile",
            "id": "00ubzemkdtSSmQ9Po0h7",
            "attributes": {
                "identityId": "00ubzemkdtSSmQ9Po0h7",
                "contactInfo": {
                    "contactId": "1234",
                    "firstName": "James",
                    "lastName": "Vuong",
                    "email": userEmail,
                    "phone": {
                        "contactNumber": "0409778009",
                        "phoneType": "mobile"
                    },
                    "contactPreference": "email",
                    "agreeToSmsNotification": true
                },
                "authorisations": [
                    {
                        "type": "PolicyAuthorisation",
                        "assertions": [
                              {
                                  "policyNumber": "8334567",
                                  "entityName": "Xxxxxxxxx",
                                  "role": "Administrator"
                              },
                              {
                                  "policyNumber": "1234567",
                                  "entityName": "Bunnings",
                                  "role": "Administrator"
                              }
                        ]
                    }
                ]
            }
        }
    };
    return responseObject;
}


/* rebind policy quote*/
exports.rebindPolicyQuote = function (req) {
    var responseObject = {
        "data": {
            "type": "UpdatePolicy",
            "id": 110403501,
            "attributes": {
                "accountID": "1104035",
                "quoteID": "100000000003504",
                "status": "Bound",
                "bindData": {
                    "paymentReference": "258251",
                    "fullPayDiscountPercentage": "20",
                    "paymentPlans": [
                      {
                          "name": "icare NI Payment Plan_Yearly",
                          "downPayment": {
                              "amount": 3659.79,
                              "amountCurrency": "[aud]"
                          },
                          "total": {
                              "amount": 3659.79,
                              "amountCurrency": "[aud]"
                          },
                          "installment": {
                              "amount": 0,
                              "amountCurrency": "[aud]"
                          },
                          "billingId": "icare_bcpp:1"
                      },
                      {
                          "name": "icare NI Payment Plan_Quarterly",
                          "downPayment": {
                              "amount": 963.11,
                              "amountCurrency": "[aud]"
                          },
                          "total": {
                              "amount": 3852.41,
                              "amountCurrency": "[aud]"
                          },
                          "installment": {
                              "amount": 963.1,
                              "amountCurrency": "[aud]"
                          },
                          "billingId": "icare_bcpp:3"
                      }
                    ],
                    "bPayReference": "110403501171",
                    "accountNumber": "12324",
                    "chosenQuote": "pc:5403",
                    "registrationCode": "12334",
                    "isUwIssueAvailable": false,
                    "billingAddress": {
                        "publicId": "123",
                        "displayName": "ade",
                        "isAddressVerified": false,
                        "addressLine1": "41 Navy Rd",
                        "addressLine2": "41 Navy Rd",
                        "addressLine3": "41 Navy Rd",
                        "city": "LLANDILO",
                        "state": "[AU_NSW,AU_JBT,AU_ACT,AU_NT,AU_QLD,AU_SA,AU_TAS,AU_VIC,AU_WA]",
                        "postalCode": "2747",
                        "country": "[AU]",
                        "addressType": "[business,home,billing,postal,headoffice,other]",
                        "isAutofilled": true
                    },
                    "paymentDetails": {
                        "paymentMethod": "card",
                        "bankAccountData": {
                            "bankAbaNumber": "12323434",
                            "bankAccountNumber": "124345435",
                            "bankAccountType": "dss",
                            "bankName": "asdad"
                        },
                        "creditCardData": {
                            "creditCardExpDate": "2018-09-11T14:00:00Z",
                            "creditCardIssuer": "dsa",
                            "creditCardNumber": "12323434"
                        }
                    },
                    "selectedPaymentPlan": "icare NI Payment Plan_Quarterly"
                }
            }
        }
    };
    return responseObject;
}


/* forgot regsitration */
exports.forgotRegistration = function (req) {
    var responseObject = {
        "data": {
            "type": "ForgotRegistrationCodeRequest",
            "attributes": {
                "accountId": "xASd761Nas",
                "accountName": "MULE GROUP TRUST",
                "email": "david@bbawyong.com.au",
                "firstName": "DAVID",
                "lastName": "BRINE",
                "contactId": "",
                "publicId": "hid:acc:A02352016",
                "policyNumber": "110403501"
            }
        }
    };
    return responseObject;
}


/* policyPatch */
exports.policyPatch = function (req) {
    var responseObject = {
        "data": {
            "type": "Policy",
            "id": req.params.policyId,
            "attributes": {
                "sendDocument": "true",
                "accountId": "1400932",
                "quoteId": "100000000003659",
                "policyPending": false,
                "updateTime": "2018-09-11T14:00:00Z",
                "employerCategory": "Employer",
                "status": "Quoted", //"[Draft,Quoted,Bound,Declined,NotTaken,Closed,Quoting,Binding,Renewing,NonRenewing,NotTaking,Canceling,Rescinding,Rescinded,Reinstating,AuditComplete,Waived,InForce_icare]",
                "policyChangeReason": "PremiumChange", //"[cancellationadjsutment,Changetoestimatewages,Changetohindsight,ChangetoWIC,Expirydatechange,groupcascade,Hindsight,hindsight3yr,hindsight5yr,LPRfirstadjustment,LPRfourthadjustment,LPRsecondadjustment,LPRthirdadjustment,niladjustbatch,Nonpremiumchange,PremiumChange]",
                "draftData": {
                    "productName": "Workers' Insurance",
                    "productCode": "WC_ICARE",
                    "previousEndDate": "2019-09-11T14:01:00.000Z",
                    "periodStartDate": "2018-12-06T14:01:00.000Z",
                    "periodEndDate": "2019-12-06T14:01:00.000Z",
                    "offering": "WCNI_icare",
                    "accountNumber": "1233",
                    "termType": "Annual",
                    "account": {
                        "tempId": 1,
                        "publicId": "pc:4605",
                        "displayName": "Plumbing Corp. PTY LTD.",
                        "companyName": "Plumbing Corp. PTY LTD.",
                        "accountHolder": true,
                        "particle": "asd",
                        "primaryPhoneType": "home",
                        "communicationPreference": "email",
                        "workNumber": 12345566,
                        "mobileNumber": "0411444555",
                        "homeNumber": 12345566,
                        "website": "abc.com",
                        "emailAddress1": "xyz@test.com",
                        "accountOrgType": "partnership",
                        "abn": "88000014675",
                        "acn": "123453",
                        "isVedaValidated": false,
                        "acnStatus": "dfs",
                        "abnStatus": "sdf",
                        "trusteeType": "partnership",
                        "trustABNStatus": "ok",
                        "trustABN": "ABN123",
                        "trustACN": "1221",
                        "trustName": "abc",
                        "trusteeName": "wewq",
                        "tradingBusinessName": "Jim's Plumbing",
                        "primaryLanguage": "en_US",
                        "gstRegistration": true,
                        "itcEntitlement": "100",
                        "commencementDate": "2017-08-29T14:01:00.000Z",
                        "cluster": "eweq",
                        "newAccountReason": "brnfrmsplit",
                        "industryCode": "abc",
                        "descriptionOfBusiness": "weqw",
                        "serviceTier": "silver",
                        "yearBusinessStarted": "2010",
                        "isAnonymousAccount": false,
                        "accountId": "1234"
                    },
                    "policyAddress": {
                        "publicId": "123",
                        "displayName": "ade",
                        "isAddressVerified": false,
                        "addressLine1": "41 Navy Rd",
                        "addressLine2": "41 Navy Rd",
                        "addressLine3": "41 Navy Rd",
                        "city": "LLANDILO",
                        "state": "AU_NSW",
                        "postalCode": "2747",
                        "country": "AU",
                        "addressType": "postal",
                        "isAutofilled": true
                    },
                    "accountContact": [
                       {
                           "contactRole": [
                              "LocationContact_icare"
                           ],
                           "publicId": "pc:3104",
                           "contactUpdateFlag": true,
                           "contactRetired": false,
                           "personContact": {
                               "publicId": "pc:4706",
                               "lastName": "LastAuto",
                               "firstName": "FirstAuto",
                               "displayName": "FirstAuto LastAuto",
                               "tempId": 123,
                               "particle": "abc",
                               "primaryPhoneType": "work",
                               "workNumber": "0211000",
                               "mobileNumber": "0411444555",
                               "homeNumber": "0211000",
                               "faxPhone": "0211000",
                               "website": "abc.com",
                               "communicationPreference": "email",
                               "emailAddress1": "NISPUATAuto@gmail.com",
                               "smsNotifications": false
                           },
                           "contactAddress": [
                              {
                                  "publicId": "pc:8608",
                                  "displayName": "name",
                                  "isAddressVerified": false,
                                  "addressLine1": "32 Harris Street",
                                  "addressLine2": "32 Harris Street",
                                  "addressLine3": "32 Harris Street",
                                  "city": "BALMAIN",
                                  "state": "AU_NSW",
                                  "postalCode": "2041",
                                  "country": "[AU]",
                                  "addressType": "postal",
                                  "isAutofilled": true
                              }
                           ]
                       }
                    ],
                    "preQualQuestionSets": [
                       {
                           "code": "WCNIPreQual_icare",
                           "answers": {
                               "annualGrossWages": false,
                               "employeeApprenticeTrainee": true,
                               "employeePerCapita": false,
                               "employeeNSW": true,
                               "groupOfCompanies": false
                           }
                       }
                    ],
                    "lobs": {
                        "wcLine": {
                            "policyPeriod": {
                                "offerings": "offs",
                                "termNumber": 0,
                                "siFund": "[sifund]",
                                "brokerId": "icarewc",
                                "brokerOrganisation": "AON",
                                "termType": "[Annual,HalfYear,Other]",
                                "brokerName": "AON Parramatta",
                                "legacyPolicyNumber": "123456",
                                "schemeAgentContact": "cont",
                                "publicId": "pc:4890",
                                "wcLabourHireCode": "1", //"[1,2,3]",
                                "isLegacy": false,
                                "policyType": "Standard",
                                "writtenDate": "2018-09-19T14:01:00.000Z",
                                "rateAsOfDate": "2018-09-12T03:36:46Z",
                                "producerCodeOrg": "abc",
                                "gstRegistration": false,
                                "itcEntitlement": "100",
                                "schemeAgent": "Allianz-033",
                                "reasonLowWages": "none",
                                "groupNumber": "2318569",
                                "groupName": "Jim's Co.",
                                "policy": {
                                    "primaryLanguage": "en_US",
                                    "priorPolicy": [
                                       {
                                           "carrier": "car",
                                           "expirationDate": "2018-09-19T14:01:00.000Z",
                                           "effectiveDate": "2018-09-19T14:01:00.000Z",
                                           "annualPremiumAmount": 123442,
                                           "numLosses": 2,
                                           "totalLossesAmount": 450,
                                           "wcCarrier": "allianz",
                                           "policy": "pol",
                                           "comments": "adsf",
                                           "wagesAmount": 234324,
                                           "numberOfWICs": 4
                                       }
                                    ],
                                    "lossHistoryEntry": [
                                       {
                                           "locationName": "adsa",
                                           "costCenterNo": "123",
                                           "directWageId": {
                                               "wicRate": 4.432,
                                               "wicDescription": "Banks",
                                               "wicCode": "732100 - Banks",
                                               "perCapitaFlag": false,
                                               "pacDescription": "Development bank operation",
                                               "pacCode": "732100001",
                                               "ddlContribution": 0.028,
                                               "code": "732100"
                                           },
                                           "claimAmountYear3": null,
                                           "claimAmountYear2": 333344,
                                           "claimAmountYear1": 333344,
                                           "basicTariffPremiumAmountYear3": 333344,
                                           "basicTariffPremiumAmountYear2": 333344,
                                           "basicTariffPremiumAmountYear1": 333344,
                                           "wages1": 3452,
                                           "wages2": 3452,
                                           "wages3": 3452,
                                           "occurrenceDate": "2018-09-19T14:01:00.000Z",
                                           "description": "sfd",
                                           "totalIncurredAmount": 123333333,
                                           "lossStatus": "[Open,Closed]",
                                           "amountReserved": 12332432,
                                           "amountPaid": 324345
                                       }
                                    ]
                                },
                                "claim": [
                                   {
                                       "totalIncurred": 0,
                                       "status": "abc",
                                       "policyInForce": false,
                                       "lossDate": "2018-09-19T14:01:00.000Z",
                                       "claimNumber": "123"
                                   }
                                ],
                                "policyTerm": {
                                    "startDate": "2018-09-11T14:00:00Z",
                                    "organization": "ade",
                                    "endDate": "2018-09-11T14:00:00Z",
                                    "affinitygroupType": "[Closed,Open]",
                                    "affinityGroupName": "avb"
                                },
                                "jobRoleAssignment": [
                                   {
                                       "assignedUser": "integportal user",
                                       "assignedGroup": "Icare Grp",
                                       "jobUserRoleAssignment": "100000000003538"
                                   }
                                ],
                                "policyLocation": [
                                   {
                                       "publicId": "pc:5504",
                                       "primaryLocation": true,
                                       "locationRetired": false,
                                       "isRatedLocation": true,
                                       "accountLocation": {
                                           "nonSpecific": true,
                                           "locationName": "sds",
                                           "locationCode": "fsd",
                                           "address": {
                                               "publicId": "pc:8609",
                                               "displayName": "dsds",
                                               "isAddressVerified": false,
                                               "addressLine1": "481-499 Pittwater Road",
                                               "addressLine2": "",
                                               "addressLine3": "",
                                               "city": "BROOKVALE",
                                               "state": "NSW",
                                               "postalCode": "2100",
                                               "country": "AU",
                                               "addressType": "business",
                                               "isAutofilled": true
                                           }
                                       },
                                       "costCenters": [
                                          {
                                              "name": "wewe",
                                              "fullName": "PolicyLocation : 1",
                                              "centerNumber": "123",
                                              "publicId": "pc:7614",
                                              "directWages": [
                                                 {
                                                     "wic": 0,
                                                     "pac": 0,
                                                     "grossValue": 600000,
                                                     "numberOfUnits": 3444,
                                                     "numberOfApprentices": 432,
                                                     "numberOfEmployees": 12,
                                                     "apprenticesWages": 0,
                                                     "totalWages": 600000,
                                                     "wagesAmount": 0,
                                                     "claimAmountYear3": 1000.99,
                                                     "claimAmountYear2": 1000.99,
                                                     "claimAmountYear1": 1000.99,
                                                     "basicTariffPremiumAmountYear3": 1000.99,
                                                     "basicTariffPremiumAmountYear2": 1000.99,
                                                     "basicTariffPremiumAmountYear1": 1000.99,
                                                     "businessDescription": "General Commerce",
                                                     "directWageId": {
                                                         "wicRate": 4.432,
                                                         "wicDescription": "Plumbing Services",
                                                         "wicCode": "732100 - Plumbing Services",
                                                         "perCapitaFlag": false,
                                                         "pacDescription": "Development bank operation",
                                                         "pacCode": "423100",
                                                         "ddlContribution": 0.028,
                                                         "code": "423100"
                                                     },
                                                     "asbestoses": [
                                                        {
                                                            "descriptionOfWork": "ww",
                                                            "numberOfWorkersExposed": 3,
                                                            "grossWagesForAsbestosHandling": 6324328
                                                        }
                                                     ],
                                                     "taxiPlates": [
                                                        {
                                                            "taxiPlate": "12323",
                                                            "startDate": "2018-09-11T14:00:00Z",
                                                            "endDate": "2018-10-11T14:00:00Z"
                                                        }
                                                     ],
                                                     "contactorWages": [
                                                        {
                                                            "labourComponent": 1000,
                                                            "percent": 10.1,
                                                            "contractType": "labouronly",
                                                            "totalValueOfContract": 1000.99,
                                                            "numberOfContractWorkers": 10,
                                                            "description": "dews"
                                                        }
                                                     ]
                                                 }
                                              ]
                                          }
                                       ]
                                   }
                                ]
                            }
                        }
                    }
                },
                "quoteData": {
                    "offeredQuotes": [
                       {
                           "publicId": "pc:5403",
                           "branchCode": "WCNI_icare",
                           "branchName": "branch123",
                           "isCustom": true,
                           "annualPremium": {
                               "amount": 18746,
                               "amountCurrency": "aud"
                           },
                           "previousAnnualPremium": {
                               "amount": 18374,
                               "amountCurrency": "aud"
                           },
                           "transactionCost": {
                               "amount": 18746,
                               "amountCurrency": "aud"
                           },
                           "termMonths": 12,
                           "taxes": {
                               "amountCurrency": "aud",
                               "amount": 3509.1
                           },
                           "lobs": {
                               "wcLine": {
                                   "wageDetails": {
                                       "wagePremium": [
                                        {
                                            "wic": "ds",
                                            "rate": 21,
                                            "totalAnnualWages": {
                                                "amount": 18746,
                                                "amountCurrency": "aud"
                                            },
                                            "averagePerformancePremium": {
                                                "amount": 18746,
                                                "amountCurrency": "aud"
                                            }
                                        }
                                       ],
                                       "totalAveragePerformancePremium": {
                                           "amount": 18746,
                                           "amountCurrency": "aud"
                                       }
                                   },
                                   "esiPremium": {
                                       "esiRate": 1.2,
                                       "esi": {
                                           "amountCurrency": "aud",
                                           "amount": -4160
                                       },
                                       "averagePerformancePremium": {
                                           "amount": 157.1,
                                           "amountCurrency": "aud"
                                       }
                                   },
                                   "claimPerformance": {
                                       "totalPriorApp": {
                                           "amount": 157.1,
                                           "amountCurrency": "aud"
                                       },
                                       "totalPriorLoss": {
                                           "amount": 4268.27,
                                           "amountCurrency": "aud"
                                       },
                                       "schemePerformanceMeasure": 3,
                                       "claimPerformanceAdjustment": 2,
                                       "claimPerformanceRate": 2.1,
                                       "claimPerformanceMeasure": 5,
                                       "premiumRiskAssessment": {
                                           "amount": 4268.27,
                                           "amountCurrency": "aud"
                                       },
                                       "cprAdjustment": {
                                           "amount": 4268.27,
                                           "amountCurrency": "aud"
                                       }
                                   },
                                   "averagePerformancePremium": {
                                       "amount": 4268.27,
                                       "amountCurrency": "aud"
                                   },
                                   "annualisedApp": {
                                       "amount": 4280,
                                       "amountCurrency": "aud"
                                   },
                                   "premium": {
                                       "amount": 3852.41,
                                       "amountCurrency": "aud"
                                   },
                                   "dustDiseaseLavy": {
                                       "amount": 10.97,
                                       "amountCurrency": "aud"
                                   },
                                   "asbestosCost": {
                                       "amount": 0,
                                       "amountCurrency": "aud"
                                   },
                                   "totalDdl": {
                                       "amount": 157.1,
                                       "amountCurrency": "aud"
                                   },
                                   "apprenticeDiscount": {
                                       "amount": 0,
                                       "amountCurrency": "aud"
                                   },
                                   "performanceDiscount": {
                                       "amount": 0,
                                       "amountCurrency": "aud"
                                   },
                                   "mineSafetyCost": {
                                       "amount": 0,
                                       "amountCurrency": "aud"
                                   },
                                   "itcAmount": {
                                       "amount": 0,
                                       "amountCurrency": "aud"
                                   }
                               }
                           }
                       }
                    ]
                },
                "bindData": {
                    "bPayReference": "5123",
                    "paymentReference": "258251",
                    "accountNumber": "12324",
                    "fullPayDiscountPercentage": "20",
                    "chosenQuote": "pc:5403",
                    "paymentPlans": [
                       {
                           "name": "icare NI Payment Plan_Quarterly",
                           "downPayment": {
                               "amountCurrency": "aud",
                               "amount": 9650
                           },
                           "total": {
                               "amountCurrency": "aud",
                               "amount": 38600
                           },
                           "installment": {
                               "amountCurrency": "aud",
                               "amount": 9650
                           },
                           "billingId": "icare_bcpp:3"
                       }
                    ],
                    "selectedPaymentPlan": "icare_bcpp:1",
                    "isUwIssueAvailable": false,
                    "billingAddress": {
                        "publicId": "123",
                        "displayName": "ade",
                        "isAddressVerified": false,
                        "addressLine1": "41 Navy Rd",
                        "addressLine2": "41 Navy Rd",
                        "addressLine3": "41 Navy Rd",
                        "city": "LLANDILO",
                        "state": "AU_NSW",
                        "postalCode": "2747",
                        "country": "AU",
                        "addressType": "business",
                        "isAutofilled": true
                    },
                    "paymentDetails": {
                        "paymentMethod": "card",
                        "bankAccountData": {
                            "bankAbaNumber": "12323434",
                            "bankAccountNumber": "124345435",
                            "bankAccountType": "dss",
                            "bankName": "asdad"
                        },
                        "creditCardData": {
                            "creditCardExpDate": "2018-09-11T14:00:00Z",
                            "creditCardIssuer": "dsa",
                            "creditCardNumber": "12323434"
                        }
                    },
                    "registrationCode": "123234"
                }
            }
        }
    };

    return responseObject;
}

/* policyRenewal */
exports.policyRenewal = function (req) {
    var xTokenID1 = helpers.returnXTokenID1Header(req);
    var policyStatus = "Renewing";
    var totalEmployees = 0;
    var grossValue = 0;
    var contractorWages = 0;
    var numberOfContractWorkers = 0;
    var policyStartDate = "2019-01-15T14:00:00Z";
    var policyEndDate = "2020-01-15T14:00:00Z";
    var abn = "26 129 448 871";
    var entityName = "Timber Supplies Pty Ltd";
    var brokerOrg = "AoN Parramatta";
    var wicCode = "453100";
    var wicDesc = "Timber Wholesaling";
    var businessActivity = "Plywood dealing";
    var businessActivityCode = "453100001";
    var policyAddress = {
        "displayName": "481-499 Pittwater Road, Brookvale NSW 2100",
        "isAddressVerified": true,
        "addressLine1": "481-499 Pittwater Road",
        "city": "Brookvale",
        "state": "AU_NSW",
        "postalCode": "2100",
        "country": "AU",
        "addressType": "business",
        "publicId": "pc:12018",
        "addressLine2": "",
        "addressLine3": "",
        "isAutofilled": true
    };
    if (!Array.isArray(xTokenID1) && xTokenID1 === "1xtestemployer2@gmail.com") {
        policyStatus = "Quoted";
        totalEmployees = 3;
        numberOfContractWorkers = 0;
        grossValue = 125000;
        contractorWages = 0;
        policyStartDate = "2019-01-12T14:00:00Z";
        policyEndDate = "2020-01-12T14:00:00Z";
        abn = "88 000 014 675";
        entityName = "Jim's Prawns";
        brokerOrg = "AoN Parramatta";
        wicCode = "041200";
        wicDesc = "Prawn Fishing";
        businessActivity = "Prawn Fishing";
        businessActivityCode = "041200001";
        policyAddress = {
            "displayName": "41 Navy Rd, LLANDILO NSW 2747",
            "isAddressVerified": true,
            "addressLine1": "41 Navy Rd",
            "city": "LLANDILO",
            "state": "AU_NSW",
            "postalCode": "2747",
            "country": "AU",
            "addressType": "business",
            "publicId": "pc:12018",
            "addressLine2": "",
            "addressLine3": "",
            "isAutofilled": true
        };
    }

    if (!Array.isArray(xTokenID1) && xTokenID1 === "1xtestbroker3@gmail.com") {
        policyStatus = "Quoted";
        totalEmployees = 0;
        numberOfContractWorkers = 0;
        grossValue = 0;
        contractorWages = 0;
    }

    var responseObject = {
        "data": {
            "type": "PolicyRenewal",
            "id": req.params.policyId,
            "attributes": {
                "status": policyStatus,
                "accountId": "1400932",
                "sendDocument": "true",
                "employerCategory": "Employer",
                "policyChangeReason": "cancellationadjsutment",
                "isSubmitAgent": false,
                "quoteId": "100000000003955",
                "hadPolicylast12Months": 'yes',
                "draftData": {
                    "account": {
                        "displayName": entityName,
                        "companyName": entityName,
                        "accountHolder": false,
                        "primaryPhoneType": "home",
                        "mobileNumber": "0426353063",
                        "emailAddress1": "abc@test.com",
                        "accountOrgType": "partnership",
                        "isVedaValidated": false,
                        "primaryLanguage": "en_US",
                        "gstRegistration": false,
                        "itcEntitlement": "0",
                        "commencementDate": "2018-09-11T14:00:00Z",
                        "abn": abn,
                        "publicId": "pc:4464",
                        "tempId": "1234",
                        "particle": "asd",
                        "workNumber": "12345566",
                        "homeNumber": "12345566",
                        "faxPhone": "0426353063",
                        "website": "abc.com",
                        "communicationPreference": "email",
                        "industryCode": "abc",
                        "acn": "",
                        "abnStatus": "ok",
                        "acnStatus": "ok",
                        "trusteeType": "partnership",
                        "trustABN": "ABN123",
                        "trustName": "name",
                        "trusteeName": "wewq",
                        "trustACN": "1221",
                        "trustABNStatus": "ok",
                        "tradingBusinessName": "",
                        "descriptionOfBusiness": "djdjd",
                        "cluster": "sde",
                        "newAccountReason": "New",
                        "serviceTier": "platinum",
                        "yearBusinessStarted": "2010",
                        "isAnonymousAccount": false,
                        "accountId": "1234"
                    },
                    "policyAddress": policyAddress,
                    "productCode": "WC_ICARE",
                    "offering": "WCNI_icare",
                    "accountContact": [
                      {
                          "contactRole": [
                            "LocationContact_icare"
                          ],
                          "contactUpdateFlag": false,
                          "contactRetired": false,
                          "personContact": {
                              "displayName": "John John",
                              "firstName": "John",
                              "lastName": "John",
                              "primaryPhoneType": "home",
                              "mobileNumber": "0426353063",
                              "communicationPreference": "email",
                              "emailAddress1": "xyz@test.com",
                              "smsNotifications": true,
                              "publicId": "pc:4889",
                              "particle": "abc",
                              "workNumber": "0211000",
                              "homeNumber": "0211000",
                              "faxPhone": "0211000",
                              "website": "abc.com",
                              "tempId": 12345
                          },
                          "contactAddress": [
                            {
                                "displayName": "name",
                                "isAddressVerified": false,
                                "addressLine1": "41 Navy Rd",
                                "city": "LLANDILO",
                                "state": "AU_NSW",
                                "postalCode": "2747",
                                "country": "AU",
                                "addressType": "business",
                                "publicId": "pc:14426",
                                "addressLine2": "41 Navy Rd",
                                "addressLine3": "41 Navy Rd",
                                "isAutofilled": true
                            }
                          ],
                          "publicId": "pc:2316"
                      }
                    ],
                    "productName": "Workers' Insurance",
                    "previousEndDate": "2019-09-30T14:01:00Z",
                    "periodStartDate": policyStartDate,
                    "periodEndDate": policyEndDate,
                    "preQualQuestionSets": [
                      {
                          "code": "WCNIPreQual_icare",
                          "answers": {
                              "annualGrossWages": false,
                              "employeeApprenticeTrainee": true,
                              "employeePerCapita": false,
                              "employeeNSW": true,
                              "groupOfCompanies": false
                          }
                      }
                    ],
                    "lobs": {
                        "wcLine": {
                            "policyPeriod": {
                                "policyType": "Standard",
                                "termNumber": 0,
                                "brokerOrganisation": brokerOrg,
                                "termType": "Annual",
                                "writtenDate": "2018-09-11T14:00:00Z",
                                "rateAsOfDate": "2018-09-12T03:36:46Z",
                                "gstRegistration": false,
                                "itcEntitlement": "0",
                                "schemeAgent": "icare-422",
                                "reasonLowWages": "none",
                                "policy": {
                                    "primaryLanguage": "[en_US,en_US_edg]",
                                    "priorPolicy": [
                                      {
                                          "wcCarrier": "icare",
                                          "policyNumber": null,
                                          "effectiveDate": "2018-09-11T14:00:00Z",
                                          "policy": "ewrw",
                                          "carrier": "we",
                                          "expirationDate": "2018-09-14T14:00:00Z",
                                          "annualPremiumAmount": 123442,
                                          "numLosses": 3,
                                          "totalLossesAmount": 45,
                                          "comments": "ewr",
                                          "wagesAmount": 234324,
                                          "numberOfWICs": 4
                                      }
                                    ],
                                    "lossHistoryEntry": [
                                      {
                                          "locationName": "adsa",
                                          "occurrenceDate": "2018-09-11T14:00:00Z",
                                          "description": "awe",
                                          "costCenterNo": "123",
                                          "directWageId": {
                                              "wicRate": 4.432,
                                              "wicDescription": wicDesc,
                                              "wicCode": "732100 - Banks",
                                              "perCapitaFlag": false,
                                              "pacDescription": businessActivity,
                                              "pacCode": businessActivityCode,
                                              "ddlContribution": 0.028,
                                              "code": wicCode
                                          },
                                          "claimYear3": 333344,
                                          "claimYear2": 1233432,
                                          "claimYear1": 12324,
                                          "basicTariffPremiumYear3": 21234,
                                          "basicTariffPremiumYear2": 4354,
                                          "basicTariffPremiumYear1": 55656,
                                          "wages1": 3452,
                                          "wages2": 3452,
                                          "wages3": 3452,
                                          "totalIncurredAmount": 23,
                                          "amountPaid": 20,
                                          "amountReserved": 3,
                                          "lossStatus": "Open"
                                      }
                                    ]
                                },
                                "policyTerm": {
                                    "affinityGroupName": "avb",
                                    "affinitygroupType": "Open",
                                    "startDate": "2018-09-11T14:00:00Z",
                                    "endDate": "2018-10-11T14:00:00Z",
                                    "organization": "werew"
                                },
                                "jobRoleAssignment": [
                                  {
                                      "assignedUser": "integportal user",
                                      "assignedGroup": "Icare Grp",
                                      "jobUserRoleAssignment": "100000000003538"
                                  },
                                  {
                                      "assignedUser": "Super User",
                                      "assignedGroup": "Icare Grp",
                                      "jobUserRoleAssignment": "100000000003538"
                                  }
                                ],
                                "policyLocation": [
                                  {
                                      "isRatedLocation": true,
                                      "locationRetired": false,
                                      "primaryLocation": true,
                                      "accountLocation": {
                                          "nonSpecific": true,
                                          "address": policyAddress,
                                          "locationCode": "we",
                                          "locationName": "sfs"
                                      },
                                      "costCenters": [
                                        {
                                            "fullName": "PolicyLocation : 1",
                                            "directWages": [
                                              {
                                                  "wic": 0,
                                                  "pac": 0,
                                                  "grossValue": grossValue,
                                                  "numberOfUnits": 0,
                                                  "totalWages": grossValue,
                                                  "numberOfEmployees": totalEmployees,
                                                  "wagesAmount": 0,
                                                  "businessDescription": businessActivity,
                                                  "asbestoses": [],
                                                  "taxiPlates": [],
                                                  "contactorWages": [],
                                                  "directWageId": {
                                                      "pacCode": businessActivityCode,
                                                      "pacDescription": businessActivity,
                                                      "wicRate": 4.432,
                                                      "ddlContribution": 0.028,
                                                      "code": wicCode,
                                                      "wicCode": "0",
                                                      "perCapitaFlag": false,
                                                      "wicDescription": wicDesc
                                                  },
                                                  "numberOfApprentices": null,
                                                  "apprenticesWages": null,
                                                  "claimAmountYear1": 2323,
                                                  "claimAmountYear2": 1000.99,
                                                  "claimAmountYear3": 1000.99,
                                                  "basicTariffPremiumAmountYear1": 1000.99,
                                                  "basicTariffPremiumAmountYear2": 1000.99,
                                                  "basicTariffPremiumAmountYear3": 1000.99
                                              }
                                            ],
                                            "publicId": "pc:7614",
                                            "name": "deer",
                                            "centerNumber": "wqe2"
                                        }
                                      ],
                                      "publicId": "pc:4314"
                                  }
                                ],
                                "brokerName": brokerOrg,
                                "wcLabourHireCode": "1",
                                "brokerId": "icarewc",
                                "publicId": "pc:4890",
                                "legacyPolicyNumber": null,
                                "offerings": "were",
                                "siFund": "[sifund]",
                                "isLegacy": false,
                                "schemeAgentContact": "cont",
                                "groupNumber": "2341212",
                                "groupName": "",
                                "claim": [
                                  {
                                      "policyInForce": false,
                                      "lossDate": "2018-09-19T14:01:00.000Z",
                                      "claimNumber": "123",
                                      "status": "awde",
                                      "totalIncurred": 0
                                  }
                                ],
                                "producerCodeOrg": "wdw"
                            }
                        }
                    },
                    "termType": "Annual",
                    "accountNumber": "1233"
                },
                "quoteData": {
                    "offeredQuotes": [
                      {
                          "branchCode": "WCNI_icare",
                          "isCustom": true,
                          "annualPremium": {
                              "amount": 3659.79,
                              "amountCurrency": "[aud]"
                          },
                          "termMonths": 11,
                          "taxes": {
                              "amount": 350.22,
                              "amountCurrency": "[aud]"
                          },
                          "lobs": {
                              "wcLine": {
                                  "wageDetails": {
                                      "wagePremium": [
                                        {
                                            "wic": "041200",
                                            "rate": 4.28,
                                            "totalAnnualWages": {
                                                "amount": 99726.03,
                                                "amountCurrency": "[aud]"
                                            },
                                            "averagePerformancePremium": {
                                                "amount": 4268.27,
                                                "amountCurrency": "[aud]"
                                            }
                                        }
                                      ],
                                      "totalAveragePerformancePremium": {
                                          "amount": 4268.27,
                                          "amountCurrency": "[aud]"
                                      }
                                  },
                                  "esiPremium": {
                                      "esiRate": 10,
                                      "averagePerformancePremium": {
                                          "amount": 4268.27,
                                          "amountCurrency": "[aud]"
                                      },
                                      "esi": {
                                          "amount": -426.83,
                                          "amountCurrency": "[aud]"
                                      }
                                  },
                                  "claimPerformance": {
                                      "claimPerformanceMeasure": 0,
                                      "schemePerformanceMeasure": 0,
                                      "claimPerformanceRate": 0,
                                      "claimPerformanceAdjustment": 0,
                                      "totalPriorApp": {
                                          "amount": 4268.27,
                                          "amountCurrency": "[aud]"
                                      },
                                      "totalPriorLoss": {
                                          "amount": 4268.27,
                                          "amountCurrency": "[aud]"
                                      },
                                      "premiumRiskAssessment": {
                                          "amount": 4268.27,
                                          "amountCurrency": "[aud]"
                                      },
                                      "cprAdjustment": {
                                          "amount": 4268.27,
                                          "amountCurrency": "[aud]"
                                      }
                                  },
                                  "averagePerformancePremium": {
                                      "amount": 4268.27,
                                      "amountCurrency": "[aud]"
                                  },
                                  "premium": {
                                      "amount": 3852.41,
                                      "amountCurrency": "[aud]"
                                  },
                                  "dustDiseaseLavy": {
                                      "amount": 10.97,
                                      "amountCurrency": "[aud]"
                                  },
                                  "asbestosCost": {
                                      "amount": 0,
                                      "amountCurrency": "[aud]"
                                  },
                                  "apprenticeDiscount": {
                                      "amount": 0,
                                      "amountCurrency": "[aud]"
                                  },
                                  "performanceDiscount": {
                                      "amount": 0,
                                      "amountCurrency": "[aud]"
                                  },
                                  "mineSafetyCost": {
                                      "amount": 0,
                                      "amountCurrency": "[aud]"
                                  },
                                  "annualisedApp": {
                                      "amount": 4280,
                                      "amountCurrency": "[aud]"
                                  },
                                  "totalDdl": {
                                      "amount": 10.97,
                                      "amountCurrency": "[aud]"
                                  },
                                  "itcAmount": {
                                      "amount": 0,
                                      "amountCurrency": "[aud]"
                                  }
                              }
                          },
                          "publicId": "pc:4890",
                          "branchName": "branch123",
                          "previousAnnualPremium": {
                              "amount": 3852.41,
                              "amountCurrency": "[aud]"
                          },
                          "transactionCost": {
                              "amount": 3852.41,
                              "amountCurrency": "[aud]"
                          }
                      }
                    ]
                },
                "bindData": {
                    "paymentReference": "258251",
                    "fullPayDiscountPercentage": "",
                    "paymentPlans": [
                      {
                          "name": "icare NI Payment Plan_Yearly",
                          "downPayment": {
                              "amount": 3659.79,
                              "amountCurrency": "[aud]"
                          },
                          "total": {
                              "amount": 3659.79,
                              "amountCurrency": "[aud]"
                          },
                          "installment": {
                              "amount": 0,
                              "amountCurrency": "[aud]"
                          },
                          "billingId": "icare_bcpp:1"
                      },
                      {
                          "name": "icare NI Payment Plan_Quarterly",
                          "downPayment": {
                              "amount": 963.11,
                              "amountCurrency": "[aud]"
                          },
                          "total": {
                              "amount": 3852.41,
                              "amountCurrency": "[aud]"
                          },
                          "installment": {
                              "amount": 963.1,
                              "amountCurrency": "[aud]"
                          },
                          "billingId": "icare_bcpp:3"
                      }
                    ],
                    "bPayReference": "5123",
                    "accountNumber": "12324",
                    "chosenQuote": "pc:5403",
                    "policyNumber": null,
                    "registrationCode": "12334",
                    "isUwIssueAvailable": false,
                    "billingAddress": policyAddress,
                    "paymentDetails": {
                        "paymentMethod": "card",
                        "bankAccountData": {
                            "bankAbaNumber": "12323434",
                            "bankAccountNumber": "124345435",
                            "bankAccountType": "dss",
                            "bankName": "asdad"
                        },
                        "creditCardData": {
                            "creditCardExpDate": "2018-09-11T14:00:00Z",
                            "creditCardIssuer": "dsa",
                            "creditCardNumber": "12323434"
                        }
                    },
                    "selectedPaymentPlan": "icare_bcpp:1"
                }
            }
        }
    };

    return responseObject;
}

/* policySaveRenewal */
exports.policySaveRenewal = function (req) {
    var xTokenID1 = helpers.returnXTokenID1Header(req);
    var policyStatus = "Renewing";
    var totalEmployees = 0;
    var grossValue = 0;
    var contractorWages = 0;
    var numberOfContractWorkers = 0;
    var policyStartDate = "2019-01-15T14:00:00Z";
    var policyEndDate = "2020-01-15T14:00:00Z";
    var abn = "26 129 448 871";
    var entityName = "Timber Supplies Pty Ltd";
    var brokerOrg = "AoN Parramatta";
    var wicCode = "453100";
    var wicDesc = "Timber Wholesaling";
    var businessActivity = "Plywood dealing";
    var businessActivityCode = "453100001";
    var policyAddress = {
        "displayName": "481-499 Pittwater Road, Brookvale NSW 2100",
        "isAddressVerified": true,
        "addressLine1": "481-499 Pittwater Road",
        "city": "Brookvale",
        "state": "AU_NSW",
        "postalCode": "2100",
        "country": "AU",
        "addressType": "business",
        "publicId": "pc:12018",
        "addressLine2": "",
        "addressLine3": "",
        "isAutofilled": true
    };
    if (!Array.isArray(xTokenID1) && xTokenID1 === "1xtestemployer2@gmail.com") {
        policyStatus = "Quoted";
        totalEmployees = 3;
        numberOfContractWorkers = 0;
        grossValue = 125000;
        contractorWages = 0;
        policyStartDate = "2019-01-12T14:00:00Z";
        policyEndDate = "2020-01-12T14:00:00Z";
        abn = "88 000 014 675";
        entityName = "Jim's Prawns";
        brokerOrg = "AoN Parramatta";
        wicCode = "041200";
        wicDesc = "Prawn Fishing";
        businessActivity = "Prawn Fishing";
        businessActivityCode = "041200001";
        policyAddress = {
            "displayName": "41 Navy Rd, LLANDILO NSW 2747",
            "isAddressVerified": true,
            "addressLine1": "41 Navy Rd",
            "city": "LLANDILO",
            "state": "AU_NSW",
            "postalCode": "2747",
            "country": "AU",
            "addressType": "business",
            "publicId": "pc:12018",
            "addressLine2": "",
            "addressLine3": "",
            "isAutofilled": true
        };
    }

    if (!Array.isArray(xTokenID1) && xTokenID1 === "1xtestbroker3@gmail.com") {
        policyStatus = "Quoted";
        totalEmployees = 0;
        numberOfContractWorkers = 0;
        grossValue = 0;
        contractorWages = 0;
    }

    var responseObject = {
        "data": {
            "type": "PolicyRenewal",
            "id": req.params.policyId,
            "attributes": {
                "status": policyStatus,
                "accountId": "1400932",
                "sendDocument": "true",
                "employerCategory": "Employer",
                "policyChangeReason": "cancellationadjsutment",
                "isSubmitAgent": false,
                "quoteId": "100000000003955",
                "hadPolicylast12Months": 'yes',
                "draftData": {
                    "account": {
                        "displayName": entityName,
                        "companyName": entityName,
                        "accountHolder": false,
                        "primaryPhoneType": "home",
                        "mobileNumber": "0426353063",
                        "emailAddress1": "abc@test.com",
                        "accountOrgType": "partnership",
                        "isVedaValidated": false,
                        "primaryLanguage": "en_US",
                        "gstRegistration": false,
                        "itcEntitlement": "0",
                        "commencementDate": "2018-09-11T14:00:00Z",
                        "abn": abn,
                        "publicId": "pc:4464",
                        "tempId": "1234",
                        "particle": "asd",
                        "workNumber": "12345566",
                        "homeNumber": "12345566",
                        "faxPhone": "0426353063",
                        "website": "abc.com",
                        "communicationPreference": "email",
                        "industryCode": "abc",
                        "acn": "",
                        "abnStatus": "ok",
                        "acnStatus": "ok",
                        "trusteeType": "partnership",
                        "trustABN": "ABN123",
                        "trustName": "name",
                        "trusteeName": "wewq",
                        "trustACN": "1221",
                        "trustABNStatus": "ok",
                        "tradingBusinessName": "",
                        "descriptionOfBusiness": "djdjd",
                        "cluster": "sde",
                        "newAccountReason": "New",
                        "serviceTier": "platinum",
                        "yearBusinessStarted": "2010",
                        "isAnonymousAccount": false,
                        "accountId": "1234"
                    },
                    "policyAddress": policyAddress,
                    "productCode": "WC_ICARE",
                    "offering": "WCNI_icare",
                    "accountContact": [
                      {
                          "contactRole": [
                            "LocationContact_icare"
                          ],
                          "contactUpdateFlag": false,
                          "contactRetired": false,
                          "personContact": {
                              "displayName": "John John",
                              "firstName": "John",
                              "lastName": "John",
                              "primaryPhoneType": "home",
                              "mobileNumber": "0426353063",
                              "communicationPreference": "email",
                              "emailAddress1": "xyz@test.com",
                              "smsNotifications": true,
                              "publicId": "pc:4889",
                              "particle": "abc",
                              "workNumber": "0211000",
                              "homeNumber": "0211000",
                              "faxPhone": "0211000",
                              "website": "abc.com",
                              "tempId": 12345
                          },
                          "contactAddress": [
                            {
                                "displayName": "name",
                                "isAddressVerified": false,
                                "addressLine1": "41 Navy Rd",
                                "city": "LLANDILO",
                                "state": "AU_NSW",
                                "postalCode": "2747",
                                "country": "AU",
                                "addressType": "business",
                                "publicId": "pc:14426",
                                "addressLine2": "41 Navy Rd",
                                "addressLine3": "41 Navy Rd",
                                "isAutofilled": true
                            }
                          ],
                          "publicId": "pc:2316"
                      }
                    ],
                    "productName": "Workers' Insurance",
                    "previousEndDate": "2019-09-30T14:01:00Z",
                    "periodStartDate": policyStartDate,
                    "periodEndDate": policyEndDate,
                    "preQualQuestionSets": [
                      {
                          "code": "WCNIPreQual_icare",
                          "answers": {
                              "annualGrossWages": false,
                              "employeeApprenticeTrainee": true,
                              "employeePerCapita": false,
                              "employeeNSW": true,
                              "groupOfCompanies": false
                          }
                      }
                    ],
                    "lobs": {
                        "wcLine": {
                            "policyPeriod": {
                                "policyType": "Standard",
                                "termNumber": 0,
                                "brokerOrganisation": brokerOrg,
                                "termType": "Annual",
                                "writtenDate": "2018-09-11T14:00:00Z",
                                "rateAsOfDate": "2018-09-12T03:36:46Z",
                                "gstRegistration": false,
                                "itcEntitlement": "0",
                                "schemeAgent": "icare-422",
                                "reasonLowWages": "none",
                                "policy": {
                                    "primaryLanguage": "[en_US,en_US_edg]",
                                    "priorPolicy": [
                                      {
                                          "wcCarrier": "icare",
                                          "policyNumber": null,
                                          "effectiveDate": "2018-09-11T14:00:00Z",
                                          "policy": "ewrw",
                                          "carrier": "we",
                                          "expirationDate": "2018-09-14T14:00:00Z",
                                          "annualPremiumAmount": 123442,
                                          "numLosses": 3,
                                          "totalLossesAmount": 45,
                                          "comments": "ewr",
                                          "wagesAmount": 234324,
                                          "numberOfWICs": 4
                                      }
                                    ],
                                    "lossHistoryEntry": [
                                      {
                                          "locationName": "adsa",
                                          "occurrenceDate": "2018-09-11T14:00:00Z",
                                          "description": "awe",
                                          "costCenterNo": "123",
                                          "directWageId": {
                                              "wicRate": 4.432,
                                              "wicDescription": wicDesc,
                                              "wicCode": "732100 - Banks",
                                              "perCapitaFlag": false,
                                              "pacDescription": businessActivity,
                                              "pacCode": businessActivityCode,
                                              "ddlContribution": 0.028,
                                              "code": wicCode
                                          },
                                          "claimYear3": 333344,
                                          "claimYear2": 1233432,
                                          "claimYear1": 12324,
                                          "basicTariffPremiumYear3": 21234,
                                          "basicTariffPremiumYear2": 4354,
                                          "basicTariffPremiumYear1": 55656,
                                          "wages1": 3452,
                                          "wages2": 3452,
                                          "wages3": 3452,
                                          "totalIncurredAmount": 23,
                                          "amountPaid": 20,
                                          "amountReserved": 3,
                                          "lossStatus": "Open"
                                      }
                                    ]
                                },
                                "policyTerm": {
                                    "affinityGroupName": "avb",
                                    "affinitygroupType": "Open",
                                    "startDate": "2018-09-11T14:00:00Z",
                                    "endDate": "2018-10-11T14:00:00Z",
                                    "organization": "werew"
                                },
                                "jobRoleAssignment": [
                                  {
                                      "assignedUser": "integportal user",
                                      "assignedGroup": "Icare Grp",
                                      "jobUserRoleAssignment": "100000000003538"
                                  },
                                  {
                                      "assignedUser": "Super User",
                                      "assignedGroup": "Icare Grp",
                                      "jobUserRoleAssignment": "100000000003538"
                                  }
                                ],
                                "policyLocation": [
                                  {
                                      "isRatedLocation": true,
                                      "locationRetired": false,
                                      "primaryLocation": true,
                                      "accountLocation": {
                                          "nonSpecific": true,
                                          "address": policyAddress,
                                          "locationCode": "we",
                                          "locationName": "sfs"
                                      },
                                      "costCenters": [
                                        {
                                            "fullName": "PolicyLocation : 1",
                                            "directWages": [
                                              {
                                                  "wic": 0,
                                                  "pac": 0,
                                                  "grossValue": grossValue,
                                                  "numberOfUnits": 0,
                                                  "totalWages": grossValue,
                                                  "numberOfEmployees": totalEmployees,
                                                  "wagesAmount": 0,
                                                  "businessDescription": businessActivity,
                                                  "asbestoses": [],
                                                  "taxiPlates": [],
                                                  "contactorWages": [],
                                                  "directWageId": {
                                                      "pacCode": businessActivityCode,
                                                      "pacDescription": businessActivity,
                                                      "wicRate": 4.432,
                                                      "ddlContribution": 0.028,
                                                      "code": wicCode,
                                                      "wicCode": "0",
                                                      "perCapitaFlag": false,
                                                      "wicDescription": wicDesc
                                                  },
                                                  "numberOfApprentices": null,
                                                  "apprenticesWages": null,
                                                  "claimAmountYear1": 2323,
                                                  "claimAmountYear2": 1000.99,
                                                  "claimAmountYear3": 1000.99,
                                                  "basicTariffPremiumAmountYear1": 1000.99,
                                                  "basicTariffPremiumAmountYear2": 1000.99,
                                                  "basicTariffPremiumAmountYear3": 1000.99
                                              }
                                            ],
                                            "publicId": "pc:7614",
                                            "name": "deer",
                                            "centerNumber": "wqe2"
                                        }
                                      ],
                                      "publicId": "pc:4314"
                                  }
                                ],
                                "brokerName": brokerOrg,
                                "wcLabourHireCode": "1",
                                "brokerId": "icarewc",
                                "publicId": "pc:4890",
                                "legacyPolicyNumber": null,
                                "offerings": "were",
                                "siFund": "[sifund]",
                                "isLegacy": false,
                                "schemeAgentContact": "cont",
                                "groupNumber": "2341212",
                                "groupName": "",
                                "claim": [
                                  {
                                      "policyInForce": false,
                                      "lossDate": "2018-09-19T14:01:00.000Z",
                                      "claimNumber": "123",
                                      "status": "awde",
                                      "totalIncurred": 0
                                  }
                                ],
                                "producerCodeOrg": "wdw"
                            }
                        }
                    },
                    "termType": "Annual",
                    "accountNumber": "1233"
                },
                "quoteData": {
                    "offeredQuotes": [
                      {
                          "branchCode": "WCNI_icare",
                          "isCustom": true,
                          "annualPremium": {
                              "amount": 3659.79,
                              "amountCurrency": "[aud]"
                          },
                          "termMonths": 11,
                          "taxes": {
                              "amount": 350.22,
                              "amountCurrency": "[aud]"
                          },
                          "lobs": {
                              "wcLine": {
                                  "wageDetails": {
                                      "wagePremium": [
                                        {
                                            "wic": "041200",
                                            "rate": 4.28,
                                            "totalAnnualWages": {
                                                "amount": 99726.03,
                                                "amountCurrency": "[aud]"
                                            },
                                            "averagePerformancePremium": {
                                                "amount": 4268.27,
                                                "amountCurrency": "[aud]"
                                            }
                                        }
                                      ],
                                      "totalAveragePerformancePremium": {
                                          "amount": 4268.27,
                                          "amountCurrency": "[aud]"
                                      }
                                  },
                                  "esiPremium": {
                                      "esiRate": 10,
                                      "averagePerformancePremium": {
                                          "amount": 4268.27,
                                          "amountCurrency": "[aud]"
                                      },
                                      "esi": {
                                          "amount": -426.83,
                                          "amountCurrency": "[aud]"
                                      }
                                  },
                                  "claimPerformance": {
                                      "claimPerformanceMeasure": 0,
                                      "schemePerformanceMeasure": 0,
                                      "claimPerformanceRate": 0,
                                      "claimPerformanceAdjustment": 0,
                                      "totalPriorApp": {
                                          "amount": 4268.27,
                                          "amountCurrency": "[aud]"
                                      },
                                      "totalPriorLoss": {
                                          "amount": 4268.27,
                                          "amountCurrency": "[aud]"
                                      },
                                      "premiumRiskAssessment": {
                                          "amount": 4268.27,
                                          "amountCurrency": "[aud]"
                                      },
                                      "cprAdjustment": {
                                          "amount": 4268.27,
                                          "amountCurrency": "[aud]"
                                      }
                                  },
                                  "averagePerformancePremium": {
                                      "amount": 4268.27,
                                      "amountCurrency": "[aud]"
                                  },
                                  "premium": {
                                      "amount": 3852.41,
                                      "amountCurrency": "[aud]"
                                  },
                                  "dustDiseaseLavy": {
                                      "amount": 10.97,
                                      "amountCurrency": "[aud]"
                                  },
                                  "asbestosCost": {
                                      "amount": 0,
                                      "amountCurrency": "[aud]"
                                  },
                                  "apprenticeDiscount": {
                                      "amount": 0,
                                      "amountCurrency": "[aud]"
                                  },
                                  "performanceDiscount": {
                                      "amount": 0,
                                      "amountCurrency": "[aud]"
                                  },
                                  "mineSafetyCost": {
                                      "amount": 0,
                                      "amountCurrency": "[aud]"
                                  },
                                  "annualisedApp": {
                                      "amount": 4280,
                                      "amountCurrency": "[aud]"
                                  },
                                  "totalDdl": {
                                      "amount": 10.97,
                                      "amountCurrency": "[aud]"
                                  },
                                  "itcAmount": {
                                      "amount": 0,
                                      "amountCurrency": "[aud]"
                                  }
                              }
                          },
                          "publicId": "pc:4890",
                          "branchName": "branch123",
                          "previousAnnualPremium": {
                              "amount": 3852.41,
                              "amountCurrency": "[aud]"
                          },
                          "transactionCost": {
                              "amount": 3852.41,
                              "amountCurrency": "[aud]"
                          }
                      }
                    ]
                },
                "bindData": {
                    "paymentReference": "258251",
                    "fullPayDiscountPercentage": "",
                    "paymentPlans": [
                      {
                          "name": "icare NI Payment Plan_Yearly",
                          "downPayment": {
                              "amount": 3659.79,
                              "amountCurrency": "[aud]"
                          },
                          "total": {
                              "amount": 3659.79,
                              "amountCurrency": "[aud]"
                          },
                          "installment": {
                              "amount": 0,
                              "amountCurrency": "[aud]"
                          },
                          "billingId": "icare_bcpp:1"
                      },
                      {
                          "name": "icare NI Payment Plan_Quarterly",
                          "downPayment": {
                              "amount": 963.11,
                              "amountCurrency": "[aud]"
                          },
                          "total": {
                              "amount": 3852.41,
                              "amountCurrency": "[aud]"
                          },
                          "installment": {
                              "amount": 963.1,
                              "amountCurrency": "[aud]"
                          },
                          "billingId": "icare_bcpp:3"
                      }
                    ],
                    "bPayReference": "5123",
                    "accountNumber": "12324",
                    "chosenQuote": "pc:5403",
                    "policyNumber": null,
                    "registrationCode": "12334",
                    "isUwIssueAvailable": false,
                    "billingAddress": policyAddress,
                    "paymentDetails": {
                        "paymentMethod": "card",
                        "bankAccountData": {
                            "bankAbaNumber": "12323434",
                            "bankAccountNumber": "124345435",
                            "bankAccountType": "dss",
                            "bankName": "asdad"
                        },
                        "creditCardData": {
                            "creditCardExpDate": "2018-09-11T14:00:00Z",
                            "creditCardIssuer": "dsa",
                            "creditCardNumber": "12323434"
                        }
                    },
                    "selectedPaymentPlan": "icare_bcpp:1"
                }
            }
        }
    };

    return responseObject;
}

/* getAddresses */
exports.getAddresses = function (req) {
    var responseObject = {
        "data": {
            "type": "Address",
            "id": "762AUS-yOAUSHALiBwAAAAAIAgEAAAAAOxt9wAAAAAAAAAAA..9kAAAAAP....8AAAAAAAAAAAAAAAAAAAAia2VudCIAQL6",
            "attributes": {
                "addressLine1": "Kentucky",
                "addressLine2": "8 Boxwood Park Rd",
                "addressLine3": null,
                "postalDeliveryNumberValue": "82379069",
                "flatOrUnitNumber": null,
                "floorOrLevelNumber": null,
                "houseNumber": null,
                "buildingOrPropertyName": "Kentucky",
                "buildingNumber": "8",
                "lotNumber": null,
                "streetName": "Boxwood Park Rd",
                "suburbOrPlaceOrLocality": "BUNGOWANNAH",
                "stateOrTerritory": "New South Wales",
                "stateOrTerritoryCode": "NSW",
                "postCode": "2640",
                "country": "AUSTRALIA",
                "countryISO": "AUS",
                "location": null
            }
        }
    };
    return responseObject;
}

/* getAddressesSearch */
exports.getAddressesSearch = function (req) {
    var responseObject = {
        "meta": {
            "totalRecords": 2,
            "totalPages": 1
        },
        "data": [
          {
              "type": "Address",
              "id": "762AUS-pOAUSHALiBwAAAAAIAgEAAAAAFkXeAAAAAAAAAAAA..9kAAAAAP....8AAAAAAAAAAAAAAAAAAAAia2VudCIAQL6",
              "attributes": {
                  "addressLine": "Kentucky, 8 Boxwood Park Road, BUNGOWANNAH  NSW 2640"
              },
              "links": {
                  "self": "https://sit2.partner-nonprod.icare.nsw.gov.au/v1/customer/addresses?id=762AUS-pOAUSHALiBwAAAAAIAgEAAAAAFkXeAAAAAAAAAAAA..9kAAAAAP....8AAAAAAAAAAAAAAAAAAAAia2VudCIAQL6"
              }
          },
          {
              "type": "Address",
              "id": "762AUS-yOAUSHALiBwAAAAAIAgEAAAAAOxt9wAAAAAAAAAAA..9kAAAAAP....8AAAAAAAAAAAAAAAAAAAAia2VudCIAQL6",
              "attributes": {
                  "addressLine": "Kenthurst Public School, 111 Kenthurst Road, KENTHURST  NSW 2156"
              },
              "links": {
                  "self": "https://sit2.partner-nonprod.icare.nsw.gov.au/v1/customer/addresses?id=762AUS-yOAUSHALiBwAAAAAIAgEAAAAAOxt9wAAAAAAAAAAA..9kAAAAAP....8AAAAAAAAAAAAAAAAAAAAia2VudCIAQL6"
              }
          }
        ]
    };
    return responseObject;
}

/* issueChangePolicy */
exports.issueChangePolicy = function (req) {
    var responseObject = {
        "data": {
            "type": "UpdatePolicy",
            "id": req.params.quoteId,
            "attributes": {
                "accountId": "1104035",
                "quoteId": "100000000003504",
                "status": "Bound",
                "bindData": {
                    "paymentReference": "258251",
                    "fullPayDiscountPercentage": "20",
                    "paymentPlans": [
                      {
                          "name": "icare NI Payment Plan_Yearly",
                          "downPayment": {
                              "amount": 3659.79,
                              "amountCurrency": "[aud]"
                          },
                          "total": {
                              "amount": 3659.79,
                              "amountCurrency": "[aud]"
                          },
                          "installment": {
                              "amount": 0,
                              "amountCurrency": "[aud]"
                          },
                          "billingId": "icare_bcpp:1"
                      },
                      {
                          "name": "icare NI Payment Plan_Quarterly",
                          "downPayment": {
                              "amount": 963.11,
                              "amountCurrency": "[aud]"
                          },
                          "total": {
                              "amount": 3852.41,
                              "amountCurrency": "[aud]"
                          },
                          "installment": {
                              "amount": 963.1,
                              "amountCurrency": "[aud]"
                          },
                          "billingId": "icare_bcpp:3"
                      }
                    ],
                    "bPayReference": "110403501171",
                    "accountNumber": "12324",
                    "chosenQuote": "pc:5403",
                    "registrationCode": "12334",
                    "isUwIssueAvailable": false,
                    "billingAddress": {
                        "publicId": "123",
                        "displayName": "ade",
                        "isAddressVerified": false,
                        "addressLine1": "41 Navy Rd",
                        "addressLine2": "41 Navy Rd",
                        "addressLine3": "41 Navy Rd",
                        "city": "LLANDILO",
                        "state": "[AU_NSW,AU_JBT,AU_ACT,AU_NT,AU_QLD,AU_SA,AU_TAS,AU_VIC,AU_WA]",
                        "postalCode": "2747",
                        "country": "[AU]",
                        "addressType": "[business,home,billing,postal,headoffice,other]",
                        "isAutofilled": true
                    },
                    "paymentDetails": {
                        "paymentMethod": "card",
                        "bankAccountData": {
                            "bankAbaNumber": "12323434",
                            "bankAccountNumber": "124345435",
                            "bankAccountType": "dss",
                            "bankName": "asdad"
                        },
                        "creditCardData": {
                            "creditCardExpDate": "2018-09-11T14:00:00Z",
                            "creditCardIssuer": "dsa",
                            "creditCardNumber": "12323434"
                        }
                    },
                    "selectedPaymentPlan": "icare NI Payment Plan_Quarterly"
                }
            }
        }
    };

    return responseObject;
}

/* policyBrokeragesPost */
exports.policyBrokeragesPost = function (req) {

    var groupId = req.body.data.attributes.brokerUserGroups.groupId;
    state.MockDataBrokeragePost = {
        "data": {
            "type": "BrokerageList",
            "attributes": [
              {
                  "accessLevel": "admin",
                  "groupId": "10561",
                  "groupName": "International Brokers root",
                  "parentGroupId": "11769"
              },
              {
                  "accessLevel": "admin",
                  "groupId": "10000",
                  "groupName": "xxxxxxxxxxxxx admin",
                  "parentGroupId": "10000"
              },
              {
                  "accessLevel": "general",
                  "groupId": "10001",
                  "groupName": "xxxxxxxxxxxxx general",
                  "parentGroupId": "10000"
              },
              {
                  "accessLevel": "general",
                  "groupId": "11771",
                  "groupName": "WILLIS AUSTRALIA LIMITED",
                  "parentGroupId": "10561"
              },
              {
                  "accessLevel": "admin",
                  "groupId": "newId",
                  "groupName": "New Group Added",
                  "parentGroupId": "newId"
              }
            ]
        }
    };
    state.MockDataBrokerageFresh = false;

    var responseObject = {
        "data": {
            "type": "Brokerage",
            "attributes": {
                "accessLevel": "admin",
                "groupId": "12430",
                "groupName": ""
            }
        }
    };

    return responseObject;
}

/* policyBrokeragesPost */
exports.policyBrokeragesPost500 = function () {

    var responseObject = {
        "errors": [
          {
              "status": "500",
              "code": "UNEXPECTED_ERROR",
              "title": "An Unexpected Error Has Occurred",
              "detail": "An unexpected error has occured for which no further information is available"
          }
        ]
    };

    return responseObject;
}

/* policyBrokeragesPost */
exports.policyBrokeragesPost400 = function () {

    var responseObject = {
        "errors": [
          {
              "status": "400",
              "code": "BAD_REQUEST",
              "title": "Bad request",
              "detail": "The request provided was not valid"
          }
        ]
    };

    return responseObject;
}


/* policyBrokeragesGet */
exports.policyBrokeragesGet = function (req) {

    var responseObject = state.MockDataBrokerages;

    if (!state.MockDataBrokerageFresh) {
        responseObject = state.MockDataBrokeragePost;
        state.MockDataBrokerageFresh = true;
    }

    return responseObject;
}

/* policyBrokeragesGetEmpty */
exports.policyBrokeragesGetEmpty = function (req) {
    var responseObject = {
        "data": {
            "type": "BrokerageList",
            "attributes": []
        }
    };

    return responseObject;
}

/* getInvoiceHistory */
exports.getInvoiceHistory = function (req) {
    var policyId = req.params.policyId;

    var invoiceDetails = state.MockDataInvoices;

    // No invoice scenario
    if (policyId === '140257201' || policyId === '123456700') {
        invoiceDetails = [];
    }

    var responseObject = {
        "data": {
            "type": "InvoiceHistory",
            "id": "93oac6984fba1a",
            "attributes": {
                "invoiceDetails": invoiceDetails
            }
        }
    };

    return responseObject;
}

/* retrievePaymentDetails */
exports.retrievePaymentDetails = function (req) {
    var policyId = req.params.policyId;

    // Outstanding Amount View - Direct debit set up (credit card details)
    var attributes = {
        "selectedPaymentPlan": "icare_bcpp:2",   // 11
        "paymentPlan": [
            {
                "name": "icare NI Payment Plan_Yearly",
                "downPayment": {
                    "amount": 3007,
                    "amountCurrency": "aud"
                },
                "total": {
                    "amount": 3007,
                    "amountCurrency": "aud"
                },
                "installment": {
                    "amount": 0,
                    "amountCurrency": "aud"
                }
            },
            {
                "name": "icare NI Payment Plan_Quarterly",
                "downPayment": {
                    "amount": 751.75,
                    "amountCurrency": "aud"
                },
                "total": {
                    "amount": 3007,
                    "amountCurrency": "aud"
                },
                "installment": {
                    "amount": 751.75,
                    "amountCurrency": "aud"
                }
            },
            {
                "name": "icare NI Payment Plan_Monthly",
                "downPayment": {
                    "amount": 250.583,
                    "amountCurrency": "aud"
                },
                "total": {
                    "amount": 3007,
                    "amountCurrency": "aud"
                },
                "installment": {
                    "amount": 250.583,
                    "amountCurrency": "aud"
                }
            }
        ],
        "paymentHistoryDetails": [
          {
              "paymentReceivedDate": "2017-12-15T13:00:00Z",    // 16B
              "paymentMethod": "Credit card",   // 16F
              "amount": {
                  "amount": 31812.32, // 16D
                  "amountCurrency": "[aud]"
              }
          }
        ],
        "outstandingAmount": {
            "amount": 7953.08, // 4
            "amountCurrency": "[aud]"
        },
        "bPayReference": "23456",   // 24
        "billerCode": "43566",  // 24
        "directDebitDetails": { // If null, display "Direct debit not setup"
            "bankDetails": {    // 12
                "branchName": "Martin Place",
                "accountName": "Name",
                "bankAccountNumber": "1234567", // 12B
                "bsb": "023454",    // 12A
                "bankName": "ANZ"
            },
            "creditCardDetails": {  // If in GW response result.directDebitDetails.creditCardDetails is not null, UI to display card number and expiry date.
                "cardNumber": "12345677",   // 12
                "cardHolderName": "Name",
                "creditCardType": "Visa", // [Visa, Mastercard]
                "expiryMonth": "March", // 12
                "expiryYear": 2020  // 12
            }
        },
        "disbursementDetails": {
            "bankDetails": {
                "bsb": "023456",
                "bankName": "ANZ",
                "branchName": "Martin Place",
                "accountName": "Name",
                "bankAccountNumber": "123456"
            }
        }
    };

    // Outstanding amount view - No direct debit - Quarterly
    if (policyId === '140272601' || policyId === '123464') {
        attributes.directDebitDetails = null;
        attributes.selectedPaymentPlan = "icare_bcpp:3";
        attributes.paymentPlan = [
            {
                "name": "icare NI Payment Plan_Quarterly",
                "downPayment": {
                    "amount": 751.75,
                    "amountCurrency": "aud"
                },
                "total": {
                    "amount": 3007,
                    "amountCurrency": "aud"
                },
                "installment": {
                    "amount": 751.75,
                    "amountCurrency": "aud"
                }
            },
            {
                "name": "icare NI Payment Plan_Monthly",
                "downPayment": {
                    "amount": 250.583,
                    "amountCurrency": "aud"
                },
                "total": {
                    "amount": 3007,
                    "amountCurrency": "aud"
                },
                "installment": {
                    "amount": 250.583,
                    "amountCurrency": "aud"
                }
            }
        ];
    }

    // NO Outstanding Amount View - Direct debit set up (credit card details) - Yearly
    if (policyId === '140260601' || policyId === '123463') {
        attributes.outstandingAmount = null;
        attributes.selectedPaymentPlan = "icare_bcpp:1";
    }


    // NO Outstanding Amount View - No invoice - Quarterly
    if (policyId === '123456703') {
        attributes.outstandingAmount = null;
        attributes.selectedPaymentPlan = "icare_bcpp:2";
        attributes.paymentPlan = [
            {
                "name": "icare NI Payment Plan_Monthly",
                "downPayment": {
                    "amount": 751.75,
                    "amountCurrency": "aud"
                },
                "total": {
                    "amount": 3007,
                    "amountCurrency": "aud"
                },
                "installment": {
                    "amount": 751.75,
                    "amountCurrency": "aud"
                }
            }
        ];
    }

    // NO Outstanding Amount View - No invoice - Quarterly - 2 Recent payments
    if (policyId === '140257201' || policyId === '123456700') {
        attributes.outstandingAmount = null;
        attributes.selectedPaymentPlan = "icare_bcpp:3";
        attributes.paymentPlan = [
            {
                "name": "icare NI Payment Plan_Quarterly",
                "downPayment": {
                    "amount": 751.75,
                    "amountCurrency": "aud"
                },
                "total": {
                    "amount": 3007,
                    "amountCurrency": "aud"
                },
                "installment": {
                    "amount": 751.75,
                    "amountCurrency": "aud"
                }
            }
        ];
        attributes.paymentHistoryDetails.push({
            "paymentReceivedDate": "2018-08-14T13:00:00Z",
            "paymentMethod": "BPAY",
            "amount": {
                "amount": 1750,
                "amountCurrency": "[aud]"
            }
        });
    }

    // Outstanding amount view - Direct debit set up (bank details) - Quarterly - Same bank account for receiving funds
    if (policyId === '140257501' || policyId === '123456701') {
        attributes.selectedPaymentPlan = "icare_bcpp:3";
        attributes.paymentPlan = [
            {
                "name": "icare NI Payment Plan_Quarterly",
                "downPayment": {
                    "amount": 751.75,
                    "amountCurrency": "aud"
                },
                "total": {
                    "amount": 3007,
                    "amountCurrency": "aud"
                },
                "installment": {
                    "amount": 751.75,
                    "amountCurrency": "aud"
                }
            },
            {
                "name": "icare NI Payment Plan_Monthly",
                "downPayment": {
                    "amount": 250.583,
                    "amountCurrency": "aud"
                },
                "total": {
                    "amount": 3007,
                    "amountCurrency": "aud"
                },
                "installment": {
                    "amount": 250.583,
                    "amountCurrency": "aud"
                }
            }
        ];
        attributes.directDebitDetails = {
            "bankDetails": {
                "branchName": "Martin Place",
                "accountName": "Name",
                "bankAccountNumber": "1234567",
                "bsb": "023454",
                "bankName": "ANZ"
            }
        };
        attributes.disbursementDetails.bankDetails = {    // 12
            "branchName": "Martin Place",
            "accountName": "Name",
            "bankAccountNumber": "1234567", // 12B
            "bsb": "023454",    // 12A
            "bankName": "ANZ"
        };
    }

    // Payment frequency == Annual
    if (policyId === '140257701' || policyId === '123456702') {
        attributes.selectedPaymentPlan = "Annual";
    }

    var responseObject = {
        "data": {
            "type": "PaymentDetails",
            "id": "93oac6984fba1a",
            "attributes": attributes
        }
    };

    return responseObject;
}

/* get Brokarege Users */
exports.policyUsersBrokeragesGet = function (req) {

    if (!state.MockDataBrokerageUserFresh) {
        var responseObject = state.MockDataBrokeragesUserAdded;
        state.MockDataBrokerageUserFresh = true;
        return responseObject;
    }

    if (state.MockDataBrokerageRemove) {

        var responseObject = {
            "meta": {
                "totalRecords": 21,
                "totalPages": 2,
                "currentPage": 1
            },
            "data": {
                "type": "BrokerageUsersList",
                "attributes": [
                  {
                      "identityId": "asdaasdsaduh5npmqehnhjwPv123",
                      "accessLevel": "admin",
                      "contactInfo": {
                          "contactId": "jblog2",
                          "firstName": "Jane",
                          "lastName": "Blog",
                          "email": "jane.blog@example.com",
                          "phone": {
                              "contactNumber": "0412345512",
                              "phoneType": "Mobile"
                          }
                      }
                  }
                ]
            }
        };
        return responseObject;
    }
    else {
        var responseObject = {
            "meta": {
                "totalRecords": 21,
                "totalPages": 2,
                "currentPage": 1
            },
            "data": {
                "type": "BrokerageUsersList",
                "attributes": [
                  {
                      "identityId": "001aasdas0l75npmqeanhjwKl098",
                      "accessLevel": "admin",
                      "contactInfo": {
                          "contactId": "jblog",
                          "firstName": "Alan",
                          "lastName": "Hughes",
                          "email": "alanh@jvbrokers.com.au",
                          "phone": {
                              "contactNumber": "0412345592",
                              "phoneType": "Mobile"
                          }
                      }
                  },
                  {
                      "identityId": "asdaasdsaduh5npmqehnhjwPv123",
                      "accessLevel": "admin",
                      "contactInfo": {
                          "contactId": "jblog2",
                          "firstName": "Jane",
                          "lastName": "Tran",
                          "email": "janet@jvbrokers.com.au",
                          "phone": {
                              "contactNumber": "0412345512",
                              "phoneType": "Mobile"
                          }
                      }
                  }
                ]
            }
        };
        return responseObject;
    }

}

/* get Brokarege Users */
exports.policyUsersBrokeragesGetCondition = function (req, condition) {

    switch (condition) {
        case "0":
            var responseObject = {
                "data": {
                    "type": "BrokerageUsersList",
                    "attributes": []
                }
            };
            return responseObject;
            break;
        case "400":
            var responseObject = {
                "errors": [
                  {
                      "status": "400",
                      "code": "BAD_REQUEST",
                      "title": "Bad request",
                      "detail": "The request provided was not valid"
                  }
                ]
            };
            return responseObject;
            break;
        case "500":
            var responseObject = {
                "errors": [
                  {
                      "status": "500",
                      "code": "UNEXPECTED_ERROR",
                      "title": "An Unexpected Error Has Occurred",
                      "detail": "An unexpected error has occured for which no further information is available"
                  }
                ]
            };
            return responseObject;
            break;
        case "full":
            var responseObject = {
                "meta": {
                    "totalRecords": 21,
                    "totalPages": 2,
                    "currentPage": 1
                },
                "data": {
                    "type": "BrokerageUsersList",
                    "attributes": [
                      {
                          "identityId": "001aasdas0l75npmqeanhjwKl098",
                          "accessLevel": "admin",
                          "contactInfo": {
                              "contactId": "jblog",
                              "firstName": "Alan",
                              "lastName": "Hughes",
                              "email": "alanh@jvbrokers.com.au",
                              "phone": {
                                  "contactNumber": "0412345592",
                                  "phoneType": "Mobile"
                              }
                          }
                      },
                      {
                          "identityId": "asdaasdsaduh5npmqehnhjwPv123",
                          "accessLevel": "admin",
                          "contactInfo": {
                              "contactId": "jblog2",
                              "firstName": "Jane",
                              "lastName": "Tran",
                              "email": "janet@jvbrokers.com.au",
                              "phone": {
                                  "contactNumber": "0412345512",
                                  "phoneType": "Mobile"
                              }
                          }
                      },
                      {
                          "identityId": "id0000001",
                          "accessLevel": "admin",
                          "contactInfo": {
                              "contactId": "jblog2",
                              "firstName": "Jane",
                              "lastName": "Tran",
                              "email": "janet@jvbrokers.com.au",
                              "phone": {
                                  "contactNumber": "0412345512",
                                  "phoneType": "Mobile"
                              }
                          }
                      },
                      {
                          "identityId": "id0000002",
                          "accessLevel": "general",
                          "contactInfo": {
                              "contactId": "jblog3",
                              "firstName": "Jane",
                              "lastName": "Blog",
                              "email": "jane02@example.com",
                              "phone": {
                                  "contactNumber": "0412345512",
                                  "phoneType": "Mobile"
                              }
                          }
                      }
                    ]
                }
            };
            return responseObject;
            break;


        default:
    }

    var responseObject = {
        "data": {
            "type": "BrokerageUsersList",
            "attributes": []
        }
    };
    return responseObject;
}


/* post Brokarege Users */
exports.policyUsersBrokeragesPost = function (req) {

    var responseObject = {
        "data": {
            "type": "BrokerageUser",
            "attributes": {
                "userType": "BROKER",
                "contactInfo": {
                    "firstName": "John",
                    "lastName": "Smith",
                    "email": "john.smith@example.com",
                    "phone": {
                        "contactNumber": "0412391203",
                        "phoneType": "Mobile"
                    }
                },
                "brokerUserGroups": [
                  {
                      "groupName": "EXAMPLE INSURANCE LTD",
                      "groupId": "1020",
                      "accessLevel": "admin"
                  }
                ]
            }
        }
    };
    state.MockDataBrokerageUserFresh = false;
    return responseObject;
}

/* post Brokarege Users */
exports.policyUsersBrokeragesPatch = function (req) {

    //neeed to capture reques if remove - noaccess
    state.MockDataBrokerageRemove = false;
    //
    //var noAccessList = this.props.brokerUserStore.brokerageUser.brokerUserGroups.filter(d => d.accessLevel === 'noaccess');
    //var isRemoveAccess = noAccessList.find(d => d.groupId === '10561');
    //if (isRemoveAccess)
    //{
    //        state.MockDataBrokerageRemove = true;
    //}

    var responseObject = {
        "data": {
            "type": "BrokerageUser",
            "attributes": {
                "userType": "BROKER",
                "contactInfo": {
                    "firstName": "John",
                    "lastName": "Smith",
                    "email": "john.smith@example.com",
                    "phone": {
                        "contactNumber": "0412391203",
                        "phoneType": "Mobile"
                    }
                },
                "brokerUserGroups": [
                  {
                      "groupName": "EXAMPLE INSURANCE LTD",
                      "groupId": "1020",
                      "accessLevel": "admin"
                  },
                  {
                      "groupName": "ACME LTD",
                      "groupId": "1101",
                      "accessLevel": "general"
                  }
                ]
            }
        }
    };
    return responseObject;
}


exports.policyUsersBrokeragesGetUserID = function (req) {

    var responseObject = {
        "data": {
            "type": "UserBrokerages",
            "attributes": {
                "brokerUserGroups": [
                    {
                        "accessLevel": "admin",
                        "groupId": "10561",
                        "groupName": "JV Brokers (Sydney)",
                        "parentGroupId": "11769"
                    },
                    {
                        "accessLevel": "admin",
                        "groupId": "10000",
                        "groupName": "JV Brokers (Wollonggong)",
                        "parentGroupId": "10000"
                    },
                    {
                        "groupName": "Group Name test Full - mock data",
                        "groupId": "00001",
                        "accessLevel": "general",
                        "parentGroupId": "11769"
                    },
                    {
                        "groupName": "400 Group Name Test",
                        "groupId": "00002",
                        "accessLevel": "admin",
                        "parentGroupId": "11769"
                    }
                ],
                "contactInformation": {
                    "firstName": "Alan",
                    "lastName": "Hughes",
                    "mobilePhone": "0412345592",
                    "email": "alanh@jvbrokers.com.au",
                    "delegatedUserId": "00uieg2frjlWm6MFu0h7"
                }
            }
        }
    };
    return responseObject;
}

/* financialInstitutions */
exports.financialInstitutions = function (req) {
    var input = req.query["BSB"];

    var responseObject = {
        "data": {
            "type": "FinancialInstitution",
            "id": input,
            "attributes": {
                "BSB": input,
                "bankCode": "ANZ",
                "name": "ANZ Smart Choice",
                "facility": "Sydney",
                "address": {
                    "addressLine1": "115 Pitt Street",
                    "suburbOrPlaceOrLocality": "Sydney",
                    "stateOrTerritoryCode": "NSW",
                    "postCode": "2000"
                }
            }
        }
    };

    if ((input === "123-987") || (input === "123987") || (input === "062468")) {
        responseObject.data.attributes.bankCode = "CBA";
        responseObject.data.attributes.name = "Commonwealth Bank of Australia";
        responseObject.data.attributes.facility = "Lake Haven";
        responseObject.data.attributes.address.addressLine1 = "Shop 72, Lake Haven Shopping Centre";
        responseObject.data.attributes.address.suburbOrPlaceOrLocality = "Lake Haven";
        responseObject.data.attributes.address.stateOrTerritoryCode = "NSW";
        responseObject.data.attributes.address.postCode = "2263";
    }

    return responseObject;
}

/* saveAndQuoteRenewalSubmission */
exports.saveAndQuoteRenewalSubmission = function (req) {
    var xTokenID1 = helpers.returnXTokenID1Header(req);
    var policyStatus = "Renewing";
    var totalEmployees = 0;
    var grossValue = 0;
    var contractorWages = 0;
    var numberOfContractWorkers = 0;
    var policyStartDate = "2019-01-15T14:00:00Z";
    var policyEndDate = "2020-01-15T14:00:00Z";
    var abn = "26 129 448 871";
    var entityName = "Timber Supplies Pty Ltd";
    var brokerOrg = "AoN Parramatta";
    var wicCode = "453100";
    var wicDesc = "Timber Wholesaling";
    var businessActivity = "Plywood dealing";
    var businessActivityCode = "453100001";
    var policyAddress = {
        "displayName": "481-499 Pittwater Road, Brookvale NSW 2100",
        "isAddressVerified": true,
        "addressLine1": "481-499 Pittwater Road",
        "city": "Brookvale",
        "state": "AU_NSW",
        "postalCode": "2100",
        "country": "AU",
        "addressType": "business",
        "publicId": "pc:12018",
        "addressLine2": "",
        "addressLine3": "",
        "isAutofilled": true
    };
    if (!Array.isArray(xTokenID1) && xTokenID1 === "1xtestemployer2@gmail.com") {
        policyStatus = "Quoted";
        totalEmployees = 3;
        numberOfContractWorkers = 0;
        grossValue = 125000;
        contractorWages = 0;
        policyStartDate = "2019-01-12T14:00:00Z";
        policyEndDate = "2020-01-12T14:00:00Z";
        abn = "88 000 014 675";
        entityName = "Jim's Prawns";
        brokerOrg = "AoN Parramatta";
        wicCode = "041200";
        wicDesc = "Prawn Fishing";
        businessActivity = "Prawn Fishing";
        businessActivityCode = "041200001";
        policyAddress = {
            "displayName": "41 Navy Rd, LLANDILO NSW 2747",
            "isAddressVerified": true,
            "addressLine1": "41 Navy Rd",
            "city": "LLANDILO",
            "state": "AU_NSW",
            "postalCode": "2747",
            "country": "AU",
            "addressType": "business",
            "publicId": "pc:12018",
            "addressLine2": "",
            "addressLine3": "",
            "isAutofilled": true
        };
    }

    if (!Array.isArray(xTokenID1) && xTokenID1 === "1xtestbroker3@gmail.com") {
        policyStatus = "Quoted";
        totalEmployees = 0;
        numberOfContractWorkers = 0;
        grossValue = 0;
        contractorWages = 0;
    }

    var responseObject = {
        "data": {
            "type": "PolicyRenewal",
            "id": req.params.policyId,
            "attributes": {
                "status": policyStatus,
                "accountId": "1400932",
                "sendDocument": "true",
                "employerCategory": "Employer",
                "policyChangeReason": "cancellationadjsutment",
                "isSubmitAgent": false,
                "quoteId": "100000000003955",
                "hadPolicylast12Months": 'yes',
                "draftData": {
                    "account": {
                        "displayName": entityName,
                        "companyName": entityName,
                        "accountHolder": false,
                        "primaryPhoneType": "home",
                        "mobileNumber": "0426353063",
                        "emailAddress1": "abc@test.com",
                        "accountOrgType": "partnership",
                        "isVedaValidated": false,
                        "primaryLanguage": "en_US",
                        "gstRegistration": false,
                        "itcEntitlement": "0",
                        "commencementDate": "2018-09-11T14:00:00Z",
                        "abn": abn,
                        "publicId": "pc:4464",
                        "tempId": "1234",
                        "particle": "asd",
                        "workNumber": "12345566",
                        "homeNumber": "12345566",
                        "faxPhone": "0426353063",
                        "website": "abc.com",
                        "communicationPreference": "email",
                        "industryCode": "abc",
                        "acn": "",
                        "abnStatus": "ok",
                        "acnStatus": "ok",
                        "trusteeType": "partnership",
                        "trustABN": "ABN123",
                        "trustName": "name",
                        "trusteeName": "wewq",
                        "trustACN": "1221",
                        "trustABNStatus": "ok",
                        "tradingBusinessName": "",
                        "descriptionOfBusiness": "djdjd",
                        "cluster": "sde",
                        "newAccountReason": "New",
                        "serviceTier": "platinum",
                        "yearBusinessStarted": "2010",
                        "isAnonymousAccount": false,
                        "accountId": "1234"
                    },
                    "policyAddress": policyAddress,
                    "productCode": "WC_ICARE",
                    "offering": "WCNI_icare",
                    "accountContact": [
                      {
                          "contactRole": [
                            "LocationContact_icare"
                          ],
                          "contactUpdateFlag": false,
                          "contactRetired": false,
                          "personContact": {
                              "displayName": "John John",
                              "firstName": "John",
                              "lastName": "John",
                              "primaryPhoneType": "home",
                              "mobileNumber": "0426353063",
                              "communicationPreference": "email",
                              "emailAddress1": "xyz@test.com",
                              "smsNotifications": true,
                              "publicId": "pc:4889",
                              "particle": "abc",
                              "workNumber": "0211000",
                              "homeNumber": "0211000",
                              "faxPhone": "0211000",
                              "website": "abc.com",
                              "tempId": 12345
                          },
                          "contactAddress": [
                            {
                                "displayName": "name",
                                "isAddressVerified": false,
                                "addressLine1": "41 Navy Rd",
                                "city": "LLANDILO",
                                "state": "AU_NSW",
                                "postalCode": "2747",
                                "country": "AU",
                                "addressType": "business",
                                "publicId": "pc:14426",
                                "addressLine2": "41 Navy Rd",
                                "addressLine3": "41 Navy Rd",
                                "isAutofilled": true
                            }
                          ],
                          "publicId": "pc:2316"
                      }
                    ],
                    "productName": "Workers' Insurance",
                    "previousEndDate": "2019-09-30T14:01:00Z",
                    "periodStartDate": policyStartDate,
                    "periodEndDate": policyEndDate,
                    "preQualQuestionSets": [
                      {
                          "code": "WCNIPreQual_icare",
                          "answers": {
                              "annualGrossWages": false,
                              "employeeApprenticeTrainee": true,
                              "employeePerCapita": false,
                              "employeeNSW": true,
                              "groupOfCompanies": false
                          }
                      }
                    ],
                    "lobs": {
                        "wcLine": {
                            "policyPeriod": {
                                "policyType": "Standard",
                                "termNumber": 0,
                                "brokerOrganisation": brokerOrg,
                                "termType": "Annual",
                                "writtenDate": "2018-09-11T14:00:00Z",
                                "rateAsOfDate": "2018-09-12T03:36:46Z",
                                "gstRegistration": false,
                                "itcEntitlement": "0",
                                "schemeAgent": "icare-422",
                                "reasonLowWages": "none",
                                "policy": {
                                    "primaryLanguage": "[en_US,en_US_edg]",
                                    "priorPolicy": [
                                      {
                                          "wcCarrier": "icare",
                                          "policyNumber": null,
                                          "effectiveDate": "2018-09-11T14:00:00Z",
                                          "policy": "ewrw",
                                          "carrier": "we",
                                          "expirationDate": "2018-09-14T14:00:00Z",
                                          "annualPremiumAmount": 123442,
                                          "numLosses": 3,
                                          "totalLossesAmount": 45,
                                          "comments": "ewr",
                                          "wagesAmount": 234324,
                                          "numberOfWICs": 4
                                      }
                                    ],
                                    "lossHistoryEntry": [
                                      {
                                          "locationName": "adsa",
                                          "occurrenceDate": "2018-09-11T14:00:00Z",
                                          "description": "awe",
                                          "costCenterNo": "123",
                                          "directWageId": {
                                              "wicRate": 4.432,
                                              "wicDescription": wicDesc,
                                              "wicCode": "732100 - Banks",
                                              "perCapitaFlag": false,
                                              "pacDescription": businessActivity,
                                              "pacCode": businessActivityCode,
                                              "ddlContribution": 0.028,
                                              "code": wicCode
                                          },
                                          "claimYear3": 333344,
                                          "claimYear2": 1233432,
                                          "claimYear1": 12324,
                                          "basicTariffPremiumYear3": 21234,
                                          "basicTariffPremiumYear2": 4354,
                                          "basicTariffPremiumYear1": 55656,
                                          "wages1": 3452,
                                          "wages2": 3452,
                                          "wages3": 3452,
                                          "totalIncurredAmount": 23,
                                          "amountPaid": 20,
                                          "amountReserved": 3,
                                          "lossStatus": "Open"
                                      }
                                    ]
                                },
                                "policyTerm": {
                                    "affinityGroupName": "avb",
                                    "affinitygroupType": "Open",
                                    "startDate": "2018-09-11T14:00:00Z",
                                    "endDate": "2018-10-11T14:00:00Z",
                                    "organization": "werew"
                                },
                                "jobRoleAssignment": [
                                  {
                                      "assignedUser": "integportal user",
                                      "assignedGroup": "Icare Grp",
                                      "jobUserRoleAssignment": "100000000003538"
                                  },
                                  {
                                      "assignedUser": "Super User",
                                      "assignedGroup": "Icare Grp",
                                      "jobUserRoleAssignment": "100000000003538"
                                  }
                                ],
                                "policyLocation": [
                                  {
                                      "isRatedLocation": true,
                                      "locationRetired": false,
                                      "primaryLocation": true,
                                      "accountLocation": {
                                          "nonSpecific": true,
                                          "address": policyAddress,
                                          "locationCode": "we",
                                          "locationName": "sfs"
                                      },
                                      "costCenters": [
                                        {
                                            "fullName": "PolicyLocation : 1",
                                            "directWages": [
                                              {
                                                  "wic": 0,
                                                  "pac": 0,
                                                  "grossValue": grossValue,
                                                  "numberOfUnits": 0,
                                                  "totalWages": grossValue,
                                                  "numberOfEmployees": totalEmployees,
                                                  "wagesAmount": 0,
                                                  "businessDescription": businessActivity,
                                                  "asbestoses": [],
                                                  "taxiPlates": [],
                                                  "contactorWages": [],
                                                  "directWageId": {
                                                      "pacCode": businessActivityCode,
                                                      "pacDescription": businessActivity,
                                                      "wicRate": 4.432,
                                                      "ddlContribution": 0.028,
                                                      "code": wicCode,
                                                      "wicCode": "0",
                                                      "perCapitaFlag": false,
                                                      "wicDescription": wicDesc
                                                  },
                                                  "numberOfApprentices": null,
                                                  "apprenticesWages": null,
                                                  "claimAmountYear1": 2323,
                                                  "claimAmountYear2": 1000.99,
                                                  "claimAmountYear3": 1000.99,
                                                  "basicTariffPremiumAmountYear1": 1000.99,
                                                  "basicTariffPremiumAmountYear2": 1000.99,
                                                  "basicTariffPremiumAmountYear3": 1000.99
                                              }
                                            ],
                                            "publicId": "pc:7614",
                                            "name": "deer",
                                            "centerNumber": "wqe2"
                                        }
                                      ],
                                      "publicId": "pc:4314"
                                  }
                                ],
                                "brokerName": brokerOrg,
                                "wcLabourHireCode": "1",
                                "brokerId": "icarewc",
                                "publicId": "pc:4890",
                                "legacyPolicyNumber": null,
                                "offerings": "were",
                                "siFund": "[sifund]",
                                "isLegacy": false,
                                "schemeAgentContact": "cont",
                                "groupNumber": "",
                                "groupName": "",
                                "claim": [
                                  {
                                      "policyInForce": false,
                                      "lossDate": "2018-09-19T14:01:00.000Z",
                                      "claimNumber": "123",
                                      "status": "awde",
                                      "totalIncurred": 0
                                  }
                                ],
                                "producerCodeOrg": "wdw"
                            }
                        }
                    },
                    "termType": "Annual",
                    "accountNumber": "1233"
                },
                "quoteData": {
                    "offeredQuotes": [
                      {
                          "branchCode": "WCNI_icare",
                          "isCustom": true,
                          "annualPremium": {
                              "amount": 3659.79,
                              "amountCurrency": "[aud]"
                          },
                          "termMonths": 11,
                          "taxes": {
                              "amount": 350.22,
                              "amountCurrency": "[aud]"
                          },
                          "lobs": {
                              "wcLine": {
                                  "wageDetails": {
                                      "wagePremium": [
                                        {
                                            "wic": "041200",
                                            "rate": 4.28,
                                            "totalAnnualWages": {
                                                "amount": 99726.03,
                                                "amountCurrency": "[aud]"
                                            },
                                            "averagePerformancePremium": {
                                                "amount": 4268.27,
                                                "amountCurrency": "[aud]"
                                            }
                                        }
                                      ],
                                      "totalAveragePerformancePremium": {
                                          "amount": 4268.27,
                                          "amountCurrency": "[aud]"
                                      }
                                  },
                                  "esiPremium": {
                                      "esiRate": 10,
                                      "averagePerformancePremium": {
                                          "amount": 4268.27,
                                          "amountCurrency": "[aud]"
                                      },
                                      "esi": {
                                          "amount": -426.83,
                                          "amountCurrency": "[aud]"
                                      }
                                  },
                                  "claimPerformance": {
                                      "claimPerformanceMeasure": 0,
                                      "schemePerformanceMeasure": 0,
                                      "claimPerformanceRate": 0,
                                      "claimPerformanceAdjustment": 0,
                                      "totalPriorApp": {
                                          "amount": 4268.27,
                                          "amountCurrency": "[aud]"
                                      },
                                      "totalPriorLoss": {
                                          "amount": 4268.27,
                                          "amountCurrency": "[aud]"
                                      },
                                      "premiumRiskAssessment": {
                                          "amount": 4268.27,
                                          "amountCurrency": "[aud]"
                                      },
                                      "cprAdjustment": {
                                          "amount": 4268.27,
                                          "amountCurrency": "[aud]"
                                      }
                                  },
                                  "averagePerformancePremium": {
                                      "amount": 4268.27,
                                      "amountCurrency": "[aud]"
                                  },
                                  "premium": {
                                      "amount": 3852.41,
                                      "amountCurrency": "[aud]"
                                  },
                                  "dustDiseaseLavy": {
                                      "amount": 10.97,
                                      "amountCurrency": "[aud]"
                                  },
                                  "asbestosCost": {
                                      "amount": 0,
                                      "amountCurrency": "[aud]"
                                  },
                                  "apprenticeDiscount": {
                                      "amount": 0,
                                      "amountCurrency": "[aud]"
                                  },
                                  "performanceDiscount": {
                                      "amount": 0,
                                      "amountCurrency": "[aud]"
                                  },
                                  "mineSafetyCost": {
                                      "amount": 0,
                                      "amountCurrency": "[aud]"
                                  },
                                  "annualisedApp": {
                                      "amount": 4280,
                                      "amountCurrency": "[aud]"
                                  },
                                  "totalDdl": {
                                      "amount": 10.97,
                                      "amountCurrency": "[aud]"
                                  },
                                  "itcAmount": {
                                      "amount": 0,
                                      "amountCurrency": "[aud]"
                                  }
                              }
                          },
                          "publicId": "pc:4890",
                          "branchName": "branch123",
                          "previousAnnualPremium": {
                              "amount": 3852.41,
                              "amountCurrency": "[aud]"
                          },
                          "transactionCost": {
                              "amount": 3852.41,
                              "amountCurrency": "[aud]"
                          }
                      }
                    ]
                },
                "bindData": {
                    "paymentReference": "258251",
                    "fullPayDiscountPercentage": "",
                    "paymentPlans": [
                      {
                          "name": "icare NI Payment Plan_Yearly",
                          "downPayment": {
                              "amount": 3659.79,
                              "amountCurrency": "[aud]"
                          },
                          "total": {
                              "amount": 3659.79,
                              "amountCurrency": "[aud]"
                          },
                          "installment": {
                              "amount": 0,
                              "amountCurrency": "[aud]"
                          },
                          "billingId": "icare_bcpp:1"
                      },
                      {
                          "name": "icare NI Payment Plan_Quarterly",
                          "downPayment": {
                              "amount": 963.11,
                              "amountCurrency": "[aud]"
                          },
                          "total": {
                              "amount": 3852.41,
                              "amountCurrency": "[aud]"
                          },
                          "installment": {
                              "amount": 963.1,
                              "amountCurrency": "[aud]"
                          },
                          "billingId": "icare_bcpp:3"
                      }
                    ],
                    "bPayReference": "5123",
                    "accountNumber": "12324",
                    "chosenQuote": "pc:5403",
                    "policyNumber": null,
                    "registrationCode": "12334",
                    "isUwIssueAvailable": false,
                    "billingAddress": policyAddress,
                    "paymentDetails": {
                        "paymentMethod": "card",
                        "bankAccountData": {
                            "bankAbaNumber": "12323434",
                            "bankAccountNumber": "124345435",
                            "bankAccountType": "dss",
                            "bankName": "asdad"
                        },
                        "creditCardData": {
                            "creditCardExpDate": "2018-09-11T14:00:00Z",
                            "creditCardIssuer": "dsa",
                            "creditCardNumber": "12323434"
                        }
                    },
                    "selectedPaymentPlan": "icare_bcpp:3"
                }
            }
        }
    };

    return responseObject;
}

/* directDebit */
exports.directDebit = function (req) {
    var responseObject = {
        "data": req.body.data
    };

    responseObject.data.type = "DirectDebitResponse";

    return responseObject;
}


/* updatePaymentPlan */
exports.updatePaymentPlan = function (req) {
    var data = req.body.data;

    var responseObject = {
        "data": {
            "type": "PaymentPlan",
            "attributes": {
                "name": data.attributes.name,
                "amount": {
                    "amount": "data.attributes.amount.amount",
                    "amountCurrency": "[aud]",
                }
            }
        }
    };

    return responseObject;
}

/* password reset */
exports.policyUsersBrokeragesPasswordReset = function (req) {
    var responseObject = {
        "data": {
            "type": "BrokerageUserPasswordReset",
            "attributes": {
                "resetPasswordUrl": "https://icare-sandbox.oktapreview.com/reset_password/9Yb0iH-lVheIFFCbmihe"
            }
        }
    };

    return responseObject;
}
