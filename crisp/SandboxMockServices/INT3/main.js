﻿/** Constants **/
var authPath = "me";
var basePath = "/v1";
var basePathPortal = "/v2/portal";
var mockData = require("./mockData.js");
var validators = require("./validators.js");
var helpers = require("./helpers.js");

var v = require("./v1.js");
var MOCKDATA = mockData.mockDataStore();
var ERRORS = mockData.errors();


// track date of first operation 
state.dateOfFirstOp = state.dateOfFirstOp || new Date();

// Create an editable clone of the mock data that refreshes every 24 hours. To facilitate CRUD operations. 
state.MockDataQuickQuotesEditable = state.MockDataQuickQuotesEditable || _.clone(MOCKDATA.MockDataQuickQuotes);
state.MockDataFullQuotesEditable = state.MockDataFullQuotesEditable || _.clone(MOCKDATA.MockDataFullQuotes);
state.MockDataPoliciesEditable = state.MockDataPoliciesEditable || _.clone(MOCKDATA.MockDataPolicies);
state.MockDataQuoteSearchEditable = state.MockDataQuoteSearchEditable || _.clone(MOCKDATA.MockDataQuoteSearch);
if (helpers.hasOneDayElapsed(state.dateOfFirstOp)) {
    // reset to constant mock data 
    state.MockDataQuickQuotesEditable = _.clone(MOCKDATA.MockDataQuickQuotes);
    state.MockDataFullQuotesEditable = _.clone(MOCKDATA.MockDataFullQuotes);
    state.MockDataPoliciesEditable = _.clone(MOCKDATA.MockDataPolicies);
    state.MockDataQuoteSearchEditable = _.clone(MOCKDATA.MockDataQuoteSearch);

    // reset to current date 
    state.dateOfFirstOp = new Date();
}


/** Route Definitions **/
/*
 *	define(path, method, function)
 *
 */

/* Various */
Sandbox.define(basePathPortal + '/businesses', 'GET', v.businessNames);
Sandbox.define(basePath + '/referenceData/wics', 'GET', v.wicSearch);
Sandbox.define(basePathPortal + '/workersInsurance/policies/referenceData', 'GET', v.referenceData);

/* Quotes */
Sandbox.define(basePathPortal + '/workersInsurance/quotes', 'POST', v.quickQuote); // Create Quote
Sandbox.define(basePathPortal + '/workersInsurance/quotes/{quoteId}', 'PATCH', v.fullQuote);   // Update Quote
Sandbox.define(basePathPortal + '/workersInsurance/quotes/{quoteId}', 'GET', v.retrieveQuote); // Get Quote
Sandbox.define(basePathPortal + '/workersInsurance/quotes/{quoteId}/save', 'POST', v.saveQuote); // Save & Exit
Sandbox.define(basePathPortal + '/workersInsurance/me/quotes/{quoteId}', 'DELETE', v.deleteQuote) // Delete Quote (To be removed)
Sandbox.define(basePathPortal + '/workersInsurance/quotes/me/{quoteId}', 'DELETE', v.deleteQuote) // Delete Quote Updated
Sandbox.define(basePathPortal + '/workersInsurance/quotes/me', 'POST', v.quickAuthQuote); // Create Auth Quote
Sandbox.define(basePathPortal + '/workersInsurance/quotes/me/{quoteId}', 'PATCH', v.fullAuthQuote);   // Update Quote
Sandbox.define(basePathPortal + '/workersInsurance/quotes/me/{quoteId}/issueChangePolicy', 'POST', v.issueChangePolicy); // Rebind Policy
Sandbox.define(basePathPortal + '/workersInsurance/me/quotes/{quoteId}/save', 'POST', v.saveAuthQuote); // Save & Exit (Auth)

/* Policies */
Sandbox.define(basePathPortal + '/workersInsurance/policies/policy', 'POST', v.bindPolicy); // Bind Policy
Sandbox.define(basePathPortal + '/workersInsurance/policies/me/policy', 'POST', v.bindPolicy); // Bind Policy (Auth)
Sandbox.define(basePathPortal + '/workersInsurance/me/policies/{policyId}', 'GET', v.policyDetails); // Policy Details (To be removed)
Sandbox.define(basePathPortal + '/workersInsurance/me/policies/cancel', 'POST', v.cancelPolicy); // Cancel Policy (To be removed)
Sandbox.define(basePathPortal + '/workersInsurance/me/policies/{policyId}/history', 'GET', v.policyHistory); // Policy History (To be removed)
Sandbox.define(basePathPortal + '/workersInsurance/policies/me/{policyId}', 'GET', v.policyDetails); // Policy Details
Sandbox.define(basePathPortal + '/workersInsurance/policies/me/{policyId}', 'PATCH', v.policyPatch);   // patch policy
Sandbox.define(basePathPortal + '/workersInsurance/policies/me/cancel', 'POST', v.cancelPolicy); // Cancel Policy
Sandbox.define(basePathPortal + '/workersInsurance/policies/me/{policyId}/history', 'GET', v.policyHistory); // Policy History
Sandbox.define(basePathPortal + '/workersInsurance/policies/me/brokers', 'GET', v.brokersPolicy); // brokers Policy
Sandbox.define(basePathPortal + '/workersInsurance/policies/me/policy/{quoteId}', 'POST', v.rebindPolicyQuote); // Rebind Policy (To be removed??)
Sandbox.define(basePathPortal + '/workersInsurance/processes/users/forgotRegistrationCode', 'POST', v.forgotRegistration); // forgot Registration
Sandbox.define(basePathPortal + '/workersInsurance/policies/me/{policyId}/renewals', 'GET', v.policyRenewal); // GET: Policy Renewal
Sandbox.define(basePathPortal + '/workersInsurance/policies/me/{policyId}/renewals', 'POST', v.policySaveRenewal); // POST: Policy Renewal
Sandbox.define(basePathPortal + '/workersInsurance/me/policies/{policyId}/renewals/paymentPlans', 'PATCH', v.saveAndQuoteRenewalSubmission); // Save and Quote Renewal Submission

/* Brokerages */
Sandbox.define(basePathPortal + '/workersInsurance/me/brokerages', 'POST', v.policyBrokeragesPost); // policy brokerages post
Sandbox.define(basePathPortal + '/workersInsurance/me/brokerages', 'GET', v.policyBrokeragesGet); // policy brokerages get list
Sandbox.define(basePathPortal + '/workersInsurance/me/brokerages/{brokerageId}/users', 'GET', v.policyUsersBrokeragesGet); // policy brokerages post user
Sandbox.define(basePathPortal + '/workersInsurance/me/brokerages/users/search', 'POST', v.policySearchUsersBrokerages); // policy Search brokerages user
Sandbox.define(basePathPortal + '/workersInsurance/me/brokerages/{brokerageId}/users', 'POST', v.policyUsersBrokeragesPost); // policy brokerages post user

// -> /workersInsurance/me/users/{userId}/brokerages new path
Sandbox.define(basePathPortal + '/workersInsurance/me/users/{userId}/brokerages', 'GET', v.policyUsersBrokeragesGetUserID);
Sandbox.define(basePathPortal + '/workersInsurance/me/brokerages/{brokerageId}/users/{userId}', 'PATCH', v.policyUsersBrokeragesPatch);
Sandbox.define(basePathPortal + '/workersInsurance/me/brokerages/{brokerageId}/users/{userId}/resetPassword', 'POST', v.policyUsersBrokeragesResetPassword);


/* Brokerages */
Sandbox.define(basePathPortal + '/workersInsurance/policies/me/brokerages', 'POST', v.policyBrokeragesPost); // policy brokerages post
Sandbox.define(basePathPortal + '/workersInsurance/policies/me/brokerages', 'GET', v.policyBrokeragesGet); // policy brokerages get list
Sandbox.define(basePathPortal + '/workersInsurance/policies/me/brokerages/{brokerageId}/users', 'GET', v.policyUsersBrokeragesGet); // policy brokerages get users list


/* Dashboard */
Sandbox.define(basePathPortal + '/workersInsurance/me/quotes/summaries/search', 'POST', v.quoteSearch)  // Quote Search (To be removed)
Sandbox.define(basePathPortal + '/workersInsurance/quotes/me/summaries/search', 'POST', v.quoteSearch)  // Quote Search
Sandbox.define(basePathPortal + '/workersInsurance/me/policies/summaries/search', 'POST', v.policySearch); // Policy Search (To be removed)
Sandbox.define(basePathPortal + '/workersInsurance/policies/me/summaries/search', 'POST', v.policySearch); // Policy Search

/* Adress */
Sandbox.define(basePathPortal + '/workersInsurance/addresses', 'GET', v.getAddresses); // get Addresses
Sandbox.define(basePathPortal + '/workersInsurance/addresses/search', 'GET', v.getAddressesSearch); // get Addresses Search
Sandbox.define(basePathPortal + '/addresses', 'GET', v.getAddresses); // get Addresses
Sandbox.define(basePathPortal + '/addresses/search', 'GET', v.getAddressesSearch); // get Addresses Search

/* Identity */
Sandbox.define(basePathPortal + '/workersInsurance/processes/users/register', 'POST', v.registerUser); // Register User
Sandbox.define(basePathPortal + '/workersInsurance/me', 'GET', v.userDetails); // get users details
Sandbox.define(basePathPortal + '/workersInsurance/me', 'PATCH', v.patchUserDetails);   // patch users details

/* Documents */
Sandbox.define(basePathPortal + '/workersInsurance/policies/me/{policyId}/documents', 'GET', v.getDocuments); // Get Documents
Sandbox.define(basePathPortal + '/workersInsurance/policies/me/{policyId}/stagingLocations', 'POST', v.stagingLocations); // Document Staging Locations
Sandbox.define(basePathPortal + '/workersInsurance/policies/me/{policyId}/documents/archive', 'POST', v.archive); // Document Archive
Sandbox.define(basePathPortal + '/workersInsurance/policies/me/{policyId}/upload', 'PUT', v.upload); // Document Upload
Sandbox.define(basePathPortal + '/workersInsurance/policies/me/{policyId}/delete', 'DELETE', v.delete); // Document Delete
Sandbox.define(basePathPortal + '/workersInsurance/policies/me/{policyId}/download', 'GET', v.download); // Document Download
Sandbox.define(basePathPortal + '/workersInsurance/me/policies/{policyId}/payments/paymentPlans', 'PATCH', v.updatePaymentPlan); // Update Payment Plan

/* Payments */
Sandbox.define(basePathPortal + '/workersInsurance/policies/{policyId}/invoices', 'GET', v.getInvoiceHistory); // Get Invoice History
Sandbox.define(basePathPortal + '/workersInsurance/policies/{policyId}/payments', 'GET', v.retrievePaymentDetails); // Retrieve Payment Details
Sandbox.define(basePathPortal + '/workersInsurance/payments/financialInstitutions', 'GET', v.financialInstitutions); // Validate BSB
Sandbox.define(basePathPortal + '/workersInsurance/payments/directDebit', 'POST', v.directDebit); // Create and Update Direct Debit
Sandbox.define(basePathPortal + '/workersInsurance/payments/me/directDebit', 'POST', v.directDebit); // Create and Update Direct Debit (Auth)

/* test validators - helpers */
Sandbox.define(basePathPortal + '/workersInsurance/test/header', 'POST', v.testMockAuthorizationHeader); // test Mock
Sandbox.define(basePathPortal + '/workersInsurance/test/xtokenid1', 'POST', v.testMockXTokenID1); // test Mock
Sandbox.define(basePathPortal + '/workersInsurance/test/xtokenid2', 'POST', v.testMockXTokenID2); // test Mock


Sandbox.define('/services/quickweb/CommunityTokenRequestServlet', 'POST', function (req, res) {
    // Send the response body.
    return res.json(200, "token=sometokenvalue");
});
