﻿/** Validators **/

/* validRequestMediaTypeHeader */
exports.validRequestMediaTypeHeader = function (req) {
    return req.is('application/vnd.api+json');
};

/* validAuthorizationHeader*/
exports.validAuthorizationHeader = function (req) {
    var authorization = req.get("Authorization");
    authorization = authorization ? authorization : undefined;

    return (authorization !== undefined);
};

/* validXTokenID1Header */
exports.validXTokenID1Header = function (req) {
    var tokenID = req.get("X-Token-ID1");
    if (tokenID === undefined) return false;

    return tokenID;
};

/* validXTokenID2Header */
exports.validXTokenID2Header = function (req) {
    var tokenID = req.get("X-Token-ID2");
    if (tokenID === undefined) return false;

    return tokenID;
};

/* validQueryParam */
exports.validQueryParam = function (req, q) {
    var input = req.query[q] ? req.query[q] : undefined;
    if (input === undefined) return false;

    return true;
};

/* validParam */
exports.validParam = function (req, p, shouldTestForInt) {
    var params = req.params ? req.params : undefined;
    if (params === undefined) return false;

    var targetParam = params[p] ? params[p] : undefined;
    if (targetParam === undefined) return false;

    if (shouldTestForInt) {
        var intParam = parseInt(targetParam, 10);
        return _.isInteger(intParam);
    }

    return true;
};

exports.validPolicySearchRequest = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.data) return false;
    if (!input.data.attributes) return false;
    if (!input.data.attributes.params) return false;
    if (!input.data.attributes.params.orderBy) return false;
    if (!input.data.attributes.params.page) return false;
    if (!input.data.attributes.params.pageSize) return false;

    return true;
};

exports.validPatchUserDetailsRequest = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.data) return false;
    if (!input.data.attributes) return false;
    if (!input.data.type) return false;

    return true;
};

exports.validUpdatePaymentPlanRequest = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.data) return false;
    if (!input.data.attributes) return false;
    if (!input.data.attributes.name) return false;
    if (!input.data.attributes.amount.amount) return false;

    return true;
};