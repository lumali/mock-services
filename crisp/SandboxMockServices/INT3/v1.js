﻿/** Route Functions **/


/* Various */
exports.referenceData = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // request payload validation 
    // TODO: replace this with the actual helper function 
    //reqErrors = helpers.someSpecificPayloadValidation(req);
    //if (reqErrors.length) {
    //    return helpers.returnErrorResponse(res, reqErrors);
    //}

    // proceed with actual operation
    var responseObject = helpers.getReferenceData(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

exports.wicSearch = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // request payload validation 
    // TODO: replace this with the actual helper function 
    //reqErrors = helpers.someSpecificPayloadValidation(req);
    //if (reqErrors.length) {
    //    return helpers.returnErrorResponse(res, reqErrors);
    //}

    // proceed with actual operation
    var responseObject = helpers.wicSearch(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

exports.businessNames = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    if (!validators.validQueryParam(req, "abn") && !validators.validQueryParam(req, "acn")) {
        var errorArray = [];
        errorArray.push(ERRORS.E500);
        return helpers.returnErrorResponse(res, errorArray);
    }

    // proceed with actual operation
    // TODO: replace this with the actual helper function 
    var responseObject = helpers.businessNames(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};


/* Quotes */
exports.quickQuote = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // request payload validation 
    // TODO: replace this with the actual helper function 
    //reqErrors = helpers.someSpecificPayloadValidation(req);
    //if (reqErrors.length) {
    //    return helpers.returnErrorResponse(res, reqErrors);
    //}

    // proceed with actual operation
    var responseObject = helpers.quickQuote(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

exports.quickAuthQuote = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // request validation common to all authenticated requests 
    // test header - authorization
    //var test = helpers.returnAuthorizationHeader(req);
    //if (Array.isArray(test)) {
    //    if (test.length) {
    //        return helpers.returnErrorResponse(res, test);
    //    }
    //return res.json(500, 'unknown error');
    //}


    // proceed with actual operation
    var responseObject = helpers.quickQuote(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

exports.fullQuote = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // request payload validation 
    // TODO: replace this with the actual helper function 
    //reqErrors = helpers.someSpecificPayloadValidation(req);
    //if (reqErrors.length) {
    //    return helpers.returnErrorResponse(res, reqErrors);
    //}

    // proceed with actual operation
    var responseObject = helpers.fullQuote(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

exports.fullAuthQuote = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // request validation common to all authenticated requests 
    // test header - authorization
    var test = helpers.returnAuthorizationHeader(req);
    if (Array.isArray(test)) {
        if (test.length) {
            return helpers.returnErrorResponse(res, test);
        }
        return res.json(500, 'unknown error');
    }

    // proceed with actual operation
    var responseObject = helpers.fullQuote(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

exports.retrieveQuote = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // request payload validation 
    reqErrors = helpers.emailQueryParamValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // proceed with actual operation
    var responseObject = helpers.retrieveQuote(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

exports.saveQuote = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // request payload validation 
    // TODO: replace this with the actual helper function 
    //reqErrors = helpers.someSpecificPayloadValidation(req);
    //if (reqErrors.length) {
    //    return helpers.returnErrorResponse(res, reqErrors);
    //}

    // proceed with actual operation
    var responseObject = helpers.saveQuote(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

exports.saveAuthQuote = function (req, res) {
    // set content-type 
    res.type('application/json');

    var test = helpers.returnAuthorizationHeader(req);
    if (Array.isArray(test)) {
        if (test.length) {
            return helpers.returnErrorResponse(res, test);
        }
        return res.json(500, 'unknown error');
    }


    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // request payload validation 
    // TODO: replace this with the actual helper function 
    //reqErrors = helpers.someSpecificPayloadValidation(req);
    //if (reqErrors.length) {
    //    return helpers.returnErrorResponse(res, reqErrors);
    //}

    // proceed with actual operation
    var responseObject = helpers.saveAuthQuote(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

exports.deleteQuote = function (req, res) {
    // set content-type 
    res.type('application/vnd.api+json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // request validation common to all authenticated requests 
    // test header - authorization
    var test = helpers.returnAuthorizationHeader(req);
    if (Array.isArray(test)) {
        if (test.length) {
            return helpers.returnErrorResponse(res, test);
        }
        return res.json(500, 'unknown error');
    }

    // proceed with actual operation
    var responseObject = helpers.deleteQuote(req);

    // if invalid response object 
    if (!responseObject) {
        var errorArray = [];
        errorArray.push(ERRORS.E401);
        return helpers.returnErrorResponse(res, errorArray);
    }

    // Note: Ensure correct status code as defined in service contract 
    // Set the status code of the response.
    res.status(204);

    // Send the response body.
    res.json({
        "status": "ok"
    });
};

exports.issueChangePolicy = function (req, res) {
    // set content-type 
    res.type('application/json');


    var test = helpers.returnAuthorizationHeader(req);
    if (Array.isArray(test)) {
        if (test.length) {
            return helpers.returnErrorResponse(res, test);
        }
        return res.json(500, 'unknown error');
    }

    // proceed with actual operation
    var responseObject = helpers.issueChangePolicy(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
}


/* Policies */
exports.bindPolicy = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // request payload validation 
    // TODO: replace this with the actual helper function 
    //reqErrors = helpers.someSpecificPayloadValidation(req);
    //if (reqErrors.length) {
    //    return helpers.returnErrorResponse(res, reqErrors);
    //}

    // proceed with actual operation
    var responseObject = helpers.bindPolicy(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

exports.policyDetails = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // request validation common to all authenticated requests 
    // test header - authorization
    var test = helpers.returnAuthorizationHeader(req);
    if (Array.isArray(test)) {
        if (test.length) {
            return helpers.returnErrorResponse(res, test);
        }
        return res.json(500, 'unknown error');
    }

    if (!validators.validParam(req, "policyId")) {
        var errorArray = [];
        errorArray.push(ERRORS.E500);
        return helpers.returnErrorResponse(res, errorArray);
    }

    // proceed with actual operation
    var responseObject = helpers.policyDetails(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

exports.brokersPolicy = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    /*
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
    return helpers.returnErrorResponse(res, reqErrors);
    }*/

    // request validation common to all authenticated requests 
    // test header - authorization
    var test = helpers.returnAuthorizationHeader(req);
    if (Array.isArray(test)) {
        if (test.length) {
            return helpers.returnErrorResponse(res, test);
        }
        return res.json(500, 'unknown error');
    }

    // request validation common to all requests 
    var test = helpers.returnXTokenID1Header(req);
    if (Array.isArray(test)) {
        // X Token Not Provided

        // proceed with actual operation
        var responseObject = helpers.brokersPolicyList(req);

        // Note: Ensure correct status code as defined in service contract 
        return res.json(200, responseObject);
    }

    if (test === "1xtestbroker2@gmail.com") {
        // proceed with actual operation
        var responseObject = helpers.brokersPolicyListSingle(req);
        return res.json(200, responseObject);
    }

    // proceed with actual operation
    var responseObject = helpers.brokersPolicyList(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};


exports.cancelPolicy = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // request validation common to all authenticated requests 
    // test header - authorization
    var test = helpers.returnAuthorizationHeader(req);
    if (Array.isArray(test)) {
        if (test.length) {
            return helpers.returnErrorResponse(res, test);
        }
        return res.json(500, 'unknown error');
    }

    // Mock the different error responses
    var cancellationDescription = req.body.data.attributes.cancellationDescription;
    switch (cancellationDescription) {
        case "1303":
            var e1303 = _.clone(ERRORS.E400);
            e1303.code = "1303";
            e1303.detail = "1303 : UnderWriting Issue or an Activity has triggered on Policy. Please Review.policy140058001";
            return helpers.returnErrorResponse(res, [e1303]);
            break;
        case "1307":
            var e1307 = _.clone(ERRORS.E400);
            e1307.code = "1307";
            e1307.detail = "1307: Some details here";
            return helpers.returnErrorResponse(res, [e1307]);
            break;
        case "1301":
            var e1301 = _.clone(ERRORS.E400);
            e1301.code = "1301";
            e1301.detail = "1301: Some details here";
            return helpers.returnErrorResponse(res, [e1301]);
            break;
        case "1304":
            var e1304 = _.clone(ERRORS.E400);
            e1304.code = "1304";
            e1304.detail = "1304: Some details here";
            return helpers.returnErrorResponse(res, [e1304]);
            break;
        case "error":
            return helpers.returnErrorResponse(res, [ERRORS.E400]);
            break;

        default:

    }
    //reqErrors = helpers.someSpecificPayloadValidation(req);
    //if (reqErrors.length) {
    //    return helpers.returnErrorResponse(res, reqErrors);
    //}

    // proceed with actual operation
    var responseObject = helpers.cancelPolicy(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

exports.policyHistory = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // request validation common to all authenticated requests 
    // test header - authorization
    var test = helpers.returnAuthorizationHeader(req);
    if (Array.isArray(test)) {
        if (test.length) {
            return helpers.returnErrorResponse(res, test);
        }
        return res.json(500, 'unknown error');
    }

    // proceed with actual operation
    var responseObject = helpers.policyHistory(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};


/* Dashboard */
exports.quoteSearch = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // request validation common to all authenticated requests 
    // test header - authorization
    var test = helpers.returnAuthorizationHeader(req);
    if (Array.isArray(test)) {
        if (test.length) {
            return helpers.returnErrorResponse(res, test);
        }
        return res.json(500, 'unknown error');
    }

    // proceed with actual operation
    var responseObject = helpers.quoteSearch(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

exports.policySearch = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // request validation common to all authenticated requests 
    // test header - authorization
    var test = helpers.returnAuthorizationHeader(req);
    if (Array.isArray(test)) {
        if (test.length) {
            return helpers.returnErrorResponse(res, test);
        }
        return res.json(500, 'unknown error');
    }

    // request payload validation 
    reqErrors = helpers.policySearchRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // proceed with actual operation
    var responseObject = helpers.policySearch(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};


/* Identity */
exports.registerUser = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // Mock the different error responses
    var email = req.body.data.attributes.email;
    switch (email) {
        case "invalidbrokercode@website.com":
            var eInvalidBrokerCode = _.clone(ERRORS.E400);
            eInvalidBrokerCode.detail = "INVALID_BROKER_CODE: Details here";
            return helpers.returnErrorResponse(res, [eInvalidBrokerCode]);
            break;
        case "existingaccount@website.com":
            var eExistingAccount = _.clone(ERRORS.E400);
            eExistingAccount.detail = "EXISTING: Details here";
            return helpers.returnErrorResponse(res, [eExistingAccount]);
            break;
        case "incorrectpolicy@website.com":
            var eIncorrectPolicy = _.clone(ERRORS.E400);
            eIncorrectPolicy.detail = "INCORRECT_POLICY_NUMBER: Details here";
            return helpers.returnErrorResponse(res, [eIncorrectPolicy]);
            break;
        case "incorrectregcode@website.com":
            var eIncorrectRegCode = _.clone(ERRORS.E400);
            eIncorrectRegCode.detail = "INCORRECT_REGISTRATION_CODE: Details here";
            return helpers.returnErrorResponse(res, [eIncorrectRegCode]);
            break;
        case "expiredregcode@website.com":
            var eExpiredRegCode = _.clone(ERRORS.E400);
            eExpiredRegCode.detail = "EXPIRED_REGISTRATION_CODE: Details here";
            return helpers.returnErrorResponse(res, [eExpiredRegCode]);
            break;
        case "witherror@website.com":
            var eOther = _.clone(ERRORS.E400);
            eOther.detail = "ERROR: Details here";
            return helpers.returnErrorResponse(res, [eOther]);
            break;
        default:
    }

    //reqErrors = helpers.someSpecificPayloadValidation(req);
    //if (reqErrors.length) {
    //    return helpers.returnErrorResponse(res, reqErrors);
    //}

    // proceed with actual operation
    var responseObject = helpers.registerUser(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(202, responseObject);
};


/* Documents */
exports.getDocuments = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    //reqErrors = helpers.someSpecificPayloadValidation(req);
    //if (reqErrors.length) {
    //    return helpers.returnErrorResponse(res, reqErrors);
    //}

    // proceed with actual operation
    var responseObject = helpers.getDocuments(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

exports.stagingLocations = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    //reqErrors = helpers.someSpecificPayloadValidation(req);
    //if (reqErrors.length) {
    //    return helpers.returnErrorResponse(res, reqErrors);
    //}

    // proceed with actual operation
    var responseObject = helpers.stagingLocations(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

exports.archive = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    //reqErrors = helpers.someSpecificPayloadValidation(req);
    //if (reqErrors.length) {
    //    return helpers.returnErrorResponse(res, reqErrors);
    //}

    // proceed with actual operation
    var responseObject = helpers.archive(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(204, responseObject);
};

exports.upload = function (req, res) {
    // proceed with actual operation
    var responseObject = helpers.upload(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(204, responseObject);
};

exports.delete = function (req, res) {
    // proceed with actual operation
    var responseObject = helpers.delete(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(204, responseObject);
};

exports.download = function (req, res) {
    // proceed with actual operation
    var responseObject = helpers.download(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};


/* test validators - helpers */
exports.testMockAuthorizationHeader = function (req, res) {
    // test returnAuthorizationHeader
    // set content-type 
    res.type('application/json');

    // request validation common to all authenticated requests 
    var test = helpers.returnAuthorizationHeader(req);
    if (Array.isArray(test)) {
        if (test.length) {
            return helpers.returnErrorResponse(res, test);
        }
        return res.json(500, 'unknown error');
    }

    return res.json(200, test);
};

exports.testMockXTokenID1 = function (req, res) {
    // test returnXTokenID1Header
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var test = helpers.returnXTokenID1Header(req);
    if (Array.isArray(test)) {
        if (test.length) {
            return helpers.returnErrorResponse(res, "error" + test);
        }
        return res.json(500, '');
    }

    if (test === "1xtestbroker2@gmail.com") {
        // proceed with actual operation
        return res.json(200, 'here  ' + test);
    }

    if (test === "CODEBASE64_TEST_TOKEN_ID1") {
        // proceed with actual operation
        return res.json(200, 'This is it ' + test);
    }

    return res.json(200, "last" + test);
};

exports.testMockXTokenID2 = function (req, res) {
    // test returnXTokenID2Header
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var test = helpers.returnXTokenID2Header(req);
    if (Array.isArray(test)) {
        if (test.length) {
            return helpers.returnErrorResponse(res, test);
        }
        return res.json(500, '');
    }

    return res.json(200, test);
};

exports.userDetails = function (req, res) {
    // set content-type 
    var test = helpers.returnAuthorizationHeader(req);

    if (Array.isArray(test)) {
        if (test.length) {
            return helpers.returnErrorResponse(res, test);
        }
        return res.json(500, 'unknown error');
    }

    var userEmail = helpers.returnXTokenID1Header(req);
    if (Array.isArray(userEmail)) {
        if (userEmail.length) {
            //return helpers.returnErrorResponse(res, userEmail);
            var responseObject = helpers.usersGetDetails(req, "1xtestbroker1@gmail.com");
            return res.json(200, responseObject);
        }
    }

    if (userEmail === "1xtestbroker1@gmail.com") {
        // proceed with actual operation

        var responseObject = helpers.usersGetDetails(req, userEmail);
        return res.json(200, responseObject);
    }

    var responseObject = helpers.usersGetDetailsEmployer(req, userEmail);
    return res.json(200, responseObject);
};

exports.patchUserDetails = function (req, res) {
    // set content-type 
    var test = helpers.returnAuthorizationHeader(req);

    if (Array.isArray(test)) {
        if (test.length) {
            return helpers.returnErrorResponse(res, test);
        }
        return res.json(500, 'unknown error');
    }

    var userEmail = helpers.returnXTokenID1Header(req);
    if (Array.isArray(userEmail)) {
        if (userEmail.length) {
            return helpers.returnErrorResponse(res, userEmail);
        }
        return res.json(500, 'unknown error');
    }

    // request payload validation 
    var reqErrors = helpers.patchUserDetailsRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    var responseObject = helpers.patchUserDetails(req);
    return res.json(200, responseObject);
};


exports.rebindPolicyQuote = function (req, res) {
    // set content-type 
    res.type('application/json');


    var test = helpers.returnAuthorizationHeader(req);
    if (Array.isArray(test)) {
        if (test.length) {
            return helpers.returnErrorResponse(res, test);
        }
        return res.json(500, 'unknown error');
    }

    // proceed with actual operation
    var responseObject = helpers.rebindPolicyQuote(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};


exports.forgotRegistration = function (req, res) {
    // set content-type 
    res.type('application/json');

    // Mock the different error responses
    var policyNumber = req.body.data.attributes.securityQuestions.policyNumber;
    switch (policyNumber) {
        case "123456":
            return res.json(500, 'mock 500 error return');
            break;
        case "234567":
            return res.json(400, 'mock 400 error return');
            break;
        default:
    }

    // proceed with actual operation
    var responseObject = helpers.forgotRegistration(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

exports.policyPatch = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // query param validation
    if (!validators.validParam(req, "policyId")) {
        var errorArray = [];
        errorArray.push(ERRORS.E500);
        return helpers.returnErrorResponse(res, errorArray);
    }

    // test header - authorization
    var test = helpers.returnAuthorizationHeader(req);
    if (Array.isArray(test)) {
        if (test.length) {
            return helpers.returnErrorResponse(res, test);
        }
        return res.json(500, 'unknown error');
    }
    var userEmail = helpers.returnXTokenID1Header(req);
    if (Array.isArray(userEmail)) {
        if (userEmail.length) {
            //return helpers.returnErrorResponse(res, userEmail);
            var responseObject = helpers.policyPatch(req);
            return res.json(200, responseObject);
        }
        //return res.json(500, 'unknown error');
    }

    // proceed with actual operation
    var responseObject = helpers.policyPatch(req);

    if (userEmail === "1xtestbroker2@gmail.com") {
        // Set isUwIssueAvailable to true
        responseObject.data.attributes.bindData.isUwIssueAvailable = true;
    }

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

exports.policyRenewal = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // request validation common to all authenticated requests 
    // test header - authorization
    //var test = helpers.returnAuthorizationHeader(req);
    //if (Array.isArray(test)) {
    //    if (test.length) {
    //        return helpers.returnErrorResponse(res, test);
    //    }
    //    return res.json(500, 'unknown error');
    //}

    if (!validators.validParam(req, "policyId")) {
        var errorArray = [];
        errorArray.push(ERRORS.E500);
        return helpers.returnErrorResponse(res, errorArray);
    }

    // proceed with actual operation
    var responseObject = helpers.policyRenewal(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

exports.policySaveRenewal = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // request validation common to all authenticated requests 
    // test header - authorization
    //var test = helpers.returnAuthorizationHeader(req);
    //if (Array.isArray(test)) {
    //    if (test.length) {
    //        return helpers.returnErrorResponse(res, test);
    //    }
    //    return res.json(500, 'unknown error');
    //}

    if (!validators.validParam(req, "policyId")) {
        var errorArray = [];
        errorArray.push(ERRORS.E500);
        return helpers.returnErrorResponse(res, errorArray);
    }

    // proceed with actual operation
    var responseObject = helpers.policySaveRenewal(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

/* getAddresses */
exports.getAddresses = function (req, res) {
    // set content-type 
    //res.type('application/json');

    // request validation common to all requests 
    //var reqErrors = helpers.genericRequestValidation(req);
    //if (reqErrors.length) {
    //    return helpers.returnErrorResponse(res, reqErrors);
    //}

    // proceed with actual operation
    var responseObject = helpers.getAddresses(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

/* getAddressesSearch */
exports.getAddressesSearch = function (req, res) {
    // set content-type 
    //res.type('application/json');

    // request validation common to all requests 
    //var reqErrors = helpers.genericRequestValidation(req);
    //if (reqErrors.length) {
    //    return helpers.returnErrorResponse(res, reqErrors);
    //}

    // proceed with actual operation
    var responseObject = helpers.getAddressesSearch(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

/* /policies/brokerages */
exports.policyBrokeragesPost = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    //var reqErrors = helpers.genericRequestValidation(req);
    //if (reqErrors.length) {
    //    return helpers.returnErrorResponse(res, reqErrors);
    //}

    // request validation common to all authenticated requests 
    // test header - authorization
    var test = helpers.returnAuthorizationHeader(req);
    if (Array.isArray(test)) {
        if (test.length) {
            return helpers.returnErrorResponse(res, test);
        }
        return res.json(500, 'unknown error');
    }

    var errorGroup = req.body.data.attributes.brokerUserGroups[0].groupId;
    switch (errorGroup) {
        case "123456":
            var responseObject = helpers.policyBrokeragesPost500();
            return res.json(500, responseObject);
            break;
        case "234567":
            var responseObject = helpers.policyBrokeragesPost400();
            return res.json(400, responseObject);
            break;
        default:
    }

    // proceed with actual operation
    var responseObject = helpers.policyBrokeragesPost(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};


/* /policies/brokerages GET */
exports.policyBrokeragesGet = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    //var reqErrors = helpers.genericRequestValidation(req);
    //if (reqErrors.length) {
    //    return helpers.returnErrorResponse(res, reqErrors);
    //}

    // request validation common to all authenticated requests 
    // test header - authorization
    var test = helpers.returnAuthorizationHeader(req);
    if (Array.isArray(test)) {
        if (test.length) {
            return helpers.returnErrorResponse(res, test);
        }
        return res.json(500, 'unknown error');
    }

    var userEmail = helpers.returnXTokenID1Header(req);
    if (Array.isArray(userEmail)) {
        if (userEmail.length) {
            var responseObject = helpers.policyBrokeragesGetEmpty(req);
            return res.json(200, responseObject);
        }
    }

    if (userEmail === "1xtestbroker2@gmail.com") {
        var responseObject = helpers.policyBrokeragesGetEmpty(req);
        return res.json(200, responseObject);
    }

    // proceed with actual operation
    var responseObject = helpers.policyBrokeragesGet(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};


/* getInvoiceHistory */
exports.getInvoiceHistory = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // Mock the different error responses
    var policyId = req.params.policyId;
    switch (policyId) {
        case "140166111":
            var eInvalidPolicyId = _.clone(ERRORS.E400);
            eInvalidPolicyId.detail = "INVALID: Details here";
            return helpers.returnErrorResponse(res, [eInvalidPolicyId]);
            break;
        default:
    }

    // proceed with actual operation
    var responseObject = helpers.getInvoiceHistory(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

/* retrievePaymentDetails */
exports.retrievePaymentDetails = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // proceed with actual operation
    var responseObject = helpers.retrievePaymentDetails(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};


exports.policyUsersBrokeragesGet = function (req, res) {
    // set content-type 
    res.type('application/json');

    if (!validators.validParam(req, "brokerageId")) {
        var errorArray = [];
        errorArray.push(ERRORS.E500);
        return helpers.returnErrorResponse(res, errorArray);
    }

    // proceed with actual operation
    var responseObject = helpers.policyUsersBrokeragesGet(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

exports.policyUsersBrokeragesPost = function (req, res) {
    // set content-type 
    res.type('application/json');

    if (!validators.validParam(req, "brokerageId")) {
        var errorArray = [];
        errorArray.push(ERRORS.E500);
        return helpers.returnErrorResponse(res, errorArray);
    }

    // Mock the different error responses
    var email = req.body.data.attributes.contactInfo.email;
    switch (email) {
        case "error500@website.com":
            var responseObject = helpers.policyBrokeragesPost500();
            return res.json(500, responseObject);
            break;
        case "error400@website.com":
            var responseObject = helpers.policyBrokeragesPost400();
            return res.json(400, responseObject);
            break;
        case "invalidbrokercode@website.com":
            var eInvalidBrokerCode = _.clone(ERRORS.E400);
            eInvalidBrokerCode.detail = "INVALID_BROKER_CODE: Details here";
            return helpers.returnErrorResponse(res, [eInvalidBrokerCode]);
            break;
        case "existingaccount@website.com":
            var eExistingAccount = _.clone(ERRORS.E400);
            eExistingAccount.detail = "EXISTING: Details here";
            return helpers.returnErrorResponse(res, [eExistingAccount]);
            break;
        case "incorrectpolicy@website.com":
            var eIncorrectPolicy = _.clone(ERRORS.E400);
            eIncorrectPolicy.detail = "INCORRECT_POLICY_NUMBER: Details here";
            return helpers.returnErrorResponse(res, [eIncorrectPolicy]);
            break;
        case "incorrectregcode@website.com":
            var eIncorrectRegCode = _.clone(ERRORS.E400);
            eIncorrectRegCode.detail = "INCORRECT_REGISTRATION_CODE: Details here";
            return helpers.returnErrorResponse(res, [eIncorrectRegCode]);
            break;
        case "expiredregcode@website.com":
            var eExpiredRegCode = _.clone(ERRORS.E400);
            eExpiredRegCode.detail = "EXPIRED_REGISTRATION_CODE: Details here";
            return helpers.returnErrorResponse(res, [eExpiredRegCode]);
            break;
        case "witherror@website.com":
            var eOther = _.clone(ERRORS.E400);
            eOther.detail = "ERROR: Details here";
            return helpers.returnErrorResponse(res, [eOther]);
            break;
        default:
    }

    // proceed with actual operation
    var responseObject = helpers.policyUsersBrokeragesPost(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(202, responseObject);
};

/* financialInstitutions */
exports.financialInstitutions = function (req, res) {
    // set content-type 
    res.type('application/json');

    /*
        // request validation common to all requests 
        var reqErrors = helpers.genericRequestValidation(req);
        if (reqErrors.length) {
            return helpers.returnErrorResponse(res, reqErrors);
        }
    */
    // Mock the different error responses
    var input = req.query["BSB"];
    switch (input) {
        case "000001":
            var eInvalid = _.clone(ERRORS.E400);
            eInvalid.detail = "INVALID: Details here";
            return helpers.returnErrorResponse(res, [eInvalid]);
            break;
        default:
    }

    // proceed with actual operation
    var responseObject = helpers.financialInstitutions(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

exports.saveAndQuoteRenewalSubmission = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // request validation common to all authenticated requests 
    // test header - authorization
    var test = helpers.returnAuthorizationHeader(req);
    if (Array.isArray(test)) {
        if (test.length) {
            return helpers.returnErrorResponse(res, test);
        }
        return res.json(500, 'unknown error');
    }

    if (!validators.validParam(req, "policyId")) {
        var errorArray = [];
        errorArray.push(ERRORS.E500);
        return helpers.returnErrorResponse(res, errorArray);
    }

    // proceed with actual operation
    var responseObject = helpers.saveAndQuoteRenewalSubmission(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

/* directDebit */
exports.directDebit = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    // Mock the different error responses
    var policyNumber = req.body.data.attributes.policyNumber;
    switch (policyNumber) {
        case "123463":
            var eInvalid = _.clone(ERRORS.E400);
            eInvalid.detail = "INVALID: Details here";
            return helpers.returnErrorResponse(res, [eInvalid]);
            break;
        case "140260601":
            var eInvalid = _.clone(ERRORS.E400);
            eInvalid.detail = "INVALID: Details here";
            return helpers.returnErrorResponse(res, [eInvalid]);
            break;
        default:
    }

    // proceed with actual operation
    var responseObject = helpers.directDebit(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
}


/* brokerage users search */
exports.policySearchUsersBrokerages = function (req, res) {
    // set content-type 
    res.type('application/json');

    // Mock the different responses
    var groupName = req.body.data.attributes.criteria.groupName;
    switch (groupName) {
        case "emptyGroup":
            var responseObject = helpers.policyUsersBrokeragesGetCondition(req, "0");
            return res.json(200, responseObject);
            break;
        case "error400":
            var responseObject = helpers.policyUsersBrokeragesGetCondition(req, "400");
            return res.json(400, responseObject);
            break;
        case "full":
            var responseObject = helpers.policyUsersBrokeragesGetCondition(req, "full");
            return res.json(400, responseObject);
            break;
        default:
    }

    // proceed with actual operation
    var responseObject = helpers.policyUsersBrokeragesGet(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};

/* updatePaymentPlan */
exports.updatePaymentPlan = function (req, res) {
    // set content-type 
    res.type('application/json');

    // request validation common to all requests 
    var reqErrors = helpers.genericRequestValidation(req);
    if (reqErrors.length) {
        return helpers.returnErrorResponse(res, reqErrors);
    }

    //// request validation common to all authenticated requests 
    //// test header - authorization
    //var test = helpers.returnAuthorizationHeader(req);
    //if (Array.isArray(test)) {
    //    if (test.length) {
    //        return helpers.returnErrorResponse(res, test);
    //    }
    //    return res.json(500, 'unknown error');
    //}

    //if (!validators.validParam(req, "policyId")) {
    //    var errorArray = [];
    //    errorArray.push(ERRORS.E500);
    //    return helpers.returnErrorResponse(res, errorArray);
    //}

    //if (!validators.validUpdatePaymentPlanRequest(req)) {
    //    return res.json(500, "Name and amount attributes are missing");
    //}

    // proceed with actual operation
    var responseObject = helpers.updatePaymentPlan(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(202, responseObject);
};

/* /workersInsurance/me/brokerages/{brokerageId}/users/{userId} */
exports.policyUsersBrokeragesPatch = function (req, res) {
    // set content-type 
    res.type('application/json');

    if (!validators.validParam(req, "brokerageId")) {
        var errorArray = [];
        errorArray.push(ERRORS.E500);
        return helpers.returnErrorResponse(res, errorArray);
    }

    if (!validators.validParam(req, "userId")) {
        var errorArray = [];
        errorArray.push(ERRORS.E500);
        return helpers.returnErrorResponse(res, errorArray);
    }

    var emailPatch = req.body.data.attributes.contactInfo.email;
    switch (emailPatch) {
        case "validate500@email.com":
            var responseObject = helpers.policyBrokeragesPost500();
            return res.json(500, responseObject);
            break;
        case "validate400@email.com":
            var responseObject = helpers.policyBrokeragesPost400();
            return res.json(400, responseObject);
            break;
        default:
    }

    // proceed with actual operation
    var responseObject = helpers.policyUsersBrokeragesPatch(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(202, responseObject);
};

exports.policyUsersBrokeragesGetUserID = function (req, res) {
    // set content-type 

    //if (!validators.validParam(req, "brokerageId")) {
    //    var errorArray = [];
    //    errorArray.push(ERRORS.E500);
    //    return helpers.returnErrorResponse(res, errorArray);
    //}

    if (!validators.validParam(req, "userId")) {
        var errorArray = [];
        errorArray.push(ERRORS.E500);
        return helpers.returnErrorResponse(res, errorArray);
    }

    // proceed with actual operation
    var responseObject = helpers.policyUsersBrokeragesGetUserID(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(200, responseObject);
};


/* /workersInsurance/me/brokerages/{brokerageId}/users/{userId}/resetPassword */
exports.policyUsersBrokeragesResetPassword = function (req, res) {
    // set content-type 
    res.type('application/json');

    if (!validators.validParam(req, "brokerageId")) {
        var errorArray = [];
        errorArray.push(ERRORS.E500);
        return helpers.returnErrorResponse(res, errorArray);
    }

    if (!validators.validParam(req, "userId")) {
        var errorArray = [];
        errorArray.push(ERRORS.E500);
        return helpers.returnErrorResponse(res, errorArray);
    }

    var emailPatch = req.body.data.attributes.email;
    switch (emailPatch) {
        case "validate500@email.com":
            var responseObject = helpers.policyBrokeragesPost500();
            return res.json(500, responseObject);
            break;
        case "validate400@email.com":
            var responseObject = helpers.policyBrokeragesPost400();
            return res.json(400, responseObject);
            break;
        default:
    }

    // proceed with actual operation
    var responseObject = helpers.policyUsersBrokeragesPasswordReset(req);

    // Note: Ensure correct status code as defined in service contract 
    return res.json(202, responseObject);
};