﻿

/*
 * GET /v1/portal/health
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 *  - parameter -
 */
exports.getV1PortalHealth = function(req, res) {
	req.check('', 'Invalid parameter').notEmpty();
	if (req.validationErrors()) {
		return res.json(400,req.validationErrorsJson());
	}
	res.status(200);

	// set response body and send
	res.json({});
};

/*
 * POST /v1/portal/workersInsurance/claims
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 *  - parameter -
 *  - parameter -
 */
exports.postV1PortalWorkersinsuranceClaims = function(req, res) {
	req.check('', 'Invalid parameter').notEmpty();
	req.check('', 'Invalid parameter').notEmpty();
	if (req.validationErrors()) {
		return res.json(400,req.validationErrorsJson());
	}
	res.status(201);

	// set response body and send
	res.json({});
};

/*
 * PATCH /v1/portal/workersInsurance/claims/attachments
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 *  - parameter -
 *  - parameter -
 */
exports.patchV1PortalWorkersinsuranceClaimsAttachments = function(req, res) {
	req.check('', 'Invalid parameter').notEmpty();
	req.check('', 'Invalid parameter').notEmpty();
	if (req.validationErrors()) {
		return res.json(400,req.validationErrorsJson());
	}
	res.status(202);

	// set response body and send
	res.send('');
};

/*
 * PATCH /v1/portal/workersInsurance/claims/{id}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 *  - parameter -
 *  - parameter -
 */
exports.patchV1PortalWorkersinsuranceClaims = function(req, res) {
	req.check('', 'Invalid parameter').notEmpty();
	req.check('', 'Invalid parameter').notEmpty();
	if (req.validationErrors()) {
		return res.json(400,req.validationErrorsJson());
	}
	res.status(204);

	// set response body and send
	res.send('');
};

/*
 * PARAMETERS /v1/portal/workersInsurance/claims/{id}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 */
exports.parametersV1PortalWorkersinsuranceClaims = function(req, res) {
	res.status(200);

	// set response body and send
	res.json({});
};

/*
 * POST /v1/portal/workersInsurance/claims/{id}/lodge
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 *  - parameter -
 *  - parameter -
 */
exports.postV1PortalWorkersinsuranceClaimsLodge = function(req, res){
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    /*// validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
    return helpers.returnError(res, 400, ERRORS.Error400);
    }*/
    
    
    // Set the type of response, sets the content type.
    res.type('application/json');
    
    var data = [];
    data.applicationId = req.params.applicationId;
    if (data.applicationId) {
        // Set the status code of the response.
        res.status(204);
        res.json({});
    
    } else {
        res.status(404)
        res.json({});
    
    }
};

/*
 * PARAMETERS /v1/portal/workersInsurance/claims/{id}/lodge
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 */
exports.parametersV1PortalWorkersinsuranceClaimsLodge = function(req, res){
    res.status(200);
    
    // set response body and send
    res.json({});
};

/*
 * POST /v1/portal/workersInsurance/claims/attachmentLocations
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 *  - parameter -
 *  - parameter -
 */
exports.postV1PortalWorkersinsuranceClaimsAttachmentlocations = function(req, res){
    req.check('', 'Invalid parameter').notEmpty();
    req.check('', 'Invalid parameter').notEmpty();
    if (req.validationErrors()) {
        return res.json(400, req.validationErrorsJson());
    }
    res.status(200);
    
    // set response body and send
    res.type('application/json');
    res.render('v1_postV1PortalWorkersinsuranceClaimsAttachmentlocations');
};

/*
 * GET /v1/portal/referenceData/languages
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 *  - parameter -
 */
exports.getV1PortalReferencedataLanguages = function(req, res){
    req.check('', 'Invalid parameter').notEmpty();
    if (req.validationErrors()) {
        return res.json(400, req.validationErrorsJson());
    }
    res.status(200);
    
    // set response body and send
    res.type('application/json');
    res.render('v1_getV1PortalReferencedataLanguages');
};

/*
 * GET /v1/portal/referenceData/injuryClassifications
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 *  - parameter -
 * injuryClassification1(type: string) - query parameter - first level injuryClassification
 * injuryClassification2(type: string) - query parameter - second level injuryClassification
 * injuryClassification3(type: string) - query parameter - third level injuryClassification
 */
exports.getV1PortalReferencedataInjuryclassifications = function(req, res){
    req.check('', 'Invalid parameter').notEmpty();
    req.checkQuery('injuryClassification1', 'Invalid query parameter').notEmpty();
    req.checkQuery('injuryClassification2', 'Invalid query parameter').notEmpty();
    req.checkQuery('injuryClassification3', 'Invalid query parameter').notEmpty();
    if (req.validationErrors()) {
        return res.json(400, req.validationErrorsJson());
    }
    res.status(200);
    
    // set response body and send
    res.type('application/json');
    res.render('v1_getV1PortalReferencedataInjuryclassifications');
};

/*
 * GET /v1/portal/referenceData/injuryClassifications/{injuryClassification1}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 *  - parameter -
 */
exports.getV1PortalReferencedataInjuryclassifications2 = function(req, res){
    req.check('', 'Invalid parameter').notEmpty();
    if (req.validationErrors()) {
        return res.json(400, req.validationErrorsJson());
    }
    res.status(200);
    
    // set response body and send
    res.type('application/json');
    res.render('v1_getV1PortalReferencedataInjuryclassifications2');
};

/*
 * PARAMETERS /v1/portal/referenceData/injuryClassifications/{injuryClassification1}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 */
exports.parametersV1PortalReferencedataInjuryclassifications = function(req, res) {
	res.status(200);

	// set response body and send
	res.json({});
};

/*
 * GET /v1/portal/referenceData/injuryClassifications/{injuryClassification1}/{injuryClassification2}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 *  - parameter -
 */
exports.getV1PortalReferencedataInjuryclassifications3 = function(req, res){
    req.check('', 'Invalid parameter').notEmpty();
    if (req.validationErrors()) {
        return res.json(400, req.validationErrorsJson());
    }
    res.status(200);
    
    // set response body and send
    res.type('application/json');
    res.render('v1_getV1PortalReferencedataInjuryclassifications3');
};

/*
 * PARAMETERS /v1/portal/referenceData/injuryClassifications/{injuryClassification1}/{injuryClassification2}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 */
exports.parametersV1PortalReferencedataInjuryclassifications2 = function(req, res) {
	res.status(200);

	// set response body and send
	res.json({});
};

/*
 * GET /v1/portal/referenceData/injuryClassifications/{injuryClassification1}/{injuryClassification2}/{injuryClassification3}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 *  - parameter -
 */
exports.getV1PortalReferencedataInjuryclassifications4 = function(req, res){
    req.check('', 'Invalid parameter').notEmpty();
    if (req.validationErrors()) {
        return res.json(400, req.validationErrorsJson());
    }
    res.status(200);
    
    // set response body and send
    res.type('application/json');
    res.render('v1_getV1PortalReferencedataInjuryclassifications4');
};

/*
 * PARAMETERS /v1/portal/referenceData/injuryClassifications/{injuryClassification1}/{injuryClassification2}/{injuryClassification3}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 */
exports.parametersV1PortalReferencedataInjuryclassifications3 = function(req, res) {
	res.status(200);

	// set response body and send
	res.json({});
};

/*
 * GET /v1/portal/dustDiseasesCare/me/applications
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 *  - parameter -
 */
exports.getV1PortalDustdiseasescareMeApplications = function(req, res){
    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    res.type('application/json');
    
    res.status(200);
        return res.render('v1_getV1PortalReferencedataDDCOnboardingApplicationList');
};

/*
 * POST /v1/portal/dustDiseasesCare/me/applications/compensation
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 *  - parameter -
 *  - parameter -
 */
exports.postV1PortalDustdiseasescareMeApplicationsCompensation = function(req, res){
    /*req.check('', 'Invalid parameter').notEmpty();
    req.check('', 'Invalid parameter').notEmpty();
    if (req.validationErrors()) {
    return res.json(400,req.validationErrorsJson());
    }*/
    
    // validate request type 
    if (!req.is('json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    res.status(201);
    res.type('json');
    
    res.render('responses/post_ddc_applications_response');
};

/*
 * GET /v1/portal/dustDiseasesCare/me/applications/compensation/{applicationId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 *  - parameter -
 */
exports.getV1PortalDustdiseasescareMeApplicationsCompensation = function(req, res){
    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    res.type('application/json');
    
    var data = [];
    data.applicationId = req.params.applicationId;
    if (data.applicationId) {
        // Set the status code of the response.
        res.status(200);
        return res.render('responses/get_ddc_applications_compensation');
    }
    res.status(404)
    res.json({});
};

/*
 * PATCH /v1/portal/dustDiseasesCare/me/applications/compensation/{applicationId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 *  - parameter -
 */
exports.patchV1PortalDustdiseasescareMeApplicationsCompensation = function(req, res){
    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    res.type('json');
    
    var data = [];
    data.applicationId = req.params.applicationId;
    if (data.applicationId) {
        res.status(202);
        // set response body and send
        return res.render("responses/get_ddc_applications_compensation");
    }
    
    res.status(404)
    res.json({});
};

/*
 * PARAMETERS /v1/portal/dustDiseasesCare/me/applications/compensation/{applicationId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 */
exports.parametersV1PortalDustdiseasescareMeApplicationsCompensation = function(req, res) {
	res.status(200);

	// set response body and send
	res.json({});
};

/*
 * POST /v1/portal/dustDiseasesCare/me/applications/compensation/{applicationId}/lodge
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 *  - parameter -
 */
exports.postV1PortalDustdiseasescareMeApplicationsCompensationLodge = function(req, res){
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    
    // Set the type of response, sets the content type.
    res.type('application/json');
    
    var data = [];
    data.applicationId = req.params.applicationId;
    if (data.applicationId) {
        // Set the status code of the response.
        res.status(204);
        res.json({});
    
    } else {
        res.status(404)
        res.json({});
    
    }
};

/*
 * PARAMETERS /v1/portal/dustDiseasesCare/me/applications/compensation/{applicationId}/lodge
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 */
exports.parametersV1PortalDustdiseasescareMeApplicationsCompensationLodge = function(req, res) {
	res.status(200);

	// set response body and send
	res.json({});
};

/*
 * POST /v1/portal/dustDiseasesCare/me/applications/dependentCompensation
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 *  - parameter -
 *  - parameter -
 */
exports.postV1PortalDustdiseasescareMeApplicationsDependentcompensation = function(req, res){
    /*req.check('', 'Invalid parameter').notEmpty();
    req.check('', 'Invalid parameter').notEmpty();
    if (req.validationErrors()) {
    return res.json(400,req.validationErrorsJson());
    }*/
    
    // validate request type 
    if (!req.is('json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    res.status(201);
    res.type('json');
    
    res.render('responses/post_ddc_applications_response');
};

/*
 * GET /v1/portal/dustDiseasesCare/me/applications/dependentCompensation/{applicationId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 *  - parameter -
 */
exports.getV1PortalDustdiseasescareMeApplicationsDependentcompensation = function(req, res){
    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    req.check('', 'Invalid parameter').notEmpty();
    
    res.status(200);
    
    // set response body and send
    res.type('json');
    res.render('v1_getV1PortalReferencedataDDCOnboardingApplicationGetRes');
};

/*
 * PATCH /v1/portal/dustDiseasesCare/me/applications/dependentCompensation/{applicationId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 *  - parameter -
 */
exports.patchV1PortalDustdiseasescareMeApplicationsDependentcompensation = function(req, res){
    // validate request type 
    if (!req.is('json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    /*req.check('', 'Invalid parameter').notEmpty();
    if (req.validationErrors()) {
    return res.json(400,req.validationErrorsJson());
    }*/
    res.type('json');
    
    
    var data = [];
    data.applicationId = req.params.applicationId;
    if (data.applicationId) {
        res.status(202);
        // set response body and send
        return res.render("responses/get_ddc_applications_depCompensation");
    }
    
    res.status(404)
    res.json({});
};

/*
 * PARAMETERS /v1/portal/dustDiseasesCare/me/applications/dependentCompensation/{applicationId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 */
exports.parametersV1PortalDustdiseasescareMeApplicationsDependentcompensation = function(req, res) {
	res.status(200);

	// set response body and send
	res.json({});
};

/*
 * POST /v1/portal/dustDiseasesCare/me/applications/dependentCompensation/{applicationId}/lodge
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 */
exports.postV1PortalDustdiseasescareMeApplicationsDependentcompensationLodge = function(req, res){
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    
    // Set the type of response, sets the content type.
    res.type('application/json');
    
    var data = [];
    data.applicationId = req.params.applicationId;
    if (data.applicationId) {
        // Set the status code of the response.
        res.status(204);
        res.json({});
    
    } else {
        res.status(404)
        res.json({});
    
    }
};

/*
 * PARAMETERS /v1/portal/dustDiseasesCare/me/applications/dependentCompensation/{applicationId}/lodge
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 */
exports.parametersV1PortalDustdiseasescareMeApplicationsDependentcompensationLodge = function(req, res) {
	res.status(200);

	// set response body and send
	res.json({});
};

/*
 * POST /v1/portal/dustDiseasesCare/me/applications/medicalExaminations
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 *  - parameter -
 *  - parameter -
 */
exports.postV1PortalDustdiseasescareMeApplicationsMedicalexaminations = function(req, res){
    /* /v1/portal/dustDiseasesCare/applications/medicalExaminations */
    
    /*req.check('', 'Invalid parameter').notEmpty();
    req.check('', 'Invalid parameter').notEmpty();
    if (req.validationErrors()) {
    return res.json(400,req.validationErrorsJson());
    }*/
    
    
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    res.status(201);
    res.type('application/json');
    
    res.render('responses/post_ddc_applications_response');
};

/*
 * GET /v1/portal/dustDiseasesCare/me/applications/medicalExaminations/{applicationId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 *  - parameter -
 */
exports.getV1PortalDustdiseasescareMeApplicationsMedicalexaminations = function(req, res){
    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    req.check('', 'Invalid parameter').notEmpty();
    
    res.status(200);
    
    // set response body and send
    res.type('application/json');
    res.render('v1_getV1PortalReferencedataDDCOnboardingApplicationGetRes');
};

/*
 * PATCH /v1/portal/dustDiseasesCare/me/applications/medicalExaminations/{applicationId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 *  - parameter -
 */
exports.patchV1PortalDustdiseasescareMeApplicationsMedicalexaminations = function(req, res){
    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    res.type('application/json');
    
    var data = [];
    data.applicationId = req.params.applicationId;
    if (data.applicationId) {
        res.status(202);
        // set response body and send
        return res.render("responses/get_ddc_applications_medical_exams");
    }
    
    res.status(404)
    res.json({});
};

/*
 * PARAMETERS /v1/portal/dustDiseasesCare/me/applications/medicalExaminations/{applicationId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 */
exports.parametersV1PortalDustdiseasescareMeApplicationsMedicalexaminations = function(req, res) {
	res.status(200);

	// set response body and send
	res.json({});
};

/*
 * POST /v1/portal/dustDiseasesCare/me/applications/medicalExaminations/{applicationId}/lodge
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 */
exports.postV1PortalDustdiseasescareMeApplicationsMedicalexaminationsLodge = function(req, res){
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
    return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    
    // Set the type of response, sets the content type.
    res.type('application/json');
    
    var data = [];
    data.applicationId = req.params.applicationId;
    if (data.applicationId) {
        // Set the status code of the response.
        res.status(204);
        res.json({});
    
    } else {
        res.status(404)
        res.json({});
    
    }
};

/*
 * PARAMETERS /v1/portal/dustDiseasesCare/me/applications/medicalExaminations/{applicationId}/lodge
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 */
exports.parametersV1PortalDustdiseasescareMeApplicationsMedicalexaminationsLodge = function(req, res) {
	res.status(200);

	// set response body and send
	res.json({});
};

/*
 * POST /v1/portal/dustDiseasesCare/applications/compensation
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 *  - parameter -
 *  - parameter -
 */
exports.postV1PortalDustdiseasescareApplicationsCompensation = function(req, res){
    /*req.check('', 'Invalid parameter').notEmpty();
    req.check('', 'Invalid parameter').notEmpty();
    if (req.validationErrors()) {
    return res.json(400,req.validationErrorsJson());
    }*/
    
    
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    
    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
    return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    
    res.status(201);
    res.type('application/json');
    
    res.render('responses/post_ddc_applications_response');
};

/*
 * PATCH /v1/portal/dustDiseasesCare/applications/compensation/{applicationId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 *  - parameter -
 */
exports.patchV1PortalDustdiseasescareApplicationsCompensation = function(req, res){
    /*req.check('', 'Invalid parameter').notEmpty();
    if (req.validationErrors()) {
    return res.json(400,req.validationErrorsJson());
    }*/
    
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    res.type('application/json');
    var data = [];
    data.applicationId = req.params.applicationId;
    if (data.applicationId) {
        res.status(202);
        // set response body and send
        return res.render("responses/get_ddc_applications_compensation");
    }
    
    res.status(404)
    res.json({});
};

/*
 * PARAMETERS /v1/portal/dustDiseasesCare/applications/compensation/{applicationId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 */
exports.parametersV1PortalDustdiseasescareApplicationsCompensation = function(req, res) {
	res.status(200);

	// set response body and send
	res.json({});
};

/*
 * POST /v1/portal/dustDiseasesCare/applications/compensation/{applicationId}/attachmentLocations
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 *  - parameter -
 */
exports.postV1PortalDustdiseasescareApplicationsCompensationAttachmentlocations = function(req, res){
    /*req.check('', 'Invalid parameter').notEmpty();
    if (req.validationErrors()) {
    return res.json(400,req.validationErrorsJson());
    }*/
    res.status(200);
    
    // set response body and send
    res.type('application/json');
    res.render('responses/post_attachmentLocations');
};

/*
 * PARAMETERS /v1/portal/dustDiseasesCare/applications/compensation/{applicationId}/attachmentLocations
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 */
exports.parametersV1PortalDustdiseasescareApplicationsCompensationAttachmentlocations = function(req, res) {
	res.status(200);

	// set response body and send
	res.json({});
};


/*
 * PARAMETERS /v1/portal/dustDiseasesCare/applications/compensation/{applicationId}/contacts/{contactId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 */
exports.parametersV1PortalDustdiseasescareApplicationsCompensationContactsContactId = function(req, res) {
	res.status(200);

	// set response body and send
	res.json({});
};

/*
 * POST /v1/portal/dustDiseasesCare/applications/dependentCompensation
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 *  - parameter -
 *  - parameter -
 */
exports.postV1PortalDustdiseasescareApplicationsDependentcompensation = function(req, res){
    /*req.check('', 'Invalid parameter').notEmpty();
    req.check('', 'Invalid parameter').notEmpty();
    if (req.validationErrors()) {
    return res.json(400,req.validationErrorsJson());
    }*/
    
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    
    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
    return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    res.status(201);
    res.type('application/json');
    
    res.render('responses/post_ddc_applications_response');
};

/*
 * PATCH /v1/portal/dustDiseasesCare/applications/dependentCompensation/{applicationId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 *  - parameter -
 */
exports.patchV1PortalDustdiseasescareApplicationsDependentcompensation = function(req, res){
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    res.type('application/json');
    var data = [];
    data.applicationId = req.params.applicationId;
    if (data.applicationId) {
        res.status(202);
        // set response body and send
        return res.render("responses/get_ddc_applications_depCompensation");
    }
    
    res.status(404)
    res.json({});
};

/*
 * PARAMETERS /v1/portal/dustDiseasesCare/applications/dependentCompensation/{applicationId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 */
exports.parametersV1PortalDustdiseasescareApplicationsDependentcompensation = function(req, res) {
	res.status(200);

	// set response body and send
	res.json({});
};

/*
 * POST /v1/portal/dustDiseasesCare/applications/dependentCompensation/{applicationId}/attachmentLocations
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 *  - parameter -
 */
exports.postV1PortalDustdiseasescareApplicationsDependentcompensationAttachmentlocations = function(req, res) {
	/*req.check('', 'Invalid parameter').notEmpty();
	if (req.validationErrors()) {
		return res.json(400,req.validationErrorsJson());
	}*/
	res.status(200);

	// set response body and send
	res.type('json');
	res.render('v1_getV1PortalReferencedataDDCOnboardingPreSignedURLRes');
};

/*
 * PARAMETERS /v1/portal/dustDiseasesCare/applications/dependentCompensation/{applicationId}/attachmentLocations
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 */
exports.parametersV1PortalDustdiseasescareApplicationsDependentcompensationAttachmentlocations = function(req, res) {
	res.status(200);

	// set response body and send
	res.type('json');
	res.render('v1_getV1PortalReferencedataDDCOnboardingPreSignedURLRes');
};

/*
 * POST /v1/portal/dustDiseasesCare/applications/medicalExaminations
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 *  - parameter -
 *  - parameter -
 */
exports.postV1PortalDustdiseasescareApplicationsMedicalexaminations = function(req, res){
    /* /v1/portal/dustDiseasesCare/applications/medicalExaminations */
    
    /*req.check('', 'Invalid parameter').notEmpty();
    req.check('', 'Invalid parameter').notEmpty();
    if (req.validationErrors()) {
    return res.json(400,req.validationErrorsJson());
    }*/
    
    
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    
    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
    return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    res.status(201);
    res.type('application/json');
    
    res.render('responses/post_ddc_applications_response');
};

/*
 * PATCH /v1/portal/dustDiseasesCare/applications/medicalExaminations/{applicationId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 *  - parameter -
 */
exports.patchV1PortalDustdiseasescareApplicationsMedicalexaminations = function(req, res){
    
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    res.type('application/json');
    
    
    var data = [];
    data.applicationId = req.params.applicationId;
    if (data.applicationId) {
        res.status(202);
        // set response body and send
        return res.render("responses/get_ddc_applications_medical_exams");
    }
    
    res.status(404)
    res.json({});
};

/*
 * PARAMETERS /v1/portal/dustDiseasesCare/applications/medicalExaminations/{applicationId}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 */
exports.parametersV1PortalDustdiseasescareApplicationsMedicalexaminations = function(req, res) {
	res.status(200);

	// set response body and send
	res.json({});
};

/*
 * POST /v1/portal/dustDiseasesCare/applications/medicalExaminations/{applicationId}/attachmentLocations
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 *  - parameter -
 */
exports.postV1PortalDustdiseasescareApplicationsMedicalexaminationsAttachmentlocations = function(req, res) {
	/*req.check('', 'Invalid parameter').notEmpty();
	if (req.validationErrors()) {
		return res.json(400,req.validationErrorsJson());
	}*/
	res.status(200);

	// set response body and send
	res.json({});
};

/*
 * PARAMETERS /v1/portal/dustDiseasesCare/applications/medicalExaminations/{applicationId}/attachmentLocations
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 */
exports.parametersV1PortalDustdiseasescareApplicationsMedicalexaminationsAttachmentlocations = function(req, res) {
	res.status(200);

	// set response body and send
	res.json({});
};

/*
 * POST /v1/portal/selfInsurance/certificateOfCurrency/standard
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 */
exports.postV1PortalSelfinsuranceCertificateofcurrencyStandard = function(req, res) {
	res.status(201);

	// set response body and send
	res.json({});
};

/*
 * POST /v1/portal/selfInsurance/certificateOfCurrency/custom
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 */
exports.postV1PortalSelfinsuranceCertificateofcurrencyCustom = function(req, res) {
	res.status(202);

	// set response body and send
	res.send('');
};

/*
 * GET /v1/portal/customers/me
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 *  - parameter -
 */
exports.getV1PortalCustomersMe = function(req, res) {
	req.check('', 'Invalid parameter').notEmpty();
	if (req.validationErrors()) {
		return res.json(400,req.validationErrorsJson());
	}
	res.status(200);

	// set response body and send
	res.json({});
};