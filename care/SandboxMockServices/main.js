﻿
/*
 * icare Portal API
 *
 * 
 */

var constants = require("./utilities/constants.js");
var ERRORS = constants.errors();
var SUCCESS = constants.successResponses();

var helpers = require("./utilities/helpers.js");

var v1 = require("./routes/v1.js")

/* Route definition styles:
 *
 *	define(path, method, function)
 *	soap(path, soapAction, function)
 *
 */
Sandbox.define("/v1/portal/health", "GET", v1.getV1PortalHealth);
Sandbox.define("/v1/portal/workersInsurance/claims", "POST", v1.postV1PortalWorkersinsuranceClaims);
Sandbox.define("/v1/portal/workersInsurance/claims/attachments", "PATCH", v1.patchV1PortalWorkersinsuranceClaimsAttachments);
Sandbox.define("/v1/portal/workersInsurance/claims/{id}", "PATCH", v1.patchV1PortalWorkersinsuranceClaims);
Sandbox.define("/v1/portal/workersInsurance/claims/{id}", "PARAMETERS", v1.parametersV1PortalWorkersinsuranceClaims);
Sandbox.define("/v1/portal/workersInsurance/claims/{id}/lodge", "POST", v1.postV1PortalWorkersinsuranceClaimsLodge);
Sandbox.define("/v1/portal/workersInsurance/claims/{id}/lodge", "PARAMETERS", v1.parametersV1PortalWorkersinsuranceClaimsLodge);
Sandbox.define("/v1/portal/workersInsurance/claims/attachmentLocations", "POST", v1.postV1PortalWorkersinsuranceClaimsAttachmentlocations);
Sandbox.define("/v1/portal/referenceData/languages", "GET", v1.getV1PortalReferencedataLanguages);
Sandbox.define("/v1/portal/referenceData/injuryClassifications", "GET", v1.getV1PortalReferencedataInjuryclassifications);
Sandbox.define("/v1/portal/referenceData/injuryClassifications/{injuryClassification1}", "GET", v1.getV1PortalReferencedataInjuryclassifications2);
Sandbox.define("/v1/portal/referenceData/injuryClassifications/{injuryClassification1}", "PARAMETERS", v1.parametersV1PortalReferencedataInjuryclassifications);
Sandbox.define("/v1/portal/referenceData/injuryClassifications/{injuryClassification1}/{injuryClassification2}", "GET", v1.getV1PortalReferencedataInjuryclassifications3);
Sandbox.define("/v1/portal/referenceData/injuryClassifications/{injuryClassification1}/{injuryClassification2}", "PARAMETERS", v1.parametersV1PortalReferencedataInjuryclassifications2);
Sandbox.define("/v1/portal/referenceData/injuryClassifications/{injuryClassification1}/{injuryClassification2}/{injuryClassification3}", "GET", v1.getV1PortalReferencedataInjuryclassifications4);
Sandbox.define("/v1/portal/referenceData/injuryClassifications/{injuryClassification1}/{injuryClassification2}/{injuryClassification3}", "PARAMETERS", v1.parametersV1PortalReferencedataInjuryclassifications3);
Sandbox.define("/v1/portal/dustDiseasesCare/me/applications/compensation", "POST", v1.postV1PortalDustdiseasescareMeApplicationsCompensation);
Sandbox.define("/v1/portal/dustDiseasesCare/me/applications/compensation/{applicationId}", "GET", v1.getV1PortalDustdiseasescareMeApplicationsCompensation);
Sandbox.define("/v1/portal/dustDiseasesCare/me/applications/compensation/{applicationId}", "PATCH", v1.patchV1PortalDustdiseasescareMeApplicationsCompensation);
Sandbox.define("/v1/portal/dustDiseasesCare/me/applications/compensation/{applicationId}", "PARAMETERS", v1.parametersV1PortalDustdiseasescareMeApplicationsCompensation);
Sandbox.define("/v1/portal/dustDiseasesCare/me/applications/compensation/{applicationId}/lodge", "POST", v1.postV1PortalDustdiseasescareMeApplicationsCompensationLodge);
Sandbox.define('/v1/portal/dustDiseasesCare/me/applications', 'PARAMETERS', v1.parametersV1PortalDustdiseasescareMeApplicationsCompensationLodge);
Sandbox.define("/v1/portal/dustDiseasesCare/me/applications/dependentCompensation", "POST", v1.postV1PortalDustdiseasescareMeApplicationsDependentcompensation);
Sandbox.define("/v1/portal/dustDiseasesCare/me/applications/dependentCompensation/{applicationId}", "GET", v1.getV1PortalDustdiseasescareMeApplicationsDependentcompensation);
Sandbox.define("/v1/portal/dustDiseasesCare/me/applications/dependentCompensation/{applicationId}", "PATCH", v1.patchV1PortalDustdiseasescareMeApplicationsDependentcompensation);
Sandbox.define("/v1/portal/dustDiseasesCare/me/applications/dependentCompensation/{applicationId}", "PARAMETERS", v1.parametersV1PortalDustdiseasescareMeApplicationsDependentcompensation);
Sandbox.define("/v1/portal/dustDiseasesCare/me/applications/dependentCompensation/{applicationId}/lodge", "POST", v1.postV1PortalDustdiseasescareMeApplicationsDependentcompensationLodge);
Sandbox.define("/v1/portal/dustDiseasesCare/me/applications/dependentCompensation/{applicationId}/lodge", "PARAMETERS", v1.parametersV1PortalDustdiseasescareMeApplicationsDependentcompensationLodge);
Sandbox.define("/v1/portal/dustDiseasesCare/me/applications/medicalExaminations", "POST", v1.postV1PortalDustdiseasescareMeApplicationsMedicalexaminations);
Sandbox.define("/v1/portal/dustDiseasesCare/me/applications/medicalExaminations/{applicationId}", "GET", v1.getV1PortalDustdiseasescareMeApplicationsMedicalexaminations);
Sandbox.define("/v1/portal/dustDiseasesCare/me/applications/medicalExaminations/{applicationId}", "PATCH", v1.patchV1PortalDustdiseasescareMeApplicationsMedicalexaminations);
Sandbox.define("/v1/portal/dustDiseasesCare/me/applications/medicalExaminations/{applicationId}", "PARAMETERS", v1.parametersV1PortalDustdiseasescareMeApplicationsMedicalexaminations);
Sandbox.define("/v1/portal/dustDiseasesCare/me/applications/medicalExaminations/{applicationId}/lodge", "POST", v1.postV1PortalDustdiseasescareMeApplicationsMedicalexaminationsLodge);
Sandbox.define("/v1/portal/dustDiseasesCare/me/applications/medicalExaminations/{applicationId}/lodge", "PARAMETERS", v1.parametersV1PortalDustdiseasescareMeApplicationsMedicalexaminationsLodge);
Sandbox.define("/v1/portal/dustDiseasesCare/applications/compensation", "POST", v1.postV1PortalDustdiseasescareApplicationsCompensation);
Sandbox.define("/v1/portal/dustDiseasesCare/applications/compensation/{applicationId}", "PATCH", v1.patchV1PortalDustdiseasescareApplicationsCompensation);
Sandbox.define("/v1/portal/dustDiseasesCare/applications/compensation/{applicationId}", "PARAMETERS", v1.parametersV1PortalDustdiseasescareApplicationsCompensation);
Sandbox.define("/v1/portal/dustDiseasesCare/applications/compensation/{applicationId}/attachmentLocations", "POST", v1.postV1PortalDustdiseasescareApplicationsCompensationAttachmentlocations);
Sandbox.define("/v1/portal/dustDiseasesCare/applications/compensation/{applicationId}/attachmentLocations", "PARAMETERS", v1.parametersV1PortalDustdiseasescareApplicationsCompensationAttachmentlocations);
Sandbox.define("/v1/portal/dustDiseasesCare/applications/compensation/{applicationId}/contacts/{contactId}", "PARAMETERS", v1.parametersV1PortalDustdiseasescareApplicationsCompensationContactsContactId);
Sandbox.define("/v1/portal/dustDiseasesCare/applications/dependentCompensation", "POST", v1.postV1PortalDustdiseasescareApplicationsDependentcompensation);
Sandbox.define("/v1/portal/dustDiseasesCare/applications/dependentCompensation/{applicationId}", "PATCH", v1.patchV1PortalDustdiseasescareApplicationsDependentcompensation);
Sandbox.define("/v1/portal/dustDiseasesCare/applications/dependentCompensation/{applicationId}", "PARAMETERS", v1.parametersV1PortalDustdiseasescareApplicationsDependentcompensation);
Sandbox.define("/v1/portal/dustDiseasesCare/applications/dependentCompensation/{applicationId}/attachmentLocations", "POST", v1.postV1PortalDustdiseasescareApplicationsDependentcompensationAttachmentlocations);
Sandbox.define("/v1/portal/dustDiseasesCare/applications/dependentCompensation/{applicationId}/attachmentLocations", "PARAMETERS", v1.parametersV1PortalDustdiseasescareApplicationsDependentcompensationAttachmentlocations);
Sandbox.define("/v1/portal/dustDiseasesCare/applications/medicalExaminations", "POST", v1.postV1PortalDustdiseasescareApplicationsMedicalexaminations);
Sandbox.define("/v1/portal/dustDiseasesCare/applications/medicalExaminations/{applicationId}", "PATCH", v1.patchV1PortalDustdiseasescareApplicationsMedicalexaminations);
Sandbox.define("/v1/portal/dustDiseasesCare/applications/medicalExaminations/{applicationId}", "PARAMETERS", v1.parametersV1PortalDustdiseasescareApplicationsMedicalexaminations);
Sandbox.define("/v1/portal/dustDiseasesCare/applications/medicalExaminations/{applicationId}/attachmentLocations", "POST", v1.postV1PortalDustdiseasescareApplicationsMedicalexaminationsAttachmentlocations);
Sandbox.define("/v1/portal/dustDiseasesCare/applications/medicalExaminations/{applicationId}/attachmentLocations", "PARAMETERS", v1.parametersV1PortalDustdiseasescareApplicationsMedicalexaminationsAttachmentlocations);
Sandbox.define("/v1/portal/selfInsurance/certificateOfCurrency/standard", "POST", v1.postV1PortalSelfinsuranceCertificateofcurrencyStandard);
Sandbox.define("/v1/portal/selfInsurance/certificateOfCurrency/custom", "POST", v1.postV1PortalSelfinsuranceCertificateofcurrencyCustom);
Sandbox.define("/v1/portal/customers/me", "GET", v1.getV1PortalCustomersMe);

Sandbox.define('/v1/portal/dustDiseasesCare/applications/healthMonitoring/lodge', 'POST', function(req, res){
    /*req.check('', 'Invalid parameter').notEmpty();
    req.check('', 'Invalid parameter').notEmpty();
    if (req.validationErrors()) {
    return res.json(400,req.validationErrorsJson());
    }*/
    
    
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    
    /*// validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
    return helpers.returnError(res, 400, ERRORS.Error400);
    }*/
    
    res.status(201);
    res.type('application/json');
    
    res.json({});
})

Sandbox.define('/v1/portal/dustDiseasesCare/applications/compensation{applicationId}/callback', 'POST', function(req, res) {
    // Check the request, make sure it is a compatible type
    if (!req.is('application/json')) {
        return res.send(400, 'Invalid content type, expected application/json');
    }
    
    // Set the type of response, sets the content type.
    res.type('application/json');
    
    // Set the status code of the response.
    res.status(201);
    
    // Send the response body.
    res.json({
        "status": "ok"
    });
})

Sandbox.define('/v1/portal/dustDiseasesCare/applications/dependentCompensation/{applicationId}/callback', 'POST', function(req, res){
    // Check the request, make sure it is a compatible type
    if (!req.is('application/json')) {
        return res.send(400, 'Invalid content type, expected application/json');
    }
    
    // Set the type of response, sets the content type.
    res.type('application/json');
    
    // Set the status code of the response.
    res.status(201);
    
    // Send the response body.
    res.json({
        "status": "ok"
    });
})

Sandbox.define('/v1/portal/dustDiseasesCare/applications/compensation/{applicationId}/lodge','POST', function(req, res){
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    /*// validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
    return helpers.returnError(res, 400, ERRORS.Error400);
    }*/
    
    
    // Set the type of response, sets the content type.
    res.type('application/json');
    
    var data = [];
    data.applicationId = req.params.applicationId;
    if (data.applicationId) {
        // Set the status code of the response.
        res.status(204);
        res.json({});
    
    } else {
        res.status(404)
        res.json({});
    
    }
})

Sandbox.define('/v1/portal/dustDiseasesCare/applications/medicalExaminations/{applicationId}/lodge','POST', function(req, res){
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    /*// validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
    return helpers.returnError(res, 400, ERRORS.Error400);
    }*/
    
    
    // Set the type of response, sets the content type.
    res.type('application/json');
    
    var data = [];
    data.applicationId = req.params.applicationId;
    if (data.applicationId) {
        // Set the status code of the response.
        res.status(204);
        res.json({});
    
    } else {
        res.status(404)
        res.json({});
    
    }
})

Sandbox.define('/v1/portal/dustDiseasesCare/applications/dependentCompensation/{applicationId}/lodge','POST', function(req, res){
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    /*
    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
    return helpers.returnError(res, 400, ERRORS.Error400);
    }*/
    
    
    // Set the type of response, sets the content type.
    res.type('application/json');
    
    var data = [];
    data.applicationId = req.params.applicationId;
    if (data.applicationId) {
        // Set the status code of the response.
        res.status(204);
        res.json({});
    
    }
    else {
    res.status(404)
    res.json({});
        
    }
})

Sandbox.define('/v1/portal/dustDiseasesCare/applications/compensation/{applicationId}/contacts','POST', function(req, res){
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    /*// validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
    return helpers.returnError(res, 400, ERRORS.Error400);
    }*/
    
    
    // Set the type of response, sets the content type.
    res.type('application/json');
    var data = [];
    data.applicationId = req.params.applicationId;
    if (data.applicationId) {
        // Set the status code of the response.
        res.status(201);
        return res.render('responses/post_contact');
    }
    res.status(404)
    res.json({});
})

Sandbox.define('/v1/portal/dustDiseasesCare/applications/compensation/{applicationId}/contacts/{contactId}', 'PATCH', function(req, res){
    //req.check('', 'Invalid parameter').notEmpty();
    //if (req.validationErrors()) {
    //return res.json(400,req.validationErrorsJson());
    //}
    
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    res.type('application/json');
    var data = [];
    data.applicationId = req.params.applicationId;
    data.contactId = req.params.contactId;
    
    if ((data.applicationId) && (data.contactId)) {
        res.status(202);
        // set response body and send
        return res.render("responses/post_ddc_contact_response");
    }
    
    res.status(404)
    res.json({});
})

Sandbox.define('/v1/portal/dustDiseasesCare/applications/compensation/{applicationId}/contacts/{contactId}/unlink','POST', function(req, res){
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    res.type('application/json');
    var data = [];
    data.applicationId = req.params.applicationId;
    data.contactId = req.params.contactId;
    
    if ((data.applicationId) && (data.contactId)) {
        // Set the status code of the response.
        res.status(201);
        return res.render('responses/delete_contact');
    }
    res.status(404)
    res.json({});
})

Sandbox.define('/v1/portal/dustDiseasesCare/applications/dependentCompensation/{applicationId}/contacts','POST', function(req, res){
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    /*// validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
    return helpers.returnError(res, 400, ERRORS.Error400);
    }*/
    
    
    // Set the type of response, sets the content type.
    res.type('application/json');
    var data = [];
    data.applicationId = req.params.applicationId;
    if (data.applicationId) {
        // Set the status code of the response.
        res.status(201);
        return res.render('responses/post_ddc_contact_response');
    }
    res.status(404)
    res.json({});
})

Sandbox.define('/v1/portal/dustDiseasesCare/applications/dependentCompensation/{applicationId}/contacts/{contactId}', 'PATCH', function(req, res){
    /*req.check('', 'Invalid parameter').notEmpty();
    if (req.validationErrors()) {
    return res.json(400,req.validationErrorsJson());
    }*/
    
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    res.type('application/json');
    var data = [];
    data.applicationId = req.params.applicationId;
    data.contactId = req.params.contactId;
    
    if ((data.applicationId) && (data.contactId)) {
        res.status(202);
        // set response body and send
        return res.render("responses/post_ddc_contact_response");
    }
    
    res.status(404)
    res.json({});
})

Sandbox.define('/v1/portal/dustDiseasesCare/applications/dependentCompensation/{applicationId}/contacts/{contactId}/unlink','POST', function(req, res){
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    res.type('application/json');
    var data = [];
    data.applicationId = req.params.applicationId;
    data.contactId = req.params.contactId;
    
    if ((data.applicationId) && (data.contactId)) {
        // Set the status code of the response.
        res.status(201);
        return res.render('responses/delete_contact');
    }
    res.status(404)
    res.json({});
})

Sandbox.define('/v1/portal/dustDiseasesCare/me/applications', 'GET', function(req, res){
    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    //req.check('', 'Invalid parameter').notEmpty();
    //if (req.validationErrors()) {
    //    return res.json(400, req.validationErrorsJson());
    //}
    
    res.status(200);
    
    // set response body and send
    //res.type('application/json');
    res.type('application/vnd.api+json');
    res.render('responses/get_ddc_applications_list'); //Sample response only
})



Sandbox.define('/v1/portal/dustDiseasesCare/applications/compensation/{applicationId}', 'GET', function(req, res){
    //req.check('', 'Invalid parameter').notEmpty();
    //if (req.validationErrors()) {
    //    return res.json(400, req.validationErrorsJson());
    //}
    res.status(200);
    
    // set response body and send
    res.type('application/json');
    res.render('responses/get_ddc_applications_compensation_v2');
})

Sandbox.define('/v1/portal/dustDiseasesCare/applications/compensation/{applicationId}/callback','POST', function(req, res){
    //req.check('', 'Invalid parameter').notEmpty();
    //if (req.validationErrors()) {
    //    return res.json(400, req.validationErrorsJson());
    //}
    
    res.status(200);
    
    // set response body and send
    res.type('application/json');
    res.render('responses/post_ddc_contact_response');
})

Sandbox.define('/v1/portal/dustDiseasesCare/applications/me','GET', function(req, res){
    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    //req.check('', 'Invalid parameter').notEmpty();
    //if (req.validationErrors()) {
    //    return res.json(400, req.validationErrorsJson());
    //}
    
    res.status(200);
    
    // set response body and send
    //res.type('application/json');
    res.type('application/vnd.api+json');
    res.render('responses/get_sample_applications_list'); //Sample response only
})

Sandbox.define('/v1/portal/dustDiseasesCare/me/applications/compensation/{applicationId}/contacts/{contactid}', 'PATCH', function(req, res){
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    res.type('application/json');
    var data = [];
    data.applicationId = req.params.applicationId;
    data.contactId = req.params.contactId;
    
    if ((data.applicationId) && (data.contactId)) {
        res.status(202);
        // set response body and send
        return res.render("responses/post_contact");
    }
    
    res.status(404)
    res.json({});
})

Sandbox.define('/v1/portal/dustDiseasesCare/me/applications/compensation/{applicationId}/contacts/{contactid}','POST', function(req, res){
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    
    // Set the type of response, sets the content type.
    res.type('application/json');
    var data = [];
    data.applicationId = req.params.applicationId;
    if (data.applicationId) {
        // Set the status code of the response.
        res.status(201);
        return res.render('responses/post_ddc_contact_response');
    }
    res.status(404)
    res.json({});
})

Sandbox.define('/v1/portal/dustDiseasesCare/applications/compensation/{applicationId}/contacts/{contactId}','POST', function(req, res){
    //req.check('', 'Invalid parameter').notEmpty();
    //if (req.validationErrors()) {
    //return res.json(400,req.validationErrorsJson());
    //}
    
    // validate request type 
    if (!req.is('application/json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    
    res.type('application/json');
    var data = [];
    data.applicationId = req.params.applicationId;
    data.contactId = req.params.contactId;
    
    if ((data.applicationId) && (data.contactId)) {
        res.status(202);
        // set response body and send
        return res.render("responses/post_ddc_contact_response");
    }
    
    res.status(404)
    res.json({});
})