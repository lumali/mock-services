﻿/*
 * Common functions 
 *
 */

// returnError 
exports.returnError = function (res, code, message) {
    return res.json(code, { error: { 'message': message } });
};

// validateAccessToken 
exports.validAccessToken = function (req) {
    var accessToken = req.get("access_token");
    accessToken = accessToken ? accessToken : undefined;

    if (accessToken === undefined) return false;

    // Assume JWT 
    // header.payload.signature
    var jwtArray = accessToken.split('.');
    if (jwtArray.length < 3) return false;
    if (!jwtArray[0]) return false;
    if (!jwtArray[1]) return false;
    if (!jwtArray[2]) return false;

    // Other checking here 

    return true;
};

// validTokenHeader
exports.validTokenHeader = function (req) {
    var xToken = req.get("X-Token");
    xToken = xToken ? xToken : undefined;

    return (xToken !== undefined);
};

// validTrackingIdHeader
exports.validTrackingIdHeader = function (req) {
    var xTrackingId = req.get("X-TrackingID");
    xTrackingId = xTrackingId ? xTrackingId : undefined;

    return (xTrackingId !== undefined);
};

// validOktaTokenJsonHeader
exports.validOktaTokenJsonHeader = function (req) {
    var xOktaTokenJson = req.get("Authorization");
    xOktaTokenJson = xOktaTokenJson ? xOktaTokenJson : undefined;

    return (xOktaTokenJson !== undefined);
};

// validQueryParam
exports.validQueryParam = function (req, q) {
    var input = req.query[q] ? req.query[q] : undefined;
    if (input === undefined) return false;
    return true;
};

// validPolicyNumberRequest
exports.validPolicyNumberRequest = function (req) {
    var input = req.query.date ? req.query.date : undefined;
    if (input === undefined) return false;
    return true;
};


// validClaimRequest 
exports.validClaimRequest = function (req) {
    var input = req.body.data ? req.body.data : undefined;

    if (input === undefined) return false;

    // required:
    //  - notifier.name
    //  - notifier.contact.phone.primary
    //  - injury.occurrence
    //  - employer
    if (!input.attributes.notifier.name.given || !input.attributes.notifier.name.family) return false;
    if (!input.attributes.notifier.contact.phone.primary) return false;
    if (!input.attributes.injury.occurrence.date || !input.attributes.injury.occurrence.time) return false;
    //if (!input.attributes.employer
    //    || !input.attributes.employer.name
    //    || !input.attributes.employer.abn
    //    || !input.attributes.employer.email) return false;

    return true;
};

// validParam 
exports.validParam = function (req, p, shouldTestForInt) {
    var params = req.params ? req.params : undefined;
    if (params === undefined) return false;

    var targetParam = params[p] ? params[p] : undefined;
    if (targetParam === undefined) return false;

    if (shouldTestForInt) {
        var intParam = parseInt(targetParam, 10);
        return _.isInteger(intParam);
    }

    return true;
};

// validAttachmentRequest
exports.validAttachmentRequest = function (req) {
    var input = req.body.meta ? req.body.meta : undefined;
    if (input === undefined) return false;
    if (!input.uploadedby) return false;
    if (!input.uploaderphoneno) return false;
    if (!input.uploaderemail) return false;
    if (!input.filename) return false;
    if (!input.documenttype) return false;
    if (!input.claimnumber) return false;
    if (!input.claimid) return false;

    return true;
};

// validAccountDetails
exports.validAccountDetails = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;

    if (!input.contactNumber) return false;
    if (!input.emailAddress) return false;
    if (!input.contactPreference) return false;

    return true;
};

// validPaymentDetails
exports.validPaymentDetails = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.accountName) return false;
    if (!input.accountNumber) return false;
    if (!input.bsb) return false;
    if (!input.applicablePolicyNumber) return false;

    return true;
};

// validAddUserRequest
exports.validAddUserRequest = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;

    if (!input.givenName) return false;
    if (!input.familyName) return false;
    if (!input.contactNumber) return false;
    if (!input.emailAddress) return false;
    if (!input.policyRoles) return false;
    if (input.policyRoles.length < 1) return false;
    if (!input.policyRoles[0].policyNumber) return false;
    if (!input.policyRoles[0].policyName) return false;
    if (!input.policyRoles[0].role) return false;

    return true;
}

// validUpdateUserRequest
exports.validUpdateUserRequest = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;

    if (!input.policyRoles) return false;
    if (input.policyRoles.length < 1) return false;
    if (!input.policyRoles[0].policyNumber) return false;
    if (!input.policyRoles[0].role) return false;

    return true;
}

// validGetTeamRequest
exports.validGetTeamRequest = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.sort) return false;
    if (!input.sort.columnName) return false;
    if (!input.page) return false;
    if (!input.pageSize) return false;

    return true;
}

// validGetPortalClaimsRequest
exports.validGetPortalClaimsRequest = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.sort) return false;
    if (!input.sort.columnName) return false;
    if (!input.page) return false;
    if (!input.pageSize) return false;

    return true;
}

// validEnquiry
exports.validEnquiry = function (req) {
    var input = req.body ? req.body : undefined;
    if (input === undefined) return false;
    if (!input.data) return false;
    if (!input.data.attributes) return false;
    if (!input.data.attributes.enquiry) return false;
    if (!input.data.attributes.enquiry.problemTitle) return false;
    if (!input.data.attributes.enquiry.problemDescription) return false;

    return true;
}

// validClaimDocumentRequest 
exports.validClaimDocumentRequest = function (req) {
    var input = req.body.data ? req.body.data : undefined;

    if (input === undefined) return false;

    return true;
};

// validDuplicateClaimCheckRequest 
exports.validDuplicateClaimCheckRequest = function (req) {
    var input = req.body.data ? req.body.data : undefined;

    if (input === undefined) return false;

    /*
    if (!input.attributes
        || !input.attributes.worker
        || !input.attributes.worker.name
        || !input.attributes.worker.name.given
        || !input.attributes.worker.name.family
        || !input.attributes.injury
        || !input.attributes.injury.date) return false;
    */

    return true;
};


// getDate
exports.getDate = function (dateString) {
    var date = dateString.split(/\D/);
    return new Date(date[2], date[1] - 1, date[0]);
}

// hasOneDayElapsed 
exports.hasOneDayElapsed = function (previousDate) {
    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    var currentDate = new Date();
    var diffDays = Math.round(Math.abs((currentDate.getTime() - previousDate.getTime()) / (oneDay)));

    return diffDays >= 1;
}


/*
 * Other functions 
 *
 */

// addClaim 
// object should have been validated prior calling this function 
exports.addClaim = function (req) {
    var input = req.body;

    // Generate a 7-digit random claimNo 
    var claimNo = _.floor(Math.random() * 9000000) + 1000000;
    input.data.attributes.claimNo = claimNo;

    // Add the claim 
    state.AddUpdateClaimRepo.push(input);

    var responseObject = {};
    responseObject = {
        type: input.data.type,
        id: input.data.id,
        attributes: {
            claimNo: _.replace(claimNo, ".0", ""),
            id: input.data.attributes.id
        }
    };

    return responseObject;
};

// updateClaimById 
exports.updateClaimById = function (req) {
    // check if the claimNo exists 
    var existingClaim = _.find(state.AddUpdateClaimRepo, function (o) {
        return _.toString(o.data.attributes.claimNo) === _.toString(req.params.id);
    });

    if (!existingClaim) return false;

    // remove the claim
    state.AddUpdateClaimRepo = _.reject(state.AddUpdateClaimRepo, function (o) {
        return _.toString(o.data.attributes.claimNo) === _.toString(req.params.id);
    });

    // modify input to ensure we retain the claimNo 
    var input = req.body;
    input.data.attributes.claimNo = _.toString(req.params.id);

    // add the claim from input 
    state.AddUpdateClaimRepo.push(input);

    var responseObject = { description: SUCCESS.Success204 };
    return responseObject;
};

// lodgeClaimById 
exports.lodgeClaimById = function (req) {
    // check if the claimNo exists 
    var existingClaim = _.find(state.AddUpdateClaimRepo, function (o) {
        return _.toString(o.data.attributes.claimNo) === _.toString(req.params.id);
    });

    if (!existingClaim) return false;

    // remove the claim
    state.AddUpdateClaimRepo = _.reject(state.AddUpdateClaimRepo, function (o) {
        return _.toString(o.data.attributes.claimNo) === _.toString(req.params.id);
    });

    // modify input to ensure we retain the claimNo 
    var input = req.body;
    input.data.attributes.claimNo = _.toString(req.params.id);

    // add the claim from input 
    state.AddUpdateClaimRepo.push(input);

    var responseObject = { description: SUCCESS.Success202 };
    return responseObject;
};

// addAttachmentByClaimId 
exports.addAttachmentByClaimId = function (req) {
    var responseObject = {};
    responseObject.data = [];

    var item1 = {};
    item1.type = "type1";
    item1.id = "id1";
    item1.attributes = {};
    item1.attributes.method = "method1";
    item1.attributes.url = "url1";

    var item2 = {};
    item2.type = "type2";
    item2.id = "id2";
    item2.attributes = {};
    item2.attributes.method = "method2";
    item2.attributes.url = "url2";

    responseObject.data.push(item1);
    responseObject.data.push(item2);

    return responseObject;
};

// getPolicyNumbers
exports.getPolicyNumbers = function (req) {
    var responseObject = {};
    responseObject.items = [];

    var item1 = {};
    item1.policyNumber = "111234";
    item1.businessName = "EFS Enterprise";

    var item2 = {};
    item2.policyNumber = "2020123";
    item2.businessName = "Grant, Trantow and Raynor";

    responseObject.items.push(item1);
    responseObject.items.push(item2);

    return responseObject;
};

// getCostCentres
exports.getCostCentres = function (req) {
    var responseObject = {};
    responseObject.type = "type";
    responseObject.items = [];

    var item1 = {};
    item1.id = "cc1";
    item1.name = "Cost Centre 3001";
    item1.description = "Cost Centre 3001";

    var item2 = {};
    item2.id = "cc2";
    item2.name = "Cost Centre 3022";
    item2.description = "Cost Centre 3022";

    responseObject.items.push(item1);
    responseObject.items.push(item2);

    return responseObject;
};

// getLocations
exports.getLocations = function (req) {
    var responseObject = {};
    responseObject.type = "type";
    responseObject.items = [];

    var item1 = {};
    item1.id = "loc1";
    item1.name = "Sydney, NSW";
    item1.description = "Sydney, NSW";

    var item2 = {};
    item2.id = "loc2";
    item2.name = "Melbourne, VIC";
    item2.description = "Melbourne, VIC";

    responseObject.items.push(item1);
    responseObject.items.push(item2);

    return responseObject;
};

// getMyDetails
exports.getMyDetails = function (req) {
    var policyUsers = state.MockPolicyUsersEditable;

    // Get user email address from the X-OktaTokenJson 
    var xOktaTokenJson = req.get("X-OktaTokenJson");
    xOktaTokenJson = xOktaTokenJson ? xOktaTokenJson : undefined;
    if (xOktaTokenJson === undefined) return false;

    // check if the user email address exists 
    var targetUser = _.filter(policyUsers, function (o) {
        return _.toLower(_.toString(o.Email)) === _.toLower(_.toString(xOktaTokenJson));
    });

    if (!targetUser || targetUser.length < 1) return false;

    // Create response object 
    var responseObject = {};
    responseObject.contactNumber = targetUser[0].PhoneNumber;
    responseObject.emailAddress = targetUser[0].Email;
    responseObject.contactPreference = "phone";
    responseObject.givenName = targetUser[0].FirstName;
    responseObject.familyName = targetUser[0].LastName;
    responseObject.policyRoles = [];
    _.forEach(targetUser, function (o) {
        var newPolicy = {
            policyNumber: o.policyNumber,
            policyName: o.policyNumber,
            role: o.DetailedAuthDbRole
        };
        responseObject.policyRoles.push(newPolicy);
    });

    return responseObject;
};

// updateMyDetails
exports.updateMyDetails = function (req) {
    var policyUsers = state.MockPolicyUsersEditable;

    // Get user email address from the X-OktaTokenJson 
    var xOktaTokenJson = req.get("X-OktaTokenJson");
    xOktaTokenJson = xOktaTokenJson ? xOktaTokenJson : undefined;
    if (xOktaTokenJson === undefined) return false;

    // check if the user email address exists 
    var targetUser = _.filter(policyUsers, function (o) {
        return _.toLower(_.toString(o.Email)) === _.toLower(_.toString(xOktaTokenJson));
    });

    if (!targetUser || targetUser.length < 1) return false;

    // Create response object 
    var responseObject = {};
    responseObject.contactNumber = req.body.contactNumber;              // just echoing the input 
    responseObject.emailAddress = req.body.emailAddress;                // just echoing the input 
    responseObject.contactPreference = req.body.contactPreference;      // just echoing the input 
    responseObject.givenName = targetUser[0].FirstName;
    responseObject.familyName = targetUser[0].LastName;
    responseObject.policyRoles = [];
    _.forEach(targetUser, function (o) {
        var newPolicy = {
            policyNumber: o.policyNumber,
            policyName: o.policyNumber,
            role: o.DetailedAuthDbRole
        };
        responseObject.policyRoles.push(newPolicy);
    });

    return responseObject;
};

// getPaymentDetails
exports.getPaymentDetails = function (req) {
    var responseObject = {};
    responseObject.data = [];

    var item1 = {};
    item1.accountName = "MBA Checking";
    item1.accountNumber = "12356789";
    item1.bsb = "111555";
    item1.policyName = "Policy 1189110";
    item1.policyNumber = "POL1189110";

    var item2 = {};
    item2.accountName = "MBA Savings";
    item2.accountNumber = "23456789";
    item2.bsb = "222666";
    item2.policyName = "Policy 2289110";
    item2.policyNumber = "POL2289110";

    responseObject.data.push(item1);
    responseObject.data.push(item2);

    return responseObject;
};

// updatePaymentDetails
exports.updatePaymentDetails = function (req) {
    return req.body;
};

// getBankName 
exports.getBankName = function (req) {
    var bsbBanks = TEMPSTORE.MockBsbBanks;

    // check if the bank exists 
    var targetBank = _.find(bsbBanks, function (o) {
        return _.toString(o.BSB) === _.toString(req.query["bsb"]);
    });
    if (!targetBank) return {
        "BankName": null
    };

    return targetBank;
};

// addUser
exports.addUser = function (req) {
    var input = req.body;

    // Generate a 7-digit random identityId 
    var identityId = _.floor(Math.random() * 9000000) + 1000000;
    input.identityId = _.toString(identityId);

    // Add the user 
    var addedEntries = [];
    _.forEach(input.policyRoles, function (o) {
        var newUser = {
            "IsOktaUser": "",
            "IdentityId": input.identityId,
            "Email": input.emailAddress,
            "FirstName": input.givenName,
            "LastName": input.familyName,
            "OktaRole": input.identityId,
            "PhoneNumber": input.identityId,
            "DetailedAuthDbRole": o.role,
            "policyNumber": o.policyNumber,
            "associatedClaims": "",
            "numOpenNotificationsClaims": 0
        };

        state.MockPolicyUsersEditable.push(newUser);
        addedEntries.push(newUser);
    });

    var responseObject = addedEntries;

    return responseObject;
};

// getUser 
exports.getUser = function (req) {
    // check if the identityId exists 
    var existingUser = _.filter(state.MockPolicyUsersEditable, function (o) {
        return _.toString(o.IdentityId) === _.toString(req.query.identityId);
    });

    if (!existingUser || existingUser.length == 0) return false;

    var returnValue = {
        "givenName": existingUser[0].FirstName,
        "familyName": existingUser[0].LastName,
        "contactNumber": existingUser[0].PhoneNumber,
        "emailAddress": existingUser[0].Email,
        "identityId": existingUser[0].IdentityId,
        "policyRoles": []
    };

    _.forEach(existingUser, function (o) {
        var policyRole = {
            "policyNumber": o.policyNumber,
            "policyName": o.policyNumber,
            "role": o.DetailedAuthDbRole
        };
        
        returnValue.policyRoles.push(policyRole);
    });

    return returnValue;
};

// updateUser
exports.updateUser = function (req) {
    // check if the identityId exists 
    var existingUser = _.filter(state.MockPolicyUsersEditable, function (o) {
        return _.toString(o.IdentityId) === _.toString(req.params.identityId);
    });

    if (!existingUser || existingUser.length == 0) return false;

    // create users having the same count as the input policy roles 
    var newUsers = [];
    _.forEach(req.body.policyRoles, function (o) {
        var newUser = {
            "IsOktaUser": existingUser[0].IsOktaUser,
            "IdentityId": existingUser[0].IdentityId,
            "Email": existingUser[0].Email,
            "FirstName": existingUser[0].FirstName,
            "LastName": existingUser[0].LastName,
            "OktaRole": existingUser[0].OktaRole,
            "PhoneNumber": existingUser[0].PhoneNumber,
            "associatedClaims": existingUser[0].associatedClaims,
            "numOpenNotificationsClaims": existingUser[0].numOpenNotificationsClaims,

            // Use the input policy role 
            "policyNumber": o.policyNumber,
            "policyName": o.policyName,
            "DetailedAuthDbRole": o.role
        };

        newUsers.push(newUser);
    });

    // remove the existing user 
    state.MockPolicyUsersEditable = _.reject(state.MockPolicyUsersEditable, function (o) {
        return _.toString(o.IdentityId) === _.toString(req.params.identityId);
    });

    // add the updated user 
    _.forEach(newUsers, function (o) {
        state.MockPolicyUsersEditable.push(o);
    });

    var responseObject = newUsers;

    return responseObject;
};

// deleteUser 
exports.deleteUser = function (req) {
    // check if the identityId exists 
    var existingUser = _.filter(state.MockPolicyUsersEditable, function (o) {
        return _.toString(o.IdentityId) === _.toString(req.params.identityId);
        });

    if (!existingUser || existingUser.length == 0) return false;

    // remove the existing user 
    state.MockPolicyUsersEditable = _.reject(state.MockPolicyUsersEditable, function (o) {
        return _.toString(o.IdentityId) === _.toString(req.params.identityId);
    });

    return true;
};


// getTeam 
exports.getTeam = function (req) {
    var stateUsers = state.users;                       // these are users added through the /users/user (Add a new user) endpoint 
    var policyUsers = TEMPSTORE.MockPolicyUsersEditable;

    // Get user email address from the X-OktaTokenJson 
    var xOktaTokenJson = req.get("X-OktaTokenJson");
    xOktaTokenJson = xOktaTokenJson ? xOktaTokenJson : undefined;
    if (xOktaTokenJson === undefined) return false;

    var userPoliciesArr = [];

    // Get the policies associated with the current user (MockPolicyUsers) 
    var userPoliciesFromMockPolicyUsers = _.filter(policyUsers, function (o) {
        var isValid = _.toLower(o.Email) === _.toLower(xOktaTokenJson);
        if (isValid) userPoliciesArr.push(o.policyNumber);

        return isValid;
    });

    // Get all users from MockPolicyUsers that has access to the same policies 
    var fromMockPolicyUsers = _.filter(policyUsers, function (o) {
        return _.indexOf(userPoliciesArr, o.policyNumber) > -1;
    });

    // All users 
    var allUsers = [];

    _.forEach(fromMockPolicyUsers, function (o) {
        var user = {};
        user.givenName = o.FirstName;
        user.familyName = o.LastName;
        user.contactNumber = o.PhoneNumber;
        user.emailAddress = o.Email;
        user.role = o.DetailedAuthDbRole;
        user.identityId = o.IdentityId;
        allUsers.push(user);
    });

    // Sorted 
    var sortFilter = req.body.sort.columnName;
    var sortDirectionFilter = req.body.sort.direction;
    var sortedUsers = _.sortBy(allUsers, [function (o) { return o[sortFilter]; }]);         // ascending 
    if (sortDirectionFilter !== "asc") sortedUsers = _.reverse(sortedUsers);                // descending 

    // Paginated 
    var paginatedUsers = _.chunk(sortedUsers, req.body.pageSize);
    var targetPage = paginatedUsers[req.body.page - 1];

    // Response object 
    var responseObject = {};
    responseObject.totalResults = sortedUsers.length;
    responseObject.totalPages = paginatedUsers.length;
    responseObject.currentPage = req.body.page;
    responseObject.items = targetPage;

    return responseObject;
};

// getUserSummary
exports.getUserSummary = function (req) {
    var policyUsers = TEMPSTORE.MockPolicyUsers;

    // Get target userId from the X-Token 
    var xToken = req.get("X-Token");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    // check if the userId exists 
    var targetUser = _.find(policyUsers, function (o) {
        return _.toLower(_.toString(o.Email)) === _.toLower(_.toString(xToken));
    });

    if (!targetUser) return false;

    // Create UserSummary response object 
    var userSummary = {};
    userSummary.givenName = targetUser.FirstName;
    userSummary.familyName = targetUser.LastName;
    userSummary.userId = targetUser.Email;
    userSummary.numOpenNotificationsClaims = targetUser.numOpenNotificationsClaims;


    return userSummary;
}

// getPortalClaims 
exports.getPortalClaims = function (req) {
    var claimsList = TEMPSTORE.MockClaimSummary;
    var policyUsers = TEMPSTORE.MockPolicyUsers;

    // Get target userId from the X-Token 
    var xToken = req.get("X-Token");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var userPoliciesArr = [];
    var associatedClaimsPerPolicy = {};

    // Get the policies associated with the current user 
    var userPolicies = _.filter(policyUsers, function (o) {
        var isValid = _.toLower(o.Email) === _.toLower(xToken);
        if (isValid) {
            userPoliciesArr.push(o.policyNumber);
            associatedClaimsPerPolicy[o.policyNumber] = _.split(o.associatedClaims, ",");
            _.forEach(associatedClaimsPerPolicy[o.policyNumber], function (v, i) {
                associatedClaimsPerPolicy[o.policyNumber][i] = _.trim(v);
            });

        }
        return isValid;
    });

    // Claims list per user 
    var userClaimsList = [];
    _.forEach(userPoliciesArr, function (o, i) {
        var targetClaims = [];
        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] === "*") {
            targetClaims = _.filter(claimsList, function (v) {
                return o === v.policyNumber;
            });

            userClaimsList = _.concat(userClaimsList, targetClaims);
        }

        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] !== "*" && associatedClaimsPerPolicy[o][0] !== "null") {
            targetClaims = _.filter(claimsList, function (v) {
                return _.indexOf(associatedClaimsPerPolicy[o], v.claimNumber) > -1;
            });

            userClaimsList = _.concat(userClaimsList, targetClaims);
        }
    });

    // Filtered 
    var filteredClaims = userClaimsList;
    var policyNumberFilter = req.body.filter.policy ? req.body.filter.policy : undefined;
    var claimStatusFilterArr = req.body.filter.claimStatus ? req.body.filter.claimStatus : undefined;
    var claimStatusFilterArrLowerCased = _.map(claimStatusFilterArr, function (o) { return _.toLower(o); });
    var injuryDateFilter = req.body.filter.injuryDate ? req.body.filter.injuryDate : undefined;
    var expectedTimeOffWorkFilter = req.body.filter.expectedTimeOffWork ? req.body.filter.expectedTimeOffWork : undefined;

    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds

    filteredClaims = _.filter(userClaimsList, function (o) {
        // Assume first that the item is to be included 
        var returnValue = true;

        // If any of the filters has value and the value is not contained in the item, then do not include that item 

        // policy 
        if (policyNumberFilter !== undefined
            && policyNumberFilter !== "undefined"
            && o.policyNumber.indexOf(policyNumberFilter) === -1) returnValue = false;

        // claimStatus: ["open", "saved", "closed"] 
        if (claimStatusFilterArr !== undefined
            && claimStatusFilterArr !== "undefined"
            && claimStatusFilterArr.length > 0
            && _.indexOf(claimStatusFilterArrLowerCased, _.toLower(o.claimStatus)) === -1) returnValue = false;

        // injuryDate: "last30days" or "last60days" or "last365days"
        if (injuryDateFilter !== undefined && injuryDateFilter !== "undefined") {
            //var injuryDateFilterNum = parseInt(injuryDateFilter.replace("last", "").replace("days", ""), 10);
            var currentDate = new Date();
            var injuryDate = helpers.getDate(o.dateOfInjury);
            var diffDays = Math.round(Math.abs((currentDate.getTime() - injuryDate.getTime()) / (oneDay)));

            switch (injuryDateFilter) {
                case 'last30days':
                    if (diffDays > 30) returnValue = false;
                    break;

                case 'last60days':
                    if (diffDays > 60) returnValue = false;
                    break;

                case 'last365days':
                    if (diffDays > 365) returnValue = false;
                    break;
            }
        }

        // expectedTimeOffWork: ["0-2weeks", "2-4weeks", "4-6weeks", "6-8weeks", "8+weeks"] 
        if (expectedTimeOffWorkFilter !== undefined
            && expectedTimeOffWorkFilter !== "undefined"
            && expectedTimeOffWorkFilter.length > 0) {

            var isIncluded = [];
            _.forEach(expectedTimeOffWorkFilter, function (value, index) {

                var returnToWorkTimeframeNum = parseInt(o.returnToWorkTimeframe, 10);
                switch (value) {
                    case '0-2weeks':    // 0 - 14 days 
                        if (returnToWorkTimeframeNum <= 14) isIncluded.push(true);
                        break;

                    case '2-4weeks':    // 14 - 28 days 
                        if (returnToWorkTimeframeNum >= 14 && returnToWorkTimeframeNum <= 28) isIncluded.push(true);
                        break;

                    case '4-6weeks':    // 28 - 42 days 
                        if (returnToWorkTimeframeNum >= 28 && returnToWorkTimeframeNum <= 42) isIncluded.push(true);
                        break;

                    case '6-8weeks':    // 42 - 56 days 
                        if (returnToWorkTimeframeNum >= 42 && returnToWorkTimeframeNum <= 56) isIncluded.push(true);
                        break;

                    case '8+weeks':     // 56+ days 
                        if (returnToWorkTimeframeNum >= 56) isIncluded.push(true);
                        break;

                    default:
                        isIncluded.push(false);
                }
            });

            if (_.findIndex(isIncluded, function (z) { return z; }) === -1) returnValue = false;
        }

        return returnValue;
    });

    // Sorted 
    var sortFilter = req.body.sort.columnName;
    switch (sortFilter) {
        case 'recoveryTimeframe':
            sortFilter = "returnToWorkTimeframe";
            break;

        case 'lastName':
            sortFilter = "RowNumber";
            break;
    }
    var sortDirectionFilter = req.body.sort.direction;
    var sortedClaims = _.sortBy(filteredClaims, [function (o) {
        switch (sortFilter) {
            case 'dateOfInjury':
                return helpers.getDate(o[sortFilter]);
                break;
            default:
                return parseInt(o[sortFilter], 10);
                break;
        }
    }]);      // ascending 
    if (sortDirectionFilter !== "asc") sortedClaims = _.reverse(sortedClaims);                  // descending 

    // Paginated 
    var paginatedClaims = _.chunk(sortedClaims, req.body.pageSize);
    var targetPage = paginatedClaims[req.body.page - 1];

    // Response object 
    var responseObject = {};
    responseObject.totalResults = sortedClaims.length;
    responseObject.totalPages = paginatedClaims.length;
    responseObject.currentPage = req.body.page;
    responseObject.items = targetPage;

    return responseObject;
};

// getClaimByClaimId_deprecated 
exports.getClaimByClaimId_deprecated = function (req) {
    var claimNumber = req.params.id;

    // Get claimDetail from TEMPSTORE.MockClaimDetails given the claim number 
    var targetClaimDetails = _.find(TEMPSTORE.MockClaimDetails, function (o) {
        return o.data.attributes.claimNo === claimNumber;
    });
    if (!targetClaimDetails) return false;


    /* Generate output JSON in the schema as requested by FED */
    var output = { "claimOverviewSections": [] };
    var employerDetails = { "id": "employerDetails", "fields": [] };
    var injuredPersonDetails = { "id": "injuredPersonDetails", "fields": [] };
    var injuryDetails = { "id": "injuryDetails", "fields": [] };
    var injuredPersonWorkDetails = { "id": "injuredPersonWorkDetails", "fields": [] };
    var uploadSupportingDocuments = { "id": "uploadSupportingDocuments", "fields": [] };

    // Attach claimSummry data 
    var targetClaimSummary = _.find(TEMPSTORE.MockClaimSummary, function (o) {
        return o.claimNumber === claimNumber;
    });
    if (!targetClaimSummary) return false;
    output.claimNumber = targetClaimSummary.claimNumber;
    output.claimName = targetClaimSummary.claimName;
    output.policyNumber = targetClaimSummary.policyNumber;
    output.policyName = targetClaimSummary.policyName;
    output.claimStatus = targetClaimSummary.claimStatus;
    output.dateOfInjury = targetClaimSummary.dateOfInjury;
    output.expectedTimeOff = targetClaimSummary.returnToWorkTimeframe;
    output.liability = targetClaimSummary.liabilityStatus;
    output.certificateOfCapacityExpiry = targetClaimSummary.certCapacityExpires;
    output.wasIncident = targetClaimSummary.wasIncident;
    output.wasMedical = targetClaimSummary.wasMedical;
    output.wasWage = targetClaimSummary.wasWage;
    output.workStatusCode = targetClaimSummary.workStatusCode;
    output.workStatusStartDate = targetClaimSummary.workStatusStartDate;

    // Attach claimContacts data 
    var targetClaimContactIds = targetClaimSummary.relatedContacts;
    if (!targetClaimContactIds || targetClaimContactIds.split(",").length < 1) targetClaimDetails.data.attributes.claimContacts = null;
    targetClaimContactIds = targetClaimContactIds.split(",");
    var targetClaimContactArray = [];
    _.forEach(targetClaimContactIds, function (value) {
        var detail = _.find(TEMPSTORE.MockClaimContacts, function (o) {
            return o.contactId === value;
        });

        if (detail) targetClaimContactArray.push(detail);
    });
    output.claimContacts = targetClaimContactArray;

    // Employer Details 
    employerDetails.fields.push({
        "id": "employerRepresentativeFirstName",
        "value": targetClaimDetails.data.attributes.employer.representative.name.given
    });
    employerDetails.fields.push({
        "id": "employerRepresentativeLastName",
        "value": targetClaimDetails.data.attributes.employer.representative.name.family
    });
    employerDetails.fields.push({
        "id": "employerBestContactNumber",
        "value": targetClaimDetails.data.attributes.employer.representative.contact.phone.primary
    });
    employerDetails.fields.push({
        "id": "employerEmailAddress",
        "value": targetClaimDetails.data.attributes.employer.email
    });
    employerDetails.fields.push({
        "id": "injuryDate",
        "value": targetClaimDetails.data.attributes.injury.occurrence.date
    });
    employerDetails.fields.push({
        "id": "injuryTime",
        "value": targetClaimDetails.data.attributes.injury.occurrence.time
    });
    employerDetails.fields.push({
        "id": "associatedPolicy",
        "value": targetClaimDetails.data.attributes.employer.policyNo
    });
    employerDetails.fields.push({
        "id": "location",
        "value": targetClaimDetails.data.attributes.employer.suburb
    });
    employerDetails.fields.push({
        "id": "costCenter",
        "value": targetClaimDetails.data.attributes.employer.costCenter
    });
    employerDetails.fields.push({
        "id": "abn",
        "value": targetClaimDetails.data.attributes.employer.abn
    });
    employerDetails.fields.push({
        "id": "relationshipToInjuredPerson",
        "value": targetClaimDetails.data.attributes.employer.representative.contact.relationship.worker
    });
    employerDetails.fields.push({
        "id": "employerName",
        "value": targetClaimDetails.data.attributes.employer.name
    });
    employerDetails.fields.push({
        "id": "policyNo",
        "value": targetClaimDetails.data.attributes.employer.policyNo
    });
    employerDetails.fields.push({
        "id": "wicCode",
        "value": "WIC Code"
    });

    // Injured Person's Details 
    injuredPersonDetails.fields.push({
        "id": "workerFirstName",
        "value": targetClaimDetails.data.attributes.worker.name.given
    });
    injuredPersonDetails.fields.push({
        "id": "workerLastName",
        "value": targetClaimDetails.data.attributes.worker.name.family
    });
    injuredPersonDetails.fields.push({
        "id": "dateOfBirth",
        "value": targetClaimDetails.data.attributes.worker.dateOfBirth
    });
    injuredPersonDetails.fields.push({
        "id": "gender",
        "value": targetClaimDetails.data.attributes.worker.gender
    });
    injuredPersonDetails.fields.push({
        "id": "primaryPhone",
        "value": targetClaimDetails.data.attributes.worker.contact.phone.primary
    });
    injuredPersonDetails.fields.push({
        "id": "email",
        "value": targetClaimDetails.data.attributes.worker.contact.email
    });
    var homeAddress = targetClaimDetails.data.attributes.worker.address[0].addressLine1
        + " " + targetClaimDetails.data.attributes.worker.address[0].suburb
        + " " + targetClaimDetails.data.attributes.worker.address[0].city
        + " " + targetClaimDetails.data.attributes.worker.address[0].state
        + " " + targetClaimDetails.data.attributes.worker.address[0].postCode
        + " " + targetClaimDetails.data.attributes.worker.address[0].country;
    injuredPersonDetails.fields.push({
        "id": "homeAddress",
        "value": homeAddress
    });
    injuredPersonDetails.fields.push({
        "id": "sameAsResidential",
        "value": targetClaimDetails.data.attributes.worker.address[1].sameAsResidential
    });
    var postalAddress = targetClaimDetails.data.attributes.worker.address[1].addressLine1
        + " " + targetClaimDetails.data.attributes.worker.address[1].suburb
        + " " + targetClaimDetails.data.attributes.worker.address[1].city
        + " " + targetClaimDetails.data.attributes.worker.address[1].state
        + " " + targetClaimDetails.data.attributes.worker.address[1].postCode
        + " " + targetClaimDetails.data.attributes.worker.address[1].country;
    postalAddress = targetClaimDetails.data.attributes.worker.address[1].sameAsResidential ? homeAddress : postalAddress;
    injuredPersonDetails.fields.push({
        "id": "postalAddress",
        "value": postalAddress
    });
    injuredPersonDetails.fields.push({
        "id": "interpreterRequired",
        "value": targetClaimDetails.data.attributes.worker.interpreter.required
    });
    injuredPersonDetails.fields.push({
        "id": "languageForCommunication",
        "value": targetClaimDetails.data.attributes.worker.languageForCommunication
    });

    // Injury Details 
    injuryDetails.fields.push({
        "id": "injuryDate",
        "value": targetClaimDetails.data.attributes.injury.occurrence.date
    });
    injuryDetails.fields.push({
        "id": "injuryTime",
        "value": targetClaimDetails.data.attributes.injury.occurrence.time
    });
    injuryDetails.fields.push({
        "id": "multiple",
        "value": targetClaimDetails.data.attributes.injury.multiple
    });
    injuryDetails.fields.push({
        "id": "injuryWhatHow",
        "value": targetClaimDetails.data.attributes.injury.description
    });
    injuryDetails.fields.push({
        "id": "employerNotifiedOn",
        "value": targetClaimDetails.data.attributes.injury.employerNotifiedOn
    });
    injuryDetails.fields.push({
        "id": "medicalTreatment",
        "value": targetClaimDetails.data.attributes.injury.medicalTreatment.required
    });
    injuryDetails.fields.push({
        "id": "incidentOnly",
        "value": targetClaimSummary.wasIncident ? "Yes" : "No"
    });
    injuryDetails.fields.push({
        "id": "inTheCourseOfEmployment",
        "value": targetClaimDetails.data.attributes.injury.occurrence.inTheCourseOfEmployment
    });
    injuryDetails.fields.push({
        "id": "injuryDescription",
        "value": targetClaimDetails.data.attributes.injury.description
    });
    injuryDetails.fields.push({
        "id": "dutyStatusCode",
        "value": "Duty status code"
    });

    // Injured Person's Work Details 
    injuredPersonWorkDetails.fields.push({
        "id": "hasMultipleEmployments",
        "value": targetClaimDetails.data.attributes.worker.workDetail.hasMultipleEmployments
    });
    injuredPersonWorkDetails.fields.push({
        "id": "hadTimeOffWorkDueToInjury",
        "value": targetClaimDetails.data.attributes.worker.workDetail.hadTimeOffWorkDueToInjury
    });
    injuredPersonWorkDetails.fields.push({
        "id": "dateStoppedWork",
        "value": targetClaimDetails.data.attributes.worker.workDetail.dateStoppedWork
    });
    injuredPersonWorkDetails.fields.push({
        "id": "hasReturnedToWork",
        "value": targetClaimDetails.data.attributes.worker.workDetail.hasReturnedToWork
    });
    injuredPersonWorkDetails.fields.push({
        "id": "anticipatedLengthOffWork",
        "value": targetClaimDetails.data.attributes.worker.workDetail.anticipatedLengthOffWork
    });
    injuredPersonWorkDetails.fields.push({
        "id": "employmentStartDate",
        "value": targetClaimDetails.data.attributes.worker.workDetail.employmentStartDate
    });
    injuredPersonWorkDetails.fields.push({
        "id": "employmentType",
        "value": targetClaimDetails.data.attributes.worker.wageDetail.employmentType
    });
    injuredPersonWorkDetails.fields.push({
        "id": "daysWorking",
        "value": targetClaimDetails.data.attributes.worker.wageDetail.daysWorking
    });
    injuredPersonWorkDetails.fields.push({
        "id": "weeklyWage",
        "value": targetClaimDetails.data.attributes.worker.wageDetail.weeklyWage
    });
    injuredPersonWorkDetails.fields.push({
        "id": "weeklyHours",
        "value": targetClaimDetails.data.attributes.worker.wageDetail.weeklyHours
    });
    injuredPersonWorkDetails.fields.push({
        "id": "hasShiftAllowances",
        "value": targetClaimDetails.data.attributes.worker.wageDetail.hasShiftAllowances
    });
    injuredPersonWorkDetails.fields.push({
        "id": "weeklyShiftAllowance",
        "value": targetClaimDetails.data.attributes.worker.wageDetail.weeklyShiftAllowance
    });
    injuredPersonWorkDetails.fields.push({
        "id": "weeklyOvertimeEarnings",
        "value": targetClaimDetails.data.attributes.worker.wageDetail.weeklyOvertimeEarnings
    });
    injuredPersonWorkDetails.fields.push({
        "id": "leaveTakenInLastYear",
        "value": targetClaimDetails.data.attributes.worker.wageDetail.leaveTakenInLastYear
    });
    injuredPersonWorkDetails.fields.push({
        "id": "nonMonentaryAllowances",
        "value": targetClaimDetails.data.attributes.worker.wageDetail.nonMonentaryAllowances
    });
    injuredPersonWorkDetails.fields.push({
        "id": "occupation",
        "value": targetClaimDetails.data.attributes.worker.workDetail.occupation
    });

    // Upload Supporting Documents 
    uploadSupportingDocuments.fields.push({
        "id": "certificateOfCapacity",
        "value": targetClaimDetails.data.attributes.documents.occurrence.certificateOfCapacity
    });
    uploadSupportingDocuments.fields.push({
        "id": "medicalDetails",
        "value": targetClaimDetails.data.attributes.documents.occurrence.medicalDetails
    });
    uploadSupportingDocuments.fields.push({
        "id": "wageDetails",
        "value": targetClaimDetails.data.attributes.documents.occurrence.wageDetails
    });
    uploadSupportingDocuments.fields.push({
        "id": "other",
        "value": targetClaimDetails.data.attributes.documents.occurrence.other
    });

    // Remove fields with empty values 
    _.remove(employerDetails.fields, function (o) { typeof o.value === 'undefined' || o.value === null });
    _.remove(injuredPersonDetails.fields, function (o) { typeof o.value === 'undefined' || o.value === null });
    _.remove(injuryDetails.fields, function (o) { typeof o.value === 'undefined' || o.value === null });
    _.remove(injuredPersonWorkDetails.fields, function (o) { typeof o.value === 'undefined' || o.value === null });
    _.remove(uploadSupportingDocuments.fields, function (o) { typeof o.value === 'undefined' || o.value === null });

    // Attach the claimOverviewSections data 
    output.claimOverviewSections.push(employerDetails);
    output.claimOverviewSections.push(injuredPersonDetails);
    output.claimOverviewSections.push(injuryDetails);
    output.claimOverviewSections.push(injuredPersonWorkDetails);
    //output.claimOverviewSections.push(uploadSupportingDocuments);

    // Additional properties 
    output.injuryDescription = targetClaimDetails.data.attributes.injury.description;

    return output;
};

// getClaimByClaimId 
exports.getClaimByClaimId = function (req) {
    var claimNumber = req.params.id;

    // Get claimDetail from TEMPSTORE.MockClaimDetails given the claim number 
    var targetClaimDetails = _.find(TEMPSTORE.MockClaimDetails, function (o) {
        return o.data.attributes.claimNo === claimNumber;
    });
    if (!targetClaimDetails) return false;


    /* Generate output JSON */
    var output = targetClaimDetails;

    // Attach claimSummry data 
    var targetClaimSummary = _.find(TEMPSTORE.MockClaimSummary, function (o) {
        return o.claimNumber === claimNumber;
    });
    if (!targetClaimSummary) return false;
    output.data.attributes.claimStatus = targetClaimSummary.claimStatus;
    output.data.attributes.liabilityStatus = targetClaimSummary.liabilityStatus;
    output.data.attributes.certificateCapacityExpires = targetClaimSummary.certCapacityExpires;
    output.data.attributes.expectedTimeOffWork = targetClaimSummary.returnToWorkTimeframe;
    output.data.attributes.triageSegment = targetClaimSummary.triageSegment;
    output.data.attributes.wasIncident = targetClaimSummary.wasIncident;
    output.data.attributes.wasMedical = targetClaimSummary.wasMedical;
    output.data.attributes.wasWage = targetClaimSummary.wasWage;

    // Attach claimContacts data 
    var targetClaimContactIds = targetClaimSummary.relatedContacts;
    if (!targetClaimContactIds || targetClaimContactIds.split(",").length < 1) targetClaimDetails.data.attributes.claimContacts = null;
    targetClaimContactIds = targetClaimContactIds.split(",");
    var targetClaimContactArray = [];
    _.forEach(targetClaimContactIds, function (value) {
        var detail = _.find(TEMPSTORE.MockClaimContacts, function (o) {
            return o.contactId === value;
        });

        if (detail) targetClaimContactArray.push(detail);
    });
    output.data.attributes.claimContacts = targetClaimContactArray;

    return output;
};

// enquiryAgainstClaimByClaimId 
exports.enquiryAgainstClaimByClaimId = function (req) {
    // check if the claimNo exists 
    var existingClaim = _.find(state.AddUpdateClaimRepo, function (o) {
        return _.toString(o.data.attributes.claimNo) === _.toString(req.params.id);
    });

    if (!existingClaim) {
        existingClaim = _.find(TEMPSTORE.MockClaimDetails, function (o) {
            return _.toString(o.data.attributes.claimNo) === _.toString(req.params.id);
        });
    }

    if (!existingClaim) return false;

    // modify input to add an enquiry ID (Enquiry-{claimNo})
    var input = req.body;
    input.data.attributes.enquiry.problemCode = "Enquiry-" + existingClaim.data.attributes.claimNo;

    // add enquiry to repo 
    state.EnquiryAgainstClaimRepo.push(input);

    var responseObject = { description: SUCCESS.Success202b };
    return responseObject;
};

// getClaimsDocuments 
exports.getClaimsDocuments = function (req) {
    var claimDocuments = TEMPSTORE.MockDocuments;
    var policyUsers = TEMPSTORE.MockPolicyUsers;

    // Get target userId from the X-Token 
    var xToken = req.get("X-Token");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var userPoliciesArr = [];
    var associatedClaimsPerPolicy = {};

    // Get the policies associated with the current user 
    var userPolicies = _.filter(policyUsers, function (o) {
        var isValid = _.toLower(o.Email) === _.toLower(xToken);
        if (isValid) {
            userPoliciesArr.push(o.policyNumber);
            associatedClaimsPerPolicy[o.policyNumber] = _.split(o.associatedClaims, ",");
            _.forEach(associatedClaimsPerPolicy[o.policyNumber], function (v, i) {
                associatedClaimsPerPolicy[o.policyNumber][i] = _.trim(v);
            });

        }
        return isValid;
    });

    // Claims list per user 
    var claimDocumentsList = [];
    _.forEach(userPoliciesArr, function (o, i) {
        var targetDocuments = [];
        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] === "*") {
            targetDocuments = _.filter(claimDocuments, function (v) {
                return v.claimNumber === req.params.id;
            });

            claimDocumentsList = _.concat(claimDocumentsList, targetDocuments);
        }

        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] !== "*" && associatedClaimsPerPolicy[o][0] !== "null") {
            targetDocuments = _.filter(claimDocuments, function (v) {
                return _.indexOf(associatedClaimsPerPolicy[o], v.claimNumber) > -1;
            });

            claimDocumentsList = _.concat(claimDocumentsList, targetDocuments);
        }
    });

    if (_.size(claimDocumentsList) === 0) return false;

    var responseObject = {
        "data": {
            "type": "documents",
            "id": req.params.id,
            "attributes": {
                "occurrence": {
                    "certificateOfCapacity": [],
                    "medicalPayments": [],
                    "weeklyPayments": [],
                    "healthAndRecovery": [],
                    "other": []
                }
            }
        }
    };

    _.forEach(claimDocumentsList, function (document) {
        var docToPush = {
            "documentId": document.documentId,
            "url": document.url,
            "title": document.title,
            "fileSize": document.fileSize,
            "fileType": document.fileType,
            "uploadedDate": document.uploadedDate
        };

        switch (document.documentType) {
            case 'certCapacity':
                docToPush.certificateOfCapacityDate = document.certCapacityDate;
                responseObject.data.attributes.occurrence.certificateOfCapacity.push(docToPush);
                break;
            case 'medicalPayment':
                responseObject.data.attributes.occurrence.medicalPayments.push(docToPush);
                break;
            case 'weeklyPayment':
                responseObject.data.attributes.occurrence.weeklyPayments.push(docToPush);
                break;
            case 'healthRecovery':
                responseObject.data.attributes.occurrence.healthAndRecovery.push(docToPush);
                break;
            case 'other':
                responseObject.data.attributes.occurrence.other.push(docToPush);
                break;
        }
    });

    // Remove fields with empty values 
    if (_.size(responseObject.data.attributes.occurrence.certificateOfCapacity) === 0)
        delete responseObject.data.attributes.occurrence.certificateOfCapacity;
    if (_.size(responseObject.data.attributes.occurrence.medicalPayments) === 0)
        delete responseObject.data.attributes.occurrence.medicalPayments;
    if (_.size(responseObject.data.attributes.occurrence.weeklyPayments) === 0)
        delete responseObject.data.attributes.occurrence.weeklyPayments;
    if (_.size(responseObject.data.attributes.occurrence.healthAndRecovery) === 0)
        delete responseObject.data.attributes.occurrence.healthAndRecovery;
    if (_.size(responseObject.data.attributes.occurrence.other) === 0)
        delete responseObject.data.attributes.occurrence.other;

    return responseObject;
};

// updateClaimDocumentsById
exports.updateClaimDocumentsById = function (req) {
    var claimDocuments = TEMPSTORE.MockDocuments;
    var input = req.body;
    var claimNumber = _.toString(req.params.id);

    // check if the claimNo exists 
    var existingClaimDocuments = _.filter(claimDocuments, function (o) {
        return _.toString(o.claimNumber) === _.toString(req.params.id);
    });

    if (_.size(existingClaimDocuments) === 0) return false;


    // Update the documents
    // TODO: Not needed now 

    var responseObject = { description: SUCCESS.Success204 };
    return responseObject;
};


// getDuplicateClaim 
exports.getDuplicateClaim = function (req) {
    var mockClaimDetails = TEMPSTORE.MockClaimDetails;
    var mockPolicyUsers = TEMPSTORE.MockPolicyUsers;

    // Get target userId from the X-Token 
    var xToken = req.get("X-Token");
    xToken = xToken ? xToken : undefined;
    if (xToken === undefined) return false;

    var userPoliciesArr = [];
    var associatedClaimsPerPolicy = {};

    // Get the policies associated with the current user 
    var userPolicies = _.filter(mockPolicyUsers, function (o) {
        var isValid = _.toLower(o.Email) === _.toLower(xToken);
        if (isValid) {
            userPoliciesArr.push(o.policyNumber);
            associatedClaimsPerPolicy[o.policyNumber] = _.split(o.associatedClaims, ",");
            _.forEach(associatedClaimsPerPolicy[o.policyNumber], function (v, i) {
                associatedClaimsPerPolicy[o.policyNumber][i] = _.trim(v);
            });

        }
        return isValid;
    });

    // Claims list per user 
    var userClaimsList = [];
    _.forEach(userPoliciesArr, function (o, i) {
        var targetClaims = [];
        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] === "*") {
            targetClaims = _.filter(mockClaimDetails, function (v) {
                return o === v.data.attributes.employer.policyNo;
            });

            userClaimsList = _.concat(userClaimsList, targetClaims);
        }

        if (associatedClaimsPerPolicy[o][0] && associatedClaimsPerPolicy[o][0] !== "*" && associatedClaimsPerPolicy[o][0] !== "null") {
            targetClaims = _.filter(mockClaimDetails, function (v) {
                return _.indexOf(associatedClaimsPerPolicy[o], v.data.attributes.claimNo) > -1;
            });

            userClaimsList = _.concat(userClaimsList, targetClaims);
        }
    });

    // Filtered 
    var filteredClaims = userClaimsList;
    var workerGivenNameFilter = req.body.data.attributes.worker.name.given ? req.body.data.attributes.worker.name.given : undefined;
    var workerFamilyNameFilter = req.body.data.attributes.worker.name.family ? req.body.data.attributes.worker.name.family : undefined;
    var injuryDateFilter = req.body.data.attributes.injury.date ? req.body.data.attributes.injury.date : undefined;

    filteredClaims = _.filter(userClaimsList, function (o) {
        // Assume first that the item is to be included 
        var returnValue = true;

        // If any of the filters has value and the value is not contained in the item, then do not include that item 

        // family name 
        if (workerFamilyNameFilter !== undefined && workerFamilyNameFilter !== "undefined") {
            if(_.toLower(workerFamilyNameFilter) !== _.toLower(o.data.attributes.worker.name.family)) return false;
        }

        // given name 
        if (workerGivenNameFilter !== undefined && workerGivenNameFilter !== "undefined") {
            if(_.toLower(workerGivenNameFilter) !== _.toLower(o.data.attributes.worker.name.given)) return false;
        }

        // injuryDate 
        if (injuryDateFilter !== undefined && injuryDateFilter !== "undefined") {
            if(_.toLower(injuryDateFilter) !== _.toLower(o.data.attributes.injury.occurrence.date)) return false;
        }

        return returnValue;
    });

    var duplicateClaims = _.map(filteredClaims, 'data.attributes.claimNo');

    var responseObject = {
        "data": {
            "type": "string",
            "id": "string",
            "attributes": {
                "claims": duplicateClaims
            }
        }
    };

    return responseObject;
}