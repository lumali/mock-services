﻿/*
 * Constants
 *
 */

exports.errors = function () {
    var errors = [];

    errors.Error400 = "Request is invalid";
    errors.Error401 = "Requester is not authorized to perform this action";
    errors.Error403 = "The request is understood, but it has been refused or access is not allowed";
    errors.Error404 = "The URI requested is invalid or the resource requested does not exist. Refer to the accompanying error message for further details.";
    errors.Error500 = "Internal Server Error";
    errors.Error502 = "Bad Gateway. Problem communicating with or an error has been returned from a downstream endpoint.";
    errors.Error504 = "Gateway Timeout. Timeout communicating with a downstream endpoint";

    errors.Error400b = "Invalid claim id value";

    return errors;
};

exports.successResponses = function () {
    var successResponses = [];

    successResponses.Success200 = "Successful";
    successResponses.Success201 = "Success";
    successResponses.Success204 = "Details of the claim in progress are updated.";
    successResponses.Success202 = "Triggered claim process is lodged successfully.";
    successResponses.Success202b = "Claim enquiry lodged successfully.";

    return successResponses;
};

