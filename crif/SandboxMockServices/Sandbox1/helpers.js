/*
 * Common functions 
 *
 */

// returnError 
exports.returnError = function (res, code, message) {
    return res.json(code, { error: { 'message': message } });
};

// validTrackingIdHeader
exports.validTrackingIdHeader = function (req) {
    var xTrackingId = req.get("X-TrackingID");
    xTrackingId = xTrackingId ? xTrackingId : undefined;

    return (xTrackingId !== undefined);
};

// validOktaTokenJsonHeader
exports.validOktaTokenJsonHeader = function (req) {
    var xOktaTokenJson = req.get("X-OktaTokenJson");
    xOktaTokenJson = xOktaTokenJson ? xOktaTokenJson : undefined;

    if (xOktaTokenJson === undefined) return false;

    // JWT: header.payload.signature
    var jwtArray = xOktaTokenJson.split('.');
    if (jwtArray.length < 3) return false;
    if (!jwtArray[0]) return false;
    if (!jwtArray[1]) return false;
    if (!jwtArray[2]) return false;

    // return user id from the payload 
    return jwtArray[1];
};

// validQueryParam
exports.validQueryParam = function (req, q) {
    var input = req.query[q] ? req.query[q] : undefined;
    if (input === undefined) return false;
    return true;
};

// validPathParam 
exports.validPathParam = function (req, p, shouldTestForInt) {
    var params = req.params ? req.params : undefined;
    if (params === undefined) return false;

    var targetParam = params[p] ? params[p] : undefined;
    if (targetParam === undefined) return false;

    if (shouldTestForInt) {
        var intParam = parseInt(targetParam, 10);
        return _.isInteger(intParam);
    }

    return true;
};

// getDate
exports.getDate = function (dateString) {
    var date = dateString.split(/\D/);
    return new Date(date[2], date[1] - 1, date[0]);
};

// hasOneDayElapsed 
exports.hasOneDayElapsed = function (previousDate) {
    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    var currentDate = new Date();
    previousDate = new Date(previousDate);
    var diffDays = Math.round(Math.abs((currentDate.getTime() - previousDate.getTime()) / (oneDay)));

    return diffDays >= 1;
};

// getProducts
exports.getProducts = function (projectValues) {
    var sumInsured = _.replace(projectValues.contractWorks.sumInsured, "$", "");
    var sumInsuredInt = parseInt(sumInsured, 10);

    // premium 
    var contractWorksPremium = _.round(sumInsuredInt * 0.50, 2);
    var liabilitiesPremium = _.round(sumInsuredInt * 0.25, 2);
    var professionalIndeminityPremium = _.round(sumInsuredInt * 0.20, 2);
    var totalPremium = _.round(contractWorksPremium + liabilitiesPremium + professionalIndeminityPremium, 2);

    // gst
    var contractWorksGst = _.round((sumInsuredInt / 2) * 0.10, 2);
    var liabilitiesGst = _.round(sumInsuredInt * 0.25 * 0.10, 2);
    var professionalIndeminityGst = _.round(sumInsuredInt * 0.020, 2);
    var totalGst = _.round(contractWorksGst + liabilitiesGst + professionalIndeminityGst, 2);
    
    var products = {
        sumInsuredInt: sumInsuredInt,
        output: {}
    };

    products.output = {
        "contractWorks": {
            "values": {
                "premium": contractWorksPremium,
                "gst": contractWorksGst,
                "total": (contractWorksPremium + contractWorksGst)
            },
            "terms": {
                "sumInsured": projectValues.contractWorks.sumInsured,
                "existingProperty": projectValues.contractWorks.existingProperty,
                "policyExcess": projectValues.contractWorks.policyExcess,
                "leg3Excess": projectValues.contractWorks.leg3Excess
            }
        },
        "publicImpairmentLiability": {
            "values": {
                "premium": liabilitiesPremium,
                "gst": liabilitiesGst,
                "total": (liabilitiesPremium + liabilitiesGst)
            },
            "terms": {
                "liabilityLimit": projectValues.liabilities.publicAndProductLiabilityLimit,
                "environmentImpairmentLiabilityLimit": projectValues.liabilities.environmentImpairmentLiabilityLimit,
                "publicLiabilityExcess": projectValues.liabilities.publicLiabilityExcess,
                "workerExcess": projectValues.liabilities.workerExcess
            }
        },
        "professionalIndeminity": {
            "values": {
                "premium": professionalIndeminityPremium,
                "gst": professionalIndeminityGst,
                "total": (professionalIndeminityPremium + professionalIndeminityGst)
            },
            "terms": {
                "liabilityLimit": projectValues.professionalIndeminity.liabilityLimit,
                "indemnityExcess": projectValues.professionalIndeminity.indemnityExcess
            }
        },
        "totalPremiumValue": {
            "values": {
                "premium": totalPremium,
                "gst": totalGst,
                "total": _.round((totalPremium + totalGst), 2)
            }
        }
    };

    return products;
};

// getReferralCodes
exports.getReferralCodes = function (projectType, contaminatedLandRegistered, sumInsured) {
    var referralCodes = [];
    //{
    //"referralType": "Project Type",
    //"referralCode": "1",
    //"description": ""
    //},
    //{
    //"referralType": "Contaminated Land",
    //"referralCode": "2",
    //"description": ""
    //},
    //{
    //"referralType": "Sums Insured",
    //"referralCode": "3",
    //"description": ""
    //},
    //{
    //"referralType": "Other",
    //"referralCode": "4",
    //"description": ""
    //}

    // Referral - Project type that falls into the referral class
    var referralProjectTypes = [
        "Airports - Terminal and Associated Buildings",
        "Airports - Runway and Airside",
        "Mining",
        "Offshore > 500m from Land",
        "Pipelines - Offshore New",
        "Pipelines - Offshore - Maintenance",
        "Power Generation - Prototype/Experimental Works",
        "Other - Not Classified"
    ];
    var referralProjectTypesLowerCased = _.map(referralProjectTypes, function (o) { return _.toLower(o) });
    if (_.indexOf(referralProjectTypesLowerCased, _.toLower(projectType)) > -1) {
        referralCodes.push({
            "referralType": "Project Type",
            "referralCode": "1",
            "description": ""
        });
    }

    // Referral - Project being listed on Contaminated Land
    if (contaminatedLandRegistered) {
        referralCodes.push({
            "referralType": "Contaminated Land",
            "referralCode": "2",
            "description": ""
        });
    }

    // Referral - Sums insured exceeding $100 million
    if (sumInsured > 100000000) {
        referralCodes.push({
            "referralType": "Sums Insured",
            "referralCode": "3",
            "description": ""
        });
    }

    return referralCodes;
}


/*
 * Other functions 
 *
 */

// getOperationalReports
exports.getOperationalReports = function (req) {
    var responseObject = {
        "data": {
            "type": "OperationalReportsDownloadUrl",
            "id": "1",
            "attributes": {
                "url": "https://dummyimage.com/181x193.bmp/cc0000/ffffff%20from%20DOC_05"
            }
        }
    };

    return responseObject;
};

// getAgencies
exports.getAgencies = function (req) {
    var mockUsers = TEMPSTORE.MockUsers;
    var mockAgencies = TEMPSTORE.MockAgencies;

    // Get target user from the payload section of X-OktaTokenJson 
    var userId = helpers.validOktaTokenJsonHeader(req);
    if (!userId) return false;

    // Get user's associated agencies 
    var targetMockUser = _.find(mockUsers, function (o) {
        return _.toString(userId) === _.toString(o.id);
    });

    if (!targetMockUser) return false;

    var associatedAgencies = targetMockUser.associatedAgencies;
    var associatedAgenciesArray = _.split(associatedAgencies, ",");
    _.forEach(associatedAgenciesArray, function (v, i) {
        associatedAgenciesArray[i] = _.trim(v);
    });

    // Construct object containing the user's associated agencies 
    var responseObject = {
        "data": {
            "type": "AgencyName",
            "id": "AgencyName",
            "attributes": {
                "items": []
            }
        }
    };

    if (associatedAgenciesArray[0] === "*") {
        responseObject.data.attributes.items = mockAgencies;

        return responseObject;
    }

    _.forEach(associatedAgenciesArray, function (a) {
        var targetAgency = _.find(mockAgencies, function (m) {
            return a === m.id;
        });

        if (targetAgency) {
            responseObject.data.attributes.items.push(targetAgency);
        }
    });

    return responseObject;
};

// getReferenceData
exports.getReferenceData = function (req) {
    var responseObject = {
        "data": {
            "type": "ReferenceData",
            "id": "ReferenceData",
            "attributes": {
                "projectType": [
                    {
                        "name": "Airports - Runway and Airside",
                        "classification": "refer"
                    },
                    {
                        "name": "Airports - Terminal and Associated Buildings",
                        "classification": "class_1"
                    },
                    {
                        "name": "Breakwater",
                        "classification": "class_3"
                    },
                    {
                        "name": "Bridge",
                        "classification": "class_2"
                    },
                    {
                        "name": "Canal",
                        "classification": "class_3"
                    },
                    {
                        "name": "Dam/ Weirs",
                        "classification": "class_3"
                    },
                    {
                        "name": "Desalination plant",
                        "classification": "class_2"
                    },
                    {
                        "name": "Earthworks",
                        "classification": "class_2"
                    },
                    {
                        "name": "Entertainment Venue - Stadium, Sports Oval, Indoor Arena",
                        "classification": "class_1"
                    },
                    {
                        "name": "Fire Suppression Systems",
                        "classification": "class_1"
                    },
                    {
                        "name": "Footpaths, Parks, Community Recreation Areas",
                        "classification": "class_2"
                    },
                    {
                        "name": "Fresh/ Waste and Storm Water Networks",
                        "classification": "class_2"
                    },
                    {
                        "name": "Harbour Upgrade Works",
                        "classification": "class_3"
                    },
                    {
                        "name": "Hospital",
                        "classification": "class_1"
                    },
                    {
                        "name": "Housing Development",
                        "classification": "class_1"
                    },
                    {
                        "name": "Land Reclamation",
                        "classification": "class_3"
                    },
                    {
                        "name": "Landscaping",
                        "classification": "class_1"
                    },
                    {
                        "name": "Maintenance",
                        "classification": "class_1"
                    },
                    {
                        "name": "Microtunnelling/ Horizontal Directional Drilling",
                        "classification": "class_2"
                    },
                    {
                        "name": "Mining",
                        "classification": "refer"
                    },
                    {
                        "name": "New Building Construction =< 3 Stories",
                        "classification": "class_1"
                    },
                    {
                        "name": "New Building Construction > 3 Stories",
                        "classification": "class_1"
                    },
                    {
                        "name": "Offshore =< 500m from Land",
                        "classification": "class_3"
                    },
                    {
                        "name": "Offshore > 500m from Land",
                        "classification": "refer"
                    },
                    {
                        "name": "Other - Not Classified",
                        "classification": "refer"
                    },
                    {
                        "name": "Pipelines - Offshore - Maintenance",
                        "classification": "refer"
                    },
                    {
                        "name": "Pipelines - Offshore New",
                        "classification": "refer"
                    },
                    {
                        "name": "Pipelines - Onshore - Maintenance",
                        "classification": "class_2"
                    },
                    {
                        "name": "Pipelines - Onshore New",
                        "classification": "class_2"
                    },
                    {
                        "name": "Port Works",
                        "classification": "class_3"
                    },
                    {
                        "name": "Power - Substations",
                        "classification": "class_2"
                    },
                    {
                        "name": "Power - Transmissions Lines",
                        "classification": "class_2"
                    },
                    {
                        "name": "Power Generation - Prototype/ Experimental Works",
                        "classification": "refer"
                    },
                    {
                        "name": "Power Generation =< 25 MW Capacity",
                        "classification": "class_3"
                    },
                    {
                        "name": "Power Generation > 25 MW Capacity",
                        "classification": "class_3"
                    },
                    {
                        "name": "Pre Project Investigation",
                        "classification": "class_1"
                    },
                    {
                        "name": "Rail Work - Excluding Track Work and Signalling",
                        "classification": "class_2"
                    },
                    {
                        "name": "Rail Work - Including Track Work and Signalling",
                        "classification": "class_3"
                    },
                    {
                        "name": "Rail monitoring and testing works",
                        "classification": "class_2"
                    },
                    {
                        "name": "Refurbishment of Existing Building/s",
                        "classification": "class_1"
                    },
                    {
                        "name": "Road Construction =< 3 Lanes (Suburban etc.)",
                        "classification": "class_2"
                    },
                    {
                        "name": "Road Construction > 3 Lanes (Motorway etc.)",
                        "classification": "class_2"
                    },
                    {
                        "name": "Road Construction including Tunnel",
                        "classification": "class_3"
                    },
                    {
                        "name": "Road/ Rail/ Pedestrian Tunnel - New",
                        "classification": "class_3"
                    },
                    {
                        "name": "Road/ Rail/ Pedestrian Tunnel - New (Cut and Cover)",
                        "classification": "class_2"
                    },
                    {
                        "name": "Road/ Rail/ Pedestrian Tunnel - Works within Existing Tunnel",
                        "classification": "class_2"
                    },
                    {
                        "name": "School",
                        "classification": "class_1"
                    },
                    {
                        "name": "Shopping Centre",
                        "classification": "class_1"
                    },
                    {
                        "name": "Test Project Type-Refer",
                        "classification": "class_2"
                    },
                    {
                        "name": "Test-PostEffectiveDate",
                        "classification": "class_1"
                    },
                    {
                        "name": "Waste Recyling Plant",
                        "classification": "class_2"
                    },
                    {
                        "name": "Waste water treatment Plant",
                        "classification": "class_3"
                    },
                    {
                        "name": "Water treatment plant",
                        "classification": "class_3"
                    },
                    {
                        "name": "Wharf Construction",
                        "classification": "class_3"
                    }
                ],
                "testingPeriod": [
                    "0",
                    "1",
                    "2",
                    "3",
                    "other"
                ],
                "completionPeriod": [
                    "6",
                    "12",
                    "24",
                    "36",
                    "48",
                    "60",
                    "other"
                ]
            }
        }
    };

    return responseObject;
};

// getQuotesPoliciesForAgency 
exports.getQuotesPoliciesForAgency = function (req) {
    var mockPolicyDataDetailsEditable = state.MockPolicyDataDetailsEditable;

    var filteredItems = _.map(mockPolicyDataDetailsEditable, function (o) {
        return o.data;
    });

    // Filter 
    if (req.body.data.attributes.criteria) {
        var agencyIdFilter = req.body.data.attributes.criteria.agencyId ? req.body.data.attributes.criteria.agencyId : undefined;
        var projectNameFilter = req.body.data.attributes.criteria.projectName ? req.body.data.attributes.criteria.projectName : undefined;
        var policyNumberFilter = req.body.data.attributes.criteria.policyNumber ? req.body.data.attributes.criteria.policyNumber : undefined;
        var contractNumberFilter = req.body.data.attributes.criteria.contractNumber ? req.body.data.attributes.criteria.contractNumber : undefined;
        var statusFilterArr = req.body.data.attributes.criteria.status ? req.body.data.attributes.criteria.status : undefined;
        var statusFilterArrLowerCased = _.map(statusFilterArr, function (o) { return _.toLower(o); });

        filteredItems = _.filter(filteredItems, function (o) {
            // Assume first that the item is to be included 
            var returnValue = true;

            // Add logic below for the scenarios that will exclude the item 

            // agencyId (exact match) 
            if (agencyIdFilter !== undefined
                && agencyIdFilter !== "undefined"
                && o.attributes.agencyId !== agencyIdFilter) returnValue = false;

            // projectName (partial match)
            if (projectNameFilter !== undefined
                && projectNameFilter !== "undefined"
                && _.toLower(o.attributes.projectName).indexOf(_.toLower(projectNameFilter)) === -1) returnValue = false;

            // policyNumber (exact match) 
            if (policyNumberFilter !== undefined
                && policyNumberFilter !== "undefined"
                && o.id !== policyNumberFilter) returnValue = false;

            // contractNumber (exact match) 
            if (contractNumberFilter !== undefined
                && contractNumberFilter !== "undefined"
                && o.attributes.governmentContractNumber !== contractNumberFilter) returnValue = false;

            // status: ["quote", "referral", "active", "expired", "cancelled"] 
            if (statusFilterArr !== undefined
                && statusFilterArr !== "undefined"
                && statusFilterArr.length > 0
                && _.indexOf(statusFilterArrLowerCased, _.toLower(o.attributes.status)) === -1) returnValue = false;

            return returnValue;
        });
    }

    // Reduce each item to an object containing only the required data for the response 
    var reducedFilteredItems = _.map(filteredItems, function (d) {
        return {
            "id": d.id,
            "agencyId": d.attributes.agencyId,
            "projectName": d.attributes.projectName,
            "governmentContractNumber": d.attributes.governmentContractNumber,
            "policyInceptionDate": d.attributes.riskDates.coverInceptionDate,
            "policyExpiryDate": d.attributes.riskDates.constructionEndDate,
            "createdDate": d.attributes.createdDate,
            "status": d.attributes.status
        };
    });

    // Status Count 
    var statusCounts = _.countBy(reducedFilteredItems, function (o) { return o.status });

    // Sort 
    //var sortFilter = req.body.data.attributes.params.orderBy.fieldName;
    var sortDirectionFilter = req.body.data.attributes.params.orderBy.direction;
    var sortedItems = _.sortBy(reducedFilteredItems, [function (o) {
        return (o.createdDate && helpers.getDate(o.createdDate));
    }]);      // ascending 
    if (sortDirectionFilter !== "ascending") sortedItems = _.reverse(sortedItems);  // descending 

    // Paginate 
    var paginatedItems = _.chunk(sortedItems, req.body.data.attributes.params.pageSize);
    var targetPage = paginatedItems[req.body.data.attributes.params.page - 1];

    // Response object 
    var responseObject = {
        "meta": {
            "totalRecords": sortedItems.length,
            "totalPages": paginatedItems.length,
            "currentPage": req.body.data.attributes.params.page
        },
        "data": {
            "type": "GlobalSearch",
            "id": "GlobalSearch",
            "attributes": {
                "quotesCount": statusCounts.Quote,
                "referralCount": statusCounts.Referral,
                "activePolicies": statusCounts.Active,
                "expiredPolicies": statusCounts.Expired,
                "cancelledPolicies": statusCounts.Cancelled,
                "items": targetPage
            }
        }
    };

    return responseObject;
};



// getProjectValues 
exports.getProjectValues = function (req) {
    var input = req.body;
    var agencyId = input.data.attributes.agencyId;
    var sumsInsured = input.data.attributes.sumsInsured;
    var sumsInsuredInt = parseInt(sumsInsured, 10);

    var responseObject = {
        "data": {
            "type": "ProjectValue",
            "id": "ProjectValue",
            "attributes": {
                "agencyId": agencyId,
                "projectValues": {
                    "contractWorks": {
                        "policyExcess": {
                            "values": [_.toString(sumsInsuredInt * 0.025), _.toString(sumsInsuredInt * 0.025 / 2), _.toString(sumsInsuredInt * 0.025 / 4)]
                        },
                        "leg3Excess": {
                            "values": [_.toString(sumsInsuredInt * 0.25), _.toString(sumsInsuredInt * 0.25 / 2), _.toString(sumsInsuredInt * 0.25 / 4)]
                        },
                        "existingProperty": {
                            "values": ["Insured elsewhere", "Existing Property 1", "Existing Property 2"]
                        }
                    },
                    "liabilities": {
                        "publicAndProductLiabilityLimit": {
                            "values": [_.toString(sumsInsuredInt), _.toString(sumsInsuredInt / 2), _.toString(sumsInsuredInt / 4)]
                        },
                        "environmentImpairmentLiabilityLimit": {
                            "values": [_.toString(sumsInsuredInt), _.toString(sumsInsuredInt / 2), _.toString(sumsInsuredInt / 4)]
                        },
                        "publicLiabilityExcess": {
                            "values": [_.toString(sumsInsuredInt * 0.050), _.toString(sumsInsuredInt * 0.050 / 2), _.toString(sumsInsuredInt * 0.050 / 4)]
                        },
                        "workerExcess": {
                            "values": [_.toString(sumsInsuredInt * 0.050), _.toString(sumsInsuredInt * 0.050 / 2), _.toString(sumsInsuredInt * 0.050 / 4)]
                        }
                    },
                    "professionalIndeminity": {
                        "liabilityLimit": {
                            "values": [_.toString(sumsInsuredInt * 0.050), _.toString(sumsInsuredInt * 0.050 / 2), _.toString(sumsInsuredInt * 0.050 / 4)]
                        },
                        "indemnityExcess": {
                            "values": [_.toString(sumsInsuredInt * 0.025), _.toString(sumsInsuredInt * 0.025 / 2), _.toString(sumsInsuredInt * 0.025 / 4)]
                        }
                    }
                }
            }
        }
    };

    return responseObject;
};

// createQuote
exports.createQuote = function (req, lossesReported) {
    var input = req.body;
    var responseObject = input;

    // Generate a 7-digit random quote number 
    var quoteNo = _.floor(Math.random() * 9000000) + 1000000;
    quoteNo = _.replace(quoteNo, ".0", "");

    // quote id
    var quoteId = "J" + quoteNo;

    // createdDate 
    var today = new Date().toISOString();
    //var dd = today.getDate();
    //var mm = today.getMonth() + 1; //January is 0!
    //var yyyy = today.getFullYear();
    //if (dd < 10) {
    //    dd = '0' + dd;
    //}
    //if (mm < 10) {
    //    mm = '0' + mm;
    //}
    //var today = dd + '-' + mm + '-' + yyyy;

    // get products if projectValues exists 
    var products;
    if (input.data.attributes.projectValues) {
        products = helpers.getProducts(input.data.attributes.projectValues);
    }
    if (products) {
        responseObject.data.attributes.products = products.output;
    }

    //// remove projectValues 
    //if (responseObject.data.attributes.projectValues) {
    //    _.unset(responseObject, 'data.attributes.projectValues');
    //}

    // set quote values 
    responseObject.data.id = quoteId;
    responseObject.data.type = "Quote";
    responseObject.data.attributes.status = "quote";
    responseObject.data.attributes.createdDate = today;

    // Referral - Project type that falls into the referral class
    var projectType = input.data.attributes.projectType ? input.data.attributes.projectType : "";
    // Referral - Project being listed on Contaminated Land
    var contaminatedLandRegistered = input.data.attributes.contaminatedLandRegistered;
    // Referral - Sums insured exceeding $100 million
    var sumInsured = "";
    var sumInsuredInt = "";
    if (input.data.attributes.projectValues) {
        sumInsured = _.replace(input.data.attributes.projectValues.contractWorks.sumInsured, "$", "");
        sumInsuredInt = parseInt(sumInsured, 10);
    } else if (input.data.attributes.products) {
        sumInsuredInt = products.sumInsuredInt;
    }
    // if Referral 
    var referralCodes = helpers.getReferralCodes(projectType, contaminatedLandRegistered, sumInsuredInt);
    if (referralCodes.length > 0) {
        responseObject.data.attributes.status = "referral";
        responseObject.data.attributes.referrals = referralCodes;
    }
    
    // handle endorsement 
    if (lossesReported) {
        responseObject.data.attributes.lossesReported = lossesReported;
    }


    // save the quote 
    state.MockPolicyDataDetailsEditable.push(responseObject);

    return responseObject;
};

// getQuote
exports.getQuote = function (req) {
    // find target quote 
    var targetQuote = _.find(state.MockPolicyDataDetailsEditable, function (o) {
        return _.toString(o.data.id) === _.toString(req.params.id);
    });

    if (!targetQuote) return false;

    // look for those of type "Quote" only 
    if (_.toLower(targetQuote.data.type) !== "quote") return false;

    // add referral node if it is not existing 
    if (!targetQuote.data.attributes.referrals) {
        targetQuote.data.attributes.referrals = [];
    }

    // TODO: Better if we update the constants for this 
    // Add referral items for those with status of 'referral' 
    if (_.toLower(targetQuote.data.attributes.status) === "referral") {
        //{
        //"referralType": "Project Type",
        //"referralCode": "1",
        //"description": ""
        //},
        //{
        //"referralType": "Contaminated Land",
        //"referralCode": "2",
        //"description": ""
        //},
        //{
        //"referralType": "Sums Insured",
        //"referralCode": "3",
        //"description": ""
        //},
        //{
        //"referralType": "Other",
        //"referralCode": "4",
        //"description": ""
        //}

        var randomNumber = _.floor(Math.random() * 11);

        if (randomNumber >= 0 && randomNumber <= 2) {
            targetQuote.data.attributes.referrals = [{
                "referralType": "Project Type",
                "referralCode": "1",
                "description": ""
            }];
        }

        if (randomNumber >= 3 && randomNumber <= 5) {
            targetQuote.data.attributes.referrals = [{
                "referralType": "Contaminated Land",
                "referralCode": "2",
                "description": ""
            }];
        }

        if (randomNumber >= 6 && randomNumber <= 8) {
            targetQuote.data.attributes.referrals = [{
                "referralType": "Sums Insured",
                "referralCode": "3",
                "description": ""
            }];
        }

        if (randomNumber >= 9) {
            targetQuote.data.attributes.referrals = [{
                "referralType": "Project Type",
                "referralCode": "1",
                "description": ""
            }, 
            {
                "referralType": "Contaminated Land",
                "referralCode": "2",
                "description": ""
            }];
        }
    }

    return targetQuote;
};

// saveQuote
exports.saveQuote = function (req) {
    var input = req.body;
    var responseObject = input;

    // find target quote 
    var targetQuote = _.find(state.MockPolicyDataDetailsEditable, function (o) {
        return _.toString(o.data.id) === _.toString(req.params.id);
    });
    if (!targetQuote) return false;

    // look for those of type "Quote" only 
    if (_.toLower(targetQuote.data.type) !== "quote") return false;

    // remove from state 
    state.MockPolicyDataDetailsEditable = _.reject(state.MockPolicyDataDetailsEditable, function (o) {
        return _.toString(o.data.id) === _.toString(req.params.id);
    });

    // modify input to ensure we retain the id 
    responseObject.data.id = _.toString(req.params.id);

    // get products if projectValues exists 
    if (responseObject.data.attributes.projectValues) {
        var products = helpers.getProducts(responseObject.data.attributes.projectValues);
        responseObject.data.attributes.products = products.output;
    }

    // Referral - Project type that falls into the referral class
    var projectType = input.data.attributes.projectType ? input.data.attributes.projectType : "";
    // Referral - Project being listed on Contaminated Land
    var contaminatedLandRegistered = input.data.attributes.contaminatedLandRegistered;
    // Referral - Sums insured exceeding $100 million
    var sumInsured = "";
    var sumInsuredInt = "";
    if (input.data.attributes.projectValues) {
        sumInsured = _.replace(input.data.attributes.projectValues.contractWorks.sumInsured, "$", "");
        sumInsuredInt = parseInt(sumInsured, 10);
    } else if (input.data.attributes.products) {
        sumInsuredInt = products.sumInsuredInt;
    }
    // if Referral 
    var referralCodes = helpers.getReferralCodes(projectType, contaminatedLandRegistered, sumInsuredInt);
    if (referralCodes.length > 0) {
        responseObject.data.attributes.status = "referral";
        responseObject.data.attributes.referrals = referralCodes;
    }
    
    // add to state 
    state.MockPolicyDataDetailsEditable.push(responseObject);

    return responseObject;
};

// rejectQuote
exports.rejectQuote = function (req) {
    // find target quote 
    var targetQuote = _.find(state.MockPolicyDataDetailsEditable, function (o) {
        return _.toString(o.data.id) === _.toString(req.params.id);
    });
    if (!targetQuote) return false;

    // look for those of type "Quote" only 
    if (_.toLower(targetQuote.data.type) !== "quote") return false;

    // remove from state 
    state.MockPolicyDataDetailsEditable = _.reject(state.MockPolicyDataDetailsEditable, function (o) {
        return _.toString(o.data.id) === _.toString(req.params.id);
    });

    // modify status to "Cancelled"
    var cancelledQuote = targetQuote;
    cancelledQuote.data.attributes.status = "cancelled";

    // add back to state 
    state.MockPolicyDataDetailsEditable.push(cancelledQuote);

    var responseObject = {
        "data": {
            "type": "Quote",
            "id": targetQuote.data.id,
            "attributes": {
                "agencyId": req.body.data.attributes.agencyId
            }
        }
    };

    return responseObject;
};

// getQuoteDocuments
exports.getQuoteDocuments = function (req) {
    // find target item 
    var targetItem = _.find(state.MockGeneratePdf, function (o) {
        return _.toString(o.data.id) === _.toString(req.params.id);
    });

    // return this if there is no matched quote ID 
    targetItem = { "data": { "type": "Quote", "id": req.params.id, "attributes": { "invoices": [{ "link": "https://nytimes.com/a/ipsum/integer.js", "title": "Invoice-Policy_61zg", "type": "PDF", "size": "389kb", "uploadDate": "2018-02-17T00:00:00Z" }], "documents": [{ "link": "http://dummyimage.com/111x173.jpg/ff4444/ffffff", "title": "Document_70zt", "type": "PDF", "size": null, "uploadDate": "2018-01-10T00:00:00Z" }, { "link": "http://dummyimage.com/184x149.jpg/ff4444/ffffff", "title": "Document_54yl", "type": "JPEG", "size": null, "uploadDate": "2018-01-11T00:00:00Z" }], "supportingDocuments": [{ "link": "http://dummyimage.com/134x139.jpg/dddddd/000000", "title": "SupportingDocument_70vy", "type": "PDF", "size": "243kb", "uploadDate": "2018-01-10T00:00:00Z" }, { "link": "http://dummyimage.com/103x152.jpg/dddddd/000000", "title": "SupportingDocument_48fc", "type": "JPEG", "size": "912kb", "uploadDate": "2018-01-11T00:00:00Z" }] } } };

    return targetItem;
};

// getQuoteUploadDocumentUrl
exports.getQuoteUploadDocumentUrl = function (req) {
    var responseObject = {
        "data": [
          {
              "type": "QuoteUploadDocumentUrl",
              "id": "1",
              "attributes": {
                  "method": "PUT",
                  "url": "https://s3.amazonaws.com/#{S3_BUCKET}/#{path_to_put}?AWSAccessKeyId=#{S3_ACCESS_KEY_ID}&Expires=#{expire_date}&Signature=#{signature}"
              }
          },
          {
              "type": "QuoteUploadDocumentUrl",
              "id": "2",
              "attributes": {
                  "method": "DELETE",
                  "url": "https://s3.amazonaws.com/#{S3_BUCKET}/#{path_to_delete}?AWSAccessKeyId=#{S3_ACCESS_KEY_ID}&Expires=#{expire_date}&Signature=#{signature}"
              }
          }
        ]
    };

    return responseObject;
};



// bindPolicy
exports.bindPolicy = function (req) {
    var responseObject = req.body;
    var targetId = responseObject.data.id;  // quoteId 

    // find target quote 
    var targetQuote = _.find(state.MockPolicyDataDetailsEditable, function (o) {
        return _.toString(o.data.id) === _.toString(targetId);
    });

    if (!targetQuote) return false;

    /** THIS LOGIC IS FOR FACILITATING UNIT TESTING. IT WILL ALLOW MULTIPLE TRIGGER OF bindPolicy USING THE SAME QUOTE ID **/
    // check first if there is already an existing policy coressponding to that quoteId 
    var targetPolicyId = targetId.replace("J", "SIC");
    var targetPolicy = _.find(state.MockPolicyDataDetailsEditable, function (o) {
        return _.toString(o.data.id) === _.toString(targetPolicyId);
    });
    // remove policy from state if it is existing already 
    if (targetPolicy) {
        state.MockPolicyDataDetailsEditable = _.reject(state.MockPolicyDataDetailsEditable, function (o) {
            return _.toString(o.data.id) === _.toString(targetPolicyId);
        });
    }
        
    // get products if projectValues exists 
    if (responseObject.data.attributes.projectValues) {
        var products = helpers.getProducts(responseObject.data.attributes.projectValues);
        responseObject.data.attributes.products = products.output;
    }
    
    // set policy values 
    responseObject.data.id = targetId.replace("J", "SIC");
    responseObject.data.type = "Policy";
    responseObject.data.attributes.status = "active";
    responseObject.data.attributes.distributionType = "One-off";

    // save the Policy 
    state.MockPolicyDataDetailsEditable.push(responseObject);

    return responseObject;
};

// getPolicy
exports.getPolicy = function (req) {
    // find target quote 
    var targetPolicy = _.find(state.MockPolicyDataDetailsEditable, function (o) {
        return _.toString(o.data.id) === _.toString(req.params.id);
    });
    if (!targetPolicy) return false;

    // look for those of type "Policy" only 
    if (_.toLower(targetPolicy.data.type) !== "policy") return false;

    return targetPolicy;
};

// amendPolicy
exports.amendPolicy = function (req) {
    // find target policy 
    var targetPolicy = _.find(state.MockPolicyDataDetailsEditable, function (o) {
        return _.toString(o.data.id) === _.toString(req.params.id);
    });
    if (!targetPolicy) return false;

    // look for those of type "Policy" only 
    if (_.toLower(targetPolicy.data.type) !== "policy") return false;

    // return the original policy with quoteId added if applicable 
    var responseObject = _.clone(targetPolicy);
    if (req.body.data.attributes.riskDates.testingPeriod !== "0 months") {
        // create a new quote 
        var newQuote = helpers.createQuote(req, req.body.data.attributes.lossesReported);
        // assign quote ID 
        responseObject.data.attributes.quoteId = newQuote.data.id;
    } else {
        responseObject.data.attributes.quoteId = "";
    }

    // 

    return responseObject;
};

// cancelPolicy
exports.cancelPolicy = function (req) {
    // find target quote 
    var targetPolicy = _.find(state.MockPolicyDataDetailsEditable, function (o) {
        return _.toString(o.data.id) === _.toString(req.params.id);
    });
    if (!targetPolicy) return false;

    // look for those of type "Policy" only 
    if (_.toLower(targetPolicy.data.type) !== "policy") return false;

    // remove from state 
    state.MockPolicyDataDetailsEditable = _.reject(state.MockPolicyDataDetailsEditable, function (o) {
        return _.toString(o.data.id) === _.toString(req.params.id);
    });

    // modify status to "Cancelled"
    var cancelledPolicy = targetPolicy;
    cancelledPolicy.data.attributes.status = "cancelled";

    // add back to state 
    state.MockPolicyDataDetailsEditable.push(cancelledPolicy);

    var responseObject = {
        "data": {
            "type": "Policy",
            "id": targetPolicy.data.id,
            "attributes": {
                "agencyId": req.body.data.attributes.agencyId
            }
        }
    };

    return responseObject;
};

// getPolicyDocuments 
exports.getPolicyDocuments = function (req) {
    // find target item 
    var targetItem = _.find(state.MockGeneratePdf, function (o) {
        return _.toString(o.data.id) === _.toString(req.params.id);
    });

    // return this if there is no matched policy ID 
    targetItem = { "data": { "type": "Policy", "id": req.params.id, "attributes": { "invoices": [{ "link": "http://ow.ly/turpis/integer.jsp", "title": "Invoice-Policy_23sp", "type": "PDF", "size": "076kb", "uploadDate": "2018-03-12T00:00:00Z" }], "documents": [{ "link": "http://dummyimage.com/181x193.bmp/cc0000/ffffff", "title": "Document_46gp", "type": "PDF", "size": "025kb", "uploadDate": "2018-01-10T00:00:00Z" }], "supportingDocuments": [{ "link": "http://dummyimage.com/237x128.png/dddddd/000000", "title": "SupportingDocument_71ev", "type": "PDF", "size": "580kb", "uploadDate": "2018-01-10T00:00:00Z" }, { "link": "http://dummyimage.com/120x125.png/cc0000/ffffff", "title": "SupportingDocument_14bd", "type": "JPEG", "size": "161kb", "uploadDate": "2018-01-11T00:00:00Z" }] } } };

    return targetItem;
};

// getPolicyUploadDocumentUrl
exports.getPolicyUploadDocumentUrl = function (req) {
    var responseObject = {
        "data": [
          {
              "type": "PolicyUploadDocumentUrl",
              "id": "1",
              "attributes": {
                  "method": "PUT",
                  "url": "https://s3.amazonaws.com/#{S3_BUCKET}/#{path_to_put}?AWSAccessKeyId=#{S3_ACCESS_KEY_ID}&Expires=#{expire_date}&Signature=#{signature}"
              }
          },
          {
              "type": "PolicyUploadDocumentUrl",
              "id": "2",
              "attributes": {
                  "method": "DELETE",
                  "url": "https://s3.amazonaws.com/#{S3_BUCKET}/#{path_to_delete}?AWSAccessKeyId=#{S3_ACCESS_KEY_ID}&Expires=#{expire_date}&Signature=#{signature}"
              }
          }
        ]
    };

    return responseObject;
};