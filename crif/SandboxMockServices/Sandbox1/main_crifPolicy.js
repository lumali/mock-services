﻿
/*
 * NISP 1.4A CRIF API
 *
 * API for the CRIF (Construction Risk Insurance Fund) tool in IfNSW Portal.
 */

var constants = require("constants.js");
var ERRORS = constants.errors();
var SUCCESS = constants.successResponses();
var TEMPSTORE = constants.tempStore();
var helpers = require("helpers.js");
var v1 = require("./routes/v1.js");

// track date of first operation 
state.dateOfFirstOp = state.dateOfFirstOp || new Date();

// create editable MockPolicyDataDetails that refreshes every 24 hours 
state.MockPolicyDataDetailsEditable = state.MockPolicyDataDetailsEditable || _.clone(TEMPSTORE.MockPolicyDataDetails);     // add it if it doesn't exists yet 
if (helpers.hasOneDayElapsed(state.dateOfFirstOp)) {
    // reset to constant mock data 
    state.MockPolicyDataDetailsEditable = _.clone(TEMPSTORE.MockPolicyDataDetails);

    // reset to current date 
    state.dateOfFirstOp = new Date();
}

/* Route definition styles:
 *
 *	define(path, method, function)
 *	soap(path, soapAction, function)
 *
 */
Sandbox.define("/v1/portal/crif/referenceData", "GET", v1.getV1PortalMeCrifReferencedata);       // POL_07
Sandbox.define("/v1/portal/crif/me/reports/operational", "GET", v1.getV1PortalMeCrifReportsOperational);     // OR_02
Sandbox.define("/v1/portal/crif/me/agencyNames", "GET", v1.getV1PortalMeCrifAgencynames);       // CUST_01
Sandbox.define("/v1/portal/crif/me/search", "POST", v1.postV1PortalMeCrifSearch); // POL_01

Sandbox.define("/v1/portal/crif/me/quotes", "POST", v1.postV1PortalMeCrifQuotes);       // POL_02
Sandbox.define("/v1/portal/crif/me/quotes/coverageDetails", "POST", v1.postV1PortalMeCrifQuotesCoveragedetails);    // POL_11 
Sandbox.define("/v1/portal/crif/me/quotes/{id}", "GET", v1.getV1PortalMeCrifQuotes);        // POL_06 
Sandbox.define("/v1/portal/crif/me/quotes/{id}", "PATCH", v1.patchV1PortalMeCrifQuotes);        // POL_04 
Sandbox.define("/v1/portal/crif/me/quotes/{id}/reject", "POST", v1.postV1PortalMeCrifQuotesReject);       // POL_03
Sandbox.define("/v1/portal/crif/me/quotes/{id}/attachments", "POST", v1.postV1PortalMeCrifQuotesDocuments);         // DOC_05 
Sandbox.define("/v1/portal/crif/me/quotes/{id}/processes/generate", "POST", v1.postV1PortalMeCrifQuotesProcessesGenerate);        // POL_12
Sandbox.define("/v1/portal/crif/me/quotes/attachmentLocations", "POST", v1.postV1PortalMeCrifQuotesAttachmentlocations);        // DOC_01
Sandbox.define("/v1/portal/crif/me/quotes/documents", "POST", v1.postV1PortalMeCrifQuotesNotifydocumentupload);     // DOC_02

Sandbox.define("/v1/portal/crif/me/policies", "POST", v1.postV1PortalMeCrifPolicies);       // POL_08
Sandbox.define("/v1/portal/crif/me/policies/{id}", "GET", v1.getV1PortalMeCrifPolicies);        // POL_10
Sandbox.define("/v1/portal/crif/me/policies/{id}", "PATCH", v1.patchV1PortalMeCrifPolicies);        // POL_09
Sandbox.define("/v1/portal/crif/me/policies/{id}/cancel", "POST", v1.postV1PortalMeCrifPoliciesCancel);       // POL_05
Sandbox.define("/v1/portal/crif/me/policies/{id}/attachments", "POST", v1.postV1PortalMeCrifPoliciesDocuments);     // DOC_05
Sandbox.define("/v1/portal/crif/me/policies/attachmentLocations", "POST", v1.postV1PortalMeCrifPoliciesAttachmentlocations);        // DOC_01
Sandbox.define("/v1/portal/crif/me/policies/documents", "POST", v1.postV1PortalMeCrifPoliciesNotifydocumentupload);     // DOC_02
