﻿/*
 * POST /v1/portal/crif/me/reports/operational
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson - header parameter -
 * X-TrackingID - header parameter -
 */
exports.getV1PortalMeCrifReportsOperational = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //// check if valid JSON body 
    //if (!req.body || req.body.isInvalid) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // check query parameter 
    if (!helpers.validQueryParam(req, "agencyID")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // TODO: Validate input 

    // ACTUAL OPERATION  
    var responseObject = helpers.getOperationalReports(req);

    // operation failed
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v1/portal/crif/me/agencyNames
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson - header parameter -
 * X-TrackingID - header parameter -
 */
exports.getV1PortalMeCrifAgencynames = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //// check if valid JSON body 
    //if (!req.body || req.body.isInvalid) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // TODO: Validate input 

    // ACTUAL OPERATION  
    var responseObject = helpers.getAgencies(req);

    // operation failed
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v1/portal/crif/referenceData
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson - header parameter -
 * X-TrackingID - header parameter -
 */
exports.getV1PortalMeCrifReferencedata = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //// check if valid JSON body 
    //if (!req.body || req.body.isInvalid) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // TODO: Validate input 

    // ACTUAL OPERATION  
    var responseObject = helpers.getReferenceData(req);

    // operation failed
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v1/portal/crif/me/quotes/coverageDetails
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson - header parameter -
 * X-TrackingID - header parameter -
 */
exports.postV1PortalMeCrifQuotesCoveragedetails = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // TODO: Validate input 

    // ACTUAL OPERATION  
    var responseObject = helpers.getProjectValues(req);

    // operation failed
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};


/*
 * POST /v1/portal/crif/me/search
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson - header parameter -
 * X-TrackingID - header parameter -
 */
exports.postV1PortalMeCrifSearch = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // TODO: Validate input 

    // ACTUAL OPERATION  
    var responseObject = helpers.getQuotesPoliciesForAgency(req);

    // operation failed
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};


/*
 * POST /v1/portal/crif/me/quotes
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson - header parameter -
 * X-TrackingID - header parameter -
 */
exports.postV1PortalMeCrifQuotes = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // TODO: Validate input 

    // ACTUAL OPERATION  
    var responseObject = helpers.createQuote(req);

    // operation failed
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v1/portal/crif/me/quotes/{id}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson - header parameter -
 * X-TrackingID - header parameter -
 * id - path parameter -
 */
exports.getV1PortalMeCrifQuotes = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //// check if valid JSON body 
    //if (!req.body || req.body.isInvalid) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // check query parameter 
    if (!helpers.validQueryParam(req, "agencyID")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // TODO: Validate input 

    // ACTUAL OPERATION  
    var responseObject = helpers.getQuote(req);

    // operation failed
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * PATCH /v1/portal/crif/me/quotes/{id}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson - header parameter -
 * X-TrackingID - header parameter -
 * id - path parameter -
 */
exports.patchV1PortalMeCrifQuotes = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // TODO: Validate input 

    // ACTUAL OPERATION  
    var responseObject = helpers.saveQuote(req);

    // operation failed
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v1/portal/crif/me/quotes/{id}/reject
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson - header parameter -
 * X-TrackingID - header parameter -
 * id - path parameter -
 */
exports.postV1PortalMeCrifQuotesReject = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // TODO: Validate input 

    // ACTUAL OPERATION  
    var responseObject = helpers.rejectQuote(req);

    // operation failed
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
    //res.json({
    //    "status": "ok"
    //});
};

/*
 * POST /v1/portal/crif/me/quotes/{id}/attachments
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson - header parameter -
 * X-TrackingID - header parameter -
 */
exports.postV1PortalMeCrifQuotesDocuments = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //// check if valid JSON body 
    //if (!req.body || req.body.isInvalid) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // check query parameter 
    if (!helpers.validQueryParam(req, "agencyID")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // TODO: Validate input 

    // ACTUAL OPERATION  
    var responseObject = helpers.getQuoteDocuments(req);

    // operation failed
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v1/portal/crif/me/quotes/attachmentLocations
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson - header parameter -
 * X-TrackingID - header parameter -
 */
exports.postV1PortalMeCrifQuotesAttachmentlocations = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //// check query parameter 
    //if (!helpers.validQueryParam(req, "agencyID")) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // TODO: Validate input 

    // ACTUAL OPERATION  
    var responseObject = helpers.getQuoteUploadDocumentUrl(req);

    // operation failed
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v1/portal/crif/me/quotes/documents
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson - header parameter -
 * X-TrackingID - header parameter -
 */
exports.postV1PortalMeCrifQuotesNotifydocumentupload = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //// check query parameter 
    //if (!helpers.validQueryParam(req, "agencyID")) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    //// TODO: Validate input 

    //// ACTUAL OPERATION  
    //var responseObject = helpers.notifyQuoteDocumentUpload(req);

    //// operation failed
    //if (!responseObject) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    //res.json(responseObject);
    res.json({
        "status": "ok"
    });
};

/*
 * POST /v1/portal/crif/me/quotes/{id}/processes/generate
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson - header parameter -
 * X-TrackingID - header parameter -
 */
exports.postV1PortalMeCrifQuotesProcessesGenerate = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //// check query parameter 
    //if (!helpers.validQueryParam(req, "agencyID")) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    //// TODO: Validate input 

    //// ACTUAL OPERATION  
    //var responseObject = helpers.notifyQuoteDocumentUpload(req);

    //// operation failed
    //if (!responseObject) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    //res.json(responseObject);
    res.json({
        "status": "ok"
    });
};


/*
 * POST /v1/portal/crif/me/policies
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson - header parameter -
 * X-TrackingID - header parameter -
 */
exports.postV1PortalMeCrifPolicies = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // TODO: Validate input 

    // ACTUAL OPERATION  
    var responseObject = helpers.bindPolicy(req);

    // operation failed
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(201);

    // set response body and send
    res.json(responseObject);
};

/*
 * GET /v1/portal/crif/me/policies/{id}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson - header parameter -
 * X-TrackingID - header parameter -
 * id - path parameter -
 */
exports.getV1PortalMeCrifPolicies = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //// check if valid JSON body 
    //if (!req.body || req.body.isInvalid) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // check query parameter 
    if (!helpers.validQueryParam(req, "agencyID")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // TODO: Validate input 

    // ACTUAL OPERATION  
    var responseObject = helpers.getPolicy(req);

    // operation failed
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * PATCH /v1/portal/crif/me/policies/{id}
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson - header parameter -
 * X-TrackingID - header parameter -
 * id - path parameter -
 */
exports.patchV1PortalMeCrifPolicies = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // TODO: Validate input 

    // ACTUAL OPERATION  
    var responseObject = helpers.amendPolicy(req);

    // operation failed
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v1/portal/crif/me/policies/{id}/cancel
 *
 * Parameters (named path params accessible on req.params and query params on req.query):
 *
 * X-OktaTokenJson - header parameter -
 * X-TrackingID - header parameter -
 * id - path parameter -
 */
exports.postV1PortalMeCrifPoliciesCancel = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //// check if valid JSON body 
    //if (!req.body || req.body.isInvalid) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // TODO: Validate input 

    // ACTUAL OPERATION  
    var responseObject = helpers.cancelPolicy(req);

    // operation failed
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v1/portal/crif/me/policies/{id}/attachments
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson - header parameter -
 * X-TrackingID - header parameter -
 */
exports.postV1PortalMeCrifPoliciesDocuments = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //// check if valid JSON body 
    //if (!req.body || req.body.isInvalid) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // check query parameter 
    if (!helpers.validQueryParam(req, "agencyID")) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // TODO: Validate input 

    // ACTUAL OPERATION  
    var responseObject = helpers.getPolicyDocuments(req);

    // operation failed
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v1/portal/crif/me/policies/attachmentLocations
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson - header parameter -
 * X-TrackingID - header parameter -
 */
exports.postV1PortalMeCrifPoliciesAttachmentlocations = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //// check query parameter 
    //if (!helpers.validQueryParam(req, "agencyID")) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // TODO: Validate input 

    // ACTUAL OPERATION  
    var responseObject = helpers.getPolicyUploadDocumentUrl(req);

    // operation failed
    if (!responseObject) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    res.json(responseObject);
};

/*
 * POST /v1/portal/crif/me/policies/documents
 *
 * Parameters (body params accessible on req.body for JSON, req.xmlDoc for XML):
 *
 * X-OktaTokenJson - header parameter -
 * X-TrackingID - header parameter -
 */
exports.postV1PortalMeCrifPoliciesNotifydocumentupload = function (req, res) {
    // validate request type 
    if (!req.is('json') && !req.is('application/vnd.api+json')) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // validate headers 
    if (!helpers.validOktaTokenJsonHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }
    if (!helpers.validTrackingIdHeader(req)) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    // check if valid JSON body 
    if (!req.body || req.body.isInvalid) {
        return helpers.returnError(res, 400, ERRORS.Error400);
    }

    //// check query parameter 
    //if (!helpers.validQueryParam(req, "agencyID")) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    //// TODO: Validate input 

    //// ACTUAL OPERATION  
    //var responseObject = helpers.notifyPolicyDocumentUpload(req);

    //// operation failed
    //if (!responseObject) {
    //    return helpers.returnError(res, 400, ERRORS.Error400);
    //}

    // set content-type 
    res.type('application/json');

    // set status code 
    res.status(200);

    // set response body and send
    //res.json(responseObject);
    res.json({
        "status": "ok"
    });
};
